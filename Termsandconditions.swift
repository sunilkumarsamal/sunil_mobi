//
//  Termsandconditions.swift
//  DV
//
//  Created by chinmay behera on 31/07/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class Termsandconditions: UIViewController {

    @IBOutlet weak var navTitle: UINavigationItem!
    @IBOutlet weak var navbar: UINavigationBar!
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navbar.barStyle = UIBarStyle.black
        self.navbar.tintColor = UIColor.white
        self.navbar?.setBackgroundImage(UIImage(), for: .default)
        self.navbar?.shadowImage = UIImage()
        self.navbar?.isTranslucent = true
        self.webView.scrollView.bounces = false
        let url = NSURL(string: "http://45.79.140.96/MobiShare/termsandconditions.html")
        let requestObj = URLRequest(url: url! as URL)
        webView.loadRequest(requestObj)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func navbarbutton_clicked(_ sender: UIBarButtonItem) {
       self.dismiss(animated: true, completion: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
