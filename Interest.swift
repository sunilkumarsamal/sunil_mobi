//
//  Interest.swift
//  DV
//
//  Created by Chinmay on 19/02/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class Interest: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,NetworkProtocol,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var white_interest: UILabel!
    @IBOutlet weak var black_interest: UILabel!
    @IBOutlet weak var interestCollection: UICollectionView!
    @IBOutlet weak var backButton:UIButton!
    var interests: NSMutableArray = NSMutableArray()
    var selectedInterests: NSMutableArray = NSMutableArray()
    var interests1: NSMutableArray = NSMutableArray()
    var selected: [Int] = [0,0,0,0,0,0,0,0,0,0,0,0]
    var previousData: [String] = []
    var width: CGFloat = 0.0
    var height: CGFloat = 0.0
    var info = PersonInfo()
    @IBOutlet weak var progress: UIProgressView!
    
    @IBOutlet weak var closeScreen: UIButton!
    var isCommutor:String?
    
    @IBOutlet weak var percentLabel: UILabel!
    var pro:UIVisualEffectView?
    
    var userId = String()
    var id:String?
    var profileType = "SignUp"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.interestCollection.delegate = self
        interests = ["Music", "dumbbell", "Movies", "cutlery","Arts", "Sports", "Shopping", "openbook", "Socializing", "Aeroplane", "coat", "dog"];
        interests1 = ["Music", "Fitness", "Movies", "Food","Arts", "Sports", "Shopping", "Reading", "Socializing", "Travel", "Fashion", "Animals"];
        selectedInterests = ["Musiccolour", "dumbbellcolor", "Moviescolour", "cutlerycolor","Artscolour", "Sportscolour", "Shoppingcolour", "openbookcolor", "Socializingcolour", "aerocolor", "coatcolor", "dogcolor"];

        self.progress.transform = CGAffineTransform(scaleX: 1, y: 15)

        progress.tintColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)
        progress.progress = 0.70
        percentLabel.text = "70%"
        
        print(info.lastName)
        if profileType == "SignUp"{
            print("")
        }
        else {
            progress.transform = progress.transform.scaledBy(x: 1, y: 20)
            progress.tintColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)

            self.white_interest.isHidden = false

            self.backButton.isHidden = false
            self.percentLabel.isHidden = true
            self.progress.isHidden = true

            if self.previousData != ["Null"]{
            for i in 0..<self.previousData.count{
                var x = Int(previousData[i])
                var y = x! - 1
                self.interests[y] = self.selectedInterests[y]
                selected[y] = 1

            }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InterestCell", for: indexPath) as! InterestCell
        cell.imageView.image = UIImage(named: interests.object(at: indexPath.item) as! String)
        cell.interestName.text = interests1[indexPath.item] as? String
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let screen: CGRect = UIScreen.main.bounds
            width = (screen.width-80)/3
            print(screen.height)
            height = (screen.height-131)/4
            let size = CGSize(width: width, height: height)
            return size

    }
    

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if profileType == "SignUp"{
        if selected[indexPath.item] == 0 {
            let cell = collectionView.cellForItem(at: indexPath) as! InterestCell
            cell.imageView.image = UIImage(named: selectedInterests.object(at: indexPath.item) as! String)
            selected[indexPath.item] = 1
        }else {
            let cell = collectionView.cellForItem(at: indexPath) as! InterestCell
            cell.imageView.image = UIImage(named: interests.object(at: indexPath.item) as! String)
            selected[indexPath.item] = 0
        }
        if selected.contains(1) {
            progress.progress = 0.80
            percentLabel.text = "80%"
        }else {
            progress.progress = 0.70
            percentLabel.text = "70%"
        }
        }
        else{
            interests = ["Music", "dumbbell", "Movies", "cutlery","Arts", "Sports", "Shopping", "openbook", "Socializing", "Aeroplane", "coat", "dog"];
            if selected[indexPath.item] == 0 {
                var str = String(describing: indexPath.item)
                if previousData.contains(str){
                    
                    let cell = collectionView.cellForItem(at: indexPath) as! InterestCell
                    cell.imageView.image = UIImage(named: interests.object(at: indexPath.item) as! String)
                    selected[indexPath.item] = 0
                    previousData = previousData.filter(){$0 != str}
                }
                else{
                let cell = collectionView.cellForItem(at: indexPath) as! InterestCell
                cell.imageView.image = UIImage(named: selectedInterests.object(at: indexPath.item) as! String)
                selected[indexPath.item] = 1
                }
            }else {
                let cell = collectionView.cellForItem(at: indexPath) as! InterestCell
                cell.imageView.image = UIImage(named: interests.object(at: indexPath.item) as! String)
                selected[indexPath.item] = 0
            }
            
            
        }

    }

    @IBAction func onClickDone(_ sender: UIButton) {
        var interestList = [Int]()
        for i in 0 ..< selected.count  {
            if selected[i] == 1 {
                var x = i + 1
                interestList.append(x)
            }
        }
        info.interest = interestList
        if profileType == "SignUp"{
        if isCommutor == "Yes" {
            let storyboard = UIStoryboard(name: "Payment", bundle: nil)
            let pickUpDropOff = storyboard.instantiateViewController(withIdentifier: "AddCard") as! AddCard
            pickUpDropOff.info = info
            pickUpDropOff.id = id!
            self.present(pickUpDropOff, animated: true, completion: nil)
        }else {
            let storyboard = UIStoryboard(name: "Documents", bundle: nil)
            let pickUpDropOff = storyboard.instantiateViewController(withIdentifier: "Documents") as! Documents
            pickUpDropOff.info = info
            pickUpDropOff.id = id!
            self.present(pickUpDropOff, animated: true, completion: nil)
        }
        }
        else{
            if isCommutor == "Yes" {
                let storyboard = UIStoryboard(name: "Payment", bundle: nil)
                let pickUpDropOff = storyboard.instantiateViewController(withIdentifier: "Payment") as! Payment
                pickUpDropOff.info = info
                pickUpDropOff.id = self.userId
                self.present(pickUpDropOff, animated: true, completion: nil)
            }else {
                DVUtility.disaplyWaitMessage()
                self.startNetworkRequest(urlString: "http://221.121.153.107/user/profile/\(id!)")
            }
            
        }
        
    }
    
    @IBAction func onClickClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    func startNetworkRequest(urlString: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let request = NSMutableURLRequest()
        //Set Params
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 60
        request.httpMethod = "POST"
        //Create boundary, it can be anything
        let boundary = "DVRidesBoundary"
        // set Content-Type in HTTP header
        let contentType = "multipart/form-data; boundary=\(boundary)"
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        // post body
        let body = NSMutableData()
        //Populate a dictionary with all the regular values you would like to send.
        
        //        firstname, lastname, contact, address, study, avatar, interests, (role, identity, registration, insurance)
        
        let param = ["interests": (info.interest)] as [String : Any]
        
        print(param)
        
        for (key, value) in param {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
                
        
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        request.httpBody = body as Data
        let postLength = "\(UInt(body.length))"
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        // set URL
        request.url = NSURL(string: urlString)! as URL
        //        GuideZeeUtallity.HideWaitMessage()
        let task = session.dataTask(with: request as URLRequest) {
            ( data, response, error) in
            
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                DispatchQueue.main.async { [unowned self] in
                    self.view.alpha = 0.7
                    self.pro = ProgressHud(text: "Please Wait...")
                    self.view.addSubview(self.pro!)
                    UIApplication.shared.beginIgnoringInteractionEvents()
                    let alert = UIAlertView(title: "Error!", message: "Something went wrong. Please try later.", delegate: self, cancelButtonTitle: "Ok")
                    alert.show()
                }
                return
            }
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            let dict = self.convertStringToDictionary(text: dataString as! String)
            print(dataString ?? NSDictionary())
            print(dict?["status_code"] ?? String())
            DispatchQueue.main.async { [unowned self] in
                self.view.alpha = 1
                //                self.pro!.removeFromSuperview()
                UIApplication.shared.endIgnoringInteractionEvents()
                let shared = SharedPreferences()
                if self.info.role == "driver" {
                    
                    shared.saveUserType("Driver")
                }
                else if self.info.role == "commutor"{
                    
                    shared.saveUserType("Commutor")
                }
                else {
                    shared.saveUserType("Both")
                }
                //                let shared = UserDefaults.standard
                if self.info.gender == "Male"{
                    shared.saveGender("Male")
                }
                else{
                    shared.saveGender("Female")
                }
                shared.saveFirstName((self.info.firstName))
                shared.saveLastName((self.info.lastName))
                shared.saveUserId(self.id!)
                //                var img = self.info?.avatar
                var str = String(describing: self.info.avatar)
                shared.saveDOB((self.info.dob))
                shared.saveDetails(contact: (self.info.contact), industry: (self.info.course) , avatar: str, role: (self.info.role))
                self.registerDeviceToken()
            }
            
        }
        
        task.resume()
    }
    func registerDeviceToken() {
        //        http://45.79.189.135/divi/user/subscribe/{id}
        let shared = SharedPreferences()
        //        let id = shared.getUserId()
        let token = shared.getToken()
        let jsonDict: NSDictionary
        jsonDict = ["token": token,
                    "device_type": "ios"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self as! NetworkProtocol
        networkCommunication.startNetworkRequest("http://221.121.153.107/user/subscribe/\(id!)", jsonData: jsonDict as! [String : Any], method: "POST", apiType: "RegisterGCM")
        
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
        
    }
    
    func failure() {
        self.view.alpha = 1
        pro!.removeFromSuperview()
        UIApplication.shared.endIgnoringInteractionEvents()
        let alert = UIAlertView(title: "Oops!!!", message: "There was a technical difficulty while fulfilling your request. Please try again.", delegate: self, cancelButtonTitle: "Ok")
        alert.show()
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        DVUtility.hideWaitMessage()
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            if apiType == "RegisterGCM" {
                DVUtility.hideWaitMessage()
               self.dismiss(animated: true, completion: nil)
            }
        case 600:
            DVUtility.displayAlertBoxTitle("", withMessage: "Some error occured. You might not get Notifications of your trips. Please Login again.")
            gotoHome()
        case 600:
            DVUtility.displayAlertBoxTitle("", withMessage: "User not found")
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }
    func gotoHome() {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        let story = UIStoryboard(name: "PickDrop", bundle: nil)
      
        let navVC:UINavigationController = story.instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
        navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
        let container: MFSideMenuContainerViewController = story.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
        container.leftMenuViewController = leftVC
        container.centerViewController = navVC
        container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
        self.present(container, animated: true, completion: nil)
    }

    
}
