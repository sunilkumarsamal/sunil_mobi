//
//  TrackDriver.swift
//  DV
//
//  Created by chinmay behera on 24/05/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class TrackDriver: UIViewController, NetworkProtocol {

    @IBOutlet weak var mapScreen: GMSMapView!
    
    let marker = GMSMarker()
    
    var trip_id: String?
    
    var myTimer:Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationItem.title = "Details"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        myTimer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.fetchLocation), userInfo: nil, repeats: true)
        self.fetchLocation(myTimer)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        myTimer.invalidate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//******************************************** Fetch Location ******************************************
    
    func fetchLocation(_ timer: Timer) {
        let jsonDict: NSDictionary = ["DV": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        networkCommunication.startNetworkRequest("http://221.121.153.107/trip/location/\(id)/\(trip_id!))", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "FetchLocation")
    }
    
    func showMap(withLongitude longitude: CLLocationDegrees, latitude: CLLocationDegrees) {
        marker.map = nil
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 16)
        mapScreen.isMyLocationEnabled = true
        mapScreen.camera = camera
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)
        
        marker.map = mapScreen
    }
    
//******************************************* API call ***********************************************
    
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
        
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        DVUtility.hideWaitMessage()
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            if apiType == "FetchLocation" {
                print("fetched")
                let lat = (responseData["data"] as! NSDictionary).object(forKey: "latitude") as! String
                let long = (responseData["data"] as! NSDictionary).object(forKey: "longitude") as! String
                self.showMap(withLongitude: Double(long)!, latitude: Double(lat)!)
            }
        case 600:
            print("")
        case 101:
            LocationService.sharedInstance.stopUpdatingLocation()
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
            LocationService.sharedInstance.stopUpdatingLocation()
        }
    }

}
