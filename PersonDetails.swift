//
//  PersonDetails.swift
//  DV
//
//  Created by chinmay behera on 20/03/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class PersonDetails: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var selectedInterests: NSArray = NSArray()
    var interests: NSArray = NSArray()
    var width: CGFloat = 0.0

    
    @IBOutlet weak var ratingimage: UIImageView!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var interestCollectionView: UICollectionView!
    @IBOutlet weak var mutualFriendCollectionView: UICollectionView!
    
    var info:NSDictionary?
    var detail:NSDictionary?
    var totalRating = String()
    var nr_rating = String()
    
    @IBOutlet weak var study: UILabel!
    
    var interest:String?
    
    var book_id:String?
    
    @IBOutlet weak var profileImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.interestCollectionView.dataSource = self
        self.interestCollectionView.delegate = self
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true
//        self.navigationController?.view.backgroundColor = .clear
        
        let shared = SharedPreferences()
        let type = shared.getUserType()
        
        if type == "Commutor" || type == "Commuter"{
            detail = info!.object(forKey: "trip_details") as? NSDictionary
        }else {
            detail = info!.object(forKey: "booking_details") as? NSDictionary
        }
        if detail!.object(forKey: "firstname") is NSNull {
            self.navigationItem.title = "Vin Diesel"
        }else {
            self.navigationItem.title = detail!.object(forKey: "firstname") as! String?
        }
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        profileImage.layer.borderWidth = 4
        profileImage.layer.borderColor = UIColor.white.cgColor
        self.interests = ["Beercolour", "Quizcolour", "Sciencecolor"];
        selectedInterests = ["Beer", "Quiz", "Science"]
        // Do any additional setup after loading the view.
        if detail?.object(forKey: "gender") is NSNull{
            self.gender.text = "Male"
        }
        else{
            var g =  self.detail?.object(forKey: "gender") as! String
            
            self.gender.text = g
        }
        if detail!.object(forKey: "total_rating") is NSNull{
            self.totalRating = "0"
        }
        else{
            self.totalRating = (detail?.object(forKey: "total_rating") as? String)!
        }
        if detail!.object(forKey: "nr_ratings") is NSNull{
            self.nr_rating = "0"
        }
        else{
            self.nr_rating = "5"
            //            (detail?.object(forKey: "nr_rating") as? String)!
        }
        var t = Int(totalRating)
        var n = Int(nr_rating)
        var x = t!/n!
        if x == 0{
            self.ratingimage = UIImage(named: "5star")
        }
        else if x > 0 && x < 1 {
            self.ratingimage = UIImage(named: "")
        }
        if detail!.object(forKey: "study") is NSNull {
            study.text = "Hollywood"
        }else {
            study.text = detail!.object(forKey: "study") as! String?
        }
        
        let ImagePath = "http://45.79.189.135/divi/public/" + (detail!.object(forKey: "avatar") as! String?)!
        let url = URL(string: ImagePath)
        DispatchQueue.global(qos: .background).async {
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async {
                if data != nil {
                    self.profileImage.image = UIImage(data: data!)
                }else {
                    self.profileImage.image = UIImage(named: "avatar")
                }
            }
        }
        
//        phone.text = (info!.object(forKey: "trip_details") as! NSDictionary).object(forKey: "contact") as! String?
//        study.text = (info!.object(forKey: "trip_details") as! NSDictionary).object(forKey: "study") as! String?
//        if (info!.object(forKey: "trip_details") as! NSDictionary).object(forKey: "interests") is NSNull {
//            
//        }else {
//            interest = (info!.object(forKey: "trip_details") as! NSDictionary).object(forKey: "interests") as! String?
//            interests = interest?.components(separatedBy: ",") as! NSArray
//        }
//        interest = (info!.object(forKey: "trip_details") as! NSDictionary).object(forKey: "interests") as! String?
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.interests.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == interestCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PersonInterestCell", for: indexPath) as! PersonInterestCell
            cell.imageView.image = UIImage(named: interests.object(at: indexPath.item) as! String)
            cell.interestName.text = selectedInterests[indexPath.item] as? String
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MutualFriendCell", for: indexPath) as! MutualFriendCell
            cell.profileImage.image = UIImage(named: "avatar")
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screen: CGRect = UIScreen.main.bounds
        width = (screen.width-80)/3
        let size = CGSize(width: width, height: width)
        return size
    }
    
    
//        func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//            let pageWidth: Float = Float(self.mutualFriendCollectionView.frame.width / 3) //480 + 50
//            // width + space
//            let currentOffset: Float = Float(scrollView.contentOffset.x)
//            let targetOffset: Float = Float(targetContentOffset.pointee.x)
//            var newTargetOffset: Float = 0
//            if targetOffset > currentOffset {
//                newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth
//            }
//            else {
//                newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth
//            }
//            if newTargetOffset < 0 {
//                newTargetOffset = 0
//            }
//            else if (newTargetOffset > Float(scrollView.contentSize.width)){
//                newTargetOffset = Float(Float(scrollView.contentSize.width))
//            }
//            
//            targetContentOffset.pointee.x = CGFloat(currentOffset)
//            scrollView.setContentOffset(CGPoint(x: CGFloat(newTargetOffset), y: scrollView.contentOffset.y), animated: true)
//            
//        }

    @IBAction func onClickConfirm(_ sender: UIButton) {
//        var arr:NSArray
//        print(<#T##items: Any...##Any#>)
        let storyboard = UIStoryboard(name: "ConfirmScreen", bundle: nil)
        let disc = storyboard.instantiateViewController(withIdentifier: "ConfirmScreen") as! ConfirmScreen
//        disc.info = listOfDrivers.object(at: indexPath.section) as! NSDictionary
        disc.info = info!
        disc.book_id = book_id!
        self.present(disc, animated: true, completion: nil)
    }
}
