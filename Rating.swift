//
//  Rating.swift
//  DV
//
//  Created by Cherian Sankey on 24/06/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class Rating: UIViewController,NetworkProtocol {

    @IBOutlet weak var star1: UIButton!
    @IBOutlet weak var star2: UIButton!
    @IBOutlet weak var star3: UIButton!
    @IBOutlet weak var star4: UIButton!
    @IBOutlet weak var star5: UIButton!
    @IBOutlet weak var driver_name: UILabel!
    @IBOutlet weak var where_from: UILabel!
    @IBOutlet weak var where_to: UILabel!
    @IBOutlet weak var commuter_name: UILabel!
    @IBOutlet weak var estimatedPrice: UILabel!
    @IBOutlet weak var travel_date_month: UILabel!
    @IBOutlet weak var trip_year: UILabel!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var driver_name_top: NSLayoutConstraint!
    @IBOutlet weak var receipt_view: UIView!
    @IBOutlet weak var driver_name_bottom: NSLayoutConstraint!
    
    @IBOutlet weak var passengername_bottom: NSLayoutConstraint!
    @IBOutlet weak var where_to_bottom: NSLayoutConstraint!
    var role = String()
    var trip_id = String()
    var rate = Int()
    var rated_to = String()
    var booking_id = String()
    var c_id = String()
    var d_id = String()
    var from_page = String()
    var rated = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.rated = "false"
        print(booking_id)
        print(trip_id)
        if from_page == "HistoryDetails"{
            self.fetchBookingList(bookTripId: "\(booking_id)")
        }
        else{
            self.fetchBookingList(bookTripId: "\(booking_id)")
        }
        // Do any additional setup after loading the view.
        DVUtility.disaplyWaitMessage()
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        let jsonDict: NSDictionary = ["dv": "mobi"]

         networkCommunication.startNetworkRequest("http://221.121.153.107/payment/booking/receipt/\(id)/\(booking_id)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "estimatedPrice")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        print(self.receipt_view.frame.size)
        if self.receipt_view.frame.height >= 400{
            self.driver_name_top.constant = 45
            self.driver_name_bottom.constant = 63
            self.where_to_bottom.constant = 30
            self.passengername_bottom.constant = 25
        }
        else if self.receipt_view.frame.height >= 390 && self.receipt_view.frame.height < 400{
            self.driver_name_top.constant = 40
            self.driver_name_bottom.constant = 43
            self.where_to_bottom.constant = 23
        }
        else if self.receipt_view.frame.height >= 290 && self.receipt_view.frame.height < 390{
            self.driver_name_top.constant = 30
            self.driver_name_bottom.constant = 33
            self.where_to_bottom.constant = 4
        }
        
    }
    
    @IBAction func OnClick_star1(_ sender: UIButton) {
        print("star1 clicked")
        self.star1.imageView?.image = UIImage(named: "1star.png")
        self.star2.imageView?.image = UIImage(named: "1starbw.png")
        self.star3.imageView?.image = UIImage(named: "1starbw.png")
        self.star4.imageView?.image = UIImage(named: "1starbw.png")
        self.star5.imageView?.image = UIImage(named: "1starbw.png")
        self.star1.setImage(UIImage(named: "1star.png"), for: .normal)
        self.star2.setImage(UIImage(named: "1starbw.png"), for: .normal)
        self.star3.setImage(UIImage(named: "1starbw.png"), for: .normal)
        self.star4.setImage(UIImage(named: "1starbw.png"), for: .normal)
        self.star5.setImage(UIImage(named: "1starbw.png"), for: .normal)
        self.rate = 1
        self.rated = "true"
    }
   
    @IBAction func OnClick_star2(_ sender: UIButton) {
        print("star2 clicked")
        self.star1.imageView?.image = UIImage(named: "1star.png")
        self.star2.imageView?.image = UIImage(named: "1star.png")
        self.star3.imageView?.image = UIImage(named: "1starbw.png")
        self.star4.imageView?.image = UIImage(named: "1starbw.png")
        self.star5.imageView?.image = UIImage(named: "1starbw.png")
        self.star1.setImage(UIImage(named: "1star.png"), for: .normal)
        self.star2.setImage(UIImage(named: "1star.png"), for: .normal)
        self.star3.setImage(UIImage(named: "1starbw.png"), for: .normal)
        self.star4.setImage(UIImage(named: "1starbw.png"), for: .normal)
        self.star5.setImage(UIImage(named: "1starbw.png"), for: .normal)

        self.rate = 2
        self.rated = "true"
    }

    @IBAction func OnClick_star3(_ sender: UIButton) {
        print("star3 clicked")
        self.star1.imageView?.image = UIImage(named: "1star.png")
        self.star2.imageView?.image = UIImage(named: "1star.png")
        self.star3.imageView?.image = UIImage(named: "1star.png")
        self.star4.imageView?.image = UIImage(named: "1starbw.png")
        self.star5.imageView?.image = UIImage(named: "1starbw.png")
        self.star1.setImage(UIImage(named: "1star.png"), for: .normal)
        self.star2.setImage(UIImage(named: "1star.png"), for: .normal)
        self.star3.setImage(UIImage(named: "1star.png"), for: .normal)
        self.star4.setImage(UIImage(named: "1starbw.png"), for: .normal)
        self.star5.setImage(UIImage(named: "1starbw.png"), for: .normal)
        self.rate = 3
        self.rated = "true"
    }
    @IBAction func OnClick_star4(_ sender: UIButton) {
        print("star4 clicked")
        self.star1.imageView?.image = UIImage(named: "1star.png")
        self.star2.imageView?.image = UIImage(named: "1star.png")
        self.star3.imageView?.image = UIImage(named: "1star.png")
        self.star4.imageView?.image = UIImage(named: "1star.png")
        self.star5.imageView?.image = UIImage(named: "1starbw.png")
        self.star1.setImage(UIImage(named: "1star.png"), for: .normal)
        self.star2.setImage(UIImage(named: "1star.png"), for: .normal)
        self.star3.setImage(UIImage(named: "1star.png"), for: .normal)
        self.star4.setImage(UIImage(named: "1star.png"), for: .normal)
        self.star5.setImage(UIImage(named: "1starbw.png"), for: .normal)
        self.rate = 4
        self.rated = "true"

    }
    @IBAction func OnClick_star5(_ sender: UIButton) {
        print("star5 clicked")
        self.star1.imageView?.image = UIImage(named: "1star.png")
        self.star2.imageView?.image = UIImage(named: "1star.png")
        self.star3.imageView?.image = UIImage(named: "1star.png")
        self.star4.imageView?.image = UIImage(named: "1star.png")
        self.star5.imageView?.image = UIImage(named: "1star.png")
        self.star1.setImage(UIImage(named: "1star.png"), for: .normal)
        self.star2.setImage(UIImage(named: "1star.png"), for: .normal)
        self.star3.setImage(UIImage(named: "1star.png"), for: .normal)
        self.star4.setImage(UIImage(named: "1star.png"), for: .normal)
        self.star5.setImage(UIImage(named: "1star.png"), for: .normal)
        self.rate = 5
        self.rated = "true"

    }

    @IBAction func SkipButtonClicked(_ sender: UIButton) {
        self.gotoHome()
    }
    func gotoHome() {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        let story = UIStoryboard(name: "PickDrop", bundle: nil)
        //let PickDrop = story.instantiateViewController(withIdentifier: "PickUpDropOff") as! PickUpDropOff
        let navVC:UINavigationController = story.instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
        DispatchQueue.main.async {
            navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
            let container: MFSideMenuContainerViewController = story.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
            container.leftMenuViewController = leftVC
            container.centerViewController = navVC
            container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
            self.present(container, animated: true, completion: nil)
            
        }
    }
    @IBAction func DoneButtonClicked(_ sender: UIButton) {
        
        //    http://45.79.189.135/divi/trip/rate/{id}
        
        DVUtility.disaplyWaitMessage()
        let shared = SharedPreferences()
        self.role = shared.getUserType()
        if self.role == "Driver" || self.role == "driver"{
            let id = self.d_id
            self.rated_to = self.c_id
            self.startNetworkRequest(urlString: "http://221.121.153.107/trip/rate/\(id)")
        }
        else{
            let id = self.c_id
            self.rated_to = self.d_id
            self.startNetworkRequest(urlString: "http://221.121.153.107/trip/rate/\(id)")
        }
    }
    
    func startNetworkRequest(urlString: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let request = NSMutableURLRequest()
        //Set Params
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 60
        request.httpMethod = "POST"
        //Create boundary, it can be anything
        let boundary = "DVRidesBoundary"
        // set Content-Type in HTTP header
        let contentType = "multipart/form-data; boundary=\(boundary)"
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        // post body
        let body = NSMutableData()
        //Populate a dictionary with all the regular values you would like to send.
        
        //        firstname, lastname, contact, address, study, avatar, interests, (role, identity, registration, insurance)
        
        let param = ["booking_id":"\(booking_id)","rating":"\(rate)","rated_to":"\(rated_to)"] as [String : Any]
        
        print(param)
        
        for (key, value) in param {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        request.httpBody = body as Data
        let postLength = "\(UInt(body.length))"
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        // set URL
        request.url = NSURL(string: urlString)! as URL
        //        GuideZeeUtallity.HideWaitMessage()
        let task = session.dataTask(with: request as URLRequest) {
            ( data, response, error) in
            
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                DispatchQueue.main.async { [unowned self] in
                                       DVUtility.disaplyWaitMessage()
//                    UIApplication.shared.beginIgnoringInteractionEvents()
                    let alert = UIAlertView(title: "Error!", message: "Something went wrong. Please try later.", delegate: self, cancelButtonTitle: "Ok")
                    alert.show()
                }
                return
            }
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            let dict = self.convertStringToDictionary(text: dataString as! String)
            print(dataString ?? NSDictionary())
            print(dict?["status_code"] ?? String())
            let code = dict?["status_code"] as! Int
            switch code {
            case 100:
                DVUtility.hideWaitMessage()
                if self.rated == "true"{
                    let alert = UIAlertController(title: "", message:"Rated successfully.", preferredStyle: UIAlertControllerStyle.alert)
                    var data = String()
                    let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel,handler: { (action) -> Void in
                        self.gotoHome()
                    })
                    alert.addAction(okButton)
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                    self.gotoHome()
                }
                
            case 600:
                DVUtility.hideWaitMessage()
                DVUtility.displayAlertBoxTitle("", withMessage: "Some error occured. Please try after sometime.code=600")
            case 603:
                DVUtility.displayAlertBoxTitle("", withMessage: "Some error occured.code=603")
            default:
                DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
            }
            
            
        }
        task.resume()
    }

    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    func fetchBookingList(bookTripId: String) {
        
        DVUtility.disaplyWaitMessage()
        
        let jsonDict: NSDictionary = ["dv": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        let userType = shared.getUserType()
        print(d_id)
        print(trip_id)
        if userType == "Driver" || userType == "driver"{
            networkCommunication.startNetworkRequest("http://221.121.153.107/trip/details/\(id)/\(trip_id)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "BookingDetails")
        }
        else {
            networkCommunication.startNetworkRequest("http://221.121.153.107/booking/details/\(id)/\(booking_id)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "BookingDetails")
        }
        
    }
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
        DVUtility.hideWaitMessage()
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        
        DVUtility.hideWaitMessage()
        
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            print("")
            if apiType == "BookingDetails"{
                var data = responseData["data"] as! [String:AnyObject]
                var t_from = data["t_from"] as! String
                var t_to = data["t_to"] as! String
                var d_firstname = data["d_firstname"] as! String
                var d_lastname = data["d_lastname"] as! String
                var c_firstname = data["c_firstname"] as! String
                var c_lastname = data["c_lastname"] as! String
                var b_from = data["b_from"] as! String
                var b_to = data["b_to"] as! String
                var b_start = data["b_start"] as! String
                var d_user_id = data["d_user_id"] as! String
                var c_user_id = data["c_user_id"] as! String
                self.c_id = c_user_id
                self.d_id = d_user_id
//                var booking_id = data["booking_id"] as! String
                self.booking_id = data["booking_id"] as! String
                var timeoftravel = String(b_start.characters.suffix(8))
                var hr_min = String(timeoftravel.characters.prefix(5))
                // 24 format to 12 format
                let dateAsString = hr_min
                let dateFormatter1 = DateFormatter()
                dateFormatter1.dateFormat = "HH:mm"
                
                let date1 = dateFormatter1.date(from: dateAsString)
                dateFormatter1.dateFormat = "h:mm a"
                let Date12 = dateFormatter1.string(from: date1!)
                print("12 hour formatted Date:",Date12)
                
                self.trip_year.text = Date12
                
                var myCurrentDate = String(b_start.characters.prefix(10))
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let date = dateFormatter.date(from: myCurrentDate)
                self.convertDateFormate(date: date!)
                var year = String(myCurrentDate.characters.prefix(4))
                var month_date = String(myCurrentDate.characters.suffix(5))
                self.commuter_name.text = c_firstname + " " + c_lastname
                self.driver_name.text = d_firstname + " " + d_lastname
                self.where_from.text = b_from
                self.where_to.text = b_to
            }
            else if apiType == "estimatedPrice"{
                let shared = SharedPreferences()
                let userType = shared.getUserType()
                var data = responseData["data"] as! [String:AnyObject]
                var booking_details = data["booking_details"] as! [String:AnyObject]
                var d_user_id = booking_details["d_user_id"] as! String
                var user_id = booking_details["user_id"] as! String
                var id = shared.getUserId()
                if id == d_user_id {
                    var cost = booking_details["driver_share"] as! String
                    self.estimatedPrice.text = "$" + cost
                }
                else{
                    
                    var cost = booking_details["cost"] as!  String
                    self.estimatedPrice.text = "$" + cost
                }
            }
            
            
            
        case 600:
            print("error")

        default:
            print("")
        }
    }
    func convertDateFormate(date : Date) -> String{
        // Day
        let calendar = Calendar.current
        let anchorComponents = calendar.dateComponents([.day, .month, .year], from: date)
        
        // Formate
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "MMM, yyyy"
        let newDate = dateFormate.string(from: date)
        
        var day  = "\(anchorComponents.day!)"
        switch (day) {
        case "1" , "21" , "31":
            day.append("st")
        case "2" , "22":
            day.append("nd")
        case "3" ,"23":
            day.append("rd")
        default:
            day.append("th")
        }
        
        self.travel_date_month.text = day + " " + String(newDate.characters.prefix(3))
        return day + " " + newDate
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
