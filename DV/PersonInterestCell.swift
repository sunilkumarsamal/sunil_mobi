//
//  PersonInterestCell.swift
//  DV
//
//  Created by chinmay behera on 20/03/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class PersonInterestCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var interestName: UILabel!
}
