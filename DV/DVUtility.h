//
//  DVUtility.h
//  DV
//
//  Created by chinmay behera on 25/04/17.
//  Copyright © 2017 Six30. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DVUtility : NSObject

+(void)DisplayAlertBoxTitle:(NSString *)title withMessage:(NSString *)Message;
+(void) PrintNSLog:(NSString *) message WithTitle:(NSString *) title;
+(NSString *)ConvertContactsTojson:(NSDictionary *)data;
+(void)DisaplyWaitMessage;
+(void)HideWaitMessage;
+(NSString *)CodewaveImageURL;
+(NSString*)getUserName :(NSString*)name;
+(NSString *)getGuideBookDateFormat:(NSString *)_date;
+(NSString*)getCurrentTime;
+(int)isCompare:(NSString*)startDate secondDate:(NSString*)endDate;
+ (NSString*) getTimestampForDate:(NSString*)dateString ;
+(void)createLocalNotification:(NSDictionary*)notification_details;

@end
