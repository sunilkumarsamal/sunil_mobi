//
//  PersonDetails.swift
//  DV
//
//  Created by chinmay behera on 20/03/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class PersonDetails: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,NetworkProtocol {
    
    @IBOutlet weak var cardetails_view: UIView!
    @IBOutlet weak var interest_view: UIView!
    @IBOutlet weak var no_mutual_Friends: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var vehicle_model_color: UILabel!
    @IBOutlet weak var vehicle_number: UILabel!
    @IBOutlet weak var cardetails_view_height: NSLayoutConstraint!
    var selectedInterests: NSArray = NSArray()
    var interests: NSArray = NSArray()
    var width: CGFloat = 0.0
    var list = NSArray()
    var noMatchesMsg = ""
    var currentTitle = String()
    var departure  = String()
    var page = String()
    var interest_string = String()
    var interest_array = NSArray()
    var d_user_id: String?
    var c_user_id: String?
    var role = String()
    var v_number = String()
    var v_model = String()
    var v_color = String()
    var pageFrom = String()
    @IBOutlet weak var departureLabel: UILabel!
    @IBOutlet weak var mutualLabel: UILabel!
    @IBOutlet weak var mutualFriendLabel: UILabel!
    @IBOutlet weak var mutualFriendView: UIView!
    @IBOutlet weak var interestLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var interesetHeight: NSLayoutConstraint!
    @IBOutlet weak var mutualFriendHeight: NSLayoutConstraint!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var ratingimage: UIImageView!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var interestCollectionView: UICollectionView!
    @IBOutlet weak var mutualFriendCollectionView: UICollectionView!
    var selectedInterest: NSMutableArray = NSMutableArray()
    var interests1: NSMutableArray = NSMutableArray()
    var mutualFriend:NSArray = []
    var mutualFriendName:NSMutableArray = []
    var mutualFriendId:NSMutableArray = []
    var mutualFriendProfilePic:NSMutableArray = []
    var viewingType = "from discovery"
    var info:NSDictionary?
    var detail:NSDictionary?
    var totalRating = String()
    var nr_rating = String()
    
    @IBOutlet weak var study: UILabel!
    
    var interest:String?
    
    var book_id:String?
    var trip_id:String?
    @IBOutlet weak var profileImage: UIImageView!
    var name = String()
    var rating = Double()
    var gen = String()
    var stu = String()
    var int = NSArray()
    var avr = String()
    var social_id =  String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.pageFrom != "fromhistory"{
        let shared = SharedPreferences()
        var id = shared.getUserId()
        if id == self.c_user_id{
            
            self.cardetails_view.isHidden = false
            self.cardetails_view_height.constant = 86
            self.vehicle_number.text = self.v_number
            self.vehicle_model_color.text = self.v_model + "(" + self.v_color + ")"
        }
        else if id == self.d_user_id{
            self.cardetails_view.isHidden = true
            self.cardetails_view_height.constant = 0
            }
        if self.trip_id == ""{
            self.cardetails_view.isHidden = false
            self.cardetails_view_height.constant = 86
            self.vehicle_number.text = self.v_number
            self.vehicle_model_color.text = self.v_model + "(" + self.v_color + ")"
            
        }
        else if self.book_id == ""{
            self.cardetails_view.isHidden = true
            self.cardetails_view_height.constant = 0
        }
        }
        else{
            self.cardetails_view_height.constant = 0
            self.cardetails_view.isHidden = true
        }
        if self.v_number == ""{
            self.cardetails_view_height.constant = 0
            self.cardetails_view.isHidden = true
        }
        selectedInterest = ["Musiccolour", "dumbbellcolor", "Moviescolour", "cutlerycolor","Artscolour", "Sportscolour", "Shoppingcolour", "openbookcolor", "Socializingcolour", "aerocolor", "coatcolor", "dogcolor"];
         interests1 = ["Music", "Fitness", "Movies", "Food","Arts", "Sports", "Shopping", "Reading", "Socializing", "Travel", "Fashion", "Animals"];
        self.no_mutual_Friends.isHidden = true
        if social_id.characters.count > 5{
            self.mutualFriends()
        }
        else{
           self.mutualLabel.isHidden = true
           self.mutualFriendLabel.isHidden = true
           self.mutualFriendHeight.constant = 0

            if let layout: UICollectionViewFlowLayout = self.interestLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .horizontal
            }

        }
        self.navigationController?.navigationBar.tintColor = UIColor.white

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
        self.interestCollectionView.dataSource = self
        self.interestCollectionView.delegate = self
        self.mutualFriendCollectionView.dataSource = self
        self.mutualFriendCollectionView.delegate = self

        if self.viewingType == "from discovery"{
        print("hii")
            if self.page != "Ongoing"{
                self.dateLabel.isHidden = true
                self.departureLabel.isHidden = true
            }
            let shared = SharedPreferences()
            let type = shared.getUserType()
            let split = departure.characters.split(separator: " ")
            let last    = String(split.suffix(1).joined(separator: [" "]))
            let result = String(last.characters.prefix(5))
            let substring:String = departure.components(separatedBy: " ")[0]
            self.departureLabel.text = "Preferred departure is at " + result
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: substring)
            self.convertDateFormate(date: date!)
            if self.trip_id == ""{
                detail = info!.object(forKey: "trip_details") as? NSDictionary
            }
            else if self.book_id == ""{
                detail = info!.object(forKey: "booking_details") as? NSDictionary
            }
            if detail!.object(forKey: "firstname") is NSNull {
                self.navigationItem.title = "Vin Diesel"
            }else {
                self.navigationItem.title = detail!.object(forKey: "firstname") as! String?
            }
            UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
            profileImage.layer.borderWidth = 4
            profileImage.layer.borderColor = UIColor.white.cgColor
            self.interests = ["Musiccolour", "dumbbellcolor", "Moviescolour"];
            selectedInterests = ["Music", "Fitness", "Movies"]
            // Do any additional setup after loading the view.
            if detail?.object(forKey: "gender") is NSNull{
                self.gender.text = "Male"
            }
            else{
                var g =  self.detail?.object(forKey: "gender") as! String
                
                self.gender.text = g
            }
            var n = self.rating
            var x = n
            // For rating only
            
            if x == 0.0 {
                self.ratingimage.image = UIImage(named: "5star")!
            }
            else if x > 0.0 && x <= 0.5 {
                self.ratingimage.image = UIImage(named: "0.5stars")
            }
            else if x > 0.5 && x <= 1.0 {
                self.ratingimage.image = UIImage(named: "1starcolor")
            }
            else if x > 1.0 && x <= 1.5 {
                self.ratingimage.image = UIImage(named: "1.5star")
            }
            else if x > 1.5 && x <= 2.0 {
                self.ratingimage.image = UIImage(named: "2starcolor")
            }
            else if x > 2.0 && x <= 2.5{
                self.ratingimage.image = UIImage(named: "2.5star")
            }
            else if x > 2.5 && x <= 3.0 {
                self.ratingimage.image = UIImage(named: "3starcolor")
            }
            else if x > 3.0 && x <= 3.5 {
                self.ratingimage.image = UIImage(named: "3.5star")
            }
            else if x > 3.5 && x <= 4.0 {
                self.ratingimage.image = UIImage(named: "4starcolor")
            }
            else if x > 4.0 && x <= 4.5 {
                self.ratingimage.image = UIImage(named: "4.5star")
            }
            else if x > 4.5 {
                self.ratingimage.image = UIImage(named: "5starcolor")
            }
            if detail!.object(forKey: "study") is NSNull {
                study.text = "Hollywood"
            }else {
                study.text = detail!.object(forKey: "study") as! String?
            }
            let profileUrl = (detail!.object(forKey: "avatar") as! String?)!
            if profileUrl.contains("avatar"){
                let ImagePath = "http://221.121.153.107/public/" + (detail!.object(forKey: "avatar") as! String?)!
                let url = URL(string: ImagePath)
                DispatchQueue.global(qos: .background).async {
                    let data = try? Data(contentsOf: url!)
                    DispatchQueue.main.async {
                        if data != nil {
                            self.profileImage.image = UIImage(data: data!)
                        }else {
                            self.profileImage.image = UIImage(named: "avatar")
                        }
                    }
                }
            }
            else{
                let ImagePath = (detail!.object(forKey: "avatar") as! String?)!
                let url = URL(string: ImagePath)
                DispatchQueue.global(qos: .background).async {
                    let data = try? Data(contentsOf: url!)
                    DispatchQueue.main.async {
                        if data != nil {
                            self.profileImage.image = UIImage(data: data!)
                        }else {
                            self.profileImage.image = UIImage(named: "avatar")
                        }
                    }
                }
            }
        }
        else if self.viewingType == "fromhistory"{

            if self.page != "Ongoing"{
                self.dateLabel.isHidden = true
                self.departureLabel.isHidden = true
            }
            let shared = SharedPreferences()
            let type = shared.getUserType()
            
            let split = departure.characters.split(separator: " ")
            let last    = String(split.suffix(1).joined(separator: [" "]))
            let result = String(last.characters.prefix(5))
            let substring:String = departure.components(separatedBy: " ")[0]
            self.departureLabel.text = "Preferred departure is at " + result
            self.confirmButton.isHidden = true
            UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
            profileImage.layer.borderWidth = 4
            profileImage.layer.borderColor = UIColor.white.cgColor
            self.navigationItem.title = self.name
            self.gender.text = self.gen
            self.study.text = self.stu
            
            self.interests = ["Beercolour", "Quizcolour", "Sciencecolor"];
            selectedInterests = ["Beer", "Quiz", "Science"]

            var y = (self.rating)
            
           
            var x = y
            // For rating only
            
            if x == 0.0 {
                self.ratingimage.image = UIImage(named: "5star")!
            }
            else if x > 0.0 && x <= 0.5 {
                self.ratingimage.image = UIImage(named: "0.5stars")
            }
            else if x > 0.5 && x <= 1.0 {
                self.ratingimage.image = UIImage(named: "1starcolor")
            }
            else if x > 1.0 && x <= 1.5 {
                self.ratingimage.image = UIImage(named: "1.5star")
            }
            else if x > 1.5 && x <= 2.0 {
                self.ratingimage.image = UIImage(named: "2starcolor")
            }
            else if x > 2.0 && x <= 2.5{
                self.ratingimage.image = UIImage(named: "2.5star")
            }
            else if x > 2.5 && x <= 3.0 {
                self.ratingimage.image = UIImage(named: "3starcolor")
            }
            else if x > 3.0 && x <= 3.5 {
                self.ratingimage.image = UIImage(named: "3.5star")
            }
            else if x > 3.5 && x <= 4.0 {
                self.ratingimage.image = UIImage(named: "4starcolor")
            }
            else if x > 4.0 && x <= 4.5 {
                self.ratingimage.image = UIImage(named: "4.5star")
            }
            else if x > 4.5 {
                self.ratingimage.image = UIImage(named: "5starcolor")
            }
            let profileUrl = self.avr
            if profileUrl.contains("avatar"){
                let ImagePath = "http://221.121.153.107/public/" + self.avr
                let url = URL(string: ImagePath)
                DispatchQueue.global(qos: .background).async {
                    let data = try? Data(contentsOf: url!)
                    DispatchQueue.main.async {
                        if data != nil {
                            self.profileImage.image = UIImage(data: data!)
                        }else {
                            self.profileImage.image = UIImage(named: "avatar")
                        }
                    }
                }
            }
            else{
                let ImagePath = self.avr
                let url = URL(string: ImagePath)
                DispatchQueue.global(qos: .background).async {
                    let data = try? Data(contentsOf: url!)
                    DispatchQueue.main.async {
                        if data != nil {
                            self.profileImage.image = UIImage(data: data!)
                        }else {
                            self.profileImage.image = UIImage(named: "avatar")
                        }
                    }
                }
            }
        }
       

        if self.currentTitle == "Outgoing"{
            self.cardetails_view.isHidden = true
            self.cardetails_view_height.constant = 0
        }
        if self.viewingType == "from discovery"{
            self.cardetails_view.isHidden = true
            self.cardetails_view_height.constant = 0
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func mutualFriends() {
        DispatchQueue.main.async {
            let params: [AnyHashable: Any] = ["fields": "context.fields(mutual_friends)"]
            let request = FBSDKGraphRequest(graphPath: "\(self.social_id)", parameters: params, httpMethod: "GET")
            request?.start(completionHandler: { (connection, result, error) -> Void in
                print(result)
                if result == nil{
                    self.mutualLabel.isHidden = true
                    self.mutualFriendLabel.isHidden = true
                    self.mutualFriendHeight.constant = 0
                    self.interesetHeight.constant = 280
                    if let layout: UICollectionViewFlowLayout = self.interestLayout as? UICollectionViewFlowLayout {
                        layout.scrollDirection = .vertical
                    }

                }
                else{
                    let y = result as? NSDictionary
                    let z = y as? [String:AnyObject]
                    for (key,value) in z!{
                        if key == "context"{
                            let w = value as? NSDictionary
                            let e = w as? [String:AnyObject]
                            print(e)
                            for (key1,value) in e!{
                                if key1 == "id"{
                                    self.no_mutual_Friends.isHidden = false
                                    self.no_mutual_Friends.text = "No mutual friends to show."
                                    return
                                }
                                if key1 == "mutual_friends"{
                                    let r = value as? NSDictionary
                                    let t = r as? [String:AnyObject]
                                    for (key2,value) in t!{
                                        if key2 == "data"{
                                            let u = value as? NSArray
                                            print(u!.count)
                                            if u?.count == 0{
                                                self.no_mutual_Friends.isHidden = false
                                                self.no_mutual_Friends.text = "No mutual friends to show."
                                                return
                                            }
                                            else{
                                                for i in 0..<u!.count{
                                                    let current = u?[i] as? NSDictionary
                                                    let currentConditions = current as? [String:AnyObject]
                                                    for (key,value) in currentConditions!
                                                    {
                                                        print("\(key) - \(value) ")
                                                        if key == "id"{
                                                            print(value)
                                                            self.mutualFriendId.add(value)
                                                        }
                                                        
                                                        if key == "name"{
                                                            print(value)
                                                            self.mutualFriendName.add(value)
                                                        }
                                                        
                                                    }
                                                    self.mutualFriendCollectionView.reloadData()
                                                    
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //            print(error)
                
                
            })
            

        }
        
        for i in 0..<self.mutualFriendId.count{
        var socialId = self.mutualFriendId[i]
        FBSDKGraphRequest(graphPath: "\(socialId)", parameters: ["fields": "picture.type(large)"]).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                print(result)
                self.mutualFriendProfilePic.add(result)
            }
        })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count:Int?
        if collectionView == interestCollectionView{

            if self.interest_string.characters.count >= 1 {
                self.interest_array = self.interest_string.components(separatedBy: ",") as NSArray
            }
            count = self.interest_array.count

        }
        else{
            count = self.mutualFriendName.count
        }
       return count!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if collectionView == interestCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PersonInterestCell", for: indexPath) as! PersonInterestCell
            var in_string = interest_array[indexPath.row] as! String
            var in_int = Int(in_string)
            var in_intt = in_int! - 1
            cell.imageView.image = UIImage(named: self.selectedInterest[in_intt] as! String)


            cell.interestName.text = interests1[in_intt] as? String
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MutualFriendCell", for: indexPath) as! MutualFriendCell
            var name = self.mutualFriendName[indexPath.row] as! String
            cell.profileName.text = name.trimmingCharacters(in: .whitespaces)
            cell.profileImage.image = UIImage(named: "avatar")
            
            return cell
        }
        
    }
    
    func fetchDriverOrComuutorList() {
        
        DVUtility.disaplyWaitMessage()
        
        let jsonDict: NSDictionary = ["dv": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        print(book_id!)
        print(trip_id!)
        let userType = shared.getUserType()
        
        if book_id == ""{
            networkCommunication.startNetworkRequest("http://221.121.153.107/booking/search/\(id)/\(trip_id!)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "CommutorList")
        }
        else if trip_id == ""{
            networkCommunication.startNetworkRequest("http://221.121.153.107/trip/search/\(id)/\(book_id!)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "DriverList")
        }
        
        
        
    }
    
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
        DVUtility.hideWaitMessage()
        
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        
        DVUtility.hideWaitMessage()
        
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            print("")
            list = responseData["data"] as! NSArray

        case 600:
            noMatchesMsg = "We cannot find any match as per your request. Please wait for some time."
           
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }
    

    
    

    @IBAction func onClickConfirm(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "ConfirmScreen", bundle: nil)
        let disc = storyboard.instantiateViewController(withIdentifier: "ConfirmScreen") as! ConfirmScreen

        disc.info = info!
        let shared = SharedPreferences()
        if self.trip_id == ""{
                shared.saveUserType("Commuter")
        }
        else if self.book_id == ""{
                shared.saveUserType("Driver")
        }
        disc.book_id = self.book_id!
        disc.trip_id = self.trip_id!

        self.present(disc, animated: true, completion: nil)
    }
    func convertDateFormate(date : Date) -> String{
        // Day
        let calendar = Calendar.current
        let anchorComponents = calendar.dateComponents([.day, .month, .year], from: date)
        
        // Formate
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "MMM"
        let newDate = dateFormate.string(from: date)
        
        var day  = "\(anchorComponents.day!)"
        switch (day) {
        case "1" , "21" , "31":
            day.append("st")
        case "2" , "22":
            day.append("nd")
        case "3" ,"23":
            day.append("rd")
        default:
            day.append("th")
        }
        self.dateLabel.text = " on " + day + " " + newDate
        return day + " " + newDate
    }
}
