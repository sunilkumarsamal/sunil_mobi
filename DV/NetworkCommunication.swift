//
//  NetworkCommunication.swift
//  DV
//
//  Created by Chinmay on 24/01/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

protocol NetworkProtocol: class {
    func success(response: [String: AnyObject], apiType: String)
    func failure()
}

class NetworkCommunication: NSObject {
    
    weak var networkDelegate: NetworkProtocol?
    
    public func startNetworkRequest(_ urlString: String, jsonData: [String: Any], method type: String, apiType: String) {
        let url:NSURL = NSURL(string: urlString)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = type
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        if type == "POST" {
            print(jsonData)
            //            let dataExample : Data = NSKeyedArchiver.archivedData(withRootObject: jsonData)
            let dataExample : Data = try! JSONSerialization.data(withJSONObject: jsonData)
            request.httpBody = dataExample
            let postLength: String? = "\(UInt(dataExample.count))"
            request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        }
        //        let paramString = "data=Hello"
        //        request.httpBody = paramString.data(using: String.Encoding.utf8)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = session.dataTask(with: request as URLRequest) {
            ( data, response, error) in
            
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                DispatchQueue.main.async { [unowned self] in
                    self.networkDelegate?.failure()
                }
                return
            }
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            let dict = self.convertStringToDictionary(text: dataString as! String)
            print(dataString ?? NSDictionary())
            print(dict?["status_code"] ?? String())
            print("api")
            DispatchQueue.main.async {
                print("call")
                //                print(self.networkDelegate!)
                //                print(dict!)
                //                print(apiType)
                if let dictNew = dict {
                    print(self.networkDelegate!)
                    self.networkDelegate?.success(response: dictNew, apiType: apiType)
                }
                
                //                print("failed")
            }
            
        }
        
        task.resume()
        
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
}
