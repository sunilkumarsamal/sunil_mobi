//
//  Payment.swift
//  DV
//
//  Created by Chinmay on 21/02/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class Payment: UIViewController, NetworkProtocol,dataSentDelegate {
    
    
    
    @IBOutlet weak var backbutton: UIButton!
    @IBOutlet weak var progress: UIProgressView!
    
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    @IBOutlet weak var cardNumberView: UIView!
    @IBOutlet weak var expDate: UILabel!
    @IBOutlet weak var modifyPayment: UIButton!
    @IBOutlet weak var addCard: UIButton!
    @IBOutlet weak var cardType: UILabel!
    @IBOutlet weak var cardNumber: UILabel!
    var info:PersonInfo?
    var profileType = "SignUp"
    var pro:UIVisualEffectView?
    var status_code = Int()
    var status = String()
    var message = String()
    var fName = String()
    var lName = String()
    var email = String()
    var cType = String()
    var cMask = String()
    var month = String()
    var year = String()
    var yr = String()
    var mon = String()
    var entryType = String()
    var id:String?
    let myVc = AddCard()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cardNumberView.isHidden = true
       
        progress.transform = progress.transform.scaledBy(x: 1, y: 15)
        progress.tintColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)
        progress.progress = 0.90
        percentLabel.text = "90%"
        self.doneButton.isHidden = true
        if profileType != "SignUp"{
            
            self.progress.isHidden = true
            self.percentLabel.isHidden = true
            self.cardNumberView.isHidden = false
        }
        var shared = SharedPreferences()
        var id = self.id
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
             self.modifyPayment.isUserInteractionEnabled = false
        if profileType == "NotSignUp" {
          
            self.backbutton.isHidden = true
            self.doneButton.isHidden = false
            let networkCommunication = NetworkCommunication()
            var shared = SharedPreferences()
            var id = self.id
 
            let jsonDict: NSDictionary = ["DV": "mobi"]
            networkCommunication.networkDelegate = self
            DVUtility.disaplyWaitMessage()
            
            
            networkCommunication.startNetworkRequest("http://221.121.153.107/payment/paymentmethods/\(id!)",jsonData: jsonDict as! [String : Any], method: "GET", apiType: "cardDetails")
            
        }
        else if profileType == "ViewProfile"{
            self.progress.isHidden = true
            self.percentLabel.isHidden = true
            self.doneButton.isHidden = true
            let networkCommunication = NetworkCommunication()
            var shared = SharedPreferences()
            if self.id as! AnyObject as! NSObject == NSNull(){
                self.id = shared.getUserId()
            }

           
            let jsonDict: NSDictionary = ["DV": "mobi"]
            networkCommunication.networkDelegate = self
            DVUtility.disaplyWaitMessage()
            
            
            networkCommunication.startNetworkRequest("http://221.121.153.107/payment/paymentmethods/\(id!)",jsonData: jsonDict as! [String : Any], method: "GET", apiType: "cardDetails")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func modifyPayment(_ sender: UIButton) {
        print("ChangeCard")
        self.entryType = "ChangeCard"
        addCards()
    }
    @IBAction func onClickDone(_ sender: UIButton) {
        if self.profileType == "ViewProfile"{
            self.dismiss(animated: true, completion: nil)
        }
        else{
            if self.cardNumberView.isHidden == true{
                let alert = UIAlertView(title: "", message: "Please make sure your card is added successfully.", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
            }
            else{
                DVUtility.disaplyWaitMessage()
                UIApplication.shared.beginIgnoringInteractionEvents()
                let shared = SharedPreferences()
                shared.saveUserId(self.id!)
                let id = shared.getUserId()
                
                self.startNetworkRequest(urlString: "http://221.121.153.107/user/profile/\(id)")
            }
        }
    }
    
    @IBAction func onClickAddCard(_ sender: UIButton) {
        print("addcard")
        self.entryType = "NewEntry"
        addCards()
        
    }
    func addCards(){
        let next = self.storyboard?.instantiateViewController(withIdentifier: "AddCard") as! AddCard
        next.entryType = self.entryType
        next.id = self.id!
        next.delegate = self
        self.present(next, animated: true, completion: nil)
    }
    @IBAction func onClickBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func gotoHome() {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        let story = UIStoryboard(name: "PickDrop", bundle: nil)
       
        let navVC:UINavigationController = story.instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
        DispatchQueue.main.async {
            navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
            let container: MFSideMenuContainerViewController = story.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
            container.leftMenuViewController = leftVC
            container.centerViewController = navVC
            container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
            self.present(container, animated: true, completion: nil)
            
        }
    }
    
    func startNetworkRequest(urlString: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let request = NSMutableURLRequest()
        //Set Params
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 60
        request.httpMethod = "POST"
        //Create boundary, it can be anything
        let boundary = "DVRidesBoundary"
        // set Content-Type in HTTP header
        let contentType = "multipart/form-data; boundary=\(boundary)"
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        // post body
        let body = NSMutableData()
        //Populate a dictionary with all the regular values you would like to send.
        
        //        firstname, lastname, contact, address, study, avatar, interests, (role, identity, registration, insurance)
        
        let param = ["firstname": (info?.firstName)!,
                     "lastname": (info?.lastName)!,
                     "contact": (info?.contact)!,
                     "study": (info?.course)!,
                     "interests": (info?.interest)!,
                     "dob": (info?.dob)!,
                     "gender": (info?.gender)!,
                     "vehicle": (info?.carName)!,
                     "plate": (info?.carNumber)!,
                     "colour": (info?.carColor)!,
                     "role": (info?.role)!,
                     "role_flag":(info?.flag)!] as [String : Any]
        
        print(param)
        
        for (key, value) in param {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        let FileParamConstant = "avatar"
        let imageData = UIImageJPEGRepresentation((info?.avatar)!, 0.5)
        if (imageData != nil) {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(FileParamConstant)\"; filename=\"image.png\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type:image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(imageData!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
        }
        
        if info?.role == "driver" {
            let FileParamConstant1 = "identity"
            let imageData1 = UIImageJPEGRepresentation((info?.identity)!, 0.5)
            if (imageData1 != nil) {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(FileParamConstant1)\"; filename=\"image.png\"\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Type:image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append(imageData1!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            let FileParamConstant2 = "registration"
            let imageData2 = UIImageJPEGRepresentation((info?.rego)!, 0.5)
            if (imageData2 != nil) {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(FileParamConstant2)\"; filename=\"image.png\"\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Type:image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append(imageData2!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            let FileParamConstant3 = "insurance"
            let imageData3 = UIImageJPEGRepresentation((info?.insurance)!, 0.5)
            if (imageData3 != nil) {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(FileParamConstant3)\"; filename=\"image.png\"\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Type:image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append(imageData3!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        request.httpBody = body as Data
        let postLength = "\(UInt(body.length))"
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        // set URL
        request.url = NSURL(string: urlString)! as URL
        //        GuideZeeUtallity.HideWaitMessage()
        let task = session.dataTask(with: request as URLRequest) {
            ( data, response, error) in
            
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                DispatchQueue.main.async { [unowned self] in
                    self.view.alpha = 0.7
                    self.pro = ProgressHud(text: "Please Wait...")
                    self.view.addSubview(self.pro!)
                    UIApplication.shared.beginIgnoringInteractionEvents()
                    let alert = UIAlertView(title: "Error!", message: "Something went wrong. Please try later.", delegate: self, cancelButtonTitle: "Ok")
                    alert.show()
                }
                return
            }
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            let dict = self.convertStringToDictionary(text: dataString as! String)
            print(dataString ?? NSDictionary())
            print(dict?["status_code"] ?? String())
            DispatchQueue.main.async { [unowned self] in
            UIApplication.shared.endIgnoringInteractionEvents()
                let shared = SharedPreferences()
                if self.info?.role == "driver" {
                    
                    shared.saveUserType("Driver")
                }else {
                    
                    shared.saveUserType("Commutor")
                }
               
                shared.saveFirstName((self.info?.firstName)!)
                shared.saveLastName((self.info?.lastName)!)
                shared.saveUserId(self.id!)
                
                self.registerDeviceToken()
            }
            
        }
        
        task.resume()
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    func registerDeviceToken() {
        
        let shared = SharedPreferences()
       
        let token = shared.getToken()
        let jsonDict: NSDictionary
        jsonDict = ["token": token,
                    "device_type": "ios"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        networkCommunication.startNetworkRequest("http://221.121.153.107/user/subscribe/\(id!)", jsonData: jsonDict as! [String : Any], method: "POST", apiType: "RegisterGCM")
        
    }
    
    func success(response: [String: AnyObject], apiType: String) {
        progress.transform = progress.transform.scaledBy(x: 1, y: 1)
        progress.tintColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)
        progress.progress = 1.00
        percentLabel.text = "100%"
        
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
       
        DVUtility.hideWaitMessage()
        UIApplication.shared.endIgnoringInteractionEvents()
        let alert = UIAlertView(title: "Oops!!!", message: "There was a technical difficulty while fulfilling your request. Please try again.", delegate: self, cancelButtonTitle: "Ok")
        alert.show()
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        DVUtility.hideWaitMessage()
        print(responseData)
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            if apiType == "RegisterGCM" {
                gotoHome()
            }
            else if apiType == "cardDetails"
            {
                let data:NSDictionary = responseData["data"] as! NSDictionary
                self.cType = data["card_type"] as! String
                self.cMask = data["card_mask"] as! String
                self.month = data["exp_month"] as! String
                self.year = data["exp_year"] as! String
                
                DispatchQueue.main.async {
                    self.modifyPayment.isUserInteractionEnabled = true
                    self.cardNumberView.isHidden = false
                    self.cardType.text = self.cType + ":"
                    self.cardNumber.text = self.cMask
                    self.mon = self.month
                    self.yr = self.year
                    if self.mon.characters.count == 1{
                        self.mon = "0"+self.month
                    }
                    if self.yr.characters.count == 4{
                        self.yr = self.yr.substring(from:self.yr.index(self.yr.endIndex, offsetBy: -2))
                    }
                    self.expDate.text = self.mon + "/" + self.yr
                   
                }
                
                
           
            }
        case 600:
            self.progress.isHidden = true
            self.percentLabel.isHidden = true
            self.cardNumberView.isHidden = true
            

        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }
   
    func getCardDetails(url:String)
    {
        
        
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        let session = URLSession.shared
        
        
        let task = session.dataTask(with: urlRequest) { data, response, error in
            
            if error != nil {
                
                
                print(error!.localizedDescription)
                
            } else {
                
                do {
                    
                    if let manageTaskApi = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary
                    {
                        
                        DispatchQueue.main.async { [unowned self] in
                            var currentArray = manageTaskApi as? NSDictionary
                            print(currentArray)
                            let currentConditions = currentArray as? [String:AnyObject]
                            for(key,value) in currentConditions!
                            {
                                if key == "status_code"{
                                    self.status_code = value as! Int
                                }
                                if key == "status"{
                                    self.status = value as! String
                                }
                                if key == "message"{
                                    self.message = value as! String
                                }
                                if key == "data"{
                                    var data = value as? NSDictionary
                                    var currentConditions1 = data as? [String:AnyObject]
                                    for(key,value) in currentConditions1!
                                    {
                                        if key == "firstname"{
                                            self.fName = (value as? String)!
                                        }
                                        if key == "lastname"{
                                            self.lName = (value as? String)!
                                        }
                                        if key == "email"{
                                            self.email = (value as? String)!
                                        }
                                        if key == "card_type"{
                                            self.cType = (value as? String)!
                                        }
                                        if key == "card_mask"{
                                            self.cMask = (value as? String)!
                                        }
                                        if key == "exp_month"{
                                            self.month = (value as? String)!
                                        }
                                        if key == "exp_year"{
                                            self.year = (value as? String)!
                                        }
                                    }
                                }
                                
                            }
                            
                        }
                        //                    }
                        if Int(self.status_code) == 100{
                            DispatchQueue.main.async {
                                self.modifyPayment.isUserInteractionEnabled = true
                                self.cardNumberView.isHidden = false
                                self.cardType.text = self.cType + ":"
                                self.cardNumber.text = self.cMask
                                self.mon = self.month
                                self.yr = self.year
                                if self.mon.characters.count == 1{
                                    self.mon = "0"+self.month
                                }
                                if self.yr.characters.count == 4{
                                    self.yr = self.yr.substring(from:self.yr.index(self.yr.endIndex, offsetBy: -2))
                                }
                                self.expDate.text = self.mon + "/" + self.yr
                                DVUtility.hideWaitMessage()
                            }
                            DVUtility.hideWaitMessage()
                        }
                        else if Int(self.status_code) == 600{
                            DispatchQueue.main.async {
                                self.cardNumberView.isHidden = true
                                DVUtility.hideWaitMessage()
                                
                            }
                            DVUtility.hideWaitMessage()
                        }
                        
                        
                    }
                }
                catch {
                    
                    print("error in JSONSerialization")
                    
                }
                
                
            }
        }
        task.resume()
    }
    func userData(data: String){
        if data.characters.count > 0 {
            self.profileType = data
        }
    }
    
    
    
}
