//
//  MutualFriendCell.swift
//  DV
//
//  Created by chinmay behera on 24/03/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class MutualFriendCell: UICollectionViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileName: UILabel!
}
