//
//  PageContent.swift
//  DV
//
//  Created by Chinmay on 04/01/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class PageContent: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var login_button: UIButton!
    
    @IBOutlet weak var terms_conditions: UIButton!
    @IBOutlet weak var signup_button: UIButton!
    var pageIndex: Int = 0
    var strPhotoName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.terms_conditions.isHidden = true
        nextButton.isHidden = true
        self.signup_button.isHidden = true
        self.login_button.isHidden = true
        imageView.image = UIImage(named: strPhotoName)
        pageControl.numberOfPages = 4
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        pageControl.currentPageIndicatorTintColor = UIColor.black
        //button design
        signup_button.layer.borderWidth = 1.0;
        signup_button.layer.borderColor = UIColor.white.cgColor;
        signup_button.clipsToBounds = true;
        signup_button.layer.cornerRadius = 22.0;
        
        login_button.layer.borderWidth = 1.0;
        login_button.layer.borderColor = UIColor.white.cgColor;
        login_button.clipsToBounds = true;
        login_button.layer.cornerRadius = 22.0;
        print(pageIndex)
        if pageIndex == 3 {

            self.terms_conditions.isHidden = false
            self.signup_button.isHidden = false
            self.login_button.isHidden = false
            self.pageControl.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func onClickNext(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LoginSignUp", bundle: nil)
        let home = storyboard.instantiateViewController(withIdentifier: "Home") as! Home
        self.present(home, animated: true, completion: nil)
        
    }
    
    @IBAction func signup_button_Clicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LoginSignUp", bundle: nil)
        let loginSignUp = storyboard.instantiateViewController(withIdentifier: "LoginSignUp") as! LoginSignUp
        loginSignUp.loginType = "Sign up"
        self.present(loginSignUp, animated: true, completion: nil)

    }
    
    @IBAction func login_button_Clicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LoginSignUp", bundle: nil)
        let loginSignUp = storyboard.instantiateViewController(withIdentifier: "LoginSignUp") as! LoginSignUp
        loginSignUp.loginType = "Log in"
        self.present(loginSignUp, animated: true, completion: nil)
    }
    
    @IBAction func terms_condition_clicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Termsandconditions", bundle: nil)
        let next = storyboard.instantiateViewController(withIdentifier: "Termsandconditions") as! Termsandconditions
        self.present(next, animated: true, completion: nil)
    }
    
    
}
