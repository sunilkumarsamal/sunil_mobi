//
//  ViewController.swift
//  UIPageViewControllerDemo
//
//  Created by Niks on 21/12/15.
//  Copyright © 2015 TheAppGuruz. All rights reserved.
//

import UIKit

class Walkthrough: UIPageViewController, UIPageViewControllerDataSource
{
    var arrPagePhoto: NSArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrPagePhoto = ["Walkthrough1.png", "Walkthrough3.png", "Walkthrough2.png","back_grnd.png"];
        
        self.dataSource = self
        
        self.setViewControllers([getViewControllerAtIndex(0)] as [UIViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
    }
    
    // MARK:- UIPageViewControllerDataSource Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        let pageContent: PageContent = viewController as! PageContent
        
        var index = pageContent.pageIndex
        
        if ((index == 0) || (index == NSNotFound) || (index == 3))
        {
            return nil
        }
        pageContent.pageControl.currentPage = index
        pageContent.pageControl.size(forNumberOfPages: 6)
        index -= 1;
        return getViewControllerAtIndex(index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        let pageContent: PageContent = viewController as! PageContent
        
        var index = pageContent.pageIndex
        
        if (index == NSNotFound)
        {
            return nil;
        }
        pageContent.pageControl.currentPage = index
        pageContent.pageControl.size(forNumberOfPages: 6)
        
        index += 1;
        if (index == arrPagePhoto.count)
        {
            let storyboard = UIStoryboard(name: "LoginSignUp", bundle: nil)
            let home = storyboard.instantiateViewController(withIdentifier: "Home") as! Home
            self.present(home, animated: false, completion: nil)
            return nil;
        }
        
        return getViewControllerAtIndex(index)
    }
    
    // MARK:- Other Methods
    func getViewControllerAtIndex(_ index: NSInteger) -> PageContent
    {
        // Create a new view controller and pass suitable data.
        let pageContentViewController = self.storyboard?.instantiateViewController(withIdentifier: "PVC") as! PageContent
        pageContentViewController.pageIndex = index

        pageContentViewController.strPhotoName = "\(arrPagePhoto[index])"

        
        
        return pageContentViewController
    }
}

