//
//  Profle.swift
//  DV
//
//  Created by Chinmay on 04/01/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class Profle: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, NetworkProtocol {

    @IBOutlet weak var interestCollections: UICollectionView!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var progressBarImage: UIImageView!
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var address: UITextField!
    
    @IBOutlet weak var driverSwitch: UISwitch!
    
    @IBOutlet weak var DLButton: UIButton!
    @IBOutlet weak var RCButton: UIButton!
    
    var progressCircle = CAShapeLayer()
    
    var cellColor = true
    var width: CGFloat = 0.0
    var arrPagePhoto: NSArray = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrPagePhoto = ["headphones", "Gaming", "Movies", "Astronomy","Arts", "Sports", "Shopping", "Quiz", "Socializing", "Travelling", "beer", "Science"];
        interestCollections.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        
        let circle = UIView(frame: CGRect(x:0, y:0, width:140, height:140))
        
        circle.layoutIfNeeded()
        

        
        let centerPoint = CGPoint (x: circle.bounds.width / 2, y: circle.bounds.width / 2)
        let circleRadius : CGFloat = circle.bounds.width / 2 * 0.95
        
        var circlePath = UIBezierPath(arcCenter: centerPoint, radius: circleRadius, startAngle: CGFloat(-0.5 * M_PI), endAngle: CGFloat(1.5 * M_PI), clockwise: true    )
        
        progressCircle = CAShapeLayer ()
        progressCircle.path = circlePath.cgPath
        progressCircle.strokeColor = UIColor.init(colorLiteralRed: 68/255, green: 168/255, blue: 157/255, alpha: 1.0).cgColor
        progressCircle.fillColor = UIColor.white.cgColor
        progressCircle.lineWidth = 10
        progressCircle.strokeStart = 0
        progressCircle.strokeEnd = 0.25
        
        circle.layer.addSublayer(progressCircle)
        
        self.progressBarImage.addSubview(circle)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as UICollectionViewCell
        
        cellColor = !cellColor
        cell.layer.cornerRadius = width/2
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.gray.cgColor
        let imageView = UIImageView()
        imageView.center = cell.center
        imageView.image = UIImage(named: arrPagePhoto.object(at: indexPath.item) as! String)

        imageView.clipsToBounds = true
        cell.addSubview(imageView)
        imageView.frame = CGRect(x: 30, y: 30, width: width-60, height: width-60)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screen: CGRect = UIScreen.main.bounds
        width = (screen.width-80)/3
        let size = CGSize(width: width, height: width)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageView = collectionView.cellForItem(at: indexPath)?.subviews.first as? UIImageView
        imageView?.image = UIImage(named: "beer")
    }
    
    @IBAction func onClickDone(_ sender: UIButton) {
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let jsonDict: [String: AnyObject] = ["email": "email" as AnyObject];

        progressCircle.strokeEnd = 0.50
        let storyboard = UIStoryboard(name: "TripDetails", bundle: nil)
        let PickUpDropOff = storyboard.instantiateViewController(withIdentifier: "TripDetails") as! TripDetails
        PickUpDropOff.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(PickUpDropOff, animated: true, completion: nil)

    }
    
    func success(response: [String : AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: (response as NSDictionary) as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String: AnyObject], apiType: String) {
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            print("hi")
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }
    
    func startNetworkRequest(urlString: String, JSONData jsonData: [NSObject : AnyObject], Method type: String, Image proImage: UIImage) {
        var config = URLSessionConfiguration.default
        var session = URLSession(configuration: config)
        var request = NSMutableURLRequest()
        //Set Params
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 60
        request.httpMethod = "POST"
        //Create boundary, it can be anything
        var boundary = "DVRidesBoundary"
        // set Content-Type in HTTP header
        var contentType = "multipart/form-data; boundary=\(boundary)"
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        // post body
        var body = NSMutableData()
        //Populate a dictionary with all the regular values you would like to send.
        let param = ["name": name.text,
              "phoneNumber": phoneNumber.text,
                  "address": address.text]
        
        for (key, value) in param {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        let FileParamConstant = "avatar"
        let imageData = UIImageJPEGRepresentation(profileImage.image!, 0.5)
        if (imageData != nil) {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(FileParamConstant)\"; filename=\"image.png\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type:image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(imageData!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
        }
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        request.httpBody = body as Data
        let postLength = "\(UInt(body.length))"
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        // set URL
        request.url = NSURL(string: urlString)! as URL

        let task = session.dataTask(with: request as URLRequest) {
            ( data, response, error) in
            
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                DispatchQueue.main.async { [unowned self] in
                }
                return
            }
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            let dict = self.convertStringToDictionary(text: dataString as! String)
            print(dataString ?? NSDictionary())
            print(dict?["status_code"] ?? String())
            DispatchQueue.main.async { [unowned self] in

            }
            
        }
        
        task.resume()
    }

    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
}
