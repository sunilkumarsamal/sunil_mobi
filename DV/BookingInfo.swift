//
//  BookingInfo.swift
//  DV
//
//  Created by chinmay behera on 16/04/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit
class BookingInfo {
    
    var request_id = ""
    
    var booking_id = ""
    var b_from = ""
    var b_from_latlng = ""
    var b_to = ""
    var b_to_latlng = ""
    var b_start = ""
    var b_end = ""
    
    var d_user_id = ""
    var d_firstname = ""
    var d_lastname = ""
    var d_email = ""
    var d_avatar = ""
    var d_contact = ""
    var d_role = ""
    var d_address = ""
    var d_study = ""
    var d_gender = ""
    
    var city_id = ""
    var city = ""
    
    var created = ""
    var updated = ""
    
    var trip_id = ""
    var trip_status_id = ""
    var trip_status = ""
    var t_from = ""
    var t_from_latlng = ""
    var t_to = ""
    var t_to_latlng = ""
    var t_start = ""
    var t_end = ""
    
    var c_user_id = ""
    var c_firstname = ""
    var c_lastname = ""
    var c_email = ""
    var c_avatar = ""
    var c_contact = ""
    var c_role = ""
    var c_address = ""
    var c_study = ""
    var c_gender = ""
    
    var interests = ""
    var c_interests = ""
    var d_interests = ""
    
    var nr_c_rating = ""
    var sum_c_rating = ""
    var nr_d_rating = ""
    var sum_d_rating = ""
    var rating = ""
    
    var message = ""
    var cost = ""
    var driver_share = ""
    var distance = Double()
    
    var vehicle = ""
    var v_color = ""
    var plate = ""
    
    var status = ""
}
//"booking_id": null,
//"b_from": null,
//"b_from_latlng": null,
//"b_to": null,
//"b_to_latlng": null,
//"b_start": null,
//"b_end": null,
//"created": null,
//"updated": null,
//"d_user_id": "475"
//"d_firstname": "sinchu",
//"d_lastname": "a",
//"d_email": "sinchan@gmail.com",
//"d_avatar": "avatar/dummy_avatar.png",
//"d_contact": null,
//"d_role": "driver",
//"d_address": null,
//"d_study": null,
//"city_id": "1",
//"city": "Bengaluru",


//"trip_id": "271",
//"t_from": "krpuram",
//"t_from_latlng": "13.00481287996189,77.63500336557627",
//"t_to": "kolar",
//"t_to_latlng": "13.024926200995539,77.63992018997669",
//"t_start": "2017-04-16 09:30:30",
//"t_end": "2017-04-16 09:13:35",
//"c_user_id": null,
//"c_firstname": null,
//"c_lastname": null,
//"c_email": null,
//"c_avatar": null,
//"c_contact": null,
//"c_role": null,
//"c_address": null,
//"c_study": null,
//"interests": null,
//"nr_ratings": "0",
//"total_rating": null
