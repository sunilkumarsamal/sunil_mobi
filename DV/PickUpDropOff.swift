//
//  PickUpDropOff.swift
//  DV
//
//  Created by Chinmay on 01/02/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class PickUpDropOff: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate, DiviLocationProtocol, GMSAutocompleteViewControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating, HandleMapSearch, NetworkProtocol{
    
    var mapView: GMSMapView?
    var timer = Timer()
    let geoCoder = CLGeocoder()
    var placemark = CLPlacemark!.self
    var longitude: String?
    var latitude: String?
    var latdegree: Double?
    var longdegree: Double?
    var marker: GMSMarker?
    var from_city_name: String?
    var to_city_name:String?
    @IBOutlet weak var addressViewBottom: UIView!
    @IBOutlet weak var mapScreen: GMSMapView!
    @IBOutlet weak var diviMenuButton: UIBarButtonItem!
    
    @IBOutlet weak var nextButtonMap: UIButton!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var cityPin: UILabel!
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var markerImage: UIImageView!
    
    var from: String?
    var to: String?
    
    var pickLat: String?
    var pickLong: String?
    
    var dropLat: String?
    var dropLong: String?
    
//*******************************************Autocomplete*******************************************
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var currentLocation: UIButton?
    var resultView: UITextView?
    
    var results: UITableViewController?
    
    var placeResults = NSMutableArray()
    
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var picLoc: UILabel!
    @IBOutlet weak var picLocHeight: NSLayoutConstraint!
    
    var country:String?
    var shouldRefresh2: Bool = false
    var shouldRefresh1: Bool = false
    var shouldRefresh: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addressViewBottom.isHidden = true
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "Rectangle").withRenderingMode(.alwaysOriginal))
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Where from?"
//         self.mutualFriends()
//        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
//        let camera = GMSCameraPosition.camera(withLatitude: 13.86, longitude: 77.20, zoom: 15.0)
////        mapScreen = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//        mapScreen?.camera = camera
        
        //        let mapView = GMSMapView()
//        self.menuContainerViewController.panMode = MFSideMenuPanModeNone
        //if self.navigationItem.title == "Where from?"
        //{
         //   self.picLoc.isHidden = true
        //}
        
        mapScreen?.isMyLocationEnabled = true
        mapScreen?.delegate = self
//        self.view.bringSubview(toFront: mapView!)
//        view = mapView
        
        // Creates a marker in the center of the map.
//        let marker = GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude: 13.86, longitude: 77.20)
//        marker.title = "India"
//        marker.snippet = "Bangalore"
//        marker.map = mapView
//        DiviLocationManager.sharedInstance.locationDelegate = self
//        DiviLocationManager.sharedInstance.requestWhenInUseAuthorization()
//        DiviLocationManager.sharedInstance.requestAlwaysAuthorization()
//        DiviLocationManager.sharedInstance.startUpdateLocation()
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = 100
//            locationManager.requestAlwaysAuthorization()
//            locationManager.startUpdatingLocation()
            locationManager.requestWhenInUseAuthorization()
            self.gotoCurrentLocation()
        }
        
        shouldRefresh = false
        shouldRefresh1 = false
        self.designAutoComplete()
//        self.placeAutocomplete(enteredString: "hi")
        
        let gradientLayer = CAGradientLayer()
        var updatedFrame = self.navigationController!.navigationBar.bounds
        updatedFrame.size.height += 20
        gradientLayer.frame = updatedFrame
        gradientLayer.colors = [UIColor.init(colorLiteralRed: 09/255, green: 148/255, blue: 145/255, alpha: 1.0).cgColor, UIColor.init(colorLiteralRed: 65/255, green: 98/255, blue: 151/255, alpha: 1.0).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.navigationController!.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
//        searchController.view.frame.origin.y = 0
//        searchController.searchBar.frame.origin.y = -20
    }
    
    func designAutoComplete () {
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
//        results = UITableViewController()
//        results?.tableView.delegate = self
//        results?.tableView.dataSource = self
        
        let storyboard = UIStoryboard(name: "PickDrop", bundle: nil)
        let resultTableViewController = storyboard.instantiateViewController(withIdentifier: "ResultTableViewController") as! ResultTableViewController
        resultTableViewController.placeDelegate = self
//        resultTableViewController.screenName = self.navigationItem.title
        
        searchController = UISearchController(searchResultsController: resultTableViewController)
//        searchController?.searchBar.frame.size.width = UIScreen.main.bounds.size.width
        searchController?.searchResultsUpdater = resultTableViewController
        searchController?.delegate = self
        searchController?.searchBar.delegate = self
       
        searchController?.searchBar.backgroundImage = UIImage(named: "clear1")
//        searchController?.searchBar.tintColor = UIColor
        let subView = UIView(frame: CGRect(x: 0, y: 64, width: UIScreen.main.bounds.size.width, height: 45.0))
    
        searchView.addSubview((searchController?.searchBar)!)
        
//        view.addSubview(subView)
//        searchController?.searchBar.frame.size.width = UIScreen.main.bounds.size.width
//        searchController?.searchBar.sizeToFit()
        
//        searchController?.searchBar.searchBarStyle = .minimal
        searchController?.hidesNavigationBarDuringPresentation = true
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
    }
//    func mutualFriends() {
////        let token = FBSDKAccessToken.current().tokenString
////        print(token)
//        var params: [AnyHashable: Any] = ["fields": "context.fields(mutual_friends)"]
//        /* make the API call */
//        var request = FBSDKGraphRequest(graphPath: "10154651686048124", parameters: params, httpMethod: "GET")
//        request?.start(completionHandler: { (connection, result, error) -> Void in
//            print(result)
//            print(error)
//        })
//    }

    func gotoCurrentLocation() {
      
        if self.navigationItem.title == "Where to?" {
            picLoc.isHidden = false
            picLocHeight.constant = 44
        }

        searchController?.searchBar.backgroundImage = UIImage(named: "clear1")
        searchController?.searchBar.text = ""
        
        locationManager.startUpdatingLocation()
//        let x = LocationService.sharedInstance.startUpdatingLocation()
//        timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(), userInfo: nil, repeats: true)
//        LocationService.sharedInstance.startUpdatingLocation()
    }
    func stopTimer() {
        timer.invalidate()
    }
    func sendPlaceDetails(placeId: String) {
        if self.navigationItem.title == "Where to?" {
            picLoc.isHidden = false
            picLocHeight.constant = 44
        }

        searchController?.searchBar.backgroundImage = UIImage(named: "clear1")
        searchController?.searchBar.text = ""
        let jsonDict: [String: Any]
        jsonDict = ["for": "map"]
        
        let urlString = "\("https://maps.googleapis.com/maps/api/place/details/json")?placeid=\(placeId)&key=\("AIzaSyBBcFwgZDNyBVnOxXSF_yx_XkKwG5_8_os")"
        

        
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        networkCommunication.startNetworkRequest(urlString, jsonData: jsonDict, method: "GET", apiType: "Direction")
    }
    
    func success(response: [String: AnyObject], apiType: String) {
        print("coming")
        print(response)
        print("yes")
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
        print("here")
    }
    
    func failure() {
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String){
        print(responseData["result"])
        let result = responseData["result"] as! [String:AnyObject]
        let geometry = result["geometry"] as! [String:AnyObject]
        let loc = geometry["location"] as! [String:AnyObject]
        latitude = (loc["lat"] as! NSNumber).stringValue
        longitude = (loc["lng"] as! NSNumber).stringValue
        self.showMapWithLongitude(longitude: (longitude as AnyObject).doubleValue, latitude: (latitude as AnyObject).doubleValue)

    }
    
    func updateSearchResults(for searchController: UISearchController) {
        print("")

    }
    
    func presentSearchController(_ searchController: UISearchController) {
        
    }
    

    
    func buttonAction() {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
//        self.menuContainerViewController.panMode = MFSideMenuPanModeCenterViewController
        self.menuContainerViewController.panMode = MFSideMenuPanModeNone
        NotificationCenter.default.addObserver(self, selector: #selector(self.logout), name: NSNotification.Name(rawValue: "logout"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchMyTrips), name: NSNotification.Name(rawValue: "MyTrips"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {

        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "logout"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "MyTrips"), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchMyTrips() {
        let storyboard = UIStoryboard(name: "MyTrips", bundle: nil)
        let myTrips = storyboard.instantiateViewController(withIdentifier: "MyTrips") as! MyTrips
        
        self.menuContainerViewController.centerViewController = myTrips
    }
    
    func logout() {
        let shared = SharedPreferences()
        let token = shared.getToken()
        shared.clearPreferences()
        shared.saveToken(token)
        self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        let storyboard = UIStoryboard(name: "LoginSignUp", bundle: nil)
        let loginSignup = storyboard.instantiateViewController(withIdentifier: "Home") as! Home
        self.present(loginSignup, animated: false, completion: nil)
    }
    
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if self.navigationItem.title == "Where from?" {
            searchBar.barTintColor = UIColor.init(colorLiteralRed: 68/255, green: 168/255, blue: 157/255, alpha: 1.0)
        }else {
            picLoc.isHidden = true
            picLocHeight.constant = 0
            searchBar.barTintColor = UIColor.init(colorLiteralRed: 65/255, green: 98/255, blue: 151/255, alpha: 1.0)
        }
        
        let attributes = [
            NSForegroundColorAttributeName : UIColor.white,
            NSFontAttributeName : UIFont.systemFont(ofSize: 17)
        ]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
        

    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        if self.navigationItem.title == "Where from?" {
            picLocHeight.constant = 0
        }else {
            self.picLocHeight.constant = 44
            picLoc.isHidden = false
        }
//        searchBar.barTintColor = UIColor.init(colorLiteralRed: 189/255, green: 189/255, blue: 195/255, alpha: 1.0)
        searchBar.backgroundImage = UIImage(named: "clear1")
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.dismiss(animated: false, completion: { _ in })
        self.showMapWithLongitude(longitude: place.coordinate.longitude, latitude: place.coordinate.latitude)
        self.address.text = place.name

        
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: \(error)")
        self.dismiss(animated: false, completion: { _ in })
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: false, completion: { _ in })
    }
    
    
    @IBAction func onClickDiviMenu(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let sideMenu = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    @IBAction func onClickBarNext(_ sender: UIBarButtonItem) {
        if self.navigationItem.title == "Where from?" {
            
            self.navigationItem.title = "Where to?"
            from = address.text
            
            pickLat = latitude
            pickLong = longitude
            
            let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: (latitude as AnyObject).doubleValue, longitude: (longitude as AnyObject).doubleValue, zoom: 16.0)
            mapScreen?.isMyLocationEnabled = true
            mapScreen?.camera = camera
            mapScreen.animate(toZoom: 17.0)
            
            markerImage.image = UIImage(named: "wt1")

            address.textColor = UIColor.init(colorLiteralRed: 65/255, green: 98/255, blue: 151/255, alpha: 1.0)
            let btn = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(self.yourMethod))
            self.navigationItem.leftBarButtonItem = btn
        }else {
            to = address.text
            
            dropLat = latitude
            dropLong = longitude
            
            let storyboard = UIStoryboard(name: "Route", bundle: nil)
            let route = storyboard.instantiateViewController(withIdentifier: "Route") as! Route
            route.from = from
            route.to = to
            route.pickLat = pickLat
            route.pickLong = pickLong
            route.dropLat = dropLat
            route.dropLong = dropLong
            self.navigationController?.pushViewController(route, animated: true)
        }
    }
    
    @IBAction func onClickNext(_ sender: UIButton) {

        if (self.searchController?.searchBar.text?.isEmpty)!{

        }else{
         
            if self.navigationItem.title == "Where from?" {
                
             
                picLocHeight.constant = 44
                picLoc.isHidden = false
                self.searchController?.searchBar.text = " "
                self.navigationItem.title = "Where to?"
                from = address.text
                picLoc.text = "   " + address.text!
                
                pickLat = latitude
                pickLong = longitude
                
                
                mapScreen.animate(toZoom: 17.0)
                
                
                markerImage.image = UIImage(named: "wt1")
                nextButtonMap.backgroundColor = UIColor.init(colorLiteralRed: 65/255, green: 98/255, blue: 151/255, alpha: 1.0)
                address.textColor = UIColor.init(colorLiteralRed: 65/255, green: 98/255, blue: 151/255, alpha: 1.0)
                let btn = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(self.yourMethod))
                self.navigationItem.leftBarButtonItem = btn
                
              
                marker?.map = mapScreen
            }else {
                
                to = address.text
                
                dropLat = latitude
                dropLong = longitude
                
                let storyboard = UIStoryboard(name: "Route", bundle: nil)
                let route = storyboard.instantiateViewController(withIdentifier: "Route") as! Route
                route.from = from
                route.to = to
                route.from_city_name = self.from_city_name
                route.to_city_name = self.to_city_name
                route.pickLat = pickLat
                route.pickLong = pickLong
                route.dropLat = dropLat
                route.dropLong = dropLong
                self.navigationController?.pushViewController(route, animated: true)
            }
        }
    }
    
    func yourMethod() {
        picLoc.isHidden = true
        picLocHeight.constant = 0
        let btn = UIBarButtonItem(image: UIImage(named: "sidemenu"), style: .plain, target: self, action: #selector(self.myMethod))
        self.navigationItem.leftBarButtonItem = btn
        self.navigationItem.title = "Where from?"
        markerImage.image = UIImage(named: "wf")
        nextButtonMap.backgroundColor = UIColor.init(colorLiteralRed: 68/255, green: 168/255, blue: 157/255, alpha: 1.0)
        address.textColor = UIColor.init(colorLiteralRed: 68/255, green: 168/255, blue: 157/255, alpha: 1.0)
        mapScreen.animate(toZoom: 16.0)
        mapScreen.clear()
    }
    
    func myMethod() {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let sideMenu = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    @IBAction func gotoCurrentLocation(_ sender: UIButton) {
        locationManager.startUpdatingLocation()
    }
    
    
    @IBAction func onClickWhereFrom(_ sender: UIButton) {
        
        self.onClickBarNext(UIBarButtonItem())
    }
    
    func updateToLocation(location: CLLocation) {
        let geoCoder: CLGeocoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(location, completionHandler: {(placemarks: [CLPlacemark], error: NSError) -> Void in
            //                let placemark = placemarks.last!
            self.showMapWithLongitude(longitude: location.coordinate.longitude, latitude: location.coordinate.latitude)
            
            } as! CLGeocodeCompletionHandler)
        print("done")
    }
    
    func showMapWithLongitude(longitude: CLLocationDegrees, latitude: CLLocationDegrees) {
        let zoom:Float
        if self.navigationItem.title == "Where from?" {
            zoom = 16.0
        }else {
            zoom = 17.0
        }
        print(latitude)
        print(longitude)
        let city = GMSCameraPosition.camera(withLatitude: latitude,longitude: longitude, zoom: zoom)
        mapScreen?.isMyLocationEnabled = true
        mapScreen.camera = city
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let newLocation = locations.first

        geoCoder.reverseGeocodeLocation(newLocation!, completionHandler: {(placemarks, error)->Void in
            if error == nil && (placemarks?.count)! > 0 {

                print(placemarks)
                
//                let address = AddressParser()
//                address.parseAppleLocationData(placemarks?[0])
//                let addressDict = address.getAddressDictionary()
                let locationAddress = (placemarks?[0])!.addressDictionary?["FormattedAddressLines"] as? NSArray
                if self.navigationItem.title == "Where from?"{
                    self.shouldRefresh1 = false
                    self.shouldRefresh2 = false
                    self.nextButtonMap.alpha = 1
                    self.nextButtonMap.isUserInteractionEnabled = true
//                    let address = (placemarks?[0].subThoroughfare)! as! String
//                    let address0 = address + "," + (placemarks?[0].thoroughfare)! as! String
//                    let address1 = address0 + "," + (placemarks?[0].subLocality)! as! String
//                    let address2 = address1 + "," + (placemarks?[0].locality)! as! String
//                    let address3 = address2 + "," + (placemarks?[0].postalCode)! as! String
                    //locationAddress?.componentsJoined(by: ", ")
                    
                    let address = NSMutableArray()
                    address.removeAllObjects()
                    if placemarks?[0].subThoroughfare != nil{
                        address.add(placemarks?[0].subThoroughfare)
                    }
                    else{
                        print("plot no is not there")
                    }
                    if placemarks?[0].thoroughfare != nil{
                        address.add(placemarks?[0].thoroughfare)
                    }
                    else{
                        print("address is not there")
                    }
                    if placemarks?[0].subLocality != nil {
                        address.add(placemarks?[0].subLocality)
                    }
                    else{
                        print("locality is not there")
                    }
                    if placemarks?[0].locality != nil{
                        address.add(placemarks?[0].locality)
                    }
                    else{
                        print("city name is not there")
                    }
                    if placemarks?[0].postalCode != nil{
                        address.add(placemarks?[0].postalCode)
                    }
                    else{
                        print("pincode is not there")
                    }
                    self.searchController?.searchBar.text = address.componentsJoined(by: ", ")
                }
                else{
                    self.searchController?.searchBar.text?.isEmpty
                    if self.shouldRefresh1 == true
                    {
                        self.nextButtonMap.alpha = 1
                        self.nextButtonMap.isUserInteractionEnabled = true
//                        let address = (placemarks?[0].subThoroughfare)! as! String
//                        let address0 = address + "," + (placemarks?[0].thoroughfare)! as! String
//                        let address1 = address0 + "," + (placemarks?[0].subLocality)! as! String
//                        let address2 = address1 + "," + (placemarks?[0].locality)! as! String
//                        let address3 = address2 + "," + (placemarks?[0].postalCode)! as! String
//                        locationAddress?.componentsJoined(by: ", ")
                        let address = NSMutableArray()
                        address.removeAllObjects()
                        if placemarks?[0].subThoroughfare != nil{
                            address.add(placemarks?[0].subThoroughfare)
                        }
                        else{
                            print("plot no is not there")
                        }
                        if placemarks?[0].thoroughfare != nil{
                            address.add(placemarks?[0].thoroughfare)
                        }
                        else{
                            print("address is not there")
                        }
                        if placemarks?[0].subLocality != nil {
                            address.add(placemarks?[0].subLocality)
                        }
                        else{
                            print("locality is not there")
                        }
                        if placemarks?[0].locality != nil{
                            address.add(placemarks?[0].locality)
                        }
                        else{
                            print("city name is not there")
                        }
                        if placemarks?[0].postalCode != nil{
                            address.add(placemarks?[0].postalCode)
                        }
                        else{
                            print("pincode is not there")
                        }
                        self.searchController?.searchBar.text = address.componentsJoined(by: ", ")
                    }
                }
                self.showMapWithLongitude(longitude: (newLocation?.coordinate.longitude)!, latitude: (newLocation?.coordinate.latitude)!)
            }
            else {
                
            }
            manager.stopUpdatingLocation()

        })
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
  
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.nextButtonMap.alpha = 0.5
        self.nextButtonMap.isUserInteractionEnabled = false
        
        if self.navigationItem.title == "Where to?"
        {
            guard shouldRefresh2 else {
            self.shouldRefresh2 = true
            return
            }
            guard shouldRefresh1 else{
                self.shouldRefresh1 = true
                return
            }
           
        }
        else if self.navigationItem.title == "Where from?"{
            self.shouldRefresh1 = false
            self.shouldRefresh2 = false
        }
    }
    
//    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
//        if self.navigationItem.title == "Where to?"
//        {
//            guard shouldRefresh1 else{
//                self.shouldRefresh1 = true
//                return
//            }
//        }
//    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        

        guard shouldRefresh else {
            shouldRefresh = true
            return
        }
        var point: CGPoint = mapView.center
        var coor: CLLocationCoordinate2D = mapView.projection.coordinate(for: point)
        print("lat is \(coor.latitude)")
        print("lat is \(coor.longitude)")
        self.latitude = String(position.target.latitude)
        self.longitude = String(position.target.longitude)
        self.latdegree = Double()
        self.longdegree = coor.longitude
        self.marker = GMSMarker()
        self.marker?.icon = UIImage(named: "pin")
        self.marker?.position = position.target
//        let geocoder = GMSGeocoder()
        var geo = CLGeocoder()

        var loc = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
        print(loc)
        CLGeocoder().reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil) {print("reverse geodcode fail: \(error?.localizedDescription)")}
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 { print((placemarks?[0])! as CLPlacemark) }
                DispatchQueue.main.async {
                let locationAddress = (placemarks?[0])!.addressDictionary?["FormattedAddressLines"] as? NSArray
                    self.country = placemarks?[0].country
                    print(placemarks?[0].postalCode)
                    print(placemarks?[0].name)
                    print(placemarks?[0].thoroughfare)
                    print(placemarks?[0].subThoroughfare)
                    print(placemarks?[0].subLocality)
                    print("cityname:"+(placemarks?[0].locality)!)
                    //                DispatchQueue.main.async {
                    let address = NSMutableArray()
                    address.removeAllObjects()
                    if placemarks?[0].subThoroughfare != nil{
                        address.add(placemarks?[0].subThoroughfare)
                    }
                    else{
                        print("plot no is not there")
                    }
                    if placemarks?[0].thoroughfare != nil{
                        address.add(placemarks?[0].thoroughfare)
                    }
                    else{
                        print("address is not there")
                    }
                    if placemarks?[0].subLocality != nil {
                        address.add(placemarks?[0].subLocality)
                    }
                    else{
                        print("locality is not there")
                    }
                    if placemarks?[0].locality != nil{
                        address.add(placemarks?[0].locality)
                    }
                    else{
                        print("city name is not there")
                    }
                    if placemarks?[0].postalCode != nil{
                        address.add(placemarks?[0].postalCode)
                    }
                    else{
                        print("pincode is not there")
                    }
                    
                    
                    
                    
                    
//                    let address(placemarks?[0].subThoroughfare)! as! String
//                    let address0 = address + "," + (placemarks?[0].thoroughfare)! as! String
//                    let address1 = address0 + "," + (placemarks?[0].subLocality)! as! String
//                    let address2 = address1 + "," + (placemarks?[0].locality)! as! String
//                    let address3 = address2 + "," + (placemarks?[0].postalCode)! as! String
                    
                    
                    self.address.text = address.componentsJoined(by: ", ")
                    if self.navigationItem.title == "Where from?"{
                        self.shouldRefresh1 = false
                        self.shouldRefresh2 = false
                        self.nextButtonMap.alpha = 1
                        self.from_city_name = placemarks?[0].locality
                        self.nextButtonMap.isUserInteractionEnabled = true
                        //locationAddress?.componentsJoined(by: ", ")

                        self.searchController?.searchBar.text = address.componentsJoined(by: ", ")
                    }
                    else{
                        self.picLoc.text = self.from
                        self.searchController?.searchBar.text?.isEmpty
                        if self.shouldRefresh1 == true{
                            self.nextButtonMap.alpha = 1
                            self.to_city_name = placemarks?[0].locality
                            self.nextButtonMap.isUserInteractionEnabled = true
//                            locationAddress?.componentsJoined(by: ", ")
                            self.searchController?.searchBar.text = address.componentsJoined(by: ", ")
                        }
                        
                    }

                    
                }
                

        })
    }
       
    
}
// Handle the user's selection.
extension PickUpDropOff: GMSAutocompleteResultsViewControllerDelegate, UISearchControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.searchBar.backgroundImage = UIImage(named: "clear1")
        searchController?.searchBar.isUserInteractionEnabled = false

        
        
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.attributions)")
        self.showMapWithLongitude(longitude: place.coordinate.longitude, latitude: place.coordinate.latitude)
        self.address.text = place.name
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
         searchController.searchBar.backgroundImage = UIImage(named: "clear1")
    }
}
