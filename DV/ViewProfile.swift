//
//  ViewProfile.swift
//  DV
//
//  Created by chinmay behera on 04/05/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class ViewProfile: UIViewController, UITextFieldDelegate,NetworkProtocol {

    @IBOutlet weak var generalInfo: UITextField!
    @IBOutlet weak var interest: UITextField!
    @IBOutlet weak var uploadDocs: UITextField!
    @IBOutlet weak var payment: UITextField!
    @IBOutlet weak var done: UIButton!
    var pro:UIVisualEffectView?
    var firstName = String()
    var userId = String()
    var lastName = String()
    var flag = String()
    var email = String()
    var study = String()
    var gender = String()
    var dob = String()
    var contact = String()
    var role = String()
    var driver_id = String()
    var interest_ids = String()
    var interests = String()
    var avatar: UIImage?
    var registration: UIImage?
    var identity: UIImage?
    var insurance: UIImage?
    var checkreg = String()
    var checkidentity = String()
    var checkinsu = String()
    var vehicle = String()
    var plate = String()
    var colour = String()
    var info:PersonInfo?
    var id = String()
    var avatarPath = String()
    var isCommutor = String()
    var profileType:String = "SignUp"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.done.isHidden = true
        generalInfo.setBottomBorder()
        interest.setBottomBorder()
        uploadDocs.setBottomBorder()
        payment.setBottomBorder()
        
        let img = UIImage(named: "greater")
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        imgView.image = img
    
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationItem.title = "Edit Profile"
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.getProfileApi()
    }
    func getProfileApi(){
        let reachability = Reachability1()
        if reachability!.isReachable || reachability!.isReachableViaWiFi || reachability!.isReachableViaWWAN
        {
        let networkCommunication = NetworkCommunication()
        let shared = SharedPreferences()
        let id = shared.getUserId()
        let jsonDict: NSDictionary = ["DV": "mobi"]
        networkCommunication.networkDelegate = self
        DVUtility.disaplyWaitMessage()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        networkCommunication.startNetworkRequest("http://221.121.153.107/user/profile/\(id)",jsonData: jsonDict as! [String : Any], method: "GET", apiType: "GetProfile")
        }
        else{
            let alert = UIAlertView(title: "Unreachable", message: "Please make sure internet is connected and retry", delegate: self, cancelButtonTitle: "retry")
            alert.tag = 100
            alert.show()
        }
    }
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if alertView.tag == 100{
            self.getProfileApi()
            return
        }
    }
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response , apiType: apiType)
    }
    
    func failure() {
        DVUtility.hideWaitMessage()
        UIApplication.shared.endIgnoringInteractionEvents()
        
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        DVUtility.hideWaitMessage()
        UIApplication.shared.endIgnoringInteractionEvents()
        
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            if apiType == "GetProfile" {
                let data:NSDictionary = responseData["data"] as! NSDictionary
                role = data["role"] as! String
                if role == "driver" || role == "Driver"{
                    if (data["firstname"] as AnyObject)as! NSObject == NSNull(){
                        firstName = "Null"
                    }
                    else{
                        firstName = data["firstname"] as! String
                    }
                    if (data["lastname"] as AnyObject)as! NSObject == NSNull(){
                        lastName = "Null"
                    }
                    else{
                        lastName = data["lastname"] as! String
                    }
                    if (data["email"] as AnyObject)as! NSObject == NSNull(){
                        email = "Null"
                    }
                    else{
                        email = data["email"] as! String
                    }
                    if (data["study"] as AnyObject)as! NSObject == NSNull(){
                        study = "Null"
                    }
                    else{
                        study = data["study"] as! String
                    }
                    if (data["gender"] as AnyObject)as! NSObject == NSNull(){
                        gender = "Null"
                    }
                    else{
                        gender = data["gender"] as! String
                    }
                    if (data["dob"] as AnyObject)as! NSObject == NSNull(){
                        dob = "Null"
                    }
                    else{
                        dob = data["dob"] as! String
                    }
                    if (data["contact"] as AnyObject)as! NSObject == NSNull(){
                        contact = "Null"
                    }
                    else{
                        contact = data["contact"] as! String
                    }
                    if (data["driver_id"] as AnyObject)as! NSObject == NSNull(){
                        driver_id = "Null"
                    }
                    else{
                        driver_id = data["driver_id"] as! String
                    }
                    if (data["vehicle"] as AnyObject)as! NSObject == NSNull(){
                        vehicle = "Null"
                    }
                    else{
                        vehicle = data["vehicle"] as! String
                    }
                    if (data["plate"] as AnyObject)as! NSObject == NSNull(){
                        plate = "Null"
                    }
                    else{
                        plate = data["plate"] as! String
                    }
                    if (data["colour"] as AnyObject) as! NSObject == NSNull(){
                        colour = "Null"
                    }
                    else{
                        colour = data["colour"] as! String
                    }
                    if (data["interest_ids"] as AnyObject) as! NSObject == NSNull(){
                        interest_ids = "Null"
                    }
                    else{
                        interest_ids = data["interest_ids"] as! String
                    }
                    if (data["interests"] as AnyObject) as! NSObject == NSNull(){
                        interests = "Null"
                    }
                    else{
                        interests = data["interests"] as! String
                    }
                    if (data["user_id"] as AnyObject) as! NSObject == NSNull(){
                        userId = "Null"
                    }
                    else{
                        userId = data["user_id"] as! String
                    }
                    if (data["role_flag"] as AnyObject) as! NSObject == NSNull(){
                        flag = "Null"
                    }
                    else{
                        flag = data["role_flag"] as! String
                    }
                    
                    avatarPath = data["avatar"] as! String
                    if data["insurance"] as AnyObject as! NSObject == NSNull(){
                        self.insurance = UIImage(named: "avatar")
                        self.checkinsu = "Null"
                    }
                    else{
                        let insurancePath = data["insurance"] as! String
                        let imagePath1 = "http://221.121.153.107/public/" + insurancePath
                        let url1 = URL(string: imagePath1)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url1!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.insurance = UIImage(data: data!)
                                    self.checkinsu = "NotNull"
                                }else {
                                    self.insurance = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                    if data["identity"] as AnyObject as! NSObject == NSNull(){
                        self.identity = UIImage(named: "avatar")
                        self.checkidentity = "Null"
                    }
                    else{
                        let identityPath = data["identity"] as! String
                        let imagePath2 = "http://221.121.153.107/public/" + identityPath
                        let url2 = URL(string: imagePath2)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url2!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.identity = UIImage(data: data!)
                                     self.checkidentity = "NotNull"
                                }else {
                                    self.identity = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                    if data["registration"] as AnyObject as! NSObject == NSNull(){
                        self.registration = UIImage(named: "avatar")
                        self.checkreg = "Null"
                    }
                    else{
                        var registrationPath = data["registration"] as! String
                        let imagePath3 = "http://221.121.153.107/public/" + registrationPath
                        let url3 = URL(string: imagePath3)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url3!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.registration = UIImage(data: data!)
                                    self.checkreg = "NotNull"
                                }else {
                                    self.registration = UIImage(named: "avatar")
                                }
                            }
                        }
                    }


                }
        else{
                firstName = data["firstname"] as! String
                lastName = data["lastname"] as! String
                email = data["email"] as! String
                study = data["study"] as! String
                gender = data["gender"] as! String
                dob = data["dob"] as! String
                contact = data["contact"] as! String
                    if (data["user_id"] as AnyObject) as! NSObject == NSNull(){
                        userId = "Null"
                    }
                    else{
                        userId = data["user_id"] as! String
                    }

                    avatarPath = data["avatar"] as! String

                    if (data["interests"] as AnyObject) as! NSObject == NSNull(){
                        interests = "Null"
                    }
                    else{
                        interests = data["interests"] as! String
                    }

            }
                
            }
            else{
                let data:NSDictionary = responseData["data"] as! NSDictionary
                firstName = data["firstname"] as! String
                lastName = data["lastname"] as! String
                email = data["email"] as! String
                study = data["study"] as! String
                gender = data["gender"] as! String
                dob = data["dob"] as! String
                contact = data["contact"] as! String
                if (data["driver_id"] as AnyObject) as! NSObject == NSNull(){
                    driver_id = "Null"
                }
                else{
                    driver_id = data["driver_id"] as! String
                }
                if (data["vehicle"] as AnyObject) as! NSObject == NSNull(){
                    vehicle = "Null"
                }
                else{
                    vehicle = data["vehicle"] as! String
                }
                if data["plate"] as AnyObject as! NSObject == NSNull(){
                    plate = "Null"
                }
                else{
                    plate = data["plate"] as! String
                }
                if data["colour"] as AnyObject as! NSObject == NSNull(){
                    colour = "Null"
                }
                else{
                    colour = data["colour"] as! String
                }
                interest_ids = data["interest_ids"] as! String
                interests = data["interests"] as! String
                userId = data["user_id"] as! String
                self.avatarPath = data["avatar"] as! String
                if avatarPath.contains("avatar"){
                    let ImagePath = "http://221.121.153.107/public/" + avatarPath
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                self.avatar = UIImage(data: data!)
                            }else {
                                self.avatar = UIImage(named: "avatar")
                            }
                        }
                    }
                }
                else{
                    let ImagePath = avatarPath
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                self.avatar = UIImage(data: data!)
                            }else {
                                self.avatar = UIImage(named: "avatar")
                            }
                        }
                    }
                }
                if data["insurance"] as AnyObject as! NSObject == NSNull(){
                    self.insurance = UIImage(named: "avatar")
                    self.checkinsu = "Null"
                }
                else{
                    let insurancePath = data["insurance"] as! String
                    let imagePath1 = "http://221.121.153.107/public/" + insurancePath
                    let url1 = URL(string: imagePath1)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url1!)
                        DispatchQueue.main.async {
                            if data != nil {
                                self.insurance = UIImage(data: data!)
                            }else {
                                self.insurance = UIImage(named: "avatar")
                            }
                        }
                    }
                }
                if data["identity"] as AnyObject as! NSObject == NSNull(){
                    self.identity = UIImage(named: "avatar")
                    self.checkidentity = "Null"
                }
                else{
                    let identityPath = data["identity"] as! String
                    let imagePath2 = "http://221.121.153.107/public/" + identityPath
                    let url2 = URL(string: imagePath2)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url2!)
                        DispatchQueue.main.async {
                            if data != nil {
                                self.identity = UIImage(data: data!)
                            }else {
                                self.identity = UIImage(named: "avatar")
                            }
                        }
                    }
                }
                if data["registration"] as AnyObject as! NSObject == NSNull(){
                    self.registration = UIImage(named: "avatar")
                    self.checkreg = "Null"
                }
                else{
                    let registrationPath = data["registration"] as! String
                    let imagePath3 = "http://221.121.153.107/public/" + registrationPath
                    let url3 = URL(string: imagePath3)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url3!)
                        DispatchQueue.main.async {
                            if data != nil {
                                self.registration = UIImage(data: data!)
                            }else {
                                self.registration = UIImage(named: "avatar")
                            }
                        }
                    }
                }

                
            }
            print("")
            
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
        
    }

    @IBAction func onClickDiviMenu(_ sender: UIBarButtonItem) {
        self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    @IBAction func onClickDone(_ sender: UIButton) {


    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case 0:
            let storyboard = UIStoryboard(name: "Details", bundle: nil)
            let detailsVC = storyboard.instantiateViewController(withIdentifier: "Details") as! Details
            detailsVC.profileType = "ViewProfile"
            if self.role == "Driver" || self.role == "driver"{
            detailsVC.gen = self.gender
            detailsVC.fName = self.firstName
            detailsVC.lName = self.lastName
            detailsVC.pNumber = self.contact
            detailsVC.indus = self.study
            detailsVC.userType = self.role
            detailsVC.flag = self.flag
            detailsVC.dob = self.dob

            // if role is driver then 
            detailsVC.CarColour = self.colour
            detailsVC.Model = self.vehicle
            detailsVC.Plate = self.plate
            detailsVC.DLImage = self.registration
            detailsVC.INSURANCEImage = self.insurance
            detailsVC.REGOImage = self.identity
            detailsVC.id = self.userId
            detailsVC.checkidentity = self.checkidentity
            detailsVC.checkinsu = self.checkinsu
            detailsVC.checkreg = self.checkreg
            detailsVC.avatarPath = self.avatarPath
            self.present(detailsVC, animated: false, completion: nil)
            }
            else{
                detailsVC.gen = self.gender
                detailsVC.fName = self.firstName
                detailsVC.lName = self.lastName
                detailsVC.pNumber = self.contact
                detailsVC.indus = self.study
                detailsVC.userType = self.role
                detailsVC.flag = self.flag
                detailsVC.dob = self.dob
                detailsVC.id = self.userId
                detailsVC.avatarPath = self.avatarPath
                detailsVC.CarColour = self.colour
                detailsVC.Model = self.vehicle
                detailsVC.Plate = self.plate
                detailsVC.DLImage = self.registration
                detailsVC.INSURANCEImage = self.insurance
                detailsVC.REGOImage = self.identity
                detailsVC.id = self.userId
                detailsVC.checkidentity = self.checkidentity
                detailsVC.checkinsu = self.checkinsu
                detailsVC.checkreg = self.checkreg
                 self.present(detailsVC, animated: false, completion: nil)
            }

        case 1:
            let storyboard = UIStoryboard(name: "Interest", bundle: nil)
            let detailsVC = storyboard.instantiateViewController(withIdentifier: "Interest") as! Interest
            detailsVC.profileType = "ViewProfile"
            let fullArr = interests.characters.split{$0 == ","}.map(String.init)
            detailsVC.userId = self.userId
            detailsVC.id = self.userId
            detailsVC.previousData = fullArr

            self.present(detailsVC, animated: false, completion: nil)

        case 2:
            if self.role == "driver" || self.role == "Driver"{
            let storyboard = UIStoryboard(name: "Documents", bundle: nil)
            let detailsVC = storyboard.instantiateViewController(withIdentifier: "Documents") as! Documents
            detailsVC.profileType = "ViewProfile"
            detailsVC.CarColour = self.colour
            detailsVC.Model = self.vehicle
            detailsVC.Plate = self.plate
            detailsVC.avatar = self.avatar
            detailsVC.DLImage = self.registration
            detailsVC.INSURANCEImage = self.insurance
            detailsVC.REGOImage = self.identity
            detailsVC.gender = self.gender
            detailsVC.firstName = self.firstName
            detailsVC.lastName = self.lastName
            detailsVC.contact = self.contact
            detailsVC.industry = self.study
            detailsVC.role = self.role
            detailsVC.flag = self.flag
            detailsVC.birthDay = self.dob
                
            detailsVC.id = self.userId

            self.present(detailsVC, animated: false, completion: nil)
            }
            else{
                let alert = UIAlertView(title: "", message: "Please make sure you are a driver.", delegate: self, cancelButtonTitle: "Ok")
                 alert.show()
            }

        case 3:
            let storyboard = UIStoryboard(name: "Payment", bundle: nil)
            let detailsVC = storyboard.instantiateViewController(withIdentifier: "Payment") as! Payment
            detailsVC.profileType = "ViewProfile"
            detailsVC.id = self.userId

            self.present(detailsVC, animated: false, completion: nil)

        default:
            print("")
        }
        return false
    }
    
    
       
    

}
