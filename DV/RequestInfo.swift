//
//  RequestInfo.swift
//  DV
//
//  Created by chinmay behera on 20/04/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import Foundation
import UIKit

class RequestInfo {
    
    var request_id = ""
    
    var booking_id = ""
    var booking_status = ""
    var b_start = ""
    var b_end = ""
    
    var d_user_id = ""
    var d_firstname = ""
    var d_lastname = ""
    var d_email = ""
    var d_avatar = ""
    var d_contact = ""
    var d_role = ""
    var d_address = ""
    var d_study = ""
    
    var trip_id = ""
    var trip_status = ""
    var t_start = ""
    var t_end = ""
    
    var c_user_id = ""
    var c_firstname = ""
    var c_lastname = ""
    var c_email = ""
    var c_avatar = ""
    var c_contact = ""
    var c_role = ""
    var c_address = ""
    var c_study = ""
    
    var nr_c_rating = ""
    var sum_c_rating = ""
    var nr_d_rating = ""
    var sum_d_rating = ""
    
}
//    "request_id": "44",
//    "booking_id": "361",
//    "trip_id": "280",
//    "request_from": "393",
//    "t_start": "2017-04-18 17:30:52",
//    "t_end": "2017-04-18 17:50:52",
//    "b_start": "2017-04-18 17:30:00",
//    "b_end": "2017-04-18 17:50:00",
//    "booking_status": "1",
//    "trip_status": "1",

//    "d_user_id": "346",
//    "d_firstname": "john smith",
//    "d_lastname": "123",
//    "d_email": "test@driver.com",
//    "d_avatar": "avatar/IMG14908145491490814545984.jpg",
//    "d_contact": "1234567890",
//    "d_role": "driver",
//    "d_address": "fchcjgvjcjvj",
//    "d_study": "cseh",

//    "c_user_id": "393",
//    "c_firstname": "chinmay",
//    "c_lastname": "behera",
//    "c_email": "chinmayabehera@gmail.com",
//    "c_avatar": "avatar/IMG1491390531image.png",
//    "c_contact": "9778477260",
//    "c_role": "commuter",
//    "c_address": null,
//    "c_study": "iOS developer ",

//    "nr_c_rating": "0",
//    "sum_c_rating": null,
//    "nr_d_rating": "0",
//    "sum_d_rating": null
