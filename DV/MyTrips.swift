//
//  MyTrips.swift
//  DV
//
//  Created by chinmay behera on 07/04/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class MyTrips: UIViewController, UITableViewDelegate, UITableViewDataSource, NetworkProtocol {

    @IBOutlet weak var bookingTable: UITableView!
    
    var pro: UIVisualEffectView?
    var deletePlanetIndexPath: NSIndexPath? = nil
    var bookingList = NSMutableArray()
    var page = String()
    var c_user_id = String()
    var d_user_id = String()
    var booking_id = String()
    var trip_id = String()
    var indexx = Int()
    @IBOutlet weak var btnLabel: UILabel!
    @IBOutlet weak var btnLabelX: NSLayoutConstraint!
    
    @IBOutlet weak var completeButton: UIButton!
    @IBOutlet weak var onGoingButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var pendingButton: UIButton!
    
    var noTripMsg = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bookingTable.register(UINib(nibName: "TripCell", bundle: nil), forCellReuseIdentifier: "TripCell")
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationItem.title = "History"
        self.page = "Pending"
        self.bookingTable.tableFooterView = UIView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.fetchBookingList(status: "pending")
        self.btnLabelX.constant = 0
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onClickDiviMenu(_ sender: UIBarButtonItem) {
        self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    @IBAction func onClickPending(_ sender: UIButton) {
        self.fetchBookingList(status: "pending")
        self.page = "Pending"
        self.btnLabelX.constant = 0
    }

    @IBAction func onClickCancelled(_ sender: UIButton) {
        self.fetchBookingList(status: "cancelled")
        self.page = "Cancelled"
        self.btnLabelX.constant = 3*self.btnLabel.frame.size.width
    }
    
    @IBAction func onClickBooked(_ sender: UIButton) {
        self.fetchBookingList(status: "ongoing")
        self.page = "Ongoing"
        self.btnLabelX.constant = self.btnLabel.frame.size.width
    }
    
    @IBAction func onClickCompleted(_ sender: UIButton) {
        self.fetchBookingList(status: "completed")
        self.page = "Completed"
        self.btnLabelX.constant = 2*self.btnLabel.frame.size.width
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if bookingList.count == 0 {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
            messageLabel.text = noTripMsg
            messageLabel.textColor = UIColor.init(colorLiteralRed: 65/255, green: 98/255, blue: 151/255, alpha: 1.0)
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            
            messageLabel.sizeToFit()
            bookingTable.backgroundView = messageLabel
            bookingTable.separatorStyle = .none
            return 0
        }else {
            bookingTable.backgroundView = nil
            bookingTable.separatorStyle = .none
            return bookingList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {

            let deleteButton = UITableViewRowAction(style: .default, title: "Cancel", handler: { (action, indexPath) in
                self.bookingTable.dataSource?.tableView!(self.bookingTable, commit: .delete, forRowAt: indexPath)
                
                return
            })
            
            deleteButton.backgroundColor = UIColor.red
            
            return [deleteButton]
        }


    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        let data = bookingList.object(at: indexPath.section) as! BookingInfo
        self.c_user_id = data.c_user_id
        self.d_user_id = data.d_user_id
        self.booking_id = data.booking_id
        self.indexx = indexPath.section
        self.trip_id = data.trip_id
        if editingStyle == .delete {
            deletePlanetIndexPath = indexPath as NSIndexPath
            let planetToDelete = bookingList.object(at: indexPath.section) as! BookingInfo
            confirmDelete(planet: String(describing: planetToDelete))
        }
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if self.page == "Ongoing" || self.page == "Pending" {
            return UITableViewCellEditingStyle.delete
        }
        return UITableViewCellEditingStyle.none
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }
    

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView()
        v.backgroundColor = UIColor.clear
        return v
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripCell") as! TripCell
        let data = bookingList.object(at: indexPath.section) as! BookingInfo
        let shared = SharedPreferences()
        let id = shared.getUserId()

        cell.book_id.isHidden = true
        if id == data.c_user_id {
            print(data.cost)
            cell.cost.text = "$" + data.cost
            cell.actingDriver.image = UIImage(named: "taxi")
            cell.book_id.text = data.booking_id
            if data.trip_status == "Pending" || data.trip_status == "Cancelled"{

                cell.rating_image.isHidden = true
                cell.cost.isHidden = true
                cell.name.text = data.c_firstname
                let profileUrl = data.c_avatar
                if profileUrl.contains("avatar"){
                    let ImagePath = "http://221.121.153.107/public/" + data.c_avatar
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                cell.profileImage.image = UIImage(data: data!)
                            }else {
                                cell.profileImage.image = UIImage(named: "avatar")
                            }
                        }
                    }
                }
                else{
                let ImagePath = data.c_avatar
                let url = URL(string: ImagePath)
                DispatchQueue.global(qos: .background).async {
                    let data = try? Data(contentsOf: url!)
                    DispatchQueue.main.async {
                        if data != nil {
                            cell.profileImage.image = UIImage(data: data!)
                        }else {
                            cell.profileImage.image = UIImage(named: "avatar")
                        }
                    }
                }
                }
                let format = DateFormatter()
                format.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let dt = format.date(from: data.b_start)
                format.dateFormat = "dd MMM yyyy hh:mm a"
                let dateString = format.string(from: dt!)
                cell.pickUpTime.text = dateString
                cell.from.text = data.b_from
                cell.to.text = data.b_to
                print(data.c_avatar)
                print(data.c_study)
            }else {
                let profileUrl = data.d_avatar
                if profileUrl.contains("avatar"){
                    let ImagePath = "http://221.121.153.107/public/" + data.d_avatar
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                cell.profileImage.image = UIImage(data: data!)
                            }else {
                                cell.profileImage.image = UIImage(named: "avatar")
                            }
                        }
                    }
                }
                else{
                let ImagePath = data.d_avatar
                let url = URL(string: ImagePath)
                DispatchQueue.global(qos: .background).async {
                    let data = try? Data(contentsOf: url!)
                    DispatchQueue.main.async {
                        if data != nil {
                            cell.profileImage.image = UIImage(data: data!)
                        }else {
                            cell.profileImage.image = UIImage(named: "avatar")
                        }
                    }
                }
                }
                cell.rating_image.isHidden = false
                cell.cost.isHidden = false
                cell.name.text = data.d_firstname + " " + data.d_lastname
                let x = Double(data.rating)!
                if x == 0.0 {
                    cell.rating_image.image = UIImage(named: "5star")!
                }
                else if x > 0.0 && x <= 0.5 {
                    cell.rating_image.image = UIImage(named: "0.5stars")
                }
                else if x > 0.5 && x <= 1.0 {
                    cell.rating_image.image = UIImage(named: "1starcolor")
                }
                else if x > 1.0 && x <= 1.5 {
                    cell.rating_image.image = UIImage(named: "1.5star")
                }
                else if x > 1.5 && x <= 2.0 {
                    cell.rating_image.image = UIImage(named: "2starcolor")
                }
                else if x > 2.0 && x <= 2.5{
                    cell.rating_image.image = UIImage(named: "2.5star")
                }
                else if x > 2.5 && x <= 3.0 {
                    cell.rating_image.image = UIImage(named: "3starcolor")
                }
                else if x > 3.0 && x <= 3.5 {
                    cell.rating_image.image = UIImage(named: "3.5star")
                }
                else if x > 3.5 && x <= 4.0 {
                    cell.rating_image.image = UIImage(named: "4starcolor")
                }
                else if x > 4.0 && x <= 4.5 {
                    cell.rating_image.image = UIImage(named: "4.5star")
                }
                else if x > 4.5 {
                    cell.rating_image.image = UIImage(named: "5starcolor")
                }
                let format = DateFormatter()
                format.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let dt = format.date(from: data.b_start)
                format.dateFormat = "dd MMM yyyy hh:mm a"
                let dateString = format.string(from: dt!)
                cell.pickUpTime.text = dateString
                cell.from.text = data.b_from
                cell.to.text = data.b_to
                print(data.c_avatar)
                print(data.c_study)

            }
    //        let format = DateFormatter()
//            format.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            let dt = format.date(from: data.b_start)
//            format.dateFormat = "dd MMM yyyy hh:mm a"
//            let dateString = format.string(from: dt!)
//            cell.pickUpTime.text = dateString
//            cell.from.text = data.b_from
//            cell.to.text = data.b_to
//            print(data.c_avatar)
//            print(data.c_study)
           
            
        }else if id == data.d_user_id {
            cell.cost.text = "$" + data.driver_share
            cell.actingDriver.image = UIImage(named: "Car icon")
            cell.book_id.text = data.trip_id
            if data.trip_status == "Pending" || data.trip_status == "Cancelled"{

                cell.rating_image.isHidden = true
                cell.cost.isHidden = true
                cell.name.text = data.d_firstname
                let profileUrl = data.d_avatar
                if profileUrl.contains("avatar"){
                    let ImagePath = "http://221.121.153.107/public/" + data.d_avatar
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                cell.profileImage.image = UIImage(data: data!)
                            }else {
                                cell.profileImage.image = UIImage(named: "avatar")
                            }
                        }
                    }
                }
                else{
                    let ImagePath = data.d_avatar
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                cell.profileImage.image = UIImage(data: data!)
                            }else {
                                cell.profileImage.image = UIImage(named: "avatar")
                            }
                        }
                    }
                }
                let format = DateFormatter()
                format.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let dt = format.date(from: data.t_start)
                format.dateFormat = "dd MMM yyyy hh:mm a"
                let dateString = format.string(from: dt!)
                cell.pickUpTime.text = dateString
                cell.from.text = data.t_from
                cell.to.text = data.t_to
            }else {
                
                cell.rating_image.isHidden = false
                cell.cost.isHidden = false
//                var profiletype = shared.getProfileType()
//                if profiletype == "FBSignUp"{
//                    let ImagePath = data.c_avatar
//                    let url = URL(string: ImagePath)
//                    DispatchQueue.global(qos: .background).async {
//                        let data = try? Data(contentsOf: url!)
//                        DispatchQueue.main.async {
//                            if data != nil {
//                                cell.profileImage.image = UIImage(data: data!)
//                            }else {
//                                cell.profileImage.image = UIImage(named: "avatar")
//                            }
//                        }
//                    }
//                }
//                else{
                
                let profileUrl = data.c_avatar
                if profileUrl.contains("avatar"){
                    let ImagePath = "http://221.121.153.107/public/" + data.c_avatar
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                cell.profileImage.image = UIImage(data: data!)
                            }else {
                                cell.profileImage.image = UIImage(named: "avatar")
                            }
                        }
                        //                }
                    }
                }
                else{
                let ImagePath = data.c_avatar
                let url = URL(string: ImagePath)
                DispatchQueue.global(qos: .background).async {
                    let data = try? Data(contentsOf: url!)
                    DispatchQueue.main.async {
                        if data != nil {
                            cell.profileImage.image = UIImage(data: data!)
                        }else {
                            cell.profileImage.image = UIImage(named: "avatar")
                        }
                    }
                }
                }
                cell.name.text = data.c_firstname + " " + data.c_lastname
                let x = Double(data.rating)!
                if x == 0.0 {
                    cell.rating_image.image = UIImage(named: "5star")!
                }
                else if x > 0.0 && x <= 0.5 {
                    cell.rating_image.image = UIImage(named: "0.5stars")
                }
                else if x > 0.5 && x <= 1.0 {
                    cell.rating_image.image = UIImage(named: "1starcolor")
                }
                else if x > 1.0 && x <= 1.5 {
                    cell.rating_image.image = UIImage(named: "1.5star")
                }
                else if x > 1.5 && x <= 2.0 {
                    cell.rating_image.image = UIImage(named: "2starcolor")
                }
                else if x > 2.0 && x <= 2.5{
                    cell.rating_image.image = UIImage(named: "2.5star")
                }
                else if x > 2.5 && x <= 3.0 {
                    cell.rating_image.image = UIImage(named: "3starcolor")
                }
                else if x > 3.0 && x <= 3.5 {
                    cell.rating_image.image = UIImage(named: "3.5star")
                }
                else if x > 3.5 && x <= 4.0 {
                    cell.rating_image.image = UIImage(named: "4starcolor")
                }
                else if x > 4.0 && x <= 4.5 {
                    cell.rating_image.image = UIImage(named: "4.5star")
                }
                else if x > 4.5 {
                    cell.rating_image.image = UIImage(named: "5starcolor")
                }
                let format = DateFormatter()
                format.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let dt = format.date(from: data.b_start)
                format.dateFormat = "dd MMM yyyy hh:mm a"
                let dateString = format.string(from: dt!)
                cell.pickUpTime.text = dateString
                cell.from.text = data.b_from
                cell.to.text = data.b_to

            }
//            let format = DateFormatter()
//            format.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            let dt = format.date(from: data.t_start)
//            format.dateFormat = "dd MMM yyyy hh:mm a"
//            let dateString = format.string(from: dt!)
//            cell.pickUpTime.text = dateString
//            cell.from.text = data.t_from
//            cell.to.text = data.t_to

        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let bookingInfo = bookingList.object(at: indexPath.section) as! BookingInfo
        let shared = SharedPreferences()
        let id = shared.getUserId()
        
        if bookingInfo.trip_status == "Pending" {
            if id == bookingInfo.c_user_id{
                let storyboard = UIStoryboard.init(name: "HistoryDetails", bundle: nil)
                let details = storyboard.instantiateViewController(withIdentifier: "HistoryDetails") as! HistoryDetails
                details.bookingInfo = bookingInfo
                details.pageFrom = "MyTrips"
                details.page = self.page
                details.role = "Commuter"
                self.navigationController?.pushViewController(details, animated: true)
            }
            else{
                let storyboard = UIStoryboard.init(name: "HistoryDetails", bundle: nil)
                let details = storyboard.instantiateViewController(withIdentifier: "HistoryDetails") as! HistoryDetails
                details.bookingInfo = bookingInfo
                details.pageFrom = "MyTrips"
                details.page = self.page
                details.role = "Driver"
                self.navigationController?.pushViewController(details, animated: true)
            }
        }else if bookingInfo.trip_status == "Alloted" {
            let storyboard = UIStoryboard.init(name: "HistoryDetails", bundle: nil)
            let details = storyboard.instantiateViewController(withIdentifier: "HistoryDetails") as! HistoryDetails
            details.bookingInfo = bookingInfo
            details.page = self.page
            self.navigationController?.pushViewController(details, animated: true)
        }
        else{
            let storyboard = UIStoryboard.init(name: "HistoryDetails", bundle: nil)
            let details = storyboard.instantiateViewController(withIdentifier: "HistoryDetails") as! HistoryDetails
            details.bookingInfo = bookingInfo
            details.role = "Driver"
            details.page = self.page
            self.navigationController?.pushViewController(details, animated: true)
        }
    }
    
    func fetchBookingList(status: String) {
        
        DVUtility.disaplyWaitMessage()
        
        let jsonDict: NSDictionary = ["dv": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        let user = shared.getUserType()
        if user == "Commutor" {
            networkCommunication.startNetworkRequest("http://221.121.153.107/user/history/\(status)/\(id)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "CommutorHistory")
        }else {
            print("http://221.121.153.107/user/history/\(status)/\(id)")
            networkCommunication.startNetworkRequest("http://221.121.153.107/user/history/\(status)/\(id)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "DriverHistory")
        }
        
        
    }
    func confirmDelete(planet: String) {
        let alert = UIAlertController(title: "Cancel Trip", message: "Are you sure you want to cancel the trip?", preferredStyle: .actionSheet)
        
        let DeleteAction = UIAlertAction(title: "Cancel Trip", style: .destructive, handler: handleDeletePlanet)
        let CancelAction = UIAlertAction(title: "Done", style: .cancel, handler: cancelDeletePlanet)
        
        alert.addAction(DeleteAction)
        alert.addAction(CancelAction)
        
        // Support display in iPad
        alert.popoverPresentationController?.sourceView = self.view

        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
        
        self.present(alert, animated: true, completion: nil)
    }
    func handleDeletePlanet(alertAction: UIAlertAction!) -> Void {
        self.cancel()
    }
    func cancel() {
        
        DVUtility.disaplyWaitMessage()
        let shared = SharedPreferences()
        let id = shared.getUserId()
        let userType = shared.getUserType()

        print(userType)
        if id == self.d_user_id{
            let url = "http://221.121.153.107/trip/cancel/\(id)"
            let jsonDict: NSDictionary = ["trip_id": self.trip_id]
            self.sendToServer(url: url, apiType: "Cancel", jsonDict: jsonDict as! [String : Any], method: "POST")
        }else {
            let url = "http://221.121.153.107/booking/cancel/\(id)"
            let jsonDict: NSDictionary = ["booking_id": self.booking_id]
            self.sendToServer(url: url, apiType: "Cancel", jsonDict: jsonDict as! [String : Any], method: "POST")
        }
        
    }
    func sendToServer(url: String, apiType: String, jsonDict: [String : Any], method: String) {
        
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        DVUtility.disaplyWaitMessage()
        networkCommunication.startNetworkRequest(url, jsonData: jsonDict , method: method, apiType: apiType)
        
      
    }

    func cancelDeletePlanet(alertAction: UIAlertAction!) {
        deletePlanetIndexPath = nil
    }

    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response , apiType: apiType)
    }
    
    func failure() {
        DVUtility.hideWaitMessage()
        DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty,please try again")
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        
        DVUtility.hideWaitMessage()
        
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            if apiType == "Cancel"{
                if let amount = responseData["amount"] as? String{
                    self.bookingList.removeObject(at: self.indexx)
                    let indexSet = NSMutableIndexSet()
                    indexSet.add(self.indexx)
                    var c = IndexPath(row: 0, section: self.indexx)
                    self.bookingTable.beginUpdates()
                    self.bookingTable.deleteSections(indexSet as IndexSet, with: .left)
                    self.bookingTable.endUpdates()
                    if self.bookingList.count == 0{
                        self.noTripMsg = "You have no trip at this time."
                        self.bookingTable.reloadData()
                    }
                    let alert = UIAlertView(title: "", message: "You have charged $\(amount) for cancelling this ride.", delegate: self, cancelButtonTitle: "Ok")
                    alert.show()
                }
                else{
                    self.bookingList.removeObject(at: self.indexx)
                    let indexSet = NSMutableIndexSet()
                    indexSet.add(self.indexx)
                    var c = IndexPath(row: 0, section: self.indexx)
                    self.bookingTable.beginUpdates()
                    self.bookingTable.deleteSections(indexSet as IndexSet, with: .left)
                    self.bookingTable.endUpdates()
                    if bookingList.count == 0{
                        noTripMsg = "You have no trip at this time."
                        bookingTable.reloadData()
                    }
                }
            }
            else{
                print("")
                self.storeTripDetails(details: responseData["data"] as! Array<Any>)
            }
        case 600:
            if apiType == "Cancel"{
                var message = responseData["message"] as! String
                DVUtility.displayAlertBoxTitle("", withMessage: "\(message)")
            }
            else{
            noTripMsg = "You have no trip at this time."
            bookingList.removeAllObjects()
            bookingTable.reloadData()
            }
        case 101:
            if apiType == "Cancel" {
                if let amount = responseData["amount"] as? String{
                    self.bookingList.removeObject(at: self.indexx)
                    let indexSet = NSMutableIndexSet()
                    indexSet.add(self.indexx)
                    var c = IndexPath(row: 0, section: self.indexx)
                    self.bookingTable.beginUpdates()
                    self.bookingTable.deleteSections(indexSet as IndexSet, with: .left)
                    self.bookingTable.endUpdates()
                    if self.bookingList.count == 0{
                        self.noTripMsg = "You have no trip at this time."
                        self.bookingTable.reloadData()
                    }
                    let alert = UIAlertView(title: "", message: "You have charged $\(amount) for cancelling this ride.", delegate: self, cancelButtonTitle: "Ok")
                    alert.show()
                }
                else{
                    self.bookingList.removeObject(at: self.indexx)
                    let indexSet = NSMutableIndexSet()
                    indexSet.add(self.indexx)
                    var c = IndexPath(row: 0, section: self.indexx)
                    self.bookingTable.beginUpdates()
                    self.bookingTable.deleteSections(indexSet as IndexSet, with: .left)
                    self.bookingTable.endUpdates()
                    if bookingList.count == 0{
                        noTripMsg = "You have no trip at this time."
                        bookingTable.reloadData()
                    }
                }

            }
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }
    
    func storeTripDetails(details: Array<Any>) {
        bookingList.removeAllObjects()
        for d in details {
            let bookingInfo = BookingInfo()
            let detail = d as! NSDictionary
            if detail.object(forKey: "booking_id") is NSNull {
                bookingInfo.booking_id = ""
            }else {
                bookingInfo.booking_id = detail.object(forKey: "booking_id") as! String
            }
            if detail.object(forKey: "b_from") is NSNull {
                bookingInfo.b_from = ""
            }else {
                bookingInfo.b_from = detail.object(forKey: "b_from") as! String
            }
            if detail.object(forKey: "b_to") is NSNull {
                bookingInfo.b_to = ""
            }else {
                bookingInfo.b_to = detail.object(forKey: "b_to") as! String
            }
            if detail.object(forKey: "b_start") is NSNull {
                bookingInfo.b_start = ""
            }else {
                bookingInfo.b_start = detail.object(forKey: "b_start") as! String
            }
            if detail.object(forKey: "b_end") is NSNull {
                bookingInfo.b_end = ""
            }else {
                bookingInfo.b_end = detail.object(forKey: "b_end") as! String
            }
            if detail.object(forKey: "b_from_latlng") is NSNull {
                bookingInfo.b_from_latlng = ""
            }else {
                bookingInfo.b_from_latlng = detail.object(forKey: "b_from_latlng") as! String
            }
            if detail.object(forKey: "b_to_latlng") is NSNull {
                bookingInfo.b_to_latlng = ""
            }else {
                bookingInfo.b_to_latlng = detail.object(forKey: "b_to_latlng") as! String
            }
            
            
            if detail.object(forKey: "c_address") is NSNull {
                bookingInfo.c_address = ""
            }else {
                bookingInfo.c_address = detail.object(forKey: "c_address") as! String
            }
            if detail.object(forKey: "c_avatar") is NSNull {
                bookingInfo.c_avatar = ""
            }else {
                bookingInfo.c_avatar = detail.object(forKey: "c_avatar") as! String
            }
            if detail.object(forKey: "c_contact") is NSNull {
                bookingInfo.c_contact = ""
            }else {
                bookingInfo.c_contact = detail.object(forKey: "c_contact") as! String
            }
            if detail.object(forKey: "c_email") is NSNull {
                bookingInfo.c_email = ""
            }else {
                bookingInfo.c_email = detail.object(forKey: "c_email") as! String
            }
            if detail.object(forKey: "c_firstname") is NSNull {
                bookingInfo.c_firstname = ""
            }else {
                bookingInfo.c_firstname = detail.object(forKey: "c_firstname") as! String
            }
            if detail.object(forKey: "c_lastname") is NSNull {
                bookingInfo.c_lastname = ""
            }else {
                bookingInfo.c_lastname = detail.object(forKey: "c_lastname") as! String
            }
            if detail.object(forKey: "c_role") is NSNull {
                bookingInfo.c_role = ""
            }else {
                bookingInfo.c_role = detail.object(forKey: "c_role") as! String
            }
            if detail.object(forKey: "c_gender") is NSNull {
                bookingInfo.c_gender = ""
            }else {
                bookingInfo.c_gender = detail.object(forKey: "c_gender") as! String
            }
            if detail.object(forKey: "c_study") is NSNull {
                bookingInfo.c_study = ""
            }else {
                bookingInfo.c_study = detail.object(forKey: "c_study") as! String
            }
            if detail.object(forKey: "c_user_id") is NSNull {
                bookingInfo.c_user_id = ""
            }else {
                bookingInfo.c_user_id = detail.object(forKey: "c_user_id") as! String
            }
            
            
            if detail.object(forKey: "created") is NSNull {
                bookingInfo.created = ""
            }else {
                bookingInfo.created = detail.object(forKey: "created") as! String
            }
            if detail.object(forKey: "updated") is NSNull {
                bookingInfo.updated = ""
            }else {
                bookingInfo.updated = detail.object(forKey: "updated") as! String
            }
            
            
            if detail.object(forKey: "trip_id") is NSNull {
                bookingInfo.trip_id = ""
            }else {
                bookingInfo.trip_id = detail.object(forKey: "trip_id") as! String
            }
            if detail.object(forKey: "trip_status_id") is NSNull {
                bookingInfo.trip_status_id = ""
            }else {
                bookingInfo.trip_status_id = detail.object(forKey: "trip_status_id") as! String
            }
            if detail.object(forKey: "trip_status") is NSNull {
                bookingInfo.trip_status = ""
            }else {
                bookingInfo.trip_status = detail.object(forKey: "trip_status") as! String
            }
            if detail.object(forKey: "t_from") is NSNull {
                bookingInfo.t_from = ""
            }else {
                bookingInfo.t_from = detail.object(forKey: "t_from") as! String
            }
            if detail.object(forKey: "t_to") is NSNull {
                bookingInfo.t_to = ""
            }else {
                bookingInfo.t_to = detail.object(forKey: "t_to") as! String
            }
            if detail.object(forKey: "t_start") is NSNull {
                bookingInfo.t_start = ""
            }else {
                bookingInfo.t_start = detail.object(forKey: "t_start") as! String
            }
            if detail.object(forKey: "t_end") is NSNull {
                bookingInfo.t_end = ""
            }else {
                bookingInfo.t_end = detail.object(forKey: "t_end") as! String
            }
            if detail.object(forKey: "t_from_latlng") is NSNull {
                bookingInfo.t_from_latlng = ""
            }else {
                bookingInfo.t_from_latlng = detail.object(forKey: "t_from_latlng") as! String
            }
            if detail.object(forKey: "t_to_latlng") is NSNull {
                bookingInfo.t_to_latlng = ""
            }else {
                bookingInfo.t_to_latlng = detail.object(forKey: "t_to_latlng") as! String
            }
            
            if let interest = detail.object(forKey: "interests"){
                if detail.object(forKey: "interests") is NSNull{
                    bookingInfo.interests = ""
                }
                else{
                    bookingInfo.interests = detail.object(forKey: "interests") as! String
                }
            }
            if let rate = detail.object(forKey: "rating"){
                if detail.object(forKey: "rating") is NSNull{
                bookingInfo.rating = ""
                }else {
                var rate = Double()
                var rate1 = detail.object(forKey: "rating") as! Double
                print(String(rate1))
                bookingInfo.rating = String(rate1)
                }
            }
            if let driver_rate = detail.object(forKey: "driver_rating"){
                if detail.object(forKey: "driver_rating") is NSNull{
                    bookingInfo.rating = ""
                }else {
                    var rate = Double()
                    var rate1 = detail.object(forKey: "driver_rating") as! Double
                    print(String(rate1))
                    bookingInfo.rating = String(rate1)
                }

            }
            if let vehicle = detail.object(forKey: "d_vehicle"){
                if detail.object(forKey: "d_vehicle") is NSNull{
                    bookingInfo.vehicle = ""
                }
                else{
                    bookingInfo.vehicle = detail.object(forKey: "d_vehicle") as! String
                }
            }
            if let plate = detail.object(forKey: "d_plate"){
                if detail.object(forKey: "d_plate") is NSNull{
                    bookingInfo.plate = ""
                }
                else{
                    bookingInfo.plate = detail.object(forKey: "d_plate") as! String
                }
            }
            if let color = detail.object(forKey: "d_colour"){
                if detail.object(forKey: "d_colour") is NSNull{
                    bookingInfo.v_color = ""
                }
                else{
                    bookingInfo.v_color = detail.object(forKey: "d_colour") as! String
                }
            }

            
            if detail.object(forKey: "d_address") is NSNull {
                bookingInfo.d_address = ""
            }else {
                bookingInfo.d_address = detail.object(forKey: "d_address") as! String
            }
            if detail.object(forKey: "d_avatar") is NSNull {
                bookingInfo.d_avatar = ""
            }else {
                bookingInfo.d_avatar = detail.object(forKey: "d_avatar") as! String
            }
            if detail.object(forKey: "d_contact") is NSNull {
                bookingInfo.d_contact = ""
            }else {
                bookingInfo.d_contact = detail.object(forKey: "d_contact") as! String
            }
            if detail.object(forKey: "d_email") is NSNull {
                bookingInfo.d_email = ""
            }else {
                bookingInfo.d_email = detail.object(forKey: "d_email") as! String
            }
            if detail.object(forKey: "d_firstname") is NSNull {
                bookingInfo.d_firstname = ""
            }else {
                bookingInfo.d_firstname = detail.object(forKey: "d_firstname") as! String
            }
            if detail.object(forKey: "d_lastname") is NSNull {
                bookingInfo.d_lastname = ""
            }else {
                bookingInfo.d_lastname = detail.object(forKey: "d_lastname") as! String
            }
            if detail.object(forKey: "d_role") is NSNull {
                bookingInfo.d_role = ""
            }else {
                bookingInfo.d_role = detail.object(forKey: "d_role") as! String
            }
            if detail.object(forKey: "d_gender") is NSNull {
                bookingInfo.d_gender = ""
            }else {
                bookingInfo.d_gender = detail.object(forKey: "d_gender") as! String
            }
            
            if detail.object(forKey: "d_study") is NSNull {
                bookingInfo.d_study = ""
            }else {
                bookingInfo.d_study = detail.object(forKey: "d_study") as! String
            }
            if detail.object(forKey: "d_user_id") is NSNull {
                bookingInfo.d_user_id = ""
            }else {
                bookingInfo.d_user_id = detail.object(forKey: "d_user_id") as! String
            }
            if let calculation = detail.object(forKey: "calculation") {
                var calculation = detail.object(forKey: "calculation") as! NSDictionary
                if calculation.object(forKey: "cost") is NSNull {
                    bookingInfo.cost = ""
                }
                else{
                    bookingInfo.cost = calculation.object(forKey: "cost") as! String
                }
                if calculation.object(forKey: "driver_share") is NSNull {
                    bookingInfo.driver_share = ""
                }
                else{
                    bookingInfo.driver_share = calculation.object(forKey: "driver_share") as! String
                }
            }
            
            bookingList.add(bookingInfo)
            
        }
        
        bookingTable.reloadData()
        
    }
    
}
