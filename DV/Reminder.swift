//
//  Reminder.swift
//  DV
//
//  Created by Cherian Sankey on 21/06/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit
//import UserNotifications
class Reminder: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {

    @IBOutlet weak var skipContinueButton: UIButton!
    @IBOutlet weak var timerTextField: UITextField!
    var window: UIWindow?
    var start_time = String()// from Database
    var pick_time = Int()
    var t_start = String()
    var pickup_time = Int()
    var bookingInfo: BookingInfo?
    var pickerView: UIPickerView?
    var pickerData: [String] = [String]()
    var name = String()
    var role = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView = UIPickerView()
        pickerView = UIPickerView()
        pickerView?.dataSource = self
        pickerView?.delegate = self
        pickerData = ["5","10","15","20","25","30","35","40","45","50","55","60"]
        let picker: UIPickerView
        picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: 100, height: 120))
        
        picker.backgroundColor = .white
        
        
        picker.showsSelectionIndicator = true
        picker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 27/255, green: 168/255, blue: 159/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        timerTextField.inputView = pickerView
        timerTextField.inputAccessoryView = toolBar
        
        if role == "from_notification"{
            self.start_time = t_start

        }
        self.timerTextField.attributedPlaceholder = NSAttributedString(string: "Min",
                                                                       attributes: [NSForegroundColorAttributeName: UIColor.white])
        self.timerTextField.rightViewMode = UITextFieldViewMode.always
        var imageView = UIImageView()
        let image = UIImage(named: "drop-down.png")
        imageView.image = image
        self.timerTextField.rightView = imageView
        // Do any additional setup after loading the view.
        
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func donePicker(sender:UIButton!){
        timerTextField.resignFirstResponder()
        timerTextField.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
    func getEightAMDate() -> NSDate? {
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let now: NSDate! = NSDate()
//        let givenTime trip time
        let last8 = String(start_time.characters.suffix(8))//hh:MM:ss
        let first5 = String(last8.characters.prefix(5))//hh:MM
        let h = String(first5.characters.prefix(2))//hh
        let m = String(first5.characters.suffix(2))//mm
        let hour = Int(h)
        let min = Int(m)
        let total:Int = hour!*60+min!
        // let reminder is 10min
        let time_int = Int(self.timerTextField.text!)
        let time:Int = time_int!
        //        let time_min = time*60
        let final_time:Int = total - time
        let final_time_format_hour:Int = final_time/60
        let final_time_format_minute:Int = final_time%60
        
        let date10h = calendar.date(bySettingHour: final_time_format_hour, minute: final_time_format_minute, second: 0, of: now as Date, options: NSCalendar.Options.matchFirst)!
        return date10h as NSDate
    }

    public func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView,titleForRow row: Int,forComponent component: Int) -> String?{
        return pickerData[row]  as? String
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        timerTextField.text = pickerData[row] as? String
        
    }
    
    @IBAction func doneButton(_ sender: UIButton) {
        if (self.timerTextField.text?.characters.count)! >= 1{

            let localNotification1 = UILocalNotification()
            localNotification1.alertTitle = "Mobi reminder"
            localNotification1.alertBody = "Your trip will start soon."
            localNotification1.userInfo = ["notification":"local"]
            localNotification1.timeZone = NSTimeZone.default
            localNotification1.fireDate = self.getEightAMDate() as! Date

            UIApplication.shared.scheduleLocalNotification(localNotification1)
            self.gotoAlloted()
        }
        else{
            let alert = UIAlertView(title: "", message: "Please set a reminder or click skip", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
        }
        
    }
    
    @IBAction func skip_button(_ sender: UIButton) {
        self.gotoAlloted()
    }
    
    func gotoAlloted() {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        let story = UIStoryboard(name: "PickDrop", bundle: nil)
        let PickDrop = story.instantiateViewController(withIdentifier: "PickUpDropOff") as! PickUpDropOff
        let navVC:UINavigationController = story.instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
        navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
        
        let container: MFSideMenuContainerViewController = story.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
        container.leftMenuViewController = leftVC
        container.centerViewController = navVC
        container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
        self.present(container, animated: true, completion: nil)

    }
    
    
    
}


