//
//  SideMenu.swift
//  DV
//
//  Created by Chinmay on 31/01/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class SideMenu: UIViewController, UITableViewDelegate, UITableViewDataSource , NetworkProtocol {
    
   
    @IBOutlet var view2: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!

    let logo: NSArray = ["findaride_image.png","myrides_image.png","myrequest_image.png","howitworks_image.png","support_image.png","logout_image.png"]
    let labels: NSArray = ["Find a Ride","My Rides","My Request","How it Works","Support","Logout"]
    var firstname = String()
    var lastname = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view2.backgroundColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)
        tableView.tableFooterView = UIView()
    }
    override func viewWillAppear(_ animated: Bool) {
        let networkCommunication = NetworkCommunication()
        let shared = SharedPreferences()
        let id = shared.getUserId()
        let jsonDict: NSDictionary = ["DV": "mobi"]
        networkCommunication.networkDelegate = self
        DVUtility.disaplyWaitMessage()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        networkCommunication.startNetworkRequest("http://221.121.153.107/user/profile/\(id)",jsonData: jsonDict as! [String : Any], method: "GET", apiType: "GetProfile")
    }
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
        DVUtility.hideWaitMessage()
        UIApplication.shared.endIgnoringInteractionEvents()
        DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty.please try again")
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        DVUtility.hideWaitMessage()
        UIApplication.shared.endIgnoringInteractionEvents()
        
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            if apiType == "GetProfile" {
                let data:NSDictionary = responseData["data"] as! NSDictionary
                    if (data["firstname"] as! AnyObject)as! NSObject == NSNull(){
                        self.firstname = "Null"
                    }
                    else{
                        self.firstname = data["firstname"] as! String
                    }
                    if (data["lastname"] as! AnyObject)as! NSObject == NSNull(){
                        self.lastname = "Null"
                    }
                    else{
                        self.lastname = data["lastname"] as! String
                    }
                var role = data["role"] as! String
                let shared = SharedPreferences()
                shared.saveUserType(role)
                self.username.text = self.firstname + " " + self.lastname
                var  avatarPath = data["avatar"] as! String
                if avatarPath.contains("avatar"){
                    let ImagePath = "http://221.121.153.107/public/" + avatarPath
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                self.profileImage.image = UIImage(data: data!)
                            }else {
                                self.profileImage.image = UIImage(named: "avatar")
                            }
                        }
                    }
                }
                else{
                    let ImagePath = avatarPath
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                self.profileImage.image = UIImage(data: data!)
                            }else {
                                self.profileImage.image = UIImage(named: "avatar")
                            }
                        }
                    }
                }
            }
            else if apiType == "RegisterGCM"{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "logout"), object: nil)
                let storyboard = UIStoryboard(name: "LoginSignUp", bundle: nil)
                let home = storyboard.instantiateViewController(withIdentifier: "Home") as! Home
                self.present(home, animated: true, completion: nil)
            }
            print("")
        case 600:
            DVUtility.displayAlertBoxTitle("", withMessage: "Some error occured. Please try again.")
            
        case 603:
            DVUtility.displayAlertBoxTitle("", withMessage: "Some error occured. Please try again.")
            let shared = SharedPreferences()
            shared.clearPreferences()

        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBOutlet weak var username: UILabel!
    
    @IBAction func onClickViewProfile(_ sender: UIButton) {
        self.menuContainerViewController.toggleLeftSideMenuCompletion({
            let storyboard = UIStoryboard(name: "ViewProfile", bundle: nil)
            let navVC:UINavigationController = storyboard.instantiateViewController(withIdentifier: "ProfileNav") as! UINavigationController
            self.menuContainerViewController.centerViewController = navVC
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labels.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideCell", for: indexPath) as! SideMenuCell
        cell.label.text = labels.object(at: indexPath.row) as? String
        cell.logo.image = UIImage(named: logo.object(at: indexPath.row) as! String)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.menuContainerViewController.toggleLeftSideMenuCompletion({
            let story = UIStoryboard(name: "PickDrop", bundle: nil)
            let navVC:UINavigationController = story.instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
            navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
            self.menuContainerViewController.centerViewController = navVC
            })
        }else if indexPath.row == 1 {
            self.menuContainerViewController.toggleLeftSideMenuCompletion({
                let storyboard = UIStoryboard(name: "MyTrips", bundle: nil)
                let navVC:UINavigationController = storyboard.instantiateViewController(withIdentifier: "MyTripsNav") as! UINavigationController
                self.menuContainerViewController.centerViewController = navVC
            })
        }else if indexPath.row == 2 {
            self.menuContainerViewController.toggleLeftSideMenuCompletion({
                let storyboard = UIStoryboard(name: "MyRequests", bundle: nil)
                let navVC:UINavigationController = storyboard.instantiateViewController(withIdentifier: "MyRequestsNav") as! UINavigationController
                self.menuContainerViewController.centerViewController = navVC
            })

        }else if indexPath.row == 3 {
            self.menuContainerViewController.toggleLeftSideMenuCompletion({
                let storyboard = UIStoryboard(name: "HowItWorks", bundle: nil)
                let navVC:UINavigationController = storyboard.instantiateViewController(withIdentifier: "HowItWorksNav") as! UINavigationController
                navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
                self.menuContainerViewController.centerViewController = navVC
            })
        }else if indexPath.row == 4 {
            self.menuContainerViewController.toggleLeftSideMenuCompletion({
                let storyboard = UIStoryboard(name: "Support", bundle: nil)
                let navVC:UINavigationController = storyboard.instantiateViewController(withIdentifier: "SupportNav") as! UINavigationController
                navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
                self.menuContainerViewController.centerViewController = navVC
            })


        }else if indexPath.row == 5 {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
            let alert = UIAlertController(title: " ", message:"Are you sure you want to logout ?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler: {(action : UIAlertAction) in
                self.unregisterDeviceToken()
            
            }))
            
        }
    }
    func phone(phoneNum: String) {
        if let url = URL(string: "tel://\(phoneNum)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    func unregisterDeviceToken() {
       
        let shared = SharedPreferences()
        let id = shared.getUserId()
        let token = shared.getToken()
        let jsonDict: NSDictionary
        jsonDict = ["token": token]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        networkCommunication.startNetworkRequest("http://221.121.153.107/user/unsubscribe/\(id)", jsonData: jsonDict as! [String : Any], method: "POST", apiType: "RegisterGCM")
        
    }
    

       


}
