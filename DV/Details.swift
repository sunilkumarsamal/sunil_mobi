//
//  Details.swift
//  DV
//
//  Created by Chinmay on 18/02/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class Details: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,NetworkProtocol,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet weak var driver: UIButton!
    @IBOutlet weak var commutor: UIButton!
    @IBOutlet weak var both: UIButton!
    
    @IBOutlet weak var uploadImage: UIButton!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var DOB: UITextField!
    var gender: String?
    var birthDate: String?
    var fName = String()
    var pro:UIVisualEffectView?
    var firstname = String()
    var lastname = String()
    var imageurl = String()
    var lName = String()
    var pNumber = String()
    var indus = String()
    var userType = String()
    var dob = String()
    var gen = String()
    var avatar: UIImage?
    var info = PersonInfo()
    var DLImage: UIImage?
    var REGOImage: UIImage?
    var INSURANCEImage: UIImage?
    var Model = String()
    var Plate = String()
    var CarColour = String()
    var entered = Date()
    var checkreg = String()
    var checkidentity = String()
    var checkinsu = String()
    var avatarPath = String()
    var pickerView: UIPickerView?
    var pickerData: [String] = [String]()
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var industry: UITextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    weak var activeField: UITextField!
    
   
    
    @IBOutlet weak var progressBarImage: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    var img: String?
    var progressCircle = CAShapeLayer()
    var percent: CGFloat = 0.0
    
    var isRoleSelected:Bool = false
    var isCommutor:Bool = false
    var role_selected = String()
    var flag = String()
    var fullname:String?
    var mobile:String?
    
    var cent:CGFloat = 100.0
    
    @IBOutlet weak var status: UILabel!
    
    var profileType = "SignUp"
    
    var id:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.segment.selectedSegmentIndex = UISegmentedControlNoSegment
        // pickerView code start
        if self.profileType == "FbSignUp"{
            self.uploadImage.isHidden = true
        }
        else{
            self.uploadImage.isHidden = false
        }
        pickerView = UIPickerView()
        pickerView = UIPickerView()
        pickerView?.dataSource = self
        pickerView?.delegate = self
        
        pickerData = ["Accounting","Banking and Finance","Administration and Office Support","Advertising","Arts and Media","Customer Service and Call Centre","CEO and General Management","Community Service and Development","Construction","Design and Architecture","Education and Training","Engineering and Manufacturing","Government and Defence","Healthcare and Medical","Hospitality and Tourism","Human Resources and Recruitment","Information Technology","Legal","Law Enforcement and Security","Marketing and Communications","Real Estate and Property","Retail & Sales","Student","Trades and Services","Transport and logistic","Other"]
        let picker: UIPickerView
        picker = UIPickerView(frame: CGRect(x: 0, y: 0, width: 100, height: 120))
        
        picker.backgroundColor = .white
        
        
        picker.showsSelectionIndicator = true
        picker.delegate = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 27/255, green: 168/255, blue: 159/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)

        
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        
        
        industry.inputView = pickerView
        industry.inputAccessoryView = toolBar

        
        
        
 
        DOB.setBottomBorder()
        let dobPicker = UIDatePicker()
        dobPicker.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        DOB.inputView = dobPicker
        self.setDoneOnKeyboard()
        
        dobPicker.datePickerMode = .date
        driver.layer.borderWidth = 2.0;
        driver.layer.borderColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0).cgColor;
        commutor.layer.borderWidth = 2.0;
        commutor.layer.borderColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0).cgColor;
        both.layer.borderWidth = 2.0;
        both.layer.borderColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0).cgColor;
        
        firstName.delegate = self
        phoneNumber.delegate = self
        lastName.delegate = self
        industry.delegate = self
        
        let circle = UIView(frame: CGRect(x:0, y:0, width:140, height:140))
        
        circle.layoutIfNeeded()
        
        let centerPoint = CGPoint (x: circle.bounds.width / 2, y: circle.bounds.width / 2)
        let circleRadius : CGFloat = circle.bounds.width / 2 * 0.95
        
        let circlePath = UIBezierPath(arcCenter: centerPoint, radius: circleRadius, startAngle: CGFloat(-0.5 * M_PI), endAngle: CGFloat(1.5 * M_PI), clockwise: true)
        
        progressCircle = CAShapeLayer ()
        progressCircle.path = circlePath.cgPath
        progressCircle.strokeColor = UIColor.init(colorLiteralRed: 68/255, green: 168/255, blue: 157/255, alpha: 1.0).cgColor
        progressCircle.fillColor = UIColor.white.cgColor
        progressCircle.lineWidth = 10
        progressCircle.strokeStart = 0
        progressCircle.strokeEnd = 0.15
        percent = 0.15 // for password and email
        let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        circle.layer.addSublayer(progressCircle)
        
        self.progressBarImage.addSubview(circle)
        
        if profileType == "SignUp" {
            print("")
            img = "No"
        }
            else if profileType == "FbSignUp"{
            self.firstName.text = firstname
            self.lastName.text = lastname
            if gen == "Male" || gen == "male"{
                
                self.segment.selectedSegmentIndex = 0
                
            }
            else if gen == "Female" || gen == "female"{
                self.segment.selectedSegmentIndex = 1
            }
            else{
                self.segment.selectedSegmentIndex = UISegmentedControlNoSegment
            }
            let url = URL(string: imageurl)
            
            DispatchQueue.global(qos: .background).async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                    if data != nil {
                        self.profileImage.image = UIImage(data: data!)
                    }else {
                        self.profileImage.image = UIImage(named: "avatar")
                    }
                }
            }
        }
        else {

            progressBarImage.isHidden = true
            progressCircle.isHidden = true
            status.isHidden = true
            let shared = UserDefaults.standard

            var birthDay = self.dob
            let first10 = birthDay.substring(to:birthDay.index(birthDay.startIndex, offsetBy: 10))
            self.birthDate = first10
            
            if gen == "Male" || gen == "male"{
                
                self.segment.selectedSegmentIndex = 0
                
            }
            else if gen == "Female" || gen == "female"{
                self.segment.selectedSegmentIndex = 1
            }
            else{
                self.segment.selectedSegmentIndex = UISegmentedControlNoSegment
            }
            if avatarPath.contains("avatar"){
                let ImagePath = "http://221.121.153.107/public/" + avatarPath
                let url = URL(string: ImagePath)
                DispatchQueue.global(qos: .background).async {
                    let data = try? Data(contentsOf: url!)
                    DispatchQueue.main.async {
                        if data != nil {
                            self.profileImage.image = UIImage(data: data!)
                        }else {
                            self.profileImage.image = UIImage(named: "avatar")
                        }
                    }
                }
            }
            else{
            let ImagePath = avatarPath
            let url = URL(string: ImagePath)
            DispatchQueue.global(qos: .background).async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                    if data != nil {
                        self.profileImage.image = UIImage(data: data!)
                    }else {
                        self.profileImage.image = UIImage(named: "avatar")
                    }
                }
            }
            }
            
            if userType == "Commuter" || userType == "commuter"{
                
                isCommutor = true
                commutor.backgroundColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)
                commutor.setTitleColor(UIColor.white, for: .normal)
                driver.backgroundColor = UIColor.white
                driver.setTitleColor(UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0), for: .normal)
                both.backgroundColor = UIColor.white
                both.setTitleColor(UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0), for: .normal)
            }
            else if userType == "Driver" || userType == "driver"{
                if self.flag == "driver" || self.flag == "Driver"{
                    isCommutor = false
                    driver.backgroundColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)
                    driver.setTitleColor(UIColor.white, for: .normal)
                    commutor.backgroundColor = UIColor.white
                    commutor.setTitleColor(UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0), for: .normal)
                    both.backgroundColor = UIColor.white
                    both.setTitleColor(UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0), for: .normal)
                }
                else if self.flag == "both" || self.flag == "Both"{
                    isCommutor = false
                    both.backgroundColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)
                    both.setTitleColor(UIColor.white, for: .normal)
                    driver.backgroundColor = UIColor.white
                    driver.setTitleColor(UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0), for: .normal)
                    commutor.backgroundColor = UIColor.white
                    commutor.setTitleColor(UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0), for: .normal)
                }
            }
            else if userType == "Both" || userType == "both"{
                isCommutor = false
                both.backgroundColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)
                both.setTitleColor(UIColor.white, for: .normal)
                driver.backgroundColor = UIColor.white
                driver.setTitleColor(UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0), for: .normal)
                commutor.backgroundColor = UIColor.white
                commutor.setTitleColor(UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0), for: .normal)
            }
            firstName.text = fName
            lastName.text = lName
            phoneNumber.text = pNumber
            industry.text = indus
            DOB.text = self.birthDate
            self.img = "Yes"
            self.isRoleSelected = true
        }
        
       
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.registerForKeyboardNotifications()

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func donePicker(sender:UIButton!){
        industry.resignFirstResponder()
        industry.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(_ pickerView: UIPickerView,titleForRow row: Int,forComponent component: Int) -> String?{
        return pickerData[row]  as? String
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        industry.text = pickerData[row] as? String
        
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 265, height: 40));
        label.lineBreakMode = .byWordWrapping;
        label.numberOfLines = 0;
        label.text = pickerData[row]
        label.sizeToFit()
        return label;

    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40.0
    }
    

    
    
    @IBAction func backButtonClicked(_ sender: UIButton) {
        if self.profileType == "ViewProfile"{
            self.dismiss(animated: true, completion: nil)
        }
        else{
            let storyboard = UIStoryboard(name: "LoginSignUp", bundle: nil)
            let home = storyboard.instantiateViewController(withIdentifier: "Home") as! Home
            self.present(home, animated: false, completion: nil)

        }
  
    }
    
    
    func compressImage(_ image: UIImage) -> UIImage {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        var maxHeight: Float = 800.0
        var maxWidth: Float = 800.0
        var imgRatio: Float = actualWidth / actualHeight
        var maxRatio: Float = maxWidth / maxHeight
        var compressionQuality: Float = 0.5
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        var rect = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        var img: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        return img!
    }
    
    
    @IBAction func onChangeGender(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            gender = "male"
        }else {
            gender = "female"
        }
    }
    @IBAction func onClickImage(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: "Choose Option", message: "", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default) { action -> Void in
            print("open camera")
            self.openCamera()
        })
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default) { action -> Void in
            print("Cancel")
            self.openGallery()
        })
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel")
        })
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func openCamera() {
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.cameraDevice = .front
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
            print("camera not available")
        }
    }
    
    func openGallery() {
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.sourceType = .photoLibrary
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
            print("camera not available")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print("captured")
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage

        img = "Yes"
        if profileImage.image == UIImage(named: "avatar") {
            percent = percent + 0.10
            progressCircle.strokeEnd = percent
            let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }
        profileImage.image = chosenImage
        
        dismiss(animated:true, completion: nil)
        
    }
    

    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated:true, completion: nil)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if isRoleSelected {
            percent = 0.25
        }else {
            percent = 0.15
        }
        if !(firstName.text?.isEmpty)! {
            percent = percent + 0.10
            progressCircle.strokeEnd = percent
            let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }else {
            progressCircle.strokeEnd = percent
            let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }
        if !(phoneNumber.text?.isEmpty)! {
            percent = percent + 0.10
            progressCircle.strokeEnd = percent
            let statusPercent = Int(round(percent*cent))
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }else {
            progressCircle.strokeEnd = percent
            let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }
        if !(lastName.text?.isEmpty)! {
            percent = percent + 0.05
            progressCircle.strokeEnd = percent
            let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }else {
            progressCircle.strokeEnd = percent
            let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }
        if !(industry.text?.isEmpty)! {
            percent = percent + 0.10
            progressCircle.strokeEnd = percent
            let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }else {
            progressCircle.strokeEnd = percent
            let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if isRoleSelected {
            percent = 0.25
        }else {
            percent = 0.15
        }
        if !(firstName.text?.isEmpty)! {
            percent = percent + 0.10
            progressCircle.strokeEnd = percent
            let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }
        if !(phoneNumber.text?.isEmpty)! {
            percent = percent + 0.10
            progressCircle.strokeEnd = percent
            let statusPercent = Int(round(percent*cent))
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }
        if !(lastName.text?.isEmpty)! {
            percent = percent + 0.05
            progressCircle.strokeEnd = percent
            let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }
        if !(industry.text?.isEmpty)! {
            percent = percent + 0.10
            progressCircle.strokeEnd = percent
            let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }
        return true
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWasShown(_ aNotification: Notification) {
        let keyboardSize = (aNotification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        let contentInsets: UIEdgeInsets? = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize?.height)!, 0.0)
        self.scrollView.contentInset = contentInsets!
        self.scrollView.scrollIndicatorInsets = contentInsets!
        var aRect: CGRect = self.view.frame
        aRect.size.height -= (keyboardSize?.height)!
        if !aRect.contains((self.activeField!.frame.origin)) {
            let scrollPoint = CGPoint(x: CGFloat(0.0), y: CGFloat((self.activeField?.frame.origin.y)! - (keyboardSize?.height)!))
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    func keyboardWillBeHidden(_ aNotification: Notification) {
        let contentInsets: UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }

    @IBAction func onClickBoth(_ sender: UIButton) {
        isCommutor = false
        self.role_selected = "driver"
        self.flag = "both"
        both.backgroundColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)
        both.setTitleColor(UIColor.white, for: .normal)
        driver.backgroundColor = UIColor.white
        driver.setTitleColor(UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0), for: .normal)
        commutor.backgroundColor = UIColor.white
        commutor.setTitleColor(UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0), for: .normal)
        if !isRoleSelected {
            isRoleSelected = true
            percent = percent + 0.10
            progressCircle.strokeEnd = percent
            let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }
    }
    @IBAction func onClickCommutor(_ sender: UIButton) {
        isCommutor = true
        self.role_selected = "commuter"
        self.flag = "commuter"
        commutor.backgroundColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)
        commutor.setTitleColor(UIColor.white, for: .normal)
        driver.backgroundColor = UIColor.white
        driver.setTitleColor(UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0), for: .normal)
        both.backgroundColor = UIColor.white
        both.setTitleColor(UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0), for: .normal)
        if !isRoleSelected {
            isRoleSelected = true
            percent = percent + 0.10
            progressCircle.strokeEnd = percent
            let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }
    }
    @IBAction func onClickDriver(_ sender: UIButton) {
        isCommutor = false
        self.role_selected = "driver"
        self.flag = "driver"
        driver.backgroundColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)
        driver.setTitleColor(UIColor.white, for: .normal)
        commutor.backgroundColor = UIColor.white
        commutor.setTitleColor(UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0), for: .normal)
        both.backgroundColor = UIColor.white
        both.setTitleColor(UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0), for: .normal)
        if !isRoleSelected {
            isRoleSelected = true
            percent = percent + 0.10
            progressCircle.strokeEnd = percent
            let statusPercent = Int(percent*cent)
            print(statusPercent)
            status.text = "\(statusPercent)" + "% " + "completed"
        }
    }
    
    func validation() -> Bool {
        if img == "No" {
            let alert = UIAlertView(title: "", message: "Please upload profile picture.", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return false
        }else if ((firstName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))?.isEmpty)! {
            let alert = UIAlertView(title: "", message: "Please enter Firstname", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return false
        }
        else if ((lastName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))?.isEmpty)! {
            let alert = UIAlertView(title: "", message: "Please enter Lastname", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return false
        }
        //else if ((phoneNumber.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))?.isEmpty)! {
         //   let alert = UIAlertView(title: "", message: "Please enter phone number", delegate: self, cancelButtonTitle: "Ok")
         //   alert.show()
        //    return false
       // }
        //else if ((phoneNumber.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))?.characters.count)! != 10 {
           // let alert = UIAlertView(title: "", message: "Please enter valid phone number", delegate: self, cancelButtonTitle: "Ok")
          //  alert.show()
          //  return false
        //}
    else if ((industry.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))?.isEmpty)! {
            let alert = UIAlertView(title: "", message: "Please enter industry", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return false
        }
        //if self.DOB.text?.characters.count == 0{
         //   let alert = UIAlertView(title: "", message: "Please enter your D.O.B .", delegate: self, cancelButtonTitle: "Ok")
        //    alert.show()
        //    return false
       // }
        if !isRoleSelected {
            let alert = UIAlertView(title: "", message: "Please select user role", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return false
        }
        let date = Date()

        
        
        if entered > date || entered == date{
            var alert = UIAlertView(title: "", message: "You can't put future date as your D.O.B", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return false
        }

        return true
    }
    
    @IBAction func onClickDone(_ sender: UIButton) {
        if validation() {
        if profileType == "SignUp"{
        
            let storyboard = UIStoryboard(name: "Interest", bundle: nil)
            let interestVC = storyboard.instantiateViewController(withIdentifier: "Interest") as! Interest
            let role:String
            let shared = SharedPreferences()
            if isCommutor {
                interestVC.isCommutor = "Yes"
                role = "commuter"
                shared.saveUserType("Commuter")
            }else {
                interestVC.isCommutor = "No"
                role = "driver"
                shared.saveUserType("Driver")
            }
           
            var info = PersonInfo()
            info.firstName = firstName.text!
            info.lastName = lastName.text!
            if (phoneNumber.text?.isEmpty)!{
                info.contact = "NA"
            }
            else{
                info.contact = phoneNumber.text!
            }
            
            info.course = industry.text!
            info.role = self.role_selected
            info.flag = self.flag
            info.avatar = profileImage.image
            if let gen = gender{
                info.gender = gender!
            }
            else{
                info.gender = "NA"
            }
            if let dob = birthDate{
                info.dob = birthDate!
            }
            else{
                info.dob = "NA"
            }
            
            interestVC.info = info
            interestVC.id = id!
            self.present(interestVC, animated: true, completion: nil)
        }
        else if profileType == "FbSignUp"{
            let storyboard = UIStoryboard(name: "Interest", bundle: nil)
            let interestVC = storyboard.instantiateViewController(withIdentifier: "Interest") as! Interest
            let role:String
            let shared = SharedPreferences()
            if isCommutor {
                interestVC.isCommutor = "Yes"
                role = "commuter"
                shared.saveUserType("Commuter")
            }else {
                interestVC.isCommutor = "No"
                role = "driver"
                shared.saveUserType("Driver")
            }
            var info = PersonInfo()
            info.firstName = firstName.text!
            info.lastName = lastName.text!
            if (phoneNumber.text?.isEmpty)!{
                info.contact = "NA"
            }
            else{
                info.contact = phoneNumber.text!
            }
            info.course = industry.text!
            info.role = self.role_selected
            info.flag = self.flag
            info.avatar = profileImage.image
            if let gen = gender{
                info.gender = gender!
            }
            else{
                info.gender = "NA"
            }
            if let dob = birthDate{
                info.dob = birthDate!
            }
            else{
                info.dob = "NA"
            }
            interestVC.info = info
            interestVC.id = id!
            self.present(interestVC, animated: true, completion: nil)
        }
        
        else{
            
            let role1:String
            let shared = SharedPreferences()
            if self.role_selected == "Commuter" || self.role_selected == "commuter" {
                
                role1 = "commuter"
                shared.saveUserType("Commuter")
                info.firstName = firstName.text!
                info.lastName = lastName.text!
                if (phoneNumber.text?.isEmpty)!{
                    info.contact = "NA"
                }
                else{
                    info.contact = phoneNumber.text!
                }
                info.course = industry.text!
                info.role = self.role_selected
                info.flag = self.flag
                info.avatar = profileImage.image
                if let gen = gender{
                    info.gender = gender!
                }
                else{
                    info.gender = "NA"
                }
                if let dob = birthDate{
                    info.dob = birthDate!
                }
                else{
                    info.dob = "NA"
                }
                DVUtility.disaplyWaitMessage()
                self.startNetworkRequest(urlString: "http://221.121.153.107/user/profile/\(id!)")
            }else {
                
                role1 = "driver"
                shared.saveUserType("Driver")
                info.firstName = firstName.text!
                info.lastName = lastName.text!
                if (phoneNumber.text?.isEmpty)!{
                    info.contact = "NA"
                }
                else{
                    info.contact = phoneNumber.text!
                }
                info.course = industry.text!
                info.role = self.role_selected
                info.flag = self.flag
                info.avatar = profileImage.image
                if let gen = gender{
                    info.gender = gender!
                }
                else{
                    info.gender = "NA"
                }
                if let dob = birthDate{
                    info.dob = birthDate!
                }
                else{
                    info.dob = "NA"
                }
                let storyboard = UIStoryboard(name: "Documents", bundle: nil)
                let detailsVC = storyboard.instantiateViewController(withIdentifier: "Documents") as! Documents
                detailsVC.CarColour = self.CarColour
                detailsVC.Model = self.Model
                detailsVC.Plate = self.Plate
                detailsVC.DLImage = self.DLImage
                detailsVC.INSURANCEImage = self.INSURANCEImage
                detailsVC.REGOImage = self.REGOImage
                detailsVC.id = self.id
                detailsVC.profileType = "ViewProfile"
                detailsVC.avatar = self.avatar
                detailsVC.firstName = firstName.text!
                detailsVC.lastName = lastName.text!
                if (phoneNumber.text?.isEmpty)!{
                    detailsVC.contact = "NA"
                }
                else{
                    detailsVC.contact = phoneNumber.text!
                }
                
                detailsVC.industry = industry.text!
                detailsVC.role = role1
                if let gen = gender{
                    detailsVC.gender = gender!
                }
                else{
                    detailsVC.gender = "NA"
                }
                if let dob = birthDate{
                    detailsVC.birthDay = birthDate!
                }
                else{
                    detailsVC.birthDay = "NA"
                }
                
                detailsVC.checkreg = self.checkreg
                detailsVC.checkinsu = self.checkinsu
                detailsVC.checkidentity = self.checkidentity
                detailsVC.info = self.info
                self.present(detailsVC, animated: true, completion: nil)



            }
            
           
            
        }
        }
    }
    
    func setDoneOnKeyboard() {
        let doneToolBar = UIToolbar()
        doneToolBar.sizeToFit()
        let btnDone = UIBarButtonItem(title: "Done", style: .bordered, target: self, action: #selector(self.doneBtnClicked))
        doneToolBar.items = [btnDone]
        doneToolBar.tintColor = UIColor(red: 27/255, green: 168/255, blue: 159/255, alpha: 1)
        DOB.inputAccessoryView = doneToolBar
    }
    
    func doneBtnClicked() {
        DOB.resignFirstResponder()
        
    }
    
    func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        DOB.text = dateFormatter.string(from: sender.date)
        entered = sender.date
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        birthDate = dateFormatter.string(from: sender.date)
        
    }
    func startNetworkRequest(urlString: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let request = NSMutableURLRequest()
        //Set Params
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 60
        request.httpMethod = "POST"
        //Create boundary, it can be anything
        let boundary = "DVRidesBoundary"
        // set Content-Type in HTTP header
        let contentType = "multipart/form-data; boundary=\(boundary)"
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        // post body
        let body = NSMutableData()
        //Populate a dictionary with all the regular values you would like to send.
        
        //        firstname, lastname, contact, address, study, avatar, interests, (role, identity, registration, insurance)
        
        let param = ["firstname": self.info.firstName,
                     "lastname": self.info.lastName,
                     "contact": self.info.contact,
                     "study": self.info.course,
                     "dob": self.info.dob,
                     "gender": self.info.gender,
                     "role": self.info.role,
                     "role_flag" : self.info.flag] as [String : Any]
        
        print(param)
        
        for (key, value) in param {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        let FileParamConstant = "avatar"
        let imageData = UIImageJPEGRepresentation((self.info.avatar)!, 0.5)
        if (imageData != nil) {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(FileParamConstant)\"; filename=\"image.png\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type:image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(imageData!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
        }
        
        
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        request.httpBody = body as Data
        let postLength = "\(UInt(body.length))"
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        // set URL
        request.url = NSURL(string: urlString)! as URL
        //        GuideZeeUtallity.HideWaitMessage()
        let task = session.dataTask(with: request as URLRequest) {
            ( data, response, error) in
            
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                DispatchQueue.main.async { [unowned self] in
                    self.view.alpha = 0.7
                    self.pro = ProgressHud(text: "Please Wait...")
                    self.view.addSubview(self.pro!)
                    UIApplication.shared.beginIgnoringInteractionEvents()
                    let alert = UIAlertView(title: "Error!", message: "Something went wrong. Please try later.", delegate: self, cancelButtonTitle: "Ok")
                    alert.show()
                }
                return
            }
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            let dict = self.convertStringToDictionary(text: dataString as! String)
            print(dataString ?? NSDictionary())
            print(dict?["status_code"] ?? String())
            DispatchQueue.main.async { [unowned self] in
                self.view.alpha = 1

                UIApplication.shared.endIgnoringInteractionEvents()
                let shared = SharedPreferences()
                if self.info.role == "driver" {
                    
                    shared.saveUserType("Driver")
                }
                else if self.info.role == "commuter"{
                    
                    shared.saveUserType("Commuter")
                }
                else {
                    shared.saveUserType("Both")
                }
               
                if self.info.gender == "Male"{
                    shared.saveGender("Male")
                }
                else if self.info.gender == "Female"{
                    shared.saveGender("Female")
                }
                else{
                    shared.saveGender("NA")
                }
                shared.saveFirstName((self.info.firstName))
                shared.saveLastName((self.info.lastName))
                shared.saveUserId(self.id!)
                
                var str = String(describing: self.info.avatar)
                shared.saveDOB((self.info.dob))
                shared.saveDetails(contact: (self.info.contact), industry: (self.info.course) , avatar: str, role: (self.info.role))
                self.registerDeviceToken()
            }
            
        }
        
        task.resume()
    }
    func registerDeviceToken() {
      
        let shared = SharedPreferences()
   
        let token = shared.getToken()
        let jsonDict: NSDictionary
        jsonDict = ["token": token,
                    "device_type": "ios"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self as! NetworkProtocol
        networkCommunication.startNetworkRequest("http://221.121.153.107/user/subscribe/\(id!)", jsonData: jsonDict as! [String : Any], method: "POST", apiType: "RegisterGCM")
        
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
        
    }
    
    func failure() {
        self.view.alpha = 1
        pro!.removeFromSuperview()
        UIApplication.shared.endIgnoringInteractionEvents()
        let alert = UIAlertView(title: "Oops!!!", message: "There was a technical difficulty while fulfilling your request. Please try again.", delegate: self, cancelButtonTitle: "Ok")
        alert.show()
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        DVUtility.hideWaitMessage()
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            if apiType == "RegisterGCM" {
                DVUtility.hideWaitMessage()

                    self.dismiss(animated: true, completion: nil)
                    self.parent?.dismiss(animated: true, completion: nil)
            }
        case 600:
            DVUtility.displayAlertBoxTitle("", withMessage: "Some error occured. You might not get Notifications of your trips. Please Login again.")
            gotoHome()
        case 600:
            DVUtility.displayAlertBoxTitle("", withMessage: "User not found")
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }
    func gotoHome() {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        let story = UIStoryboard(name: "PickDrop", bundle: nil)
    
        let navVC:UINavigationController = story.instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
        navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
        let container: MFSideMenuContainerViewController = story.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
        container.leftMenuViewController = leftVC
        container.centerViewController = navVC
        container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
        self.present(container, animated: true, completion: nil)
    }

    

}
