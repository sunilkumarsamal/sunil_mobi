//
//  ResultCell.swift
//  DV
//
//  Created by Chinmay on 02/03/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class ResultCell: UITableViewCell {

    @IBOutlet weak var placeTitle: UILabel!
    @IBOutlet weak var placeSubTitle: UILabel!
    @IBOutlet weak var placeImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
