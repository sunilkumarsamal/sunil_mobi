//
//  DiviLocationManager.swift
//  DV
//
//  Created by uytr on 02/01/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit
import CoreLocation

protocol DiviLocationProtocol: NSObjectProtocol {
    
    func updateToLocation(location: CLLocation)
    
}

class DiviLocationManager: CLLocationManager, CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()
    weak var locationDelegate: DiviLocationProtocol?
    
    static let sharedInstance = DiviLocationManager()
    
    @nonobjc func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [Any]) {
        //    [self.locationManager stopUpdatingLocation];
//        locationManager.delegate = self
        print("updated")
        self.locationDelegate?.updateToLocation(location: locations.last as! CLLocation)
        //crashed app
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("jkjkjkj")
    }
    
    func startUpdateLocation() {
        locationManager.delegate = self
        locationManager.distanceFilter = 100.0
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
//        locationManager.startUpdatingLocation()
    }
    
    func stopUpdateLocation() {
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.stopUpdatingHeading()
        locationManager.stopUpdatingLocation()
    }
}
