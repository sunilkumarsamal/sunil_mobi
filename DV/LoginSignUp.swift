//
//  LoginSignUp.swift
//  DV
//
//  Created by Chinmay on 16/01/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class LoginSignUp: UIViewController, FBSDKLoginButtonDelegate, NetworkProtocol, UITextFieldDelegate {

    @IBOutlet weak var back_of_forget_view: UIView!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var underLabel: UILabel!
    @IBOutlet weak var fbButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var forget_email_textfield: UITextField!
    
    @IBOutlet weak var confirmPassView: UIView!
    @IBOutlet weak var forget_button: UIButton!
    @IBOutlet weak var forget_password_view: UIView!
    @IBOutlet weak var emailID: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    var res = NSDictionary()
    var loginType = ""
    var firstname = String()
    var lastname = String()
    var jsonDict1: [String: AnyObject]?
    var profileType = String()
    var gender = String()
    var url = String()
    var email = String()
    @IBOutlet weak var fbLoginButton: FBSDKLoginButton!
    
    @IBOutlet weak var fullView: UIView!
    @IBOutlet weak var confirmPasswordHeight: NSLayoutConstraint!
    

    
    var pro:UIVisualEffectView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.forget_password_view.isHidden = true
        self.back_of_forget_view.isHidden = true
        loginLabel.text = loginType
       
        if loginType == "Sign up" {
            confirmPasswordHeight.constant = fullView.bounds.size.height/8
            fbButton.setTitle("Sign up via Facebook", for: .normal)
            self.forget_button.isHidden = true
//            googleButton.setTitle("Signup via Google+", for: .normal)
        }else {
            self.confirmPassView.isHidden = true
            fbButton.setTitle("Log in via Facebook", for: .normal)
            self.forget_button.isHidden = false
//            googleButton.setTitle("Login via Google+", for: .normal)
        }
        
        loginButton.setTitle(loginType, for: UIControlState.normal)
        loginButton.layer.shadowColor = UIColor.init(colorLiteralRed: 68/255, green: 168/255, blue: 157/255, alpha: 1.0).cgColor
        underLabel.layer.shadowColor = UIColor.init(colorLiteralRed: 68/255, green: 168/255, blue: 157/255, alpha: 1.0).cgColor
        fbButton.layer.shadowColor = UIColor.init(colorLiteralRed: 20/255, green: 58/255, blue: 114/255, alpha: 1.0).cgColor
//        googleButton.layer.shadowColor = UIColor.init(colorLiteralRed: 211/255, green: 72/255, blue: 54/255, alpha: 1.0).cgColor
        
//        GIDSignIn.sharedInstance().uiDelegate = self
//        GIDSignIn.sharedInstance().delegate = self
        
        emailID.delegate = self
        password.delegate = self
        self.forget_email_textfield.delegate = self
        //        self.configureFacebook()
    }
       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func configureFacebook()
    {
        fbLoginButton.readPermissions = ["public_profile", "email", "user_friends", "all_mutual_friends"];
        fbLoginButton.delegate = self
        var newFrame: CGRect = fbLoginButton.frame
        newFrame.size = CGSize(width: fbLoginButton.bounds.size.width, height: CGFloat(44))
        fbLoginButton.frame = newFrame
        fbLoginButton.transform = CGAffineTransform(scaleX: 1, y: 1.5)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    @IBAction func onClickClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickLoginOrSignup(_ sender: UIButton) {
      
        
        if !passwordValidation() {
            return
        }
        if !(self.emailValidation(emailID.text!)){
            let alert = UIAlertView(title: "", message: "Please provide the correct email address.", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return
        }
//        DVUtility.disaplyWaitMessage()
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let jsonDict: [String: Any]
        if loginType == "Log in" {
            self.loginApi()
        }else {
            self.signupApi()
        }
        
        
    }
    func loginApi()
    {
        let reachability = Reachability1()
        
        if reachability!.isReachable || reachability!.isReachableViaWiFi || reachability!.isReachableViaWWAN
        {
            DVUtility.disaplyWaitMessage()
            let networkCommunication = NetworkCommunication()
            networkCommunication.networkDelegate = self
            let jsonDict: [String: Any]
            if loginType == "Log in" {
            jsonDict = ["email": emailID.text! as Any, "password": password.text! as Any]
            print("dict is \(jsonDict)")
            networkCommunication.startNetworkRequest("http://221.121.153.107/user/login", jsonData: jsonDict, method: "POST", apiType: "Login")
        }
        }
        else
        {
            
            let alert = UIAlertView(title: "Unreachable", message: "Please make sure internet is connected and retry", delegate: self, cancelButtonTitle: "retry")
            alert.tag = 100
            alert.show()
            
            print("not connected")
            
        }
    
    }
    func signupApi()
    {
        let reachability = Reachability1()
        
        if reachability!.isReachable || reachability!.isReachableViaWiFi || reachability!.isReachableViaWWAN
        {
            
            DVUtility.disaplyWaitMessage()
            let networkCommunication = NetworkCommunication()
            networkCommunication.networkDelegate = self
            let jsonDict: [String: Any]
            
            if self.emailValidation(emailID.text!) {
                jsonDict = ["email": (emailID.text as AnyObject) as! String,
                            "password": (password.text as AnyObject) as! String,
                            "via": ("Form" as AnyObject) as! String];
                networkCommunication.startNetworkRequest("http://221.121.153.107/user/signup", jsonData: jsonDict, method: "POST", apiType: "Signup")
            }else {
                let alert = UIAlertView(title: "", message: "Please enter a valid email id.", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
            }
        }
    else
    {
    
        let alert = UIAlertView(title: "Unreachable", message: "Please make sure internet is connected and retry", delegate: self, cancelButtonTitle: "retry")
        alert.tag = 101
        alert.show()
    
        print("not connected")
    
    }
    
}
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if alertView.tag == 100{
        self.loginApi()
        return
        }
        if alertView.tag == 101{
            self.signupApi()
            return
        }
        if alertView.tag == 102{
            self.socialSignUp(jsonDict: self.jsonDict1!)
            return
        }
        if alertView.tag == 103{
            self.registerDeviceToken()
            return
        }
    }
    
    func socialSignUp(jsonDict: [String: AnyObject]) {
        
        
        let reachability = Reachability1()
        
        if reachability!.isReachable || reachability!.isReachableViaWiFi || reachability!.isReachableViaWWAN
        {
            DVUtility.disaplyWaitMessage()
            let networkCommunication = NetworkCommunication()
            networkCommunication.networkDelegate = self
            networkCommunication.startNetworkRequest("http://221.121.153.107/user/signup", jsonData: jsonDict, method: "POST", apiType: "SignupSocial")
        }
        else{
            let alert = UIAlertView(title: "Unreachable", message: "Please make sure internet is connected and retry", delegate: self, cancelButtonTitle: "retry")
            alert.tag = 102
            alert.show()
        }
        
    }
    
    func registerDeviceToken() {
      
        
        let reachability = Reachability1()
        
        if reachability!.isReachable || reachability!.isReachableViaWiFi || reachability!.isReachableViaWWAN
        {
            let shared = SharedPreferences()
            let id = shared.getUserId()
            let token = shared.getToken()
            let jsonDict: NSDictionary
            jsonDict = ["token": token,
                        "device_type": "ios"]
            let networkCommunication = NetworkCommunication()
            networkCommunication.networkDelegate = self
            networkCommunication.startNetworkRequest("http://221.121.153.107/user/subscribe/\(id)", jsonData: jsonDict as! [String : Any], method: "POST", apiType: "RegisterGCM")
        }
        else{
            let alert = UIAlertView(title: "Unreachable", message: "Please make sure internet is connected and retry", delegate: self, cancelButtonTitle: "retry")
            alert.tag = 103
            alert.show()
        }
        
    }
    
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {

        DVUtility.hideWaitMessage()
        UIApplication.shared.endIgnoringInteractionEvents()
        let alert = UIAlertView(title: "Oops!!!", message: "There was a technical difficulty while fulfilling your request. Please try again.", delegate: self, cancelButtonTitle: "Ok")
        alert.show()
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        
        DVUtility.hideWaitMessage()
        let code = responseData["status_code"] as! Int
       
        switch code {
        case 100:
            if apiType == "Login" {
                let data = responseData["data"] as! NSDictionary
                if  data.object(forKey: "firstname") is NSNull {
                    
                    let shared = SharedPreferences()
                    shared.saveUserStatus(true)
                    let storyboard = UIStoryboard(name: "Details", bundle: nil)
                    let details = storyboard.instantiateViewController(withIdentifier: "Details") as! Details
                    details.id = (responseData["data"] as! NSDictionary).object(forKey: "user_id") as? String
                    self.present(details, animated: true, completion: nil)
                }else {
                    var fName = data.object(forKey: "firstname") as? String
                    var lName = data.object(forKey: "lastname") as? String
                    var role = data.object(forKey: "role") as? String
                    let shared = SharedPreferences()
                    shared.saveUserStatus(true)
                    shared.saveUserId((responseData["data"] as! NSDictionary).object(forKey: "user_id") as! String)
                    if data["driver_id"] is NSNull {
                        shared.saveUserType("Commutor")
                    }else {
                        shared.saveUserType("Driver")
                    }
                    shared.saveUserType(role!)
                    shared.saveFirstName(fName!)
                    shared.saveLastName(lName!)
                    self.registerDeviceToken()
                    
                }
                
                
            }
            else if apiType == "SignupSocial" {

                let shared = SharedPreferences()

                let storyboard = UIStoryboard(name: "Details", bundle: nil)
                let details = storyboard.instantiateViewController(withIdentifier: "Details") as! Details
                details.id = ((responseData["user_id"])?.description)!

                shared.saveFirstName(self.firstname)
                shared.saveLastName(self.lastname)
               

                details.firstname = self.firstname
                details.lastname = self.lastname
                details.imageurl = url as! String
                details.gen = gender as! String
                details.profileType = "FbSignUp"
                shared.saveProfileType("FBSignUp")
                self.present(details, animated: true, completion: nil)
            }
            else if apiType == "Signup" {
                
                let shared = SharedPreferences()
                
                let storyboard = UIStoryboard(name: "Details", bundle: nil)
                let details = storyboard.instantiateViewController(withIdentifier: "Details") as! Details
                details.id = ((responseData["user_id"])?.description)!
                shared.saveUserId(((responseData["user_id"])?.description)!)
                details.profileType = "SignUp"
                shared.saveProfileType("SignUp")
                self.present(details, animated: true, completion: nil)
            }else if apiType == "RegisterGCM" {

                
                
                let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
                let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
                let story = UIStoryboard(name: "PickDrop", bundle: nil)
                let PickDrop = story.instantiateViewController(withIdentifier: "PickUpDropOff") as! PickUpDropOff
                let navVC:UINavigationController = story.instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
                navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
                
                let container: MFSideMenuContainerViewController = story.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
                container.leftMenuViewController = leftVC
                container.centerViewController = navVC
                container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
                self.present(container, animated: true, completion: nil)
            }

        case 101:

            let shared = SharedPreferences()
            let data = responseData["data"] as! NSDictionary
                if  data["firstname"] is NSNull{
                
                let shared = SharedPreferences()
                
                let storyboard = UIStoryboard(name: "Details", bundle: nil)
                let details = storyboard.instantiateViewController(withIdentifier: "Details") as! Details
                details.id = data["user_id"] as! String
                
                shared.saveFirstName(self.firstname)
                shared.saveLastName(self.lastname)
                
               
                details.firstname = self.firstname
                details.lastname = self.lastname
                details.imageurl = url as! String
                details.gen = gender as! String
                details.profileType = "FbSignUp"
                shared.saveProfileType("FbSignUp")
                self.present(details, animated: true, completion: nil)

            }
            else{

                let shared = SharedPreferences()
                shared.saveUserId((responseData["data"] as! NSDictionary).object(forKey: "user_id") as! String)
                
                if data["driver_id"] is NSNull {
                    shared.saveUserType("Commutor")
                }else {
                    shared.saveUserType("Driver")
                }
                
                self.registerDeviceToken()
            }
        case 600:
            DVUtility.displayAlertBoxTitle("", withMessage: "Some error occured. Please try again.")
        case 601:
            DVUtility.displayAlertBoxTitle("", withMessage: "User is already existing. Please try with a different email id.")
        case 602:
            var message  = responseData["message"] as! String
            DVUtility.displayAlertBoxTitle("", withMessage: "\(message)")
            
        case 603:
            
            DVUtility.displayAlertBoxTitle("", withMessage: "Some error occured. Please try again.")
            let shared = SharedPreferences()
            shared.clearPreferences()
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!)
    {
        FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields":"first_name, last_name, id, email, picture.type(large)"]).start { (connection, result, error) -> Void in
            
            print(result as Any)
            let res = result as! NSDictionary
            print(res.object(forKey: "email")!)
            let jsonDict: [String: AnyObject] = ["email": res.object(forKey: "email")! as AnyObject,
                                                 "social_id": res.object(forKey: "id")! as AnyObject,
                                                 "via": "Facebook" as AnyObject];
            self.socialSignUp(jsonDict: jsonDict)
                   }
    }
    
    
    
    
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func onClickFBLogin(_ sender: UIButton) {
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email","user_friends"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    print(fbloginresult.grantedPermissions)
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()

                    }
                }else {

                    DVUtility.hideWaitMessage()
                }
            }
        }
    }
    
    func getFBUserData(){
        DVUtility.disaplyWaitMessage()
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large),gender,email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
//                    self.dict = result as! [String : AnyObject]
                    DVUtility.hideWaitMessage()
                    print(result!)
                    var res = result as! NSDictionary
                    let picture = res.object(forKey: "picture") as AnyObject
                    let data = picture.object(forKey: "data") as AnyObject
                    self.url = (data.object(forKey: "url") as AnyObject) as! String
                    self.firstname = (res.object(forKey: "first_name")as AnyObject) as! String
                    self.lastname = (res.object(forKey: "last_name") as AnyObject) as! String
                    self.gender = (res.object(forKey: "gender") as AnyObject) as! String
                    let email = res.object(forKey: "email")! as AnyObject
                    let socialid = res.object(forKey: "id")! as AnyObject


                    self.jsonDict1 = ["email": email,
                                                         "social_id": socialid,
                                                         "via": "Facebook" as AnyObject];
                    var shared = SharedPreferences()
                    shared.saveFirstName(self.firstname)
                    shared.saveLastName(self.lastname)
                    shared.saveUserImage(self.url as! String)
                    self.socialSignUp(jsonDict: self.jsonDict1!)
                }
            })
        }
        

    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton) {
        print("User Logged Out")
    }
    

    
    // forget password code
    
    @IBAction func forget_password(_ sender: UIButton) {
        self.forget_password_view.isHidden = false
        self.back_of_forget_view.isHidden = false
        self.forget_password_view.layer.shadowColor = UIColor.black.cgColor
        self.forget_password_view.layer.shadowOpacity = 1
        self.forget_password_view.layer.shadowOffset = CGSize.zero
        self.forget_password_view.layer.shadowRadius = 10
        self.forget_password_view.layer.shadowPath = UIBezierPath(rect: self.forget_password_view.bounds).cgPath
        self.back_of_forget_view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
    }
    
    @IBAction func forget_done_button(_ sender: UIButton) {
//        if !(self.validateEmail(enteredEmail: emailID.text!)){
//            let alert = UIAlertView(title: "", message: "Please provide the correct email address.", delegate: self, cancelButtonTitle: "Ok")
//            alert.show()
//            return
//        }
//        else{
        self.dismissKeyboard()
        DVUtility.disaplyWaitMessage()
        self.email = self.forget_email_textfield.text!

            self.startNetworkRequest(urlString: "http://221.121.153.107/user/forgotpassword")
//        }
        }
    
    @IBAction func outside_clicked(_ sender: UIButton) {
        self.back_of_forget_view.isHidden = true
        self.forget_password_view.isHidden = true
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func emailValidation(_ emailID: String) -> Bool {
        if (emailID.characters.count ) <= 0 {
            return false
        }
        else if !self.validateEmail(enteredEmail: emailID) {
            return false
        }
        return true
    }
    
    func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    func passwordValidation() -> Bool {
        if (password.text?.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty)! {
            let alert = UIAlertView(title: "", message: "Please enter password.", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return false
        }else if loginType == "Sign up" {
            if (confirmPassword.text?.trimmingCharacters(in: CharacterSet.whitespaces).isEmpty)! {
                let alert = UIAlertView(title: "", message: "Please enter confirm password.", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
                return false
            }
            else if password.text != confirmPassword.text {
                let alert = UIAlertView(title: "", message: "passwords don't match.", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
                return false
            }
        }
        return true
    }
    // forget password codes api call
   
    
    func startNetworkRequest(urlString: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let request = NSMutableURLRequest()
        //Set Params
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 60
        request.httpMethod = "POST"
        //Create boundary, it can be anything
        let boundary = "DVRidesBoundary"
        // set Content-Type in HTTP header
        let contentType = "multipart/form-data; boundary=\(boundary)"
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        // post body
        let body = NSMutableData()
        //Populate a dictionary with all the regular values you would like to send.
        
        //        firstname, lastname, contact, address, study, avatar, interests, (role, identity, registration, insurance)
        
        let param = ["email": "\(email)"] as [String : Any]
        
        print(param)
        
        for (key, value) in param {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        request.httpBody = body as Data
        let postLength = "\(UInt(body.length))"
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        // set URL
        request.url = NSURL(string: urlString)! as URL

        let task = session.dataTask(with: request as URLRequest) {
            ( data, response, error) in
            
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                DispatchQueue.main.async { [unowned self] in
                    DVUtility.disaplyWaitMessage()
                    UIApplication.shared.beginIgnoringInteractionEvents()
                    let alert = UIAlertView(title: "Error!", message: "Something went wrong. Please try later.", delegate: self, cancelButtonTitle: "Ok")
                    alert.show()
                }
                return
            }
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            let dict = self.convertStringToDictionary(text: dataString as! String)
            print(dataString ?? NSDictionary())
            print(dict?["status_code"] ?? String())
            let code = dict?["status_code"] as! Int
            switch code {
            case 100:
                DVUtility.hideWaitMessage()
                let alert = UIAlertController(title: "", message:"Successfully email sent to your registered email address.", preferredStyle: UIAlertControllerStyle.alert)
                var data = String()
                let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel,handler: { (action) -> Void in
                    self.back_of_forget_view.isHidden = true
                    self.forget_password_view.isHidden = true
                })
                alert.addAction(okButton)
                self.present(alert, animated: true, completion: nil)
            case 600:
                DVUtility.hideWaitMessage()
                let alert = UIAlertView(title: "", message: "Please enter the correct email address.", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
            case 603:
                DVUtility.hideWaitMessage()
                let alert = UIAlertView(title: "", message: "User not found.", delegate: self, cancelButtonTitle: "Ok")
                alert.show()

            default:
                DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
            }
            
            
        }
        task.resume()
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    
        
    
    
    

}
