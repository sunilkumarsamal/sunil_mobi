//
//  ResultTableViewController.swift
//  DV
//
//  Created by Chinmay on 01/03/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit
import GooglePlaces

protocol HandleMapSearch: class {
    func sendPlaceDetails(placeId: String)
    func gotoCurrentLocation()
}

class ResultTableViewController: UITableViewController, UIGestureRecognizerDelegate, NetworkProtocol {
    
    var primaryPlaces = NSMutableArray()
    var secondaryPlaces = NSMutableArray()
    var placeIds = NSMutableArray()
    
    weak var placeDelegate: HandleMapSearch?
    var screenName: String?
    
    var typedString: String?
    
//    var tap: UITapGestureRecognizer?

    override func viewDidLoad() {
        super.viewDidLoad()


        self.tableView.tableFooterView = UIView(frame: .zero)
        

        self.tableView.register(UINib(nibName: "ResultCell", bundle: nil), forCellReuseIdentifier: "ResultCell")

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tapped(_ sender: UITapGestureRecognizer) {
        print("dismiss!")
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickCurrentLocation(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        self.placeDelegate?.gotoCurrentLocation()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        if primaryPlaces.count == 0 {
            return 0
        }else {

            return 1
        }
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if primaryPlaces.count == 0 {
            return 0
        }else {
            return primaryPlaces.count
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        dismiss(animated: true, completion: nil)
        self.placeDelegate?.sendPlaceDetails(placeId: placeIds.object(at: indexPath.row) as! String)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResultCell") as! ResultCell
        
        cell.placeTitle!.text = (primaryPlaces.object(at: indexPath.row) as AnyObject).string
        let s: String = (primaryPlaces.object(at: indexPath.row) as AnyObject).string
        let d: String = (secondaryPlaces.object(at: indexPath.row) as AnyObject).string
        cell.placeTitle?.text = s
        print(d)
        cell.placeSubTitle?.text = d
        
        
        if screenName == "green" {
            cell.placeImage.image = UIImage(named: "pin")
        }else {
            cell.placeImage.image = UIImage(named: "pinblue")
        }
        
        return cell

    }
 

  
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if self.tableView.numberOfSections == 0 {
            dismiss(animated: true, completion: nil)
        }
    }
    
    func placeAutocomplete(enteredString: String) {
        let filter = GMSAutocompleteFilter()
        filter.type = .noFilter

        filter.country = "au"

        let placeClient = GMSPlacesClient()
        placeClient.autocompleteQuery(enteredString, bounds: nil, filter: filter, callback: {(results, error) -> Void in
            self.primaryPlaces.removeAllObjects()
            self.secondaryPlaces.removeAllObjects()
            self.placeIds.removeAllObjects()
            if let error = error {
                print("Autocomplete error \(error)")
                return
            }
            if let results = results {
                print(results)
                for result in results {
                   
                    var resultsStr = NSMutableAttributedString()
                    resultsStr.append(result.attributedPrimaryText)
                    resultsStr.append(NSAttributedString(string: "\n"))
                    print("place is \(resultsStr)")
                    self.primaryPlaces.add(resultsStr)
                    
                    var resultsSecond = NSMutableAttributedString()
                    resultsSecond.append(result.attributedSecondaryText!)
                    resultsSecond.append(NSAttributedString(string: "\n"))
                    print("place is second \(resultsSecond)")
                    self.secondaryPlaces.add(resultsSecond)
                    
                    self.placeIds.add(result.placeID!)
                    print(result.placeID!)
                    
                }
            }
            if self.typedString == "" {
                self.primaryPlaces.removeAllObjects()
                self.secondaryPlaces.removeAllObjects()
                self.placeIds.removeAllObjects()
            }
            self.tableView.reloadData()
        })
        
    }
    
    func success(response: [String: AnyObject], apiType: String) {
        print("coming")
        print(response)
        print("yes")
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
        print("here")
    }
    
    func failure() {
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String){
        print(responseData["result"])
        
        print("places are \(primaryPlaces)")
        
        self.primaryPlaces.removeAllObjects()
        self.secondaryPlaces.removeAllObjects()
        self.placeIds.removeAllObjects()
        
        if let results: NSArray = responseData["predictions"] as! NSArray? {
            print(results)
            for result in results {
                let res = result as! NSDictionary
                print(res["structured_formatting"]!)
                let str_format = res["structured_formatting"] as! NSDictionary

                self.primaryPlaces.add(str_format["main_text"]!)
                

                self.secondaryPlaces.add(str_format["secondary_text"]!)
                
                self.placeIds.add(res["place_id"]!)
                print(res["place_id"])
                
            }
        }
        if self.typedString == "" {
            self.primaryPlaces.removeAllObjects()
            self.secondaryPlaces.removeAllObjects()
            self.placeIds.removeAllObjects()
        }
      
        self.tableView.reloadData()
        
    }

}

extension ResultTableViewController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if  searchController.searchBar.barTintColor == UIColor.init(colorLiteralRed: 68/255, green: 168/255, blue: 157/255, alpha: 1.0) {
            screenName = "green"
        }else {
            screenName = "blue"
        }
        typedString = searchController.searchBar.text
        searchController.searchResultsController?.view.isHidden = false
        if  searchController.searchBar.text == "" {
            self.primaryPlaces.removeAllObjects()
            self.secondaryPlaces.removeAllObjects()
            self.placeIds.removeAllObjects()
            self.tableView.reloadData()
        }else {
            self.placeAutocomplete(enteredString: searchController.searchBar.text!)
        }
        
    }
}
