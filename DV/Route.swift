//
//  Route.swift
//  DV
//
//  Created by Chinmay on 21/02/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class Route: UIViewController, NetworkProtocol {

    @IBOutlet weak var routeMap: GMSMapView!
    
    var from: String?
    var to: String?
    
    var pickLat: String?
    var pickLong: String?
    
    var dropLat: String?
    var dropLong: String?
    
    var from_city_name:String?
    var to_city_name:String?
    @IBOutlet weak var findRide: UIButton!
    @IBOutlet weak var offerRide: UIButton!
    @IBOutlet weak var segmentOption: UISegmentedControl!
    
    var actingRole: String?
    
    var bounds = GMSCoordinateBounds()
    
    override func viewDidLoad() {
        findRide.backgroundColor = .white
        findRide.layer.borderWidth = 2.0;
        findRide.layer.borderColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0).cgColor;
        
        offerRide.backgroundColor = .white
        offerRide.layer.borderWidth = 2.0;
        offerRide.layer.borderColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0).cgColor;

        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Route"
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        let shared = SharedPreferences()
        let user = shared.getUserType()
        if user == "Commuter" || user == "commuter" || user == "Commutor"{

            actingRole = "Commuter"
            findRide.isHidden = true
            offerRide.isHidden = true

        }else if user == "Driver" || user == "driver"{
            actingRole = "Driver"
            findRide.isHidden = false
            offerRide.isHidden = false
            self.offerRide.backgroundColor = UIColor.init(red: 8/255.0, green: 156/255.0, blue: 148/255.0, alpha: 1.0)
            self.offerRide.setTitleColor(UIColor.white, for: .normal)
            self.findRide.backgroundColor = UIColor.white
            self.findRide.setTitleColor(UIColor.init(colorLiteralRed: 8/255.0, green: 156/255.0, blue: 148/255.0, alpha: 1.0), for: .normal)
            print("offer ride")

        }
        else{
            actingRole = "Both"
            findRide.isHidden = false
            offerRide.isHidden = false
            self.offerRide.backgroundColor = UIColor.init(red: 8/255.0, green: 156/255.0, blue: 148/255.0, alpha: 1.0)
            self.offerRide.setTitleColor(UIColor.white, for: .normal)
            self.findRide.backgroundColor = UIColor.white
            self.findRide.setTitleColor(UIColor.init(colorLiteralRed: 8/255.0, green: 156/255.0, blue: 148/255.0, alpha: 1.0), for: .normal)
            print("offer ride")

        }
        
        
        let cameraPosition = GMSCameraPosition.camera(withLatitude: (dropLat! as AnyObject).doubleValue, longitude: (dropLong! as AnyObject).doubleValue, zoom: 12)

        routeMap.camera = cameraPosition

        // Do any additional setup after loading the view.

        DVUtility.disaplyWaitMessage()
        var urlString = "\("https://maps.googleapis.com/maps/api/directions/json")?origin=\("28.7041"),\("77.1025")&destination=\("28.6041"),\("78.1125")&sensor=true&key=\("Your-Api-key")"
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.direction()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickNext(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "TripDetails", bundle: nil)
        let tripDetails = storyboard.instantiateViewController(withIdentifier: "TripDetails") as! TripDetails

        tripDetails.from = from
        tripDetails.to = to
        tripDetails.fromLat = "\(pickLat!),\(pickLong!)"
        tripDetails.toLat = "\(dropLat!),\(dropLong!)"
        tripDetails.from_city_name = self.from_city_name
        tripDetails.to_city_name = self.to_city_name
        tripDetails.modalPresentationStyle = .overCurrentContext

        tripDetails.acting = actingRole!

        
        self.present(tripDetails, animated: false, completion: nil)
    }
    
   
    
    @IBAction func onClickOfFindRide(_ sender: UIButton) {
        self.findRide.backgroundColor = UIColor.init(red: 8/255.0, green: 156/255.0, blue: 148/255.0, alpha: 1.0)
        self.findRide.setTitleColor(UIColor.white, for: .normal)
        self.offerRide.backgroundColor = UIColor.white
        self.offerRide.setTitleColor(UIColor.init(colorLiteralRed: 8/255.0, green: 156/255.0, blue: 148/255.0, alpha: 1.0), for: .normal)
        print("find ride")
        actingRole = "Commuter"
        
        

    }
    @IBAction func onClickOfOfferRide(_ sender: UIButton) {
        self.offerRide.backgroundColor = UIColor.init(red: 8/255.0, green: 156/255.0, blue: 148/255.0, alpha: 1.0)
        self.offerRide.setTitleColor(UIColor.white, for: .normal)
        self.findRide.backgroundColor = UIColor.white
        self.findRide.setTitleColor(UIColor.init(colorLiteralRed: 8/255.0, green: 156/255.0, blue: 148/255.0, alpha: 1.0), for: .normal)
        print("offer ride")
        actingRole = "Driver"
        }
    func success(response: [String: AnyObject], apiType: String) {
        DVUtility.hideWaitMessage()
        print("coming")
        print(response)
        print("yes")
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
        print("here")
    }
    
    func failure() {
        DVUtility.hideWaitMessage()
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        print(responseData)
        let path = GMSMutablePath()
        var routes: [Any]? = responseData["routes"] as! [Any]?
        var firstRoute: [AnyHashable: Any] = routes?[0] as! [AnyHashable : Any]
        let leg: NSDictionary = ((firstRoute["legs"] as? Array)?[0])!
        
        
        let steps = leg.object(forKey: "steps") as! NSArray

        var stepIndex: Int = 1

        var stepCoordinates: [CLLocationCoordinate2D]! = []

        for step in steps {
            let stp = step as! NSDictionary
            print("hi1")
            let start_location: NSDictionary = stp.object(forKey: "start_location") as! NSDictionary

            stepCoordinates.append(self.coordinate(withLocation: start_location))
            path.add(self.coordinate(withLocation: start_location))
            print("hi2")
            let polyLinePoints: String? = ((stp.object(forKey: "polyline") as! NSDictionary)["points"] as? String)

            let polyLinePath = GMSPath.init(fromEncodedPath: polyLinePoints!)
            for p in 0..<polyLinePath!.count() {
                path.add(polyLinePath!.coordinate(at: p))
                bounds = bounds.includingCoordinate((polyLinePath?.coordinate(at: p))!)
            }
            print("hi3")
            if steps.count == stepIndex {
                stepIndex = stepIndex + 1
                print("hi4")
                let end_location: NSDictionary = stp.object(forKey: "end_location") as! NSDictionary
                stepCoordinates.append(self.coordinate(withLocation: end_location))
                path.add(self.coordinate(withLocation: end_location))
            }
            stepIndex = stepIndex + 1
        }
        print("hi5")
        var polyline: GMSPolyline? = nil
        polyline = GMSPolyline(path: path)
        polyline?.strokeColor = UIColor.red
        polyline?.strokeWidth = 3.0
        let marker = GMSMarker()
        marker.position = stepCoordinates[0]
        marker.icon = UIImage(named: "pin")

        marker.map = routeMap
        let marker1 = GMSMarker()
        marker1.icon = UIImage(named: "pinblue")
        

        marker1.position = CLLocationCoordinate2D(latitude: (dropLat! as AnyObject).doubleValue, longitude: (dropLong! as AnyObject).doubleValue)

        marker1.map = routeMap
        
        
        routeMap.animate(with: GMSCameraUpdate.fit(bounds))
        
        polyline?.map = routeMap
    }
    
    func coordinate(withLocation location: NSDictionary) -> CLLocationCoordinate2D {
        let latitude: Double? = (location.object(forKey: "lat") as AnyObject).doubleValue
        let longitude: Double? = (location.object(forKey: "lng") as AnyObject).doubleValue
        return CLLocationCoordinate2DMake(latitude!, longitude!)
    }
    
    func direction() {
        
        let jsonDict: [String: Any]
        jsonDict = ["for": "map"]
        let urlString = "\("https://maps.googleapis.com/maps/api/directions/json")?origin=\(pickLat!),\(pickLong!)&destination=\(dropLat!),\(dropLong!)&sensor=true&key=\("AIzaSyCIuDaV9vu9rDcGx0gXjitG8thasS6e1c0")"

        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        networkCommunication.startNetworkRequest(urlString, jsonData: jsonDict, method: "GET", apiType: "Direction")
        

    }
    

}
