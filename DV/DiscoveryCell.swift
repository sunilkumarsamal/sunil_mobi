//
//  DiscoveryCell.swift
//  DV
//
//  Created by Chinmay on 10/03/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class DiscoveryCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    
    
    @IBOutlet weak var contents_view: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var fieldOfWork: UILabel!
    @IBOutlet weak var chatButton: UIButton!
    
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var rating_image: UIImageView!
    @IBOutlet weak var interestCollection: UICollectionView!
    var interest_string = String()
    var interest:String?

    var interests_array = NSArray()
    var interestArray = NSMutableArray()
    var count = Int()
    var interests1 = ["Music", "Fitness", "Movies", "Food","Arts", "Sports", "Shopping", "Reading", "Socializing", "Travel", "Fashion", "Animals"];
    var selectedInterests = ["Musiccolour", "dumbbellcolor", "Moviescolour", "cutlerycolor","Artscolour", "Sportscolour", "Shoppingcolour", "openbookcolor", "Socializingcolour", "aerocolor", "coatcolor", "dogcolor"];

    
    override func awakeFromNib() {
        super.awakeFromNib()

        interestCollection.delegate = self
        interestCollection.dataSource = self
        
        interestCollection.register(UINib(nibName: "InterestImageCell", bundle: nil), forCellWithReuseIdentifier: "InterestImageCell")
        

        fieldOfWork.isHidden = false;
        profileImage.clipsToBounds = true;
        profileImage.layer.shadowColor = UIColor.red.cgColor
        profileImage.layer.borderColor = UIColor.white.cgColor
        profileImage.layer.borderWidth = 5
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if self.interest_string.characters.count >= 1 {
            self.interests_array = self.interest_string.components(separatedBy: ",") as NSArray
        }
        

        return self.interests_array.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InterestImageCell", for: indexPath) as! InterestImageCell
        var in_string = interests_array[indexPath.row] as! String
        var in_int = Int(in_string)
        var in_intt = in_int! - 1
        cell.interestImage.image = UIImage(named: self.selectedInterests[in_intt] as! String)
        
    
        
        return cell;
    }
    
}
