//
//  DVUtility.m
//  DV
//
//  Created by chinmay behera on 25/04/17.
//  Copyright © 2017 Six30. All rights reserved.
//

#import "DVUtility.h"
#import <UIKit/UIKit.h>

@implementation DVUtility

static UIAlertView *alert;

static NSString *ImageURL=@"http://45.79.189.135/guidezie/public/";

+(void)createLocalNotification:(NSDictionary*)notification_details{
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *now = [NSDate date];
    NSDate *dateToFire = [now dateByAddingTimeInterval:2];
    localNotification.fireDate = dateToFire;
    //    localNotification.alertBody = notification_details;
    localNotification.userInfo = notification_details;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.alertAction = @"alert Action";
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
//    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 0;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
}

//- (void)viewDidLoad {
//    [super viewDidLoad];
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//}

+(void) PrintNSLog:(NSString *) message WithTitle:(NSString *) title{
    NSLog(@"==> %@ : %@",title,message);
}

+(void)HideWaitMessage{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

+(void)DisaplyWaitMessage{
    
    if([alert isVisible]){
        return;
    }
    
    alert = [[UIAlertView alloc] initWithTitle:@"Please Wait..." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles: nil];
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    indicator.center = CGPointMake(alert.bounds.size.width / 2, alert.bounds.size.height - 50);
    [indicator startAnimating];
    [alert addSubview:indicator];
    [alert show];
}

+(void)DisplayAlertBoxTitle:(NSString *)title withMessage:(NSString *)Message{
    UIAlertView *ResendOTP=[[UIAlertView alloc]initWithTitle:title message:Message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    ResendOTP.tag=9999;
    [ResendOTP show];
}

@end
