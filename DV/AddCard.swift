//
//  AddCard.swift
//  DV
//
//  Created by Cherian Sankey on 05/06/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit
import AFNetworking
import Stripe

protocol dataSentDelegate {
    func userData(data: String)
}

class AddCard: UIViewController,UITextFieldDelegate,STPPaymentCardTextFieldDelegate,NetworkProtocol {
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var nameoncard: UITextField!
    @IBOutlet weak var anotherView: UIView!
    @IBOutlet weak var progress_bar: UIProgressView!
      
    @IBOutlet weak var percentLabel: UILabel!
    
    var info:PersonInfo?
    var token = String()
    var id = String()
    var delegate: dataSentDelegate?
    let paymentTextField = STPPaymentCardTextField()
    
    var entryType = "NewEntry"
    override func viewDidLoad() {
        super.viewDidLoad()
        if entryType != "NewEntry"{
            self.progress_bar.isHidden = false
            self.percentLabel.isHidden = false
        }
        self.progress_bar.transform = CGAffineTransform(scaleX: 1, y: 15)
        self.progress_bar.tintColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)
        progress_bar.progress = 0.90
        percentLabel.text = "90%"
        print(entryType)

        paymentTextField.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width - 30, height: 44)
        paymentTextField.delegate = self
        self.nameoncard.delegate = self
        view.addSubview(paymentTextField)
        self.anotherView.addSubview(paymentTextField)

        self.saveButton.isHidden = true
        self.spinner.isHidden = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        if textField.isValid{
            self.saveButton.isHidden = false
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return false
    }
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBAction func onClickOfBackButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveCardDetails(_ sender: UIButton) {
        DVUtility.disaplyWaitMessage()
        let card = paymentTextField.cardParams
        STPAPIClient.shared().createToken(withCard: card, completion: {(token,error) -> Void in
            if let error = error{
                DVUtility.hideWaitMessage()
                self.saveButton.isHidden = true
                print(error)
                let alert = UIAlertView(title: "", message: "Please provide the correct card details", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
                
                
            }
            else if let token = token{
                if token == NSNull(){
                    DVUtility.hideWaitMessage()
                    let alert = UIAlertView(title: "", message: "There is some technical error,Please try after some time.", delegate: self, cancelButtonTitle: "Ok")
                    alert.show()
                }
                else{
                    print(token)
                    self.token = String(describing: token)
                    
                    let shared = SharedPreferences()
                    let id = self.id
                    
                    
                    if self.entryType == "NewEntry"{
                        
                      
                        self.startNetworkRequest(urlString: "http://221.121.153.107/payment/save/\(id)")
                    }
                    else if self.entryType == "ChangeCard" || self.entryType == "changeCardDuringPayment"{
                       
                        self.startNetworkRequest(urlString: "http://221.121.153.107/payment/change/\(id)")
                    }
                }
                
                
            }
        })
            }
   
    
    func startNetworkRequest(urlString: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let request = NSMutableURLRequest()
        //Set Params
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 60
        request.httpMethod = "POST"
        //Create boundary, it can be anything
        let boundary = "DVRidesBoundary"
        // set Content-Type in HTTP header
        let contentType = "multipart/form-data; boundary=\(boundary)"
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        // post body
        let body = NSMutableData()
        //Populate a dictionary with all the regular values you would like to send.
        
        //        firstname, lastname, contact, address, study, avatar, interests, (role, identity, registration, insurance)
       
        var name_card = self.nameoncard.text!
        
        let param = ["token": "\(token)","u_name":"\(name_card)"] as [String : Any]
        
        print(param)
        
        for (key, value) in param {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        request.httpBody = body as Data
        let postLength = "\(UInt(body.length))"
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        // set URL
        request.url = NSURL(string: urlString)! as URL
        //        GuideZeeUtallity.HideWaitMessage()
        let task = session.dataTask(with: request as URLRequest) {
            ( data, response, error) in
            
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                DispatchQueue.main.async { [unowned self] in
                   
                    DVUtility.disaplyWaitMessage()
                    UIApplication.shared.beginIgnoringInteractionEvents()
                    let alert = UIAlertView(title: "Error!", message: "Something went wrong. Please try later.", delegate: self, cancelButtonTitle: "Ok")
                    alert.show()
                }
                return
            }
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            let dict = self.convertStringToDictionary(text: dataString as! String)
            print(dataString ?? NSDictionary())
            print(dict?["status_code"] ?? String())
            let code = dict?["status_code"] as! Int
            switch code {
            case 100:
                if self.entryType == "NewEntry"{
                    DVUtility.disaplyWaitMessage()
                    UIApplication.shared.beginIgnoringInteractionEvents()
                    let shared = SharedPreferences()
                    shared.saveUserId(self.id)
                    let id = shared.getUserId()
                    self.startNetworkRequest1(urlString: "http://221.121.153.107/user/profile/\(id)")
                    return
                }
                DVUtility.hideWaitMessage()
                let alert = UIAlertController(title: "", message:"Successfully Card Added", preferredStyle: UIAlertControllerStyle.alert)
                var data = String()
                let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel,handler: { (action) -> Void in
                    if self.entryType == "ChangeCard"{
                        data = "ViewProfile"
                    }
                    else if self.entryType == "NewEntry"{
                        data = "NotSignUp"
                    }
                    else if self.entryType == "changeCardDuringPayment"
                    {
                        print("card_changed")
                    }
                    self.delegate?.userData(data: data)
                    self.dismiss(animated: true, completion: nil)
                    
                    
                })
                alert.addAction(okButton)
                self.present(alert, animated: true, completion: nil)
                
                
            case 600:
                DVUtility.hideWaitMessage()
                DVUtility.displayAlertBoxTitle("", withMessage: "Already existing")
            case 603:
                DVUtility.displayAlertBoxTitle("", withMessage: "Some error occured.code=603")
            default:
                DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
            }
            
            
        }
        task.resume()
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    func success(response: [String: AnyObject], apiType: String) {
        progress_bar.transform = progress_bar.transform.scaledBy(x: 1, y: 1)
        progress_bar.tintColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)
        progress_bar.progress = 1.00
        percentLabel.text = "100%"
        
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
        
        DVUtility.hideWaitMessage()
        UIApplication.shared.endIgnoringInteractionEvents()
        let alert = UIAlertView(title: "Oops!!!", message: "There was a technical difficulty while fulfilling your request. Please try again.", delegate: self, cancelButtonTitle: "Ok")
        alert.show()
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        DVUtility.hideWaitMessage()
        print(responseData)
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            print("hi")
            if apiType == "RegisterGCM" {
                gotoHome()
            }
            
        case 600:
            DVUtility.displayAlertBoxTitle("", withMessage: "Some error occured. You might not get Notifications of your trips. Please Login again.")
            
        case 600:
            DVUtility.displayAlertBoxTitle("", withMessage: "User not found")
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }
    
    
    
    
    func saveCard(url: String)
    {
        let dataString =
            ["token": "\(token)"]
        
        let url = URL(string: url)!
        
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url as URL)
        
        do {
            // JSON all the things
            let auth = try JSONSerialization.data(withJSONObject: dataString, options: .prettyPrinted)
            
            // Set the request content type to JSON
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            // The magic...set the HTTP request method to POST
            request.httpMethod = "POST"
            
            // Add the JSON serialized login data to the body
            request.httpBody = auth
            
            // Create the task that will send our login request (asynchronously)
            let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                // Do something with the HTTP response
                print("Got response \(response) with error \(error)")
                print("Done.")
                print(response)
                DVUtility.hideWaitMessage()
                if self.entryType == "NewEntry"{
                    DVUtility.disaplyWaitMessage()
                    UIApplication.shared.beginIgnoringInteractionEvents()
                    let shared = SharedPreferences()
                    shared.saveUserId(self.id)
                    let id = shared.getUserId()
                    self.startNetworkRequest1(urlString: "http://221.121.153.107/user/profile/\(id)")
                }
                else{
                let alert = UIAlertController(title: "", message:"Successfully Card Added", preferredStyle: UIAlertControllerStyle.alert)
                var data = String()
                let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel,handler: { (action) -> Void in
                    if self.entryType == "ChangeCard"{
                        data = "ViewProfile"
                    }
                    else if self.entryType == "NewEntry"{


                    }
                    self.delegate?.userData(data: data)
                    self.dismiss(animated: true, completion: nil)
                    
                    
                })
                alert.addAction(okButton)
                self.present(alert, animated: true, completion: nil)
                }
                
            })
            
            // Start the task on a background thread
            task.resume()
            
        } catch {
            // Handle your errors folks...
            print("Error")
        }
        
    }
    
    func startNetworkRequest1(urlString: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let request = NSMutableURLRequest()
        //Set Params
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 60
        request.httpMethod = "POST"
        //Create boundary, it can be anything
        let boundary = "DVRidesBoundary"
        // set Content-Type in HTTP header
        let contentType = "multipart/form-data; boundary=\(boundary)"
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        // post body
        let body = NSMutableData()
        //Populate a dictionary with all the regular values you would like to send.
        
        //        firstname, lastname, contact, address, study, avatar, interests, (role, identity, registration, insurance)
        
        let param = ["firstname": (info?.firstName)!,
                     "lastname": (info?.lastName)!,
                     "contact": (info?.contact)!,
                     "study": (info?.course)!,
                     "interests": (info?.interest)!,
                     "dob": (info?.dob)!,
                     "gender": (info?.gender)!,
                     "vehicle": (info?.carName)!,
                     "plate": (info?.carNumber)!,
                     "colour": (info?.carColor)!,
                     "role": (info?.role)!,
                     "role_flag":(info?.flag)!] as [String : Any]
        
        print(param)
        
        for (key, value) in param {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }
        
        let FileParamConstant = "avatar"
        let imageData = UIImageJPEGRepresentation((info?.avatar)!, 0.5)
        if (imageData != nil) {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(FileParamConstant)\"; filename=\"image.png\"\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Type:image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append(imageData!)
            body.append("\r\n".data(using: String.Encoding.utf8)!)
        }
        
        if info?.role == "driver" {
            let FileParamConstant1 = "identity"
            let imageData1 = UIImageJPEGRepresentation((info?.identity)!, 0.5)
            if (imageData1 != nil) {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(FileParamConstant1)\"; filename=\"image.png\"\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Type:image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append(imageData1!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            let FileParamConstant2 = "registration"
            let imageData2 = UIImageJPEGRepresentation((info?.rego)!, 0.5)
            if (imageData2 != nil) {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(FileParamConstant2)\"; filename=\"image.png\"\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Type:image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append(imageData2!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            let FileParamConstant3 = "insurance"
            let imageData3 = UIImageJPEGRepresentation((info?.insurance)!, 0.5)
            if (imageData3 != nil) {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(FileParamConstant3)\"; filename=\"image.png\"\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Type:image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append(imageData3!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        request.httpBody = body as Data
        let postLength = "\(UInt(body.length))"
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        // set URL
        request.url = NSURL(string: urlString)! as URL
        //        GuideZeeUtallity.HideWaitMessage()
        let task = session.dataTask(with: request as URLRequest) {
            ( data, response, error) in
            
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                DispatchQueue.main.async { [unowned self] in
                    DVUtility.disaplyWaitMessage()
                    UIApplication.shared.beginIgnoringInteractionEvents()
                    let alert = UIAlertView(title: "Error!", message: "Something went wrong. Please try later.", delegate: self, cancelButtonTitle: "Ok")
                    alert.show()
                }
                return
            }
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            let dict = self.convertStringToDictionary(text: dataString as! String)
            print(dataString ?? NSDictionary())
            print(dict?["status_code"] ?? String())
            DispatchQueue.main.async { [unowned self] in
                UIApplication.shared.endIgnoringInteractionEvents()
                let shared = SharedPreferences()
                if self.info?.role == "driver" {
                    
                    shared.saveUserType("Driver")
                }else {
                    
                    shared.saveUserType("Commutor")
                }
                shared.saveFirstName((self.info?.firstName)!)
                shared.saveLastName((self.info?.lastName)!)
                shared.saveUserId(self.id)
                
                self.registerDeviceToken()
            }
            
        }
        
        task.resume()
    }

    func registerDeviceToken() {
      
        let shared = SharedPreferences()
       
        let token = shared.getToken()
        let jsonDict: NSDictionary
        jsonDict = ["token": token,
                    "device_type": "ios"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        networkCommunication.startNetworkRequest("http://221.121.153.107/user/subscribe/\(id)", jsonData: jsonDict as! [String : Any], method: "POST", apiType: "RegisterGCM")
        
    }
    
    func gotoHome() {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        let story = UIStoryboard(name: "PickDrop", bundle: nil)
       
        let navVC:UINavigationController = story.instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
        DispatchQueue.main.async {
            navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
            let container: MFSideMenuContainerViewController = story.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
            container.leftMenuViewController = leftVC
            container.centerViewController = navVC
            container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
            self.present(container, animated: true, completion: nil)
            
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
