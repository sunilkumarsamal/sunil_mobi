//
//  ChatScreen.swift
//  DV
//
//  Created by chinmay behera on 15/05/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

class ChatScreen: UIViewController, UITableViewDelegate, UITableViewDataSource, NetworkProtocol, ChatProtocol, UITextFieldDelegate {

    @IBOutlet weak var chatTable: UITableView!
    @IBOutlet weak var chatTextField: UITextField!
    @IBOutlet weak var chatTextFieldBottom: NSLayoutConstraint!
    @IBOutlet weak var chatSendButton: UIButton!
    @IBOutlet weak var chatSendButtonBottom: NSLayoutConstraint!
    
    @IBOutlet weak var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    var bookingInfo: BookingInfo?
    var userType: String?
    
    var chatList:Array<ChatPojo> = []
    
    var sender_id: String?
    var receiver_id: String?
    
    func notificationReceivedForChat(_ chatInfo: NSDictionary) {
        if !(isViewLoaded && (view.window != nil)) {
            DVUtility.createLocalNotification(chatInfo as! [String: AnyObject])
            return
        }
        let notification_id = ((chatInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "notification_id") as! Double
        let chat_book_id = ((chatInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "booking_id") as! String
        if notification_id == 700 && chat_book_id == bookingInfo!.booking_id {
            let time = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let chatTime: String = dateFormatter.string(from: time)
            
            let chatPojo = ChatPojo()
            chatPojo.message = ((chatInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "message") as! String
            chatPojo.time = chatTime
            chatPojo.booking_id = bookingInfo!.booking_id
            chatPojo.sender_id = ((chatInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "sender_id") as! String
            chatPojo.receiver_id = ((chatInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "receiver_id") as! String
            chatList.append(chatPojo)
            
            if chatList.count > 1 {
                let indexPath = IndexPath(row: (chatList.count - 1), section: 0)
                chatTable.beginUpdates()
                chatTable.insertRows(at: [indexPath], with: .right)
                chatTable.endUpdates()
                let ipath = IndexPath(row: chatList.count - 1, section: 0)
                chatTable.scrollToRow(at: ipath, at: .bottom, animated: false)
            }
            else {
                chatTable.reloadData()
                let ipath = IndexPath(row: chatList.count - 1, section: 0)
                chatTable.scrollToRow(at: ipath, at: .bottom, animated: false)
            }
        }else {
            print("")
            DVUtility.createLocalNotification(chatInfo as! [String: AnyObject])
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.chatTextField.setLeftPaddingPoints(5)
        self.chatTable.keyboardDismissMode = .onDrag
        chatTextField.inputAccessoryView = UIView()
        self.chatTextField.delegate = self;
        chatTextField.keyboardDistanceFromTextField = -8
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.chatDelegate = self
        chatTable.tableFooterView = UIView()
        if sender_id != nil {
            print("")
        }else {
            let shared = SharedPreferences()
            if shared.getUserId() == bookingInfo!.c_user_id {
                userType = "Commuter"
                sender_id = bookingInfo!.c_user_id
                receiver_id = bookingInfo!.d_user_id
            }else {
                userType = "Driver"
                sender_id = bookingInfo!.d_user_id
                receiver_id = bookingInfo!.c_user_id
            }
        }
        DVUtility.disaplyWaitMessage()
        self.fetchChat()
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.chatTextFieldBottom?.constant = 0.0
                self.chatSendButtonBottom?.constant = 0.0
            } else {
                self.chatTextFieldBottom?.constant = endFrame?.size.height ?? 0.0
                self.chatSendButtonBottom?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
            if chatList.count > 0 {
                let ipath = IndexPath(row: chatList.count - 1, section: 0)
                chatTable.scrollToRow(at: ipath, at: .bottom, animated: true)
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }


    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)

    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onTouchChatTextField(_ sender: UITextField) {
        
    }

    @IBAction func onClickChatSendButton(_ sender: UIButton) {
        print("ondway")
        if (self.chatTextField.text?.characters.count)! < 1{
            let alert = UIAlertView(title: "", message: "Enter message.", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
        }
        else{
            self.configureChatTable()
//            let text1 = chatTextField.text!
//            self.chatTextField.text = ""
            self.sendChat(chatTextField.text!)
        }
    }
    
//************************* TableView delegate & Datasource methods ***************************    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView()
        return v
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let chatmessage: ChatPojo = chatList[indexPath.row]
        let messageSize: CGSize? = PTSMessagingCell.messageSize(chatmessage.message)
        return (messageSize?.height)! + 2 * PTSMessagingCell.textMarginVertical() + 40.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        chatTable.separatorStyle = .none
        return chatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("hi")
        let cell = PTSMessagingCell(messagingCellWithReuseIdentifier: "Cell")
        cell!.sent = false
        cell?.textwidth = 190.0
        cell?.textheight = 17.0
        self.configureCell(cell!, at: indexPath)
        return cell!
    }
    
    func configureCell(_ cell: PTSMessagingCell, at indexPath: IndexPath) {
        let ccell: PTSMessagingCell = cell
        let chatData = chatList[indexPath.row]
        let shared = SharedPreferences()
        if chatData.sender_id == shared.getUserId() {
            ccell.sent = true
            ccell.avatarImageView?.image = UIImage(named: "avatar")
        }else {
            ccell.sent = false

        }
        ccell.backgroundColor = UIColor.clear
        ccell.messageLabel.text = (chatList[indexPath.row]).message

    }
    
//************************* Handle when keyboard appears ***************************
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWasShown(_ aNotification: Notification) {
        let keyboardSize = (aNotification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        print(keyboardSize?.size.height)

        chatTextFieldBottom.constant = ((keyboardSize?.size.height)!+0)
        chatSendButtonBottom.constant = ((keyboardSize?.size.height)!+0)
        if chatList.count > 0 {
            let ipath = IndexPath(row: chatList.count - 1, section: 0)
            chatTable.scrollToRow(at: ipath, at: .bottom, animated: true)
        }
//        }
    }
    
    func keyboardWillBeHidden(_ aNotification: Notification) {
        chatTextFieldBottom.constant = 0
        chatSendButtonBottom.constant = 0
    }
    
//**************************************** Chat API ******************************************
    
    func fetchChat() {
        let jsonDict = ["DV": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        networkCommunication.startNetworkRequest("http://221.121.153.107/converse/all/\(id)/\(bookingInfo!.booking_id)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "FetchChat")
    }
    
    func sendChat(_ message: String) {
        DVUtility.disaplyWaitMessage()

        let jsonDict:NSDictionary!
        jsonDict = ["booking_id": bookingInfo!.booking_id,
                    "receiver_id": receiver_id!,
                    "message": chatTextField.text!]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        networkCommunication.startNetworkRequest("http://221.121.153.107/converse/send/\(id)", jsonData: jsonDict as! [String : Any], method: "POST", apiType: "SendChat")
    }
    
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
        DVUtility.hideWaitMessage()
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        DVUtility.hideWaitMessage()
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            print("")
            if apiType == "FetchChat" {
                DVUtility.hideWaitMessage()
                let allChatList = responseData["data"] as! Array<[String: AnyObject]>

                for oneChat in allChatList {
                    let chatPojo = ChatPojo()
                    chatPojo.message = oneChat["message"] as! String
                    chatPojo.time = oneChat["sent_on"] as! String
                    chatPojo.sender_id = oneChat["sender_id"] as! String
                    chatPojo.receiver_id = oneChat["receiver_id"] as! String
                    chatPojo.booking_id = oneChat["booking_id"] as! String
                    chatList.append(chatPojo)
                }
                chatList.reverse()
                chatTable.reloadData()
                let ipath = IndexPath(row: chatList.count - 1, section: 0)
                if chatList.count > 0 {
                    chatTable.scrollToRow(at: ipath, at: .bottom, animated: false)
                }
                
                chatTable.reloadData()
            }else if apiType == "SendChat" {
                DVUtility.hideWaitMessage()
                chatTextField.text = ""
            }
        case 600:
            if apiType == "FetchChat"{
                print("no chat till now , start first")
                

            }
            else{
                DVUtility.displayAlertBoxTitle("", withMessage: "There are no trips on the mentioned time.")
            }
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }
    
    func configureChatTable() {
        let time = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")! as TimeZone
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let chatTime: String = dateFormatter.string(from: time)
        
        let chatPojo = ChatPojo()
        chatPojo.message = chatTextField.text!
        chatPojo.time = chatTime
        chatPojo.booking_id = bookingInfo!.booking_id
        chatPojo.sender_id = sender_id!
        chatPojo.receiver_id = receiver_id!
        chatList.append(chatPojo)
        
        if chatList.count > 1 {
            let indexPath = IndexPath(row: (chatList.count - 1), section: 0)
            chatTable.beginUpdates()
            chatTable.insertRows(at: [indexPath], with: .right)
            chatTable.endUpdates()
            let ipath = IndexPath(row: chatList.count - 1, section: 0)
            chatTable.scrollToRow(at: ipath, at: .bottom, animated: false)
        }
        else {
            chatTable.reloadData()
            let ipath = IndexPath(row: chatList.count - 1, section: 0)
            chatTable.scrollToRow(at: ipath, at: .bottom, animated: false)
        }
    }
    
    

}
