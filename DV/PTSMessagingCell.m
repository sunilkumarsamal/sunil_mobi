/*
 PTSMessagingCell.m
 
 Copyright (C) 2012 pontius software GmbH
 
 This program is free software: you can redistribute and/or modify
 it under the terms of the Createive Commons (CC BY-SA 3.0) license
 */

#import "PTSMessagingCell.h"
float textheight;
float textwidth;
@implementation PTSMessagingCell

static CGFloat textMarginHorizontal = 15.0f;
static CGFloat textMarginVertical = 15.0f;
static CGFloat messageTextSize = 17.0;


@synthesize sent, messageLabel, messageView, timeLabel, avatarImageView, balloonView;

#pragma mark -
#pragma mark Static methods

+(CGFloat)textMarginHorizontal {
    return textMarginHorizontal;
}

+(CGFloat)textMarginVertical {
    return textMarginVertical;
}

+(CGFloat)maxTextWidth {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return 220.0f;
    } else {
        return 400.0f;
    }
}

+(CGSize)messageSize:(NSString*)message {
    return [message sizeWithFont:[UIFont systemFontOfSize:messageTextSize] constrainedToSize:CGSizeMake([PTSMessagingCell maxTextWidth], CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
}

+(UIImage*)balloonImage:(BOOL)sent isSelected:(BOOL)selected {
//    if (self->textheight >= 20.0 && self->textwidth >= 190.0){
//    if (textwidth <= 21.0 && textheight <= 118.0){
        if (sent == YES && selected == YES) {//chat_back.png
            
            return [[UIImage imageNamed:@"smallgreen"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20)];
            //balloon_selected_right
        } else if (sent == YES && selected == NO) {
            return [[UIImage imageNamed:@"smallgreen"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20)];//balloon_read_right
        } else if (sent == NO && selected == YES) {
            return [[UIImage imageNamed:@"smallblue"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20)];//balloon_selected_left
        } else {
            return [[UIImage imageNamed:@"smallblue"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20)];//balloon_read_left
        }

    }
    
//    if (textwidth >= 23.0) {
//        if (sent == YES && selected == YES) {//chat_back.png
//            return [[UIImage imageNamed:@"chat1"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
//            //balloon_selected_right
//        } else if (sent == YES && selected == NO) {
//            return [[UIImage imageNamed:@"chat1"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];//balloon_read_right
//        } else if (sent == NO && selected == YES) {
//            return [[UIImage imageNamed:@"chat2"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];//balloon_selected_left
//        } else {
//            return [[UIImage imageNamed:@"chat2"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];//balloon_read_left
//        }
//    }
//    else{
//        if (sent == YES && selected == YES) {//chat_back.png
//            return [[UIImage imageNamed:@"chatgreen"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];
//            //balloon_selected_right
//        } else if (sent == YES && selected == NO) {
//            return [[UIImage imageNamed:@"chatgreen"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];//balloon_read_right
//        } else if (sent == NO && selected == YES) {
//            return [[UIImage imageNamed:@"chatblue"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];//balloon_selected_left
//        } else {
//            return [[UIImage imageNamed:@"chatblue"] stretchableImageWithLeftCapWidth:24 topCapHeight:15];//balloon_read_left
//        }
//
//    }
//    
//}

#pragma mark -
#pragma mark Object-Lifecycle/Memory management

-(id)initMessagingCellWithReuseIdentifier:(NSString*)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        /*Selection-Style of the TableViewCell will be 'None' as it implements its own selection-style.*/
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        /*Now the basic view-lements are initialized...*/
        messageView = [[UIView alloc] initWithFrame:CGRectZero];
        messageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        
        balloonView = [[UIImageView alloc] initWithFrame:CGRectZero];
        
        messageLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        timeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        avatarImageView = [[UIImageView alloc] initWithImage:nil];
        
        /*Message-Label*/
        self.messageLabel.backgroundColor = [UIColor clearColor];
        self.messageLabel.font = [UIFont fontWithName:@"OpenSans-Regular" size:messageTextSize];
        self.messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.messageLabel.numberOfLines = 0;
        
        /*Time-Label*/
        self.timeLabel.font = [UIFont boldSystemFontOfSize:12.0f];
        self.timeLabel.textColor = [UIColor darkGrayColor];
        self.timeLabel.backgroundColor = [UIColor clearColor];
        
        /*...and adds them to the view.*/
        [self.messageView addSubview: self.balloonView];
        [self.messageView addSubview: self.messageLabel];
        
        [self.contentView addSubview: self.timeLabel];
        [self.contentView addSubview: self.messageView];
        [self.contentView addSubview: self.avatarImageView];
        
        /*...and a gesture-recognizer, for LongPressure is added to the view.*/
        UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        [recognizer setMinimumPressDuration:1.0f];
        [self addGestureRecognizer:recognizer];
    }
    
    return self;
}


#pragma mark -
#pragma mark Layouting

- (void)layoutSubviews {
    /*This method layouts the TableViewCell. It calculates the frame for the different subviews, to set the layout according to size and orientation.*/
    
    /*Calculates the size of the message. */
    //    messageLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:17.0];
    CGSize textSize = [PTSMessagingCell messageSize:self.messageLabel.text];
    
    /*Calculates the size of the timestamp.*/
    CGSize dateSize = [self.timeLabel.text sizeWithFont:self.timeLabel.font forWidth:[PTSMessagingCell maxTextWidth] lineBreakMode:NSLineBreakByClipping];
    
    /*Initializes the different frames , that need to be calculated.*/
    CGRect ballonViewFrame = CGRectZero;
    CGRect messageLabelFrame = CGRectZero;
    CGRect timeLabelFrame = CGRectZero;
    CGRect avatarImageFrame = CGRectZero;
    //    messageLabel.font = [UIFont fontWithName:@"OpenSans-Semibold" size:17.0];
    //    messageLabel.textColor = [UIColor darkTextColor];
    if (self.sent == YES) {
        messageLabel.textColor = [UIColor whiteColor];
        timeLabelFrame = CGRectMake(self.frame.size.width - dateSize.width - textMarginHorizontal, 0.0f, dateSize.width, dateSize.height);
//        messageLabel.backgroundColor = [UIColor blackColor];

        textwidth = textSize.height;
        textheight = textSize.width;
        self.textheight = textSize.height;
        self.textwidth = textSize.width;
        ballonViewFrame = CGRectMake(self.frame.size.width - (textSize.width + 2*textMarginHorizontal)-15.0f, timeLabelFrame.size.height, textSize.width + 2*textMarginHorizontal, textSize.height + 2*textMarginVertical);
        
        messageLabelFrame = CGRectMake(self.frame.size.width-15.0f - (textSize.width + textMarginHorizontal),  ballonViewFrame.origin.y + textMarginVertical, textSize.width, textSize.height);
        
        timeLabelFrame = CGRectMake(self.frame.size.width - dateSize.width - textMarginHorizontal, ballonViewFrame.size.height+15, dateSize.width, dateSize.height);
        
        avatarImageFrame = CGRectMake(5.0f, timeLabelFrame.size.height, 50.0f, 50.0f);
        
    } else {
        
        
        
//        messageLabel.textColor = [UIColor whiteColor];
//        timeLabelFrame = CGRectMake(textMarginHorizontal, 0.0f, dateSize.width, dateSize.height);
//        
//        ballonViewFrame = CGRectMake(10.0f, 0, textSize.width + 6*textMarginHorizontal, textSize.height + 2*textMarginVertical+20);
//        
//        messageLabelFrame = CGRectMake(3*textMarginHorizontal+15.0f, ballonViewFrame.origin.y + textMarginVertical, textSize.width, textSize.height);
//        
//        timeLabelFrame = CGRectMake(textMarginHorizontal, ballonViewFrame.size.height+15, dateSize.width, dateSize.height);
//        
//        avatarImageFrame = CGRectMake(self.frame.size.width - 55.0f, timeLabelFrame.size.height, 50.0f, 50.0f);
//        
        
        messageLabel.textColor = [UIColor whiteColor];
        timeLabelFrame = CGRectMake(textMarginHorizontal, 0.0f, dateSize.width, dateSize.height);
        self.textheight = textSize.height;
        self.textwidth = textSize.width;
        ballonViewFrame = CGRectMake(15.0f, timeLabelFrame.size.height, textSize.width + 2*textMarginHorizontal, textSize.height + 2*textMarginVertical);
        
        messageLabelFrame = CGRectMake(textMarginHorizontal+15.0f, ballonViewFrame.origin.y + textMarginVertical, textSize.width, textSize.height);
        
        timeLabelFrame = CGRectMake(textMarginHorizontal, ballonViewFrame.size.height+15, dateSize.width, dateSize.height);
        
        avatarImageFrame = CGRectMake(self.frame.size.width - 55.0f, timeLabelFrame.size.height, 50.0f, 50.0f);

        
//        UIBezierPath * maskPath;
//        self.messageLabel.backgroundColor = [UIColor redColor];
//        maskPath = [UIBezierPath bezierPathWithRoundedRect:messageLabel.bounds
//                                                 byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight |UIRectCornerTopRight)                                                cornerRadii:CGSizeMake(20.0, 20.0)];
//        CAShapeLayer * maskLayer = [[CAShapeLayer alloc] init];
//        maskLayer.frame = messageLabel.bounds;
//        maskLayer.path = maskPath.CGPath;
        
        
        
        
        
//        [messageLabel.layer addSublayer:maskLayer];
//        [messageLabel.layer.mask addSublayer:maskLayer];

//        messageLabel.layer.mask = maskLayer;
        //        CAShapeLayer *balloonlayer = [[CAShapeLayer alloc] init];
//        self.messageLabel.backgroundColor = [UIColor redColor];
//        balloonlayer.bounds = self.messageLabel.frame;
//        balloonlayer.position = self.messageLabel.center;
//        CGSize s = CGSizeMake(10, 10);
//        balloonlayer.path = [UIBezierPath bezierPathWithRoundedRect:self.messageLabel.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(10.0f, 10.0f)]
        
    //        balloonlayer.path = (__bridge CGPathRef _Nullable)([UIBezierPath bezierPathWithRoundedRect:self.messageLabel.bounds byRoundingCorners:UIRectCornerBottomLeft | UIRectCornerBottomRight | UIRectCornerTopRight cornerRadii:s]);
        
        }
    
    self.balloonView.image = [PTSMessagingCell balloonImage:self.sent isSelected:self.selected];
    
    /*Sets the pre-initialized frames  for the balloonView and messageView.*/
    self.balloonView.frame = ballonViewFrame;
    self.messageLabel.frame = messageLabelFrame;
    
    /*If shown (and loaded), sets the frame for the avatarImageView*/
    if (self.avatarImageView.image != nil) {
        self.avatarImageView.frame = avatarImageFrame;
        self.avatarImageView.hidden=true;
    }
    
    /*If there is next for the timeLabel, sets the frame of the timeLabel.*/
    
    if (self.timeLabel.text != nil) {
        self.timeLabel.frame = timeLabelFrame;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    /*Selecting a UIMessagingCell will cause its subviews to be re-layouted. This process will not be animated! So handing animated = YES to this method will do nothing.*/
    [super setSelected:selected animated:NO];
    
    [self setNeedsLayout];
    
    /*Furthermore, the cell becomes first responder when selected.*/
    if (selected == YES) {
        [self becomeFirstResponder];
    } else {
        [self resignFirstResponder];
    }
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
}

#pragma mark -
#pragma mark UIGestureRecognizer-Handling

- (void)handleLongPress:(UILongPressGestureRecognizer *)longPressRecognizer {
    /*When a LongPress is recognized, the copy-menu will be displayed.*/
    if (longPressRecognizer.state != UIGestureRecognizerStateBegan) {
        return;
    }
    
    if ([self becomeFirstResponder] == NO) {
        return;
    }
    
    UIMenuController * menu = [UIMenuController sharedMenuController];
    [menu setTargetRect:self.balloonView.frame inView:self];
    
    [menu setMenuVisible:YES animated:YES];
}

-(BOOL)canBecomeFirstResponder {
    /*This cell can become first-responder*/
    return YES;
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender{
    /*Allows the copy-Action on this cell.*/
    if (action == @selector(copy:)) {
        return YES;
    } else {
        return [super canPerformAction:action withSender:sender];
    }
}

-(void)copy:(id)sender {
    /**Copys the messageString to the clipboard.*/
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    [pasteboard setString:self.messageLabel.text];
}
@end


