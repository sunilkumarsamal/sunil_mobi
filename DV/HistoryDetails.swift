//
//  HistoryDetails.swift
//  DV
//
//  Created by chinmay behera on 20/04/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit


class HistoryDetails: UIViewController, NetworkProtocol,CLLocationManagerDelegate {
    
    var bookingInfo: BookingInfo?
    
    var request: String?
    let locationManager = CLLocationManager()
    let marker = GMSMarker()
    var role = String()
    var start_time = String()
    var currentTitle = String()
    var nr = Int()
    var sum = Int()
    var pageFrom = String()
    var page = String()
    var rating = Double()
    var pagefrom = String()
    var firstname = String()
    var field_work = String()
    @IBOutlet weak var toastBackground: UIView!
    @IBOutlet weak var toastLabel: UILabel!
    @IBOutlet weak var toastView: UIView!
    @IBOutlet weak var rating_image: UIImageView!
    @IBOutlet weak var buttonStackView: UIStackView!
    
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var trackButton: UIButton!
    
    @IBOutlet weak var startPoint: UILabel!
    @IBOutlet weak var endPoint: UILabel!
    @IBOutlet weak var pickUp: UILabel!
    @IBOutlet weak var dropOff: UILabel!
    
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var dropTime: UILabel!
    @IBOutlet weak var endTime: UILabel!
    @IBOutlet weak var pickTime: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var someInfo: UILabel!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var driverStartView: UIView!
    @IBOutlet weak var driverEndView: UIView!
    
    @IBOutlet weak var driverStartViewDownImage: UIImageView!
    @IBOutlet weak var driverEndViewTopImage: UIImageView!
    @IBOutlet weak var infoStackView: UIStackView!
    @IBOutlet weak var mapScreen: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.delegate = self
        self.toastBackground.isHidden = true
        self.toastView.isHidden = true
        self.toastLabel.isHidden = true
        self.navigationItem.rightBarButtonItem = nil
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationItem.title = "Details"
        self.someInfo.lineBreakMode = .byWordWrapping
        self.someInfo.numberOfLines = 0
        
        var message = bookingInfo?.message
        if message != ""{
            print("message received")
            let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 30))
            backButton.setBackgroundImage(UIImage(named: "badge1.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
            backButton.addTarget(self, action: #selector(msgbox), for: .touchUpInside)
//            let rightBarButton = UIBarButtonItem(image: UIImage(named: "1msg.png")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(msgbox))
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)


        }
        else{
            self.navigationItem.rightBarButtonItem = nil
        }
//        self.estimatedPrice()
        
        let shared = SharedPreferences()
        
        var tripStartString = bookingInfo!.t_start
        var bookingStartString = bookingInfo!.b_start
        if bookingInfo!.t_start != "" {
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let tripStart = format.date(from: bookingInfo!.t_start)
            format.dateFormat = "hh:mm a dd MMM yyyy"
            tripStartString = format.string(from: tripStart!)
        }
        
        if bookingInfo!.b_start != "" {
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let bookingStart = format.date(from: bookingInfo!.b_start)
            format.dateFormat = "hh:mm a dd MMM yyyy"
            bookingStartString = format.string(from: bookingStart!)
        }
        
        
        if shared.getUserId() ==  bookingInfo!.c_user_id {
            startPoint.text = bookingInfo!.d_firstname + " " + bookingInfo!.d_lastname + "\n" +
                bookingInfo!.t_from + "\n" +
            tripStartString
            endPoint.text = bookingInfo!.d_firstname + " " + bookingInfo!.d_lastname + "\n" +
                bookingInfo!.t_to
            pickUp.text = "Picked up by " + bookingInfo!.d_firstname + " " + bookingInfo!.d_lastname + "\n" +
                "from " + bookingInfo!.b_from + "\n" +
                "at " + bookingStartString
            dropOff.text = "Dropped off by " + bookingInfo!.d_firstname + " " + bookingInfo!.d_lastname + "\n" +
                "at " + bookingInfo!.b_to
            cost.text = "$"+(bookingInfo?.cost)!
            if bookingInfo!.trip_status == "Pending" && bookingInfo!.d_user_id == "" {
                pickUp.text = "Picked up by: " + "driver" + "\n" +
                    "from " + bookingInfo!.b_from + "\n" +
                    "at " + bookingStartString
                dropOff.text = "Droppped off by: " + "driver" + "\n" +
                    "at " + bookingInfo!.b_to
            }
            if bookingInfo!.trip_status == "Cancelled" && bookingInfo!.d_user_id == "" {
                pickUp.text = "[Request was not confirmed.]"
                dropOff.text = "[Request was not confirmed.]"
            }
            infoStackView.removeArrangedSubview(driverStartView)
            driverStartView.removeFromSuperview()
            infoStackView.removeArrangedSubview(driverEndView)
            driverEndView.removeFromSuperview()
            self.driverEndViewTopImage.isHidden = true
            self.driverStartViewDownImage.isHidden = true
        }else if shared.getUserId() ==  bookingInfo!.d_user_id {
            startPoint.text = bookingInfo!.d_firstname + " " + bookingInfo!.d_lastname + "\n" +
                              bookingInfo!.t_from + "\n" + tripStartString
            endPoint.text = bookingInfo!.d_firstname + " " + bookingInfo!.d_lastname + "\n" +
                            bookingInfo!.t_to
            pickUp.text = "Pick up " + bookingInfo!.c_firstname + " " + bookingInfo!.c_lastname + "\n" +
                "from " + bookingInfo!.b_from + "\n" +
                "at " + bookingStartString
            dropOff.text = "Drop off " + bookingInfo!.c_firstname + " " + bookingInfo!.c_lastname + "\n" +
                "at " + bookingInfo!.b_to
            cost.text = "$"+(bookingInfo?.driver_share)!
            if bookingInfo!.trip_status == "Pending" && bookingInfo!.c_user_id == "" {
                pickUp.text = "Pick up commuter"
                dropOff.text = "Drop off commuter"
            }
            if bookingInfo!.trip_status == "Cancelled" && bookingInfo!.c_user_id == "" {
                pickUp.text = "[Request was not confirmed.]"
                dropOff.text = "[Request was not confirmed.]"
            }
            
        }
        
        if let info = bookingInfo {
            print(info)
            var user = ""
            let shared = SharedPreferences()
//            if shared.getUserId() ==  bookingInfo!.c_user_id {
                user = "Commuter"

//                if self.pageFrom != "Myrequest" {
//                    if bookingInfo?.trip_status == "Pending"{
//                        if bookingInfo?.c_user_id == shared.getUserId(){
//                        name.text = bookingInfo?.c_firstname
//                        someInfo.text = bookingInfo?.c_study
//                        let profileUrl = bookingInfo?.c_avatar
//                        if (profileUrl?.contains("avatar"))!{
//                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
//                            let url = URL(string: ImagePath)
//                            DispatchQueue.global(qos: .background).async {
//                                let data = try? Data(contentsOf: url!)
//                                DispatchQueue.main.async {
//                                    if data != nil {
//                                        self.profileImage.image = UIImage(data: data!)
//                                    }else {
//                                        self.profileImage.image = UIImage(named: "avatar")
//                                    }
//                                }
//                            }
//                        }
//                        else{
//                            let ImagePath = bookingInfo!.c_avatar
//                            let url = URL(string: ImagePath)
//                            DispatchQueue.global(qos: .background).async {
//                                let data = try? Data(contentsOf: url!)
//                                DispatchQueue.main.async {
//                                    if data != nil {
//                                        self.profileImage.image = UIImage(data: data!)
//                                    }else {
//                                        self.profileImage.image = UIImage(named: "avatar")
//                                    }
//                                }
//                            }
//                        }
//                        }
//                    else if bookingInfo?.d_user_id == shared.getUserId(){
//                        name.text = bookingInfo?.d_firstname
//                        someInfo.text = bookingInfo?.d_study
//                        let profileUrl = bookingInfo?.d_avatar
//                        if (profileUrl?.contains("avatar"))!{
//                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
//                            let url = URL(string: ImagePath)
//                            DispatchQueue.global(qos: .background).async {
//                                let data = try? Data(contentsOf: url!)
//                                DispatchQueue.main.async {
//                                    if data != nil {
//                                        self.profileImage.image = UIImage(data: data!)
//                                    }else {
//                                        self.profileImage.image = UIImage(named: "avatar")
//                                    }
//                                }
//                            }
//                        }
//                        else{
//                            let ImagePath = bookingInfo!.d_avatar
//                            let url = URL(string: ImagePath)
//                            DispatchQueue.global(qos: .background).async {
//                                let data = try? Data(contentsOf: url!)
//                                DispatchQueue.main.async {
//                                    if data != nil {
//                                        self.profileImage.image = UIImage(data: data!)
//                                    }else {
//                                        self.profileImage.image = UIImage(named: "avatar")
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//                }
//                else{
//                    if bookingInfo?.trip_status == "Pending"{
//                        name.text = bookingInfo?.c_firstname
//                        someInfo.text = bookingInfo?.c_study
//                        let profileUrl = bookingInfo?.d_avatar
//                        if (profileUrl?.contains("avatar"))!{
//                        let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
//                        let url = URL(string: ImagePath)
//                        DispatchQueue.global(qos: .background).async {
//                            let data = try? Data(contentsOf: url!)
//                            DispatchQueue.main.async {
//                                if data != nil {
//                                    self.profileImage.image = UIImage(data: data!)
//                                }else {
//                                    self.profileImage.image = UIImage(named: "avatar")
//                                }
//                            }
//                        }
//                        }else{
//                            let ImagePath = bookingInfo!.d_avatar
//                            let url = URL(string: ImagePath)
//                            DispatchQueue.global(qos: .background).async {
//                                let data = try? Data(contentsOf: url!)
//                                DispatchQueue.main.async {
//                                    if data != nil {
//                                        self.profileImage.image = UIImage(data: data!)
//                                    }else {
//                                        self.profileImage.image = UIImage(named: "avatar")
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    else{
//                        name.text = bookingInfo?.d_firstname
//                        someInfo.text = bookingInfo?.d_study
//                        let profileUrl = bookingInfo?.c_avatar
//                        if (profileUrl?.contains("avatar"))!{
//                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
//                            let url = URL(string: ImagePath)
//                            DispatchQueue.global(qos: .background).async {
//                                let data = try? Data(contentsOf: url!)
//                                DispatchQueue.main.async {
//                                    if data != nil {
//                                        self.profileImage.image = UIImage(data: data!)
//                                    }else {
//                                        self.profileImage.image = UIImage(named: "avatar")
//                                    }
//                                }
//                            }
//                        }
//                        else{
//                        let ImagePath = bookingInfo!.c_avatar
//                        let url = URL(string: ImagePath)
//                        DispatchQueue.global(qos: .background).async {
//                            let data = try? Data(contentsOf: url!)
//                            DispatchQueue.main.async {
//                                if data != nil {
//                                    self.profileImage.image = UIImage(data: data!)
//                                }else {
//                                    self.profileImage.image = UIImage(named: "avatar")
//                                }
//                            }
//                        }
//                        }
//                    }
//                }
//            }else if shared.getUserId() ==  bookingInfo!.d_user_id {
//                user = "Driver"
//                shared.saveUserType("Driver")
//                if self.pageFrom != "Myrequest"{
//                    if bookingInfo?.trip_status == "Pending" {
//                        name.text = bookingInfo?.d_firstname
//                        someInfo.text = bookingInfo?.d_study
//                        let profileUrl = bookingInfo?.d_avatar
//                        if (profileUrl?.contains("avatar"))!{
//                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
//                            let url = URL(string: ImagePath)
//                            DispatchQueue.global(qos: .background).async {
//                                let data = try? Data(contentsOf: url!)
//                                DispatchQueue.main.async {
//                                    if data != nil {
//                                        self.profileImage.image = UIImage(data: data!)
//                                    }else {
//                                        self.profileImage.image = UIImage(named: "avatar")
//                                    }
//                                }
//                            }
//                        }
//                        else{
//                            let ImagePath = bookingInfo!.d_avatar
//                            let url = URL(string: ImagePath)
//                            DispatchQueue.global(qos: .background).async {
//                                let data = try? Data(contentsOf: url!)
//                                DispatchQueue.main.async {
//                                    if data != nil {
//                                        self.profileImage.image = UIImage(data: data!)
//                                    }else {
//                                        self.profileImage.image = UIImage(named: "avatar")
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//                else{
//                    name.text = bookingInfo?.c_firstname
//                    someInfo.text = bookingInfo?.c_study
//                    let profileUrl = bookingInfo?.c_avatar
//                    if (profileUrl?.contains("avatar"))!{
//                        let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
//                        let url = URL(string: ImagePath)
//                        DispatchQueue.global(qos: .background).async {
//                            let data = try? Data(contentsOf: url!)
//                            DispatchQueue.main.async {
//                                if data != nil {
//                                    self.profileImage.image = UIImage(data: data!)
//                                }else {
//                                    self.profileImage.image = UIImage(named: "avatar")
//                                }
//                            }
//                        }
// 
//                    }
//                    else{
//                    let ImagePath = bookingInfo!.c_avatar
//                    let url = URL(string: ImagePath)
//                    DispatchQueue.global(qos: .background).async {
//                        let data = try? Data(contentsOf: url!)
//                        DispatchQueue.main.async {
//                            if data != nil {
//                                self.profileImage.image = UIImage(data: data!)
//                            }else {
//                                self.profileImage.image = UIImage(named: "avatar")
//                            }
//                        }
//                    }
//                    }
//                }
////            }
            
            if bookingInfo!.trip_status == "Pending" {
                

                cost.text = ""
                let req = request
                if self.pageFrom == "Myrequest" {
                    if shared.getUserId() != bookingInfo?.c_user_id{
                        name.text = bookingInfo!.c_firstname + " " + bookingInfo!.c_lastname
                        someInfo.text = bookingInfo!.c_study
                        let profileUrl = bookingInfo?.c_avatar
                        if (profileUrl?.contains("avatar"))!{
                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                        else{
                            let ImagePath = bookingInfo!.c_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                        var x = Double((bookingInfo?.rating)!)!
                        if x == 0.0 {
                            self.rating_image.image = UIImage(named: "5star")!
                        }
                        else if x > 0.0 && x <= 0.5 {
                            self.rating_image.image = UIImage(named: "0.5stars")
                        }
                        else if x > 0.5 && x <= 1.0 {
                            self.rating_image.image = UIImage(named: "1starcolor")
                        }
                        else if x > 1.0 && x <= 1.5 {
                            self.rating_image.image = UIImage(named: "1.5star")
                        }
                        else if x > 1.5 && x <= 2.0 {
                            self.rating_image.image = UIImage(named: "2starcolor")
                        }
                        else if x > 2.0 && x <= 2.5{
                            self.rating_image.image = UIImage(named: "2.5star")
                        }
                        else if x > 2.5 && x <= 3.0 {
                            self.rating_image.image = UIImage(named: "3starcolor")
                        }
                        else if x > 3.0 && x <= 3.5 {
                            self.rating_image.image = UIImage(named: "3.5star")
                        }
                        else if x > 3.5 && x <= 4.0 {
                            self.rating_image.image = UIImage(named: "4starcolor")
                        }
                        else if x > 4.0 && x <= 4.5 {
                            self.rating_image.image = UIImage(named: "4.5star")
                        }
                        else if x > 4.5 {
                            self.rating_image.image = UIImage(named: "5starcolor")
                        }

                        
                    }else {
                        name.text = bookingInfo!.d_firstname + " " + bookingInfo!.d_lastname
                        someInfo.text = bookingInfo!.d_study
                        let profileUrl = bookingInfo?.d_avatar
                        if (profileUrl?.contains("avatar"))!{
                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                            
                        }
                        else{
                            let ImagePath = bookingInfo!.d_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                        var x = Double((bookingInfo?.rating)!)!
                        if x == 0.0 {
                            self.rating_image.image = UIImage(named: "5star")!
                        }
                        else if x > 0.0 && x <= 0.5 {
                            self.rating_image.image = UIImage(named: "0.5stars")
                        }
                        else if x > 0.5 && x <= 1.0 {
                            self.rating_image.image = UIImage(named: "1starcolor")
                        }
                        else if x > 1.0 && x <= 1.5 {
                            self.rating_image.image = UIImage(named: "1.5star")
                        }
                        else if x > 1.5 && x <= 2.0 {
                            self.rating_image.image = UIImage(named: "2starcolor")
                        }
                        else if x > 2.0 && x <= 2.5{
                            self.rating_image.image = UIImage(named: "2.5star")
                        }
                        else if x > 2.5 && x <= 3.0 {
                            self.rating_image.image = UIImage(named: "3starcolor")
                        }
                        else if x > 3.0 && x <= 3.5 {
                            self.rating_image.image = UIImage(named: "3.5star")
                        }
                        else if x > 3.5 && x <= 4.0 {
                            self.rating_image.image = UIImage(named: "4starcolor")
                        }
                        else if x > 4.0 && x <= 4.5 {
                            self.rating_image.image = UIImage(named: "4.5star")
                        }
                        else if x > 4.5 {
                            self.rating_image.image = UIImage(named: "5starcolor")
                        }

                    }
                    if req == "Incoming" {
                        let cancelButton = UIButton()
                        cancelButton.setTitle("Cancel", for: .normal)
                        cancelButton.backgroundColor = UIColor.white
                        cancelButton.setTitleColor(UIColor.red, for: .normal)
                        cancelButton.addTarget(self, action: #selector(cancelRequest), for: .touchUpInside)
                        
                        let acceptButton = UIButton()
                        acceptButton.setTitle("Accept", for: .normal)
                        acceptButton.backgroundColor = UIColor.init(colorLiteralRed: 09/255, green: 148/255, blue: 145/255, alpha: 1.0)
                        acceptButton.addTarget(self, action: #selector(accept), for: .touchUpInside)
                        
                        buttonStackView.addArrangedSubview(cancelButton)
                        buttonStackView.addArrangedSubview(acceptButton)
                    }else if req == "Outgoing" {
                        let discButton = UIButton()
                        discButton.setTitle("Discovery", for: .normal)
                        discButton.backgroundColor = UIColor.init(colorLiteralRed: 09/255, green: 148/255, blue: 145/255, alpha: 1.0)
                        discButton.addTarget(self, action: #selector(gotoDiscovery), for: .touchUpInside)
                        
                        buttonStackView.addArrangedSubview(discButton)
                    }
                    
                }else{
                    if shared.getUserId() != bookingInfo?.c_user_id{
                        name.text = bookingInfo!.d_firstname + " " + bookingInfo!.d_lastname
                        someInfo.text = bookingInfo!.d_study
                        let profileUrl = bookingInfo?.d_avatar
                        if (profileUrl?.contains("avatar"))!{
                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                        else{
                            let ImagePath = bookingInfo!.d_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                        var x = Double((bookingInfo?.rating)!)!
                        if x == 0.0 {
                            self.rating_image.image = UIImage(named: "5star")!
                        }
                        else if x > 0.0 && x <= 0.5 {
                            self.rating_image.image = UIImage(named: "0.5stars")
                        }
                        else if x > 0.5 && x <= 1.0 {
                            self.rating_image.image = UIImage(named: "1starcolor")
                        }
                        else if x > 1.0 && x <= 1.5 {
                            self.rating_image.image = UIImage(named: "1.5star")
                        }
                        else if x > 1.5 && x <= 2.0 {
                            self.rating_image.image = UIImage(named: "2starcolor")
                        }
                        else if x > 2.0 && x <= 2.5{
                            self.rating_image.image = UIImage(named: "2.5star")
                        }
                        else if x > 2.5 && x <= 3.0 {
                            self.rating_image.image = UIImage(named: "3starcolor")
                        }
                        else if x > 3.0 && x <= 3.5 {
                            self.rating_image.image = UIImage(named: "3.5star")
                        }
                        else if x > 3.5 && x <= 4.0 {
                            self.rating_image.image = UIImage(named: "4starcolor")
                        }
                        else if x > 4.0 && x <= 4.5 {
                            self.rating_image.image = UIImage(named: "4.5star")
                        }
                        else if x > 4.5 {
                            self.rating_image.image = UIImage(named: "5starcolor")
                        }
                        
                        
                    }else {
                        name.text = bookingInfo!.c_firstname + " " + bookingInfo!.c_lastname
                        someInfo.text = bookingInfo!.c_study
                        let profileUrl = bookingInfo?.c_avatar
                        if (profileUrl?.contains("avatar"))!{
                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                            
                        }
                        else{
                            let ImagePath = bookingInfo!.c_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                        var x = Double((bookingInfo?.rating)!)!
                        if x == 0.0 {
                            self.rating_image.image = UIImage(named: "5star")!
                        }
                        else if x > 0.0 && x <= 0.5 {
                            self.rating_image.image = UIImage(named: "0.5stars")
                        }
                        else if x > 0.5 && x <= 1.0 {
                            self.rating_image.image = UIImage(named: "1starcolor")
                        }
                        else if x > 1.0 && x <= 1.5 {
                            self.rating_image.image = UIImage(named: "1.5star")
                        }
                        else if x > 1.5 && x <= 2.0 {
                            self.rating_image.image = UIImage(named: "2starcolor")
                        }
                        else if x > 2.0 && x <= 2.5{
                            self.rating_image.image = UIImage(named: "2.5star")
                        }
                        else if x > 2.5 && x <= 3.0 {
                            self.rating_image.image = UIImage(named: "3starcolor")
                        }
                        else if x > 3.0 && x <= 3.5 {
                            self.rating_image.image = UIImage(named: "3.5star")
                        }
                        else if x > 3.5 && x <= 4.0 {
                            self.rating_image.image = UIImage(named: "4starcolor")
                        }
                        else if x > 4.0 && x <= 4.5 {
                            self.rating_image.image = UIImage(named: "4.5star")
                        }
                        else if x > 4.5 {
                            self.rating_image.image = UIImage(named: "5starcolor")
                        }
                        
                    }
                    profileImage.image = UIImage(named: "avatar")
                    let cancelButton = UIButton()
                    cancelButton.setTitle("Cancel", for: .normal)
                    cancelButton.backgroundColor = UIColor.white
                    cancelButton.setTitleColor(UIColor.red, for: .normal)
                    cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
                    
                    let discButton = UIButton()
                    discButton.setTitle("Discovery", for: .normal)
                    discButton.backgroundColor = UIColor.init(colorLiteralRed: 09/255, green: 148/255, blue: 145/255, alpha: 1.0)
                    discButton.addTarget(self, action: #selector(gotoDiscovery), for: .touchUpInside)
                    
                    buttonStackView.addArrangedSubview(cancelButton)
                    buttonStackView.addArrangedSubview(discButton)
                }
                chatButton.isHidden = true
                trackButton.isHidden = true
                
            }else if bookingInfo!.trip_status == "Alloted" {
                if self.pageFrom == "Myrequest" {
                    if shared.getUserId() == bookingInfo?.c_user_id{
                        name.text = bookingInfo!.d_firstname + " " + bookingInfo!.d_lastname
                        someInfo.text = bookingInfo!.d_study
                        var x = Double((bookingInfo?.rating)!)!
                        if x == 0.0 {
                            self.rating_image.image = UIImage(named: "5star")!
                        }
                        else if x > 0.0 && x <= 0.5 {
                            self.rating_image.image = UIImage(named: "0.5stars")
                        }
                        else if x > 0.5 && x <= 1.0 {
                            self.rating_image.image = UIImage(named: "1starcolor")
                        }
                        else if x > 1.0 && x <= 1.5 {
                            self.rating_image.image = UIImage(named: "1.5star")
                        }
                        else if x > 1.5 && x <= 2.0 {
                            self.rating_image.image = UIImage(named: "2starcolor")
                        }
                        else if x > 2.0 && x <= 2.5{
                            self.rating_image.image = UIImage(named: "2.5star")
                        }
                        else if x > 2.5 && x <= 3.0 {
                            self.rating_image.image = UIImage(named: "3starcolor")
                        }
                        else if x > 3.0 && x <= 3.5 {
                            self.rating_image.image = UIImage(named: "3.5star")
                        }
                        else if x > 3.5 && x <= 4.0 {
                            self.rating_image.image = UIImage(named: "4starcolor")
                        }
                        else if x > 4.0 && x <= 4.5 {
                            self.rating_image.image = UIImage(named: "4.5star")
                        }
                        else if x > 4.5 {
                            self.rating_image.image = UIImage(named: "5starcolor")
                        }
                        let profileUrl = bookingInfo!.d_avatar
                        if profileUrl.contains("avatar"){
                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                        else{
                        let ImagePath = bookingInfo!.d_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                        }
                    }else {
                        name.text = bookingInfo!.c_firstname + " " + bookingInfo!.c_lastname
                        someInfo.text = bookingInfo!.c_study
                        var x = Double((bookingInfo?.rating)!)!
                        if x == 0.0 {
                            self.rating_image.image = UIImage(named: "5star")!
                        }
                        else if x > 0.0 && x <= 0.5 {
                            self.rating_image.image = UIImage(named: "0.5stars")
                        }
                        else if x > 0.5 && x <= 1.0 {
                            self.rating_image.image = UIImage(named: "1starcolor")
                        }
                        else if x > 1.0 && x <= 1.5 {
                            self.rating_image.image = UIImage(named: "1.5star")
                        }
                        else if x > 1.5 && x <= 2.0 {
                            self.rating_image.image = UIImage(named: "2starcolor")
                        }
                        else if x > 2.0 && x <= 2.5{
                            self.rating_image.image = UIImage(named: "2.5star")
                        }
                        else if x > 2.5 && x <= 3.0 {
                            self.rating_image.image = UIImage(named: "3starcolor")
                        }
                        else if x > 3.0 && x <= 3.5 {
                            self.rating_image.image = UIImage(named: "3.5star")
                        }
                        else if x > 3.5 && x <= 4.0 {
                            self.rating_image.image = UIImage(named: "4starcolor")
                        }
                        else if x > 4.0 && x <= 4.5 {
                            self.rating_image.image = UIImage(named: "4.5star")
                        }
                        else if x > 4.5 {
                            self.rating_image.image = UIImage(named: "5starcolor")
                        }
                        let profileUrl = bookingInfo!.c_avatar
                        if profileUrl.contains("avatar"){
                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                        else{
                        let ImagePath = bookingInfo!.c_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                        }
                    }

                    let backButton = UIButton()
                    backButton.setTitle("Back", for: .normal)
                    backButton.backgroundColor = UIColor.white
                    backButton.setTitleColor(UIColor.darkGray, for: .normal)
                    backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
                    buttonStackView.addArrangedSubview(backButton)
                    
                    chatButton.isHidden = true
                    trackButton.isHidden = true
                }
                else{
                if shared.getUserId() == bookingInfo?.c_user_id{
                    name.text = bookingInfo!.d_firstname + " " + bookingInfo!.d_lastname
                    someInfo.text = bookingInfo!.d_study
                    let cancelButton = UIButton()
                    cancelButton.setTitle("Cancel", for: .normal)
                    cancelButton.backgroundColor = UIColor.white
                    cancelButton.setTitleColor(UIColor.red, for: .normal)
                    cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
                    buttonStackView.addArrangedSubview(cancelButton)
                    var x = Double((bookingInfo?.rating)!)!
                    if x == 0.0 {
                        self.rating_image.image = UIImage(named: "5star")!
                    }
                    else if x > 0.0 && x <= 0.5 {
                        self.rating_image.image = UIImage(named: "0.5stars")
                    }
                    else if x > 0.5 && x <= 1.0 {
                        self.rating_image.image = UIImage(named: "1starcolor")
                    }
                    else if x > 1.0 && x <= 1.5 {
                        self.rating_image.image = UIImage(named: "1.5star")
                    }
                    else if x > 1.5 && x <= 2.0 {
                        self.rating_image.image = UIImage(named: "2starcolor")
                    }
                    else if x > 2.0 && x <= 2.5{
                        self.rating_image.image = UIImage(named: "2.5star")
                    }
                    else if x > 2.5 && x <= 3.0 {
                        self.rating_image.image = UIImage(named: "3starcolor")
                    }
                    else if x > 3.0 && x <= 3.5 {
                        self.rating_image.image = UIImage(named: "3.5star")
                    }
                    else if x > 3.5 && x <= 4.0 {
                        self.rating_image.image = UIImage(named: "4starcolor")
                    }
                    else if x > 4.0 && x <= 4.5 {
                        self.rating_image.image = UIImage(named: "4.5star")
                    }
                    else if x > 4.5 {
                        self.rating_image.image = UIImage(named: "5starcolor")
                    }
                    let profileUrl = bookingInfo!.d_avatar
                    if profileUrl.contains("avatar"){
                        let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                    else{
                    let ImagePath = bookingInfo!.d_avatar
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                self.profileImage.image = UIImage(data: data!)
                            }else {
                                self.profileImage.image = UIImage(named: "avatar")
                            }
                        }
                    }
                    }
                }else {
                    name.text = bookingInfo!.c_firstname + " " + bookingInfo!.c_lastname
                    someInfo.text = bookingInfo!.c_study
                    var x = Double((bookingInfo?.rating)!)!
                    if x == 0.0 {
                        self.rating_image.image = UIImage(named: "5star")!
                    }
                    else if x > 0.0 && x <= 0.5 {
                        self.rating_image.image = UIImage(named: "0.5stars")
                    }
                    else if x > 0.5 && x <= 1.0 {
                        self.rating_image.image = UIImage(named: "1starcolor")
                    }
                    else if x > 1.0 && x <= 1.5 {
                        self.rating_image.image = UIImage(named: "1.5star")
                    }
                    else if x > 1.5 && x <= 2.0 {
                        self.rating_image.image = UIImage(named: "2starcolor")
                    }
                    else if x > 2.0 && x <= 2.5{
                        self.rating_image.image = UIImage(named: "2.5star")
                    }
                    else if x > 2.5 && x <= 3.0 {
                        self.rating_image.image = UIImage(named: "3starcolor")
                    }
                    else if x > 3.0 && x <= 3.5 {
                        self.rating_image.image = UIImage(named: "3.5star")
                    }
                    else if x > 3.5 && x <= 4.0 {
                        self.rating_image.image = UIImage(named: "4starcolor")
                    }
                    else if x > 4.0 && x <= 4.5 {
                        self.rating_image.image = UIImage(named: "4.5star")
                    }
                    else if x > 4.5 {
                        self.rating_image.image = UIImage(named: "5starcolor")
                    }
                    let profileUrl = bookingInfo!.c_avatar
                    if profileUrl.contains("avatar"){
                        let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                    else{
                    let ImagePath = bookingInfo!.c_avatar
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                self.profileImage.image = UIImage(data: data!)
                            }else {
                                self.profileImage.image = UIImage(named: "avatar")
                            }
                        }
                    }
                    }
                    let cancelButton = UIButton()
                    cancelButton.setTitle("Cancel", for: .normal)
                    cancelButton.backgroundColor = UIColor.white
                    cancelButton.setTitleColor(UIColor.red, for: .normal)
                    cancelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)
                    
                    let startButton = UIButton()
                    startButton.setTitle("Start", for: .normal)
                    startButton.backgroundColor = UIColor.init(colorLiteralRed: 09/255, green: 148/255, blue: 145/255, alpha: 1.0)
                    startButton.setTitleColor(UIColor.white, for: .normal)
                    startButton.addTarget(self, action: #selector(start), for: .touchUpInside)
                    
                    buttonStackView.addArrangedSubview(cancelButton)
                    buttonStackView.addArrangedSubview(startButton)
                }
                }
                trackButton.isHidden = true
                
            }else if bookingInfo!.trip_status == "Started" {
                if self.pageFrom == "Myrequest" {
                    if shared.getUserId() != bookingInfo?.d_user_id{
                        self.name.text = bookingInfo?.d_firstname
                        self.someInfo.text = bookingInfo?.d_study
                        let profileUrl = bookingInfo!.d_avatar
                        if profileUrl.contains("avatar"){
                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                        else{
                        let ImagePath = bookingInfo!.d_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                        }
                    }
                    else{
                        self.name.text = bookingInfo?.c_firstname
                        self.someInfo.text = bookingInfo?.c_study
                        let profileUrl = bookingInfo!.c_avatar
                        if profileUrl.contains("avatar"){
                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                        else{
                        let ImagePath = bookingInfo!.c_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                        }
  
                    }
                    let backButton = UIButton()
                    backButton.setTitle("Back", for: .normal)
                    backButton.backgroundColor = UIColor.white
                    backButton.setTitleColor(UIColor.darkGray, for: .normal)
                    backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
                    buttonStackView.addArrangedSubview(backButton)
                    
                    chatButton.isHidden = true
                    trackButton.isHidden = true
                }
                else{
                    var x = Double((bookingInfo?.rating)!)!
                    if x == 0.0 {
                        self.rating_image.image = UIImage(named: "5star")!
                    }
                    else if x > 0.0 && x <= 0.5 {
                        self.rating_image.image = UIImage(named: "0.5stars")
                    }
                    else if x > 0.5 && x <= 1.0 {
                        self.rating_image.image = UIImage(named: "1starcolor")
                    }
                    else if x > 1.0 && x <= 1.5 {
                        self.rating_image.image = UIImage(named: "1.5star")
                    }
                    else if x > 1.5 && x <= 2.0 {
                        self.rating_image.image = UIImage(named: "2starcolor")
                    }
                    else if x > 2.0 && x <= 2.5{
                        self.rating_image.image = UIImage(named: "2.5star")
                    }
                    else if x > 2.5 && x <= 3.0 {
                        self.rating_image.image = UIImage(named: "3starcolor")
                    }
                    else if x > 3.0 && x <= 3.5 {
                        self.rating_image.image = UIImage(named: "3.5star")
                    }
                    else if x > 3.5 && x <= 4.0 {
                        self.rating_image.image = UIImage(named: "4starcolor")
                    }
                    else if x > 4.0 && x <= 4.5 {
                        self.rating_image.image = UIImage(named: "4.5star")
                    }
                    else if x > 4.5 {
                        self.rating_image.image = UIImage(named: "5starcolor")
                    }

                if shared.getUserId() != bookingInfo?.d_user_id{
                    self.name.text = bookingInfo?.d_firstname
                    self.someInfo.text = bookingInfo?.d_study
                    let profileUrl = bookingInfo!.d_avatar
                    if profileUrl.contains("avatar"){
                        let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                    else{
                    let ImagePath = bookingInfo!.d_avatar
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                self.profileImage.image = UIImage(data: data!)
                            }else {
                                self.profileImage.image = UIImage(named: "avatar")
                            }
                        }
                    }
                    }
                    let backButton = UIButton()
                    backButton.setTitle("Back", for: .normal)
                    backButton.backgroundColor = UIColor.white
                    backButton.setTitleColor(UIColor.darkGray, for: .normal)
                    backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
                    buttonStackView.addArrangedSubview(backButton)
                }else {
                    self.name.text = bookingInfo?.c_firstname
                    self.someInfo.text = bookingInfo?.c_study
                    let profileUrl = bookingInfo!.c_avatar
                    if profileUrl.contains("avatar"){
                        let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                    else{
                    let ImagePath = bookingInfo!.c_avatar
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                self.profileImage.image = UIImage(data: data!)
                            }else {
                                self.profileImage.image = UIImage(named: "avatar")
                            }
                        }
                    }
                    }
                    let pickUpButton = UIButton()
                    pickUpButton.setTitle("Pick up commuter", for: .normal)
                    pickUpButton.backgroundColor = UIColor.init(colorLiteralRed: 09/255, green: 148/255, blue: 145/255, alpha: 1.0)
                    pickUpButton.addTarget(self, action: #selector(pickUpCommuter), for: .touchUpInside)
                    buttonStackView.addArrangedSubview(pickUpButton)
                }
                }
            }else if bookingInfo!.trip_status == "Picked Up" {
                if self.pageFrom == "Myrequest" {
                    if shared.getUserId() != bookingInfo?.d_user_id{
                        self.name.text = bookingInfo?.d_firstname
                        self.someInfo.text = bookingInfo?.d_study
                        let profileUrl = bookingInfo!.d_avatar
                        if profileUrl.contains("avatar"){
                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                        else{
                        let ImagePath = bookingInfo!.d_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                        }
                    }
                    else{
                        self.name.text = bookingInfo?.c_firstname
                        self.someInfo.text = bookingInfo?.c_study
                        let profileUrl = bookingInfo!.c_avatar
                        if profileUrl.contains("avatar"){
                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                        else{
                        let ImagePath = bookingInfo!.c_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                        }
                    }
                    let backButton = UIButton()
                    backButton.setTitle("Back", for: .normal)
                    backButton.backgroundColor = UIColor.white
                    backButton.setTitleColor(UIColor.darkGray, for: .normal)
                    backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
                    buttonStackView.addArrangedSubview(backButton)
                    
                    chatButton.isHidden = true
                    trackButton.isHidden = true
                }
                else{
                    var x = Double((bookingInfo?.rating)!)!
                    if x == 0.0 {
                        self.rating_image.image = UIImage(named: "5star")!
                    }
                    else if x > 0.0 && x <= 0.5 {
                        self.rating_image.image = UIImage(named: "0.5stars")
                    }
                    else if x > 0.5 && x <= 1.0 {
                        self.rating_image.image = UIImage(named: "1starcolor")
                    }
                    else if x > 1.0 && x <= 1.5 {
                        self.rating_image.image = UIImage(named: "1.5star")
                    }
                    else if x > 1.5 && x <= 2.0 {
                        self.rating_image.image = UIImage(named: "2starcolor")
                    }
                    else if x > 2.0 && x <= 2.5{
                        self.rating_image.image = UIImage(named: "2.5star")
                    }
                    else if x > 2.5 && x <= 3.0 {
                        self.rating_image.image = UIImage(named: "3starcolor")
                    }
                    else if x > 3.0 && x <= 3.5 {
                        self.rating_image.image = UIImage(named: "3.5star")
                    }
                    else if x > 3.5 && x <= 4.0 {
                        self.rating_image.image = UIImage(named: "4starcolor")
                    }
                    else if x > 4.0 && x <= 4.5 {
                        self.rating_image.image = UIImage(named: "4.5star")
                    }
                    else if x > 4.5 {
                        self.rating_image.image = UIImage(named: "5starcolor")
                    }

                if shared.getUserId() != bookingInfo?.d_user_id{
                    self.name.text = bookingInfo?.d_firstname
                    self.someInfo.text = bookingInfo?.d_study
                    let profileUrl = bookingInfo?.d_avatar
                    if (profileUrl?.contains("avatar"))!{
                        let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                    else{
                    let ImagePath = bookingInfo!.d_avatar
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                self.profileImage.image = UIImage(data: data!)
                            }else {
                                self.profileImage.image = UIImage(named: "avatar")
                            }
                        }
                    }
                    }
                    
                    let backButton = UIButton()
                    backButton.setTitle("Back", for: .normal)
                    backButton.backgroundColor = UIColor.white
                    backButton.setTitleColor(UIColor.darkGray, for: .normal)
                    backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
                    buttonStackView.addArrangedSubview(backButton)
                }else {
                    self.name.text = bookingInfo?.c_firstname
                    self.someInfo.text = bookingInfo?.c_study
                    let profileUrl = bookingInfo!.c_avatar
                    if profileUrl.contains("avatar"){
                        let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                    else{
                    let ImagePath = bookingInfo!.c_avatar
                    let url = URL(string: ImagePath)
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: url!)
                        DispatchQueue.main.async {
                            if data != nil {
                                self.profileImage.image = UIImage(data: data!)
                            }else {
                                self.profileImage.image = UIImage(named: "avatar")
                            }
                        }
                    }
                    }
                    let dropOffButton = UIButton()
                    dropOffButton.setTitle("Drop off commuter", for: .normal)
                    dropOffButton.backgroundColor = UIColor.init(colorLiteralRed: 09/255, green: 148/255, blue: 145/255, alpha: 1.0)
                    dropOffButton.addTarget(self, action: #selector(dropOffCommuter), for: .touchUpInside)
                    buttonStackView.addArrangedSubview(dropOffButton)
                }
                }
                chatButton.isHidden = false
            }else if bookingInfo!.trip_status == "Dropped Off" {
//                if self.pageFrom == "Myrequest" {
//                    let backButton = UIButton()
//                    backButton.setTitle("Back", for: .normal)
//                    backButton.backgroundColor = UIColor.white
//                    backButton.setTitleColor(UIColor.darkGray, for: .normal)
//                    backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
//                    buttonStackView.addArrangedSubview(backButton)
//                    
//                    chatButton.isHidden = true
//                    trackButton.isHidden = true
//                }
//                else{
                if shared.getUserId() != bookingInfo?.d_user_id{
                    self.name.text = bookingInfo?.d_firstname
                    self.someInfo.text = bookingInfo?.d_study
                    let profileUrl = bookingInfo?.d_avatar
                    if (profileUrl?.contains("avatar"))!{
                        let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                    else{
                        let ImagePath = bookingInfo!.d_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                }else {
                    self.name.text = bookingInfo?.c_firstname
                    self.someInfo.text = bookingInfo?.c_study
                    let profileUrl = bookingInfo!.c_avatar
                    if profileUrl.contains("avatar"){
                        let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                    else{
                        let ImagePath = bookingInfo!.c_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                }
                
                let backButton = UIButton()
                backButton.setTitle("Back", for: .normal)
                backButton.backgroundColor = UIColor.white
                backButton.setTitleColor(UIColor.darkGray, for: .normal)
                backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
                buttonStackView.addArrangedSubview(backButton)
//                }
            }else if bookingInfo!.trip_status == "Paid" {
//                if self.pageFrom == "Myrequest" {
//                    let backButton = UIButton()
//                    backButton.setTitle("Back", for: .normal)
//                    backButton.backgroundColor = UIColor.white
//                    backButton.setTitleColor(UIColor.darkGray, for: .normal)
//                    backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
//                    buttonStackView.addArrangedSubview(backButton)
//                    
//                    chatButton.isHidden = true
//                    trackButton.isHidden = true
//                }
//                else{
                if shared.getUserId() == bookingInfo?.c_user_id{
                    self.name.text = bookingInfo?.d_firstname
                    self.someInfo.text = bookingInfo?.d_study
                    let profileUrl = bookingInfo?.d_avatar
                    if (profileUrl?.contains("avatar"))!{
                        let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                    else{
                        let ImagePath = bookingInfo!.d_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                }else {
                    self.name.text = bookingInfo?.c_firstname
                    self.someInfo.text = bookingInfo?.c_study
                    let profileUrl =  bookingInfo!.c_avatar
                    if profileUrl.contains("avatar"){
                        let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                    else{
                        let ImagePath = bookingInfo!.c_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                }
                let backButton = UIButton()
                backButton.setTitle("Back", for: .normal)
                backButton.backgroundColor = UIColor.white
                backButton.setTitleColor(UIColor.darkGray, for: .normal)
                backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
                buttonStackView.addArrangedSubview(backButton)
//                }
                chatButton.isHidden = true
                trackButton.isHidden = true
                
            }else if bookingInfo!.trip_status == "Cancelled" {
//                if self.pageFrom == "Myrequest" {
//                    let backButton = UIButton()
//                    backButton.setTitle("Back", for: .normal)
//                    backButton.backgroundColor = UIColor.white
//                    backButton.setTitleColor(UIColor.darkGray, for: .normal)
//                    backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
//                    buttonStackView.addArrangedSubview(backButton)
//                    
//                    chatButton.isHidden = true
//                    trackButton.isHidden = true
//                }
//                else{
                if self.pageFrom == "Myrequest"{
                    var shared = SharedPreferences()
                    if shared.getUserId() == bookingInfo?.c_user_id{
                        name.text = bookingInfo?.d_firstname
                        someInfo.text = bookingInfo?.d_study
                        let profileUrl = bookingInfo?.d_avatar
                        if (profileUrl?.contains("avatar"))!{
                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                        else{
                            let ImagePath = bookingInfo!.d_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                    }
                    else{
                        name.text = bookingInfo?.c_firstname
                        someInfo.text = bookingInfo?.c_study
                        let profileUrl = bookingInfo!.c_avatar
                        if profileUrl.contains("avatar"){
                            let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }
                        else{
                            let ImagePath = bookingInfo!.c_avatar
                            let url = URL(string: ImagePath)
                            DispatchQueue.global(qos: .background).async {
                                let data = try? Data(contentsOf: url!)
                                DispatchQueue.main.async {
                                    if data != nil {
                                        self.profileImage.image = UIImage(data: data!)
                                    }else {
                                        self.profileImage.image = UIImage(named: "avatar")
                                    }
                                }
                            }
                        }

                    }
                    cost.text = ""
                    let backButton = UIButton()
                    backButton.setTitle("Back", for: .normal)
                    backButton.backgroundColor = UIColor.white
                    backButton.setTitleColor(UIColor.darkGray, for: .normal)
                    backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
                    buttonStackView.addArrangedSubview(backButton)
                    //                }
                    chatButton.isHidden = true
                    trackButton.isHidden = true
                }
                else{
                var shared = SharedPreferences()
                if shared.getUserId() == bookingInfo?.c_user_id{
                name.text = bookingInfo?.c_firstname
                someInfo.text = bookingInfo?.c_study
                    let profileUrl = bookingInfo!.c_avatar
                    if profileUrl.contains("avatar"){
                        let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.c_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                    else{
                        let ImagePath = bookingInfo!.c_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                }
                else{
                    name.text = bookingInfo?.d_firstname
                    someInfo.text = bookingInfo?.d_study
                    let profileUrl = bookingInfo?.d_avatar
                    if (profileUrl?.contains("avatar"))!{
                        let ImagePath = "http://221.121.153.107/public/" + bookingInfo!.d_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                    else{
                        let ImagePath = bookingInfo!.d_avatar
                        let url = URL(string: ImagePath)
                        DispatchQueue.global(qos: .background).async {
                            let data = try? Data(contentsOf: url!)
                            DispatchQueue.main.async {
                                if data != nil {
                                    self.profileImage.image = UIImage(data: data!)
                                }else {
                                    self.profileImage.image = UIImage(named: "avatar")
                                }
                            }
                        }
                    }
                }
                cost.text = ""
                let backButton = UIButton()
                backButton.setTitle("Back", for: .normal)
                backButton.backgroundColor = UIColor.white
                backButton.setTitleColor(UIColor.darkGray, for: .normal)
                backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
                buttonStackView.addArrangedSubview(backButton)
//                }
                chatButton.isHidden = true
                trackButton.isHidden = true
            }
            }
        }
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onTapDetailsView(_ sender: UITapGestureRecognizer) {

    }
    
    func cancel() {
       
        DVUtility.disaplyWaitMessage()
        let shared = SharedPreferences()
        let id = shared.getUserId()
        let userType = shared.getUserType()
        print(role)
        print(userType)
//        if userType == "Driver" || userType == "driver"{
//            let url = "http://221.121.153.107/trip/cancel/\(id)"
//            let jsonDict: NSDictionary = ["trip_id": bookingInfo!.trip_id]
//            self.sendToServer(url: url, apiType: "Cancel", jsonDict: jsonDict as! [String : Any], method: "POST")
//        }else {
//            let url = "http://221.121.153.107/booking/cancel/\(id)"
//            let jsonDict: NSDictionary = ["booking_id": bookingInfo!.booking_id]
//            self.sendToServer(url: url, apiType: "Cancel", jsonDict: jsonDict as! [String : Any], method: "POST")
//        }
        if id == bookingInfo!.d_user_id{
            let url = "http://221.121.153.107/trip/cancel/\(id)"
            let jsonDict: NSDictionary = ["trip_id": bookingInfo!.trip_id]
            self.sendToServer(url: url, apiType: "Cancel", jsonDict: jsonDict as! [String : Any], method: "POST")
        }else {
            let url = "http://221.121.153.107/booking/cancel/\(id)"
            let jsonDict: NSDictionary = ["booking_id": bookingInfo!.booking_id]
            self.sendToServer(url: url, apiType: "Cancel", jsonDict: jsonDict as! [String : Any], method: "POST")
        }

        
    }
   
    func gotoDiscovery() {
        let storyboard1 = UIStoryboard(name: "SideMenu", bundle: nil)
        let leftVC = storyboard1.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        let story = UIStoryboard(name: "PickDrop", bundle: nil)
        //            let PickDrop = story.instantiateViewController(withIdentifier: "PickUpDropOff") as! PickUpDropOff
        let storyboard = UIStoryboard(name: "Discovery", bundle: nil)
        let discNav = storyboard.instantiateViewController(withIdentifier: "DiscNav") as! UINavigationController
        let dis = discNav.topViewController as! Discovery
        let shared = SharedPreferences()
        if  shared.getUserId() == bookingInfo!.c_user_id {
            dis.book_id = bookingInfo!.booking_id
            dis.trip_id = bookingInfo?.trip_id
        }else {
            dis.book_id = bookingInfo?.booking_id
            dis.trip_id = bookingInfo!.trip_id
        }
        
        discNav.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
        
        self.menuContainerViewController.centerViewController = discNav
    }
    func cancelRequest(){
        DVUtility.disaplyWaitMessage()
        let requestId = bookingInfo?.request_id
//        let url = "http://221.121.153.107/request/deny/\(requestId!)"
//        let jsonDict: NSDictionary = ["dv":"mobi"]
//        self.sendToServer(url: url, apiType: "CancelRequest", jsonDict: jsonDict as! [String : Any], method: "GET")
        let jsonDict: NSDictionary = ["DV": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        networkCommunication.startNetworkRequest("http://221.121.153.107/request/deny/\(requestId!)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "cancelRequest")
    }
    
    func accept() {
        DVUtility.disaplyWaitMessage()
        let jsonDict: NSDictionary = ["booking_id": bookingInfo!.booking_id,
                                      "trip_id": bookingInfo!.trip_id]
        let shared = SharedPreferences()
        let id = shared.getUserId()
        let url = "http://221.121.153.107/trip/accept/\(id)"
        self.sendToServer(url: url, apiType: "Accept", jsonDict: jsonDict as! [String : Any], method: "POST")
    }
    
    func start() {
        if CLLocationManager.locationServicesEnabled(){
            DVUtility.disaplyWaitMessage()
            let jsonDict: NSDictionary = ["booking_id": bookingInfo!.booking_id,
                                          "trip_id": bookingInfo!.trip_id]
            let shared = SharedPreferences()
            let id = shared.getUserId()
            let url = "http://221.121.153.107/trip/start/\(id)"
            self.sendToServer(url: url, apiType: "Start", jsonDict: jsonDict as! [String : Any], method: "POST")
        }
        else{
            let alert = UIAlertView(title: "", message: "Location services are not enabled", delegate: self, cancelButtonTitle: "retry")
            alert.tag = 100
            alert.show()
        }
    }
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if alertView.tag == 100{
            self.start()
        }
    }
    func pickUpCommuter() {
        DVUtility.disaplyWaitMessage()
        LocationService.sharedInstance.stopUpdatingLocation()
//        LocationService.sharedInstance.stopMonitoringSignificantLocationChanges()
//        LocationService?.sharedInstance.stopUpdatingLocation()
//        LocationService?.sharedInstance.stopUpdatingHeading()
        let jsonDict: NSDictionary = ["booking_id": bookingInfo!.booking_id,
                                      "trip_id": bookingInfo!.trip_id]
        let shared = SharedPreferences()
        let id = shared.getUserId()
//        LocationService.sharedInstance.stopUpdatingLocation()

        let url = "http://221.121.153.107/trip/pickup/\(id)"
        self.sendToServer(url: url, apiType: "Pickup", jsonDict: jsonDict as! [String : Any], method: "POST")
    }
    
    func dropOffCommuter() {
        DVUtility.disaplyWaitMessage()
        LocationService.sharedInstance.trip_id = ""
        LocationService.sharedInstance.stopUpdatingLocation()
        let jsonDict: NSDictionary = ["booking_id": bookingInfo!.booking_id,
                                      "trip_id": bookingInfo!.trip_id]
        let shared = SharedPreferences()
        let id = shared.getUserId()
        let url = "http://221.121.153.107/trip/dropoff/\(id)"
        self.sendToServer(url: url, apiType: "Dropoff", jsonDict: jsonDict as! [String : Any], method: "POST")
    }
    
    func goBack() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickChat(_ sender: UIButton) {
        let storyboard = UIStoryboard.init(name: "ChatScreen", bundle: nil)
        let chat = storyboard.instantiateViewController(withIdentifier: "ChatScreen") as! ChatScreen
        chat.bookingInfo = bookingInfo!
        self.navigationController?.pushViewController(chat, animated: true)
    }
    
    @IBAction func onClickTrack(_ sender: UIButton) {
        let shared = SharedPreferences()
        let id = shared.getUserId()
        var address = ""
        if bookingInfo!.trip_status == "Started" {
            address = bookingInfo!.b_from_latlng
        }else if bookingInfo!.trip_status == "Picked Up" {
            address = bookingInfo!.b_to_latlng
        }
        if id == bookingInfo!.c_user_id {
            print("track api calling...")
            let story = UIStoryboard.init(name: "TrackDriver", bundle: nil)
            let trackDriver = story.instantiateViewController(withIdentifier: "TrackDriver") as! TrackDriver
            trackDriver.trip_id = bookingInfo!.trip_id
            self.navigationController?.pushViewController(trackDriver, animated: true)
        }else {
            if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                UIApplication.shared.openURL(NSURL(string:
                    "comgooglemaps://?saddr=&daddr=\(address)&directionsmode=driving")! as URL)
            } else {
                print("Can't use comgooglemaps://");
            }
        }
    }
    
    func putNavGradient() {
        let gradientLayer = CAGradientLayer()
        var updatedFrame = self.navigationController!.navigationBar.bounds
        updatedFrame.size.height += 20
        gradientLayer.frame = updatedFrame
        gradientLayer.colors = [UIColor.init(colorLiteralRed: 09/255, green: 148/255, blue: 145/255, alpha: 1.0).cgColor, UIColor.init(colorLiteralRed: 65/255, green: 98/255, blue: 151/255, alpha: 1.0).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.navigationController!.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
    }
    
    func sendToServer(url: String, apiType: String, jsonDict: [String : Any], method: String) {
        
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        if apiType != "SaveLocation"{
         DVUtility.disaplyWaitMessage()
        }
       
        networkCommunication.startNetworkRequest(url, jsonData: jsonDict as! [String : Any], method: method, apiType: apiType)
        
        //http://45.79.189.135/divi/booking/request/{id}
    }
    
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
        DVUtility.hideWaitMessage()
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        

        DVUtility.hideWaitMessage()
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            if apiType == "Accept" {
                print("")

                var storyboard = UIStoryboard(name: "Reminder", bundle: nil)
                var next = storyboard.instantiateViewController(withIdentifier: "Reminder") as! Reminder
                next.role = "from_historyPage"
                
                next.start_time = self.start_time
                self.present(next, animated: true, completion: nil)

            }else if apiType == "Start" {
                for subview in buttonStackView.subviews {
                    buttonStackView.removeArrangedSubview(subview)
                    subview.removeFromSuperview()
                }
                let pickUpButton = UIButton()
                pickUpButton.setTitle("Pick up commuter", for: .normal)
                pickUpButton.backgroundColor = UIColor.init(colorLiteralRed: 09/255, green: 148/255, blue: 145/255, alpha: 1.0)
                pickUpButton.addTarget(self, action: #selector(pickUpCommuter), for: .touchUpInside)
                buttonStackView.addArrangedSubview(pickUpButton)
                trackButton.isHidden = false
                bookingInfo!.trip_status = "Started"
                locationManager.startUpdatingLocation()
                
//                self.setUpLocationUpdate()
                let alert = UIAlertView(title: "", message: "Click on the arrow mark to get directions.", delegate: self, cancelButtonTitle: "Ok")
                alert.show()

                
            }
            else if apiType == "cancelRequest"{
                for subview in buttonStackView.subviews {
                    buttonStackView.removeArrangedSubview(subview)
                    subview.removeFromSuperview()
                }
                let back = UIButton()
                back.setTitle("Back", for: .normal)
                back.backgroundColor = UIColor.init(colorLiteralRed: 09/255, green: 148/255, blue: 145/255, alpha: 1.0)
                back.addTarget(self, action: #selector(goBack), for: .touchUpInside)
                buttonStackView.addArrangedSubview(back)
               print("request cancelled")
            }else if apiType == "Pickup" {
                print("")
//                LocationService.sharedInstance.stopUpdatingLocation()
                for subview in buttonStackView.subviews {
                    buttonStackView.removeArrangedSubview(subview)
                    subview.removeFromSuperview()
                }
                let dropOffButton = UIButton()
                dropOffButton.setTitle("Drop off commuter", for: .normal)
                dropOffButton.backgroundColor = UIColor.init(colorLiteralRed: 09/255, green: 148/255, blue: 145/255, alpha: 1.0)
                dropOffButton.addTarget(self, action: #selector(dropOffCommuter), for: .touchUpInside)
                buttonStackView.addArrangedSubview(dropOffButton)
//                self.setUpLocationUpdate()
                bookingInfo!.trip_status = "Picked Up"
//                LocationService.sharedInstance.trip_id = bookingInfo?.trip_id
                locationManager.startUpdatingLocation()
//                LocationService.sharedInstance.startUpdatingLocation()
            }else if apiType == "Dropoff" {
                print("")
//                LocationService.sharedInstance.trip_id = ""
//                LocationService.sharedInstance.stopUpdatingLocation()
                locationManager.stopUpdatingLocation()
                var storyboard = UIStoryboard(name: "Rating", bundle: nil)
                var next = storyboard.instantiateViewController(withIdentifier: "Rating") as! Rating
                next.trip_id = bookingInfo!.trip_id
                next.booking_id = (bookingInfo?.booking_id)!
                next.from_page = "HistoryDetails"
                self.present(next, animated: true, completion: nil)
//
            }
            else if apiType == "Cancel" {
                
                for subview in buttonStackView.subviews {
                    buttonStackView.removeArrangedSubview(subview)
                    subview.removeFromSuperview()
                }
                let backButton = UIButton()
                backButton.setTitle("Back", for: .normal)
                backButton.backgroundColor = UIColor.white
                backButton.setTitleColor(UIColor.gray, for: .normal)
                
                buttonStackView.addArrangedSubview(backButton)
                if let amount = responseData["amount"] as? String{
                    let alert = UIAlertView(title: "", message: "You have charged \(amount) for cancelling this ride.", delegate: self, cancelButtonTitle: "Ok")
                    backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
                    alert.show()
                }
                else{
                    backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
                }
            }else if apiType == "SaveLocation"{
                locationManager.startUpdatingLocation()
//                LocationService.sharedInstance.startUpdatingLocation()
            }else if apiType == "BookingPrice" {
                let data = responseData["data"] as! NSDictionary
                let tripDtl = data.object(forKey: "booking_details") as! NSDictionary
                
                self.start_time = tripDtl.object(forKey: "start") as! String
                self.rating = tripDtl.object(forKey: "rating") as! Double

                if bookingInfo!.trip_status != "Cancelled" && bookingInfo!.trip_status != "Pending" {
                    let price = tripDtl.object(forKey: "cost") as! String
//                    cost.text = "$ " + price
                }
                if let req = request {
                    let price = tripDtl.object(forKey: "cost") as! String
//                    cost.text = "$ " + price
                }
                
            }else if apiType == "TripPrice" {
                let data = responseData["data"] as! NSDictionary
                let tripDtl = data.object(forKey: "booking_details") as! NSDictionary
                self.start_time = tripDtl.object(forKey: "start") as! String
                self.rating = tripDtl.object(forKey: "rating") as! Double
                if bookingInfo!.trip_status != "Cancelled" && bookingInfo!.trip_status != "Pending" {
                    let driverShare = tripDtl.object(forKey: "driver_share") as! String
//                    cost.text = "$ " + driverShare
                }
                if let req = request {
                    let driverShare = tripDtl.object(forKey: "driver_share") as! String
//                    cost.text = "$ " + driverShare
                }
            }else if apiType == "FetchLocation" {
                print("fetched")
                let lat = (responseData["data"] as! NSDictionary).object(forKey: "latitude") as! String
                let long = (responseData["data"] as! NSDictionary).object(forKey: "longitude") as! String
                self.showMap(withLongitude: Double(long)!, latitude: Double(lat)!)
            }
        case 600:
            if apiType == "SaveLocation" {
                locationManager.stopUpdatingLocation()
                LocationService.sharedInstance.stopUpdatingLocation()
            }else if apiType == "Cancel"{
                var message = responseData["message"] as! String
                DVUtility.displayAlertBoxTitle("", withMessage: "\(message)")
            }else if apiType == "cancelRequest"{
                var message = responseData["message"] as! String
                DVUtility.displayAlertBoxTitle("", withMessage: "\(message)")
            }
            else {
                print("sunil")
                DVUtility.displayAlertBoxTitle("", withMessage: "There is no trip at this moment.")
                }
        case 101:
            if apiType == "Cancel" {
                
                for subview in buttonStackView.subviews {
                    buttonStackView.removeArrangedSubview(subview)
                    subview.removeFromSuperview()
                }
                let backButton = UIButton()
                backButton.setTitle("Back", for: .normal)
                backButton.backgroundColor = UIColor.white
                backButton.setTitleColor(UIColor.gray, for: .normal)
                
                buttonStackView.addArrangedSubview(backButton)
                if let amount = responseData["amount"] as? String{
                    let alert = UIAlertView(title: "", message: "You have charged $\(amount) for cancelling this ride.", delegate: self, cancelButtonTitle: "Ok")
                    backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
                    alert.show()
                }
            }
            else{
                locationManager.stopUpdatingLocation()
                LocationService.sharedInstance.stopUpdatingLocation()
            }
        default:
            print("sunil")

        }
    }
    
    func gotoAlloted() {
        let story = UIStoryboard(name: "MyTrips", bundle: nil)
        let tripNav = story.instantiateViewController(withIdentifier: "MyTripsNav") as! UINavigationController
        let storyboard = UIStoryboard.init(name: "HistoryDetails", bundle: nil)
        let details = storyboard.instantiateViewController(withIdentifier: "HistoryDetails") as! HistoryDetails
        bookingInfo?.trip_status = "Alloted"
        details.bookingInfo = bookingInfo
        self.menuContainerViewController.centerViewController = tripNav
        tripNav.pushViewController(details, animated: true)
    }

    
//******************************************** Estimated Price ******************************************   
    
    func estimatedPrice() {
        
        let jsonDict: NSDictionary = ["DV": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()

        DVUtility.disaplyWaitMessage()
        if id == bookingInfo?.c_user_id {
            networkCommunication.startNetworkRequest("http://221.121.153.107/payment/booking/receipt/\(id)/\(bookingInfo!.booking_id)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "BookingPrice")
        }else if id == bookingInfo?.d_user_id{
            networkCommunication.startNetworkRequest("http://221.121.153.107/payment/trip/receipt/\(id)/\(bookingInfo!.trip_id)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "TripPrice")
        }
    }

    
//******************************************** Location Update ******************************************
    
    func setUpLocationUpdate() {
        let code = CLLocationManager.authorizationStatus()
        if code == .notDetermined {
            // choose one request according to your business.
            if (Bundle.main.object(forInfoDictionaryKey: "NSLocationAlwaysUsageDescription") != nil) {
                LocationService.sharedInstance.locationManager?.requestAlwaysAuthorization()
            }
            else if (Bundle.main.object(forInfoDictionaryKey: "NSLocationWhenInUseUsageDescription") != nil) {
                LocationService.sharedInstance.locationManager?.requestWhenInUseAuthorization()
            }
            else {
                print("Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription")
            }
        }
        LocationService.sharedInstance.trip_id = bookingInfo!.trip_id
        LocationService.sharedInstance.startUpdatingLocation()
        
    }
    
//******************************************** Fetch Location ******************************************
    
    func fetchLocation(_ timer: Timer) {
        let jsonDict: NSDictionary = ["DV": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        DVUtility.disaplyWaitMessage()
        networkCommunication.startNetworkRequest("http://221.121.153.107/trip/location/\(id)/\(bookingInfo!.trip_id))", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "FetchLocation")
    }
    
    func showMap(withLongitude longitude: CLLocationDegrees, latitude: CLLocationDegrees) {
        marker.map = nil
        let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 16)
        mapScreen.isMyLocationEnabled = true
        mapScreen.camera = camera
        marker.position = CLLocationCoordinate2DMake(latitude, longitude)

        marker.map = mapScreen
    }
    
    @IBAction func onClickOfViewProfile(_ sender: UIButton) {
        self.start_time = (bookingInfo?.t_start as? String)!
        if self.page == "Cancelled" || self.page == "Pending"{
            print("hii")
            return
        }
        if bookingInfo!.trip_status != "Pending"{
            
            let shared = SharedPreferences()
            if  shared.getUserId() == bookingInfo!.c_user_id {
                
                let storyboard = UIStoryboard(name: "PersonDetails", bundle: nil)
                let disc = storyboard.instantiateViewController(withIdentifier: "PersonDetails") as! PersonDetails
                disc.viewingType = "fromhistory"
                disc.page = self.page
                disc.role = (bookingInfo?.d_role)!
                disc.gen = (bookingInfo?.d_gender)!
                disc.stu = (bookingInfo?.d_study)!
                disc.d_user_id = bookingInfo?.d_user_id
                disc.c_user_id = bookingInfo?.c_user_id
                disc.name = (bookingInfo?.d_firstname)! + " " + (bookingInfo?.d_lastname)!
                disc.avr = (bookingInfo?.d_avatar)!
                disc.interest_string = (bookingInfo?.interests)!
                disc.departure = self.start_time
                disc.rating = Double((bookingInfo?.rating)!)!
                disc.v_color = (bookingInfo?.v_color)!
                disc.v_model = (bookingInfo?.vehicle)!
                disc.v_number = (bookingInfo?.plate)!
               
                self.navigationController?.pushViewController(disc, animated: true)
            
                
            }else {

                let storyboard = UIStoryboard(name: "PersonDetails", bundle: nil)
                let disc = storyboard.instantiateViewController(withIdentifier: "PersonDetails") as! PersonDetails
                disc.viewingType = "fromhistory"
                disc.page = self.page
                disc.role = (bookingInfo?.c_role)!
                disc.interest_string = (bookingInfo?.interests)!
                disc.rating = Double((bookingInfo?.rating)!)!
                disc.gen = (bookingInfo?.c_gender)!
                disc.stu = (bookingInfo?.c_study)!
                disc.d_user_id = bookingInfo?.d_user_id
                disc.c_user_id = bookingInfo?.c_user_id
                disc.name = (bookingInfo?.c_firstname)! + " " + (bookingInfo?.c_lastname)!
                disc.departure = self.start_time
                disc.avr = (bookingInfo?.c_avatar)!
                disc.v_color = (bookingInfo?.v_color)!
                disc.v_model = (bookingInfo?.vehicle)!
                disc.v_number = (bookingInfo?.plate)!

                
                self.navigationController?.pushViewController(disc, animated: true)

            }

            
        }
        else{
            print("sunil")
            if self.pageFrom != "MyTrips"{
            let shared = SharedPreferences()
            if  shared.getUserId() == bookingInfo!.c_user_id {
                
                let storyboard = UIStoryboard(name: "PersonDetails", bundle: nil)
                let disc = storyboard.instantiateViewController(withIdentifier: "PersonDetails") as! PersonDetails
                disc.viewingType = "fromhistory"
                disc.page = self.page
                disc.pageFrom = self.pagefrom
                if self.pageFrom == "Myrequest"{
                    disc.currentTitle = self.currentTitle
                }
                disc.role = (bookingInfo?.d_role)!
                disc.gen = (bookingInfo?.d_gender)!
                disc.stu = (bookingInfo?.d_study)!
                disc.name = (bookingInfo?.d_firstname)! + " " + (bookingInfo?.d_lastname)!
                disc.avr = (bookingInfo?.d_avatar)!
                disc.d_user_id = bookingInfo?.d_user_id
                disc.c_user_id = bookingInfo?.c_user_id
                disc.rating = Double((bookingInfo?.rating)!)!
                disc.interest_string = (bookingInfo?.interests)!
                disc.v_model = (bookingInfo?.vehicle)!
                disc.v_number = (bookingInfo?.plate)!
                disc.v_color = (bookingInfo?.v_color)!
                self.navigationController?.pushViewController(disc, animated: true)
            }else {
                let storyboard = UIStoryboard(name: "PersonDetails", bundle: nil)
                let disc = storyboard.instantiateViewController(withIdentifier: "PersonDetails") as! PersonDetails
                disc.viewingType = "fromhistory"
                disc.page = self.page
                disc.pageFrom = self.pageFrom
                if self.pageFrom == "Myrequest"{
                    disc.currentTitle = self.currentTitle
                }
                disc.role = (bookingInfo?.c_role)!
                disc.rating = Double((bookingInfo?.rating)!)!
                disc.gen = (bookingInfo?.c_gender)!
                disc.d_user_id = bookingInfo?.d_user_id
                disc.c_user_id = bookingInfo?.c_user_id
                disc.stu = (bookingInfo?.c_study)!
                disc.name = (bookingInfo?.c_firstname)! + " " + (bookingInfo?.c_lastname)!
                disc.avr = (bookingInfo?.c_avatar)!
                disc.interest_string = (bookingInfo?.interests)!
                disc.v_model = (bookingInfo?.vehicle)!
                disc.v_number = (bookingInfo?.plate)!
                disc.v_color = (bookingInfo?.v_color)!
                self.navigationController?.pushViewController(disc, animated: true)
                
            }
            }
            else{
                print("pending phase")
            }
        }
       
    }
    func msgbox(){
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 30))
        backButton.setBackgroundImage(UIImage(named: "normalmsg.png"), for: .normal)
//        backButton.addTarget(self, action: "action:", for: .touchUpInside)
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backButton)
//            UIImage(named: "email1.png")?.withRenderingMode(.alwaysOriginal)
//        self.navigationItem.rightBarButtonItem.
        let message = bookingInfo?.message
        let alert = UIAlertView(title: "Message", message: "\(message!)", delegate: self, cancelButtonTitle: "Ok")
        alert.show()
//        bookingInfo?.message = ""
    }
//    func startUpdate(){
//        locationManager.startUpdatingLocation()
//    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        
        // manager.stopUpdatingLocation()
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        let lat = String(userLocation.coordinate.latitude)
        let long = String(userLocation.coordinate.longitude)
        saveLocation(lat, long)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    func saveLocation(_ latitude: String, _ longitude: String) {
        let jsonDict: NSDictionary = ["trip_id": bookingInfo!.trip_id,
                                      "latitude": latitude,
                                      "longitude": longitude]
        let shared = SharedPreferences()
        let id = shared.getUserId()
        print("hitting savelocation api")
        let url = "http://221.121.153.107/trip/savelocation/\(id)"
        self.sendToServer(url: url, apiType: "SaveLocation", jsonDict: jsonDict as! [String : Any], method: "POST")
    }
    
//    func sendToServer(url: String, apiType: String, jsonDict: [String : Any], method: String) {
//        let networkCommunication = NetworkCommunication()
//        networkCommunication.networkDelegate = self
//        networkCommunication.startNetworkRequest(url, jsonData: jsonDict , method: method, apiType: apiType)
//    }
    
//    func success(response: [String: AnyObject], apiType: String) {
//        print(response)
//        self.responseParse(responseData: response , apiType: apiType)
//    }
//    
//    func failure() {
//        print("There was a technical difficulty. please try again")
//    }
    
//    func responseParse(responseData: [String:AnyObject], apiType: String) {
//        let code = responseData["status_code"] as! Int
//        switch code {
//        case 100:
//            print("")
//            self.startUpdatingLocation()
//        case 600:
//            self.stopUpdatingLocation()
//        case 101:
//            self.stopUpdatingLocation()
//        default:
//            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
//        }
//    }
    
}




