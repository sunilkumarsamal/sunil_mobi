//
//  ConfirmScreen.swift
//  DV
//
//  Created by chinmay behera on 31/03/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit


class ConfirmScreen: UIViewController, NetworkProtocol,UITextFieldDelegate {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var estimatedFare: UILabel!
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var dotViewLabel: UIView!
    @IBOutlet weak var request_message: UITextField!
    
    var message = String()
    var info:NSDictionary?

    var book_id:String?
    
    var detail: NSDictionary?
    var booking_id: String?
    var trip_id: String?
    
    var pro: UIVisualEffectView?
    
    @IBOutlet weak var confirmButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.request_message.delegate = self
        profileImage.layer.borderWidth = 4
        profileImage.layer.borderColor = UIColor.white.cgColor
        // Do any additional setup after loading the view.
        let shared = SharedPreferences()
        let type = shared.getUserType()
        if self.trip_id == ""{
            booking_id = book_id!
            detail = info!.object(forKey: "trip_details") as? NSDictionary
            trip_id = detail?.object(forKey: "trip_id") as? String
            var calculation = detail?.object(forKey: "calculation") as! NSDictionary
            var cost  = calculation.object(forKey: "cost") as! String
            self.estimatedFare.text = "$"+cost


            
        }
        else if self.book_id == ""{
            detail = info!.object(forKey: "booking_details") as? NSDictionary
            booking_id = detail?.object(forKey: "booking_id") as? String
            var calculation = detail?.object(forKey: "calculation") as! NSDictionary
            var d_share  = calculation.object(forKey: "driver_share") as! String
            self.estimatedFare.text = "$"+d_share
        }
        
        if detail!.object(forKey: "firstname") is NSNull {
            name.text = "Vin Diesel"
        }else {
            name.text = detail!.object(forKey: "firstname") as! String?
        }
        from.text = detail!.object(forKey: "from") as! String?
        to.text = detail!.object(forKey: "to") as! String?
        print(detail!.object(forKey: "avatar") as! String)
        let profileUrl = (detail!.object(forKey: "avatar") as! String?)!
        if profileUrl.contains("avatar"){
        let ImagePath = "http://221.121.153.107/public/" + (detail!.object(forKey: "avatar") as! String?)!
        let url = URL(string: ImagePath)
        DispatchQueue.global(qos: .background).async {
            let data = try? Data(contentsOf: url!)
            DispatchQueue.main.async {
                if data != nil {
                    self.profileImage.image = UIImage(data: data!)
                }else {
                    self.profileImage.image = UIImage(named: "avatar")
                }
            }
        }
        }else{
            let ImagePath = (detail!.object(forKey: "avatar") as! String?)!
            let url = URL(string: ImagePath)
            DispatchQueue.global(qos: .background).async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                    if data != nil {
                        self.profileImage.image = UIImage(data: data!)
                    }else {
                        self.profileImage.image = UIImage(named: "avatar")
                    }
                }
            }
        }
//        self.estimatedPrice()
       
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    

    @IBAction func onclickConfirm(_ sender: UIButton) {
        DVUtility.disaplyWaitMessage()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        if sender.titleLabel?.text == "Send Request" {
            self.message = self.request_message.text!
            self.confirm()
            
        }else {
            self.cancelBookingOrTrip()
        }
        
    }
    
    
    @IBAction func onClickClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func estimatedPrice() {
        let reachability = Reachability1()
        if reachability!.isReachable || reachability!.isReachableViaWiFi || reachability!.isReachableViaWWAN
        {

        DVUtility.disaplyWaitMessage()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let jsonDict: NSDictionary = ["DV": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        print(book_id!)
        print(trip_id!)
        let userType = shared.getUserType()
        if userType == "Commutor" || userType == "Commuter" || userType == "commuter"{
            networkCommunication.startNetworkRequest("http://221.121.153.107/payment/booking/receipt/\(id)/\(book_id!)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "BookingPrice")
        }else {
            networkCommunication.startNetworkRequest("http://221.121.153.107/payment/booking/receipt/\(id)/\(trip_id!)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "TripPrice")
        }
        }
        else{
            let alert = UIAlertView(title: "Unreachable", message: "Please make sure internet is connected and retry", delegate: self, cancelButtonTitle: "retry")
            alert.tag = 100
            alert.show()
        }
    }
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if alertView.tag == 100{
            self.estimatedPrice()
            return
        }
        else if alertView.tag == 101{
            self.confirm()
            return
        }
        else if alertView.tag == 102{
            self.cancelBookingOrTrip()
            return
        }
    }

    func confirm() {
        let reachability = Reachability1()
        if reachability!.isReachable || reachability!.isReachableViaWiFi || reachability!.isReachableViaWWAN
        {
        let jsonDict: NSDictionary = ["booking_id": booking_id!,
                                      "trip_id": trip_id!,"message": "\(self.message)"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        print(book_id!)
            networkCommunication.startNetworkRequest("http://221.121.153.107/booking/request/\(id)", jsonData: jsonDict as! [String : Any], method: "POST", apiType: "ConfirmBooking")
        }
        else{
            let alert = UIAlertView(title: "Unreachable", message: "Please make sure internet is connected and retry", delegate: self, cancelButtonTitle: "retry")
            alert.tag = 101
            alert.show()
        }
    }
    
    func cancelBookingOrTrip() {
        let reachability = Reachability1()
        if reachability!.isReachable || reachability!.isReachableViaWiFi || reachability!.isReachableViaWWAN
        {
        let jsonDict: NSDictionary = ["booking_id": book_id!]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        print(book_id!)
        let userType = shared.getUserType()
        if userType == "Commutor" {
            let jsonDict: NSDictionary = ["booking_id": book_id!]
            networkCommunication.startNetworkRequest("http://221.121.153.107/booking/cancel/\(id)", jsonData: jsonDict as! [String : Any], method: "POST", apiType: "Cancel")
        }else {
            let jsonDict: NSDictionary = ["trip_id": book_id!]
            networkCommunication.startNetworkRequest("http://221.121.153.107/trip/cancel/\(id)", jsonData: jsonDict as! [String : Any], method: "POST", apiType: "Cancel")
        }
        }
        else{
            let alert = UIAlertView(title: "Unreachable", message: "Please make sure internet is connected and retry", delegate: self, cancelButtonTitle: "retry")
            alert.tag = 102
            alert.show()
        }
    }
    
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {

        DVUtility.hideWaitMessage()
        UIApplication.shared.endIgnoringInteractionEvents()
        
        print("There was a technical difficulty. please try again")
        let alert = UIAlertView(title: "", message: "There was a technical difficulty. Please try again by going back.", delegate: self, cancelButtonTitle: "Ok")
        alert.show()
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        DVUtility.hideWaitMessage()
        UIApplication.shared.endIgnoringInteractionEvents()
        
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            if apiType == "ConfirmBooking" {
                confirmButton.setTitle("Cancel", for: .normal)
                var name = detail!.object(forKey: "firstname") as! String?

                let alert = UIAlertController(title: "", message:"Your request has been sent to \(name!)", preferredStyle: UIAlertControllerStyle.alert)
                let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel,handler: { (action) -> Void in
                   let nav = self.presentingViewController?.menuContainerViewController.centerViewController as! UINavigationController
                     self.dismiss(animated: true, completion: {() -> Void in
                        nav.popViewController(animated: true)})
                })
                alert.addAction(okButton)
                self.present(alert, animated: true, completion: nil)

            }else if apiType == "Cancel"{
                let alert = UIAlertView(title: "", message: "Booking is cancelled.", delegate: self, cancelButtonTitle: "Ok")
                alert.show()

                let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
                let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
                let story = UIStoryboard(name: "PickDrop", bundle: nil)
                let PickDrop = story.instantiateViewController(withIdentifier: "PickUpDropOff") as! PickUpDropOff
                let navVC:UINavigationController = story.instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
                navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
                
                let container: MFSideMenuContainerViewController = story.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
                container.leftMenuViewController = leftVC
                container.centerViewController = navVC
                container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
                self.present(container, animated: true, completion: nil)
            }else if apiType == "BookingPrice" {
                let data = responseData["data"] as! NSDictionary
                let tripDtl = data.object(forKey: "booking_details") as! NSDictionary
                let cost = tripDtl.object(forKey: "cost") as! String
                estimatedFare.text = "$ " + cost
            }else if apiType == "TripPrice" {
                let data = responseData["data"] as! NSDictionary
                let tripDtl = data.object(forKey: "booking_details") as! NSDictionary
                let driverShare = tripDtl.object(forKey: "driver_share") as! String
                estimatedFare.text = "$ " + driverShare
            }
            print("")
            
        case 600:
            DVUtility.displayAlertBoxTitle("", withMessage: "There are no trips on the mentioned time.")
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }
}
