//
//  AppDelegate.swift
//  DV
//
//  Created by sunil on 30/12/16.
//  Copyright © 2016 Six30labs. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps
import GooglePlaces
import FBSDKLoginKit
import Stripe
import Fabric
import Crashlytics

protocol ChatProtocol: class {
    func notificationReceivedForChat(_ chatInfo: NSDictionary)
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate, NetworkProtocol, UIAlertViewDelegate,CLLocationManagerDelegate {

    var window: UIWindow?
    var badgeCount:Int = 0
    var chattingInfo: NSDictionary?
    var req_id: String?
    var notification_book_id: String?
    var notification_trip_id: String?
    var notification_c_user_id: String?
    var notification_d_user_id: String?
    var trip_id = String()
    var booking_id = String()
    var d_id = String()
    var c_id = String()
    var locationManager: CLLocationManager?
    weak var chatDelegate: ChatProtocol?

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self])
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
//        IQKeyboardManager.sharedManager.enableAutoToolbar = false
//        IQKeyboardManager.sharedManager().shouldShowTextFieldPlaceholder = false
//        IQKeyboardManager.sharedManager().shouldHidePreviousNext = false
        UIApplication.shared.applicationIconBadgeNumber = 0
       //STPPaymentConfiguration.shared().publishableKey = "pk_test_F4lATL7CtAQ6wPUOchMhcmgJ"
        STPPaymentConfiguration.shared().publishableKey = "pk_live_BlZxLDzYlPfxrmEfhHOUh1Kl"
        GMSServices.provideAPIKey("AIzaSyBKyf2ONlscmGJSVDiODFR2Dlsc7jHOZAg")
        GMSPlacesClient.provideAPIKey("AIzaSyDVXi4Z1GhpEGnGCISO8huo0mstF4KF8UA")
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
                var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        assert(configureError == nil, "Error configuring Google services: \(configureError)")
        
        GIDSignIn.sharedInstance().delegate = self
        
        self.registerForNotification()
//        do {
//            Network.reachability = try Reachability(hostname: "www.google.com")
//            do {
//                try Network.reachability?.start()
//            } catch let error as Network.Error {
//                print(error)
//            } catch {
//                print(error)
//            }
//        } catch {
//            print(error)
//        }
        
        return true
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL!, sourceApplication: sourceApplication, annotation: annotation) || GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func application(application: UIApplication,
                     openURL url: NSURL, options: [String: AnyObject]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url as URL!, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication.rawValue] as? String, annotation: options[UIApplicationOpenURLOptionsKey.annotation.rawValue])
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            // ...
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        LocationService.sharedInstance.startUpdatingLocation()
//        HistoryDetails.startUpdate()
//        LocationService.sharedInstance.stopUpdatingLocation()
        var bgTask: UIBackgroundTaskIdentifier = 0
        let app = UIApplication.shared
        bgTask = app.beginBackgroundTask(expirationHandler: {() -> Void in
            app.endBackgroundTask(bgTask)
            bgTask = UIBackgroundTaskInvalid
        })
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        guard let locationManager = self.locationManager else {
            return
        }

        self.locationManager?.startUpdatingLocation()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        //self.saveContext()
    }
    
//*****************************************Register Notification****************************************
    
    func registerForNotification() {
        let setting = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(setting)
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token: String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        print(token)
        let shared = SharedPreferences()
        shared.saveToken(token)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("register error\(error.localizedDescription)")
    }
    
    //called when local notification is being fired
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        print(notification.userInfo!)
        var x = notification.userInfo as! [String: AnyObject]
        var y = x as! NSDictionary
        if let z = y.object(forKey: "notification"){
            if y.object(forKey: "notification") as! String == "local" {
                let alert = UIAlertView(title: "Trip Reminder", message: "Your trip is coming soon.", delegate: self, cancelButtonTitle: "Ok")
//                alert.tag = 105
                alert.show()
                return
            }
        }
        let userInfo = notification.userInfo! as! [String: AnyObject]
        chattingInfo = notification.userInfo! as NSDictionary
        let notification_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "notification_id") as! Double
        let state: UIApplicationState = application.applicationState
        if state == .active {
            var alert: UIAlertView?
            switch notification_id {
            case 700:
                let message = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "message") as! String
                let name = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "sender_name") as! String
                alert = UIAlertView(title: name, message: message, delegate: self, cancelButtonTitle:nil, otherButtonTitles: "Cancel", "Ok")
                alert?.tag = 100
                alert?.show()
            case 701:
                let request_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "request_id") as! NSNumber
                req_id = "\(request_id)"
                let message = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "message") as! String
                alert = UIAlertView(title: message, message: "", delegate: self, cancelButtonTitle:nil, otherButtonTitles: "Dismiss", "See Request")
                alert?.tag = 101
                alert?.show()
            case 702,703,704,705,706,707,708:
                notification_book_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "booking_id") as? String
                notification_trip_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "trip_id") as? String
                notification_c_user_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "c_user_id") as? String
                notification_d_user_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "d_user_id") as? String
                let message = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "message") as! String
                alert = UIAlertView(title: message, message: "", delegate: self, cancelButtonTitle:nil, otherButtonTitles: "Dismiss", "See Details")
                alert?.tag = 102
                alert?.show()
            default:
                print("")
            }
        }

        application.applicationIconBadgeNumber = 0
    }
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print("notification information: \(userInfo)")
        
        if application.applicationState == UIApplicationState.active {
            let shared = SharedPreferences()
            let notification_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "notification_id") as! Double
            self.booking_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "booking_id") as! String
//            if notification_id != 700 || notification_id != 701{
//                self.trip_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "trip_id") as! String
//                
//                self.d_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "d_user_id") as! String
//                self.c_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "c_user_id") as! String
//            }
            switch notification_id {
            case 700:
                if let delegate = chatDelegate {
                    delegate.notificationReceivedForChat(userInfo as NSDictionary)
                }else {
                    DVUtility.createLocalNotification(userInfo as! [String: AnyObject])
                }
            case 701:
                DVUtility.createLocalNotification(userInfo as! [String: AnyObject])
            case 702,703,704,706,707,708:
                self.trip_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "trip_id") as! String
                
                self.d_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "d_user_id") as! String
                self.c_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "c_user_id") as! String
                let book_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "booking_id") as! String
                let trip_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "trip_id") as! String
                let c_user_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "c_user_id") as! String
                let d_user_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "d_user_id") as! String
                if shared.getUserId() == c_user_id {
                    self.fetchBookingList(bookTripId: book_id)
                }else if shared.getUserId() == d_user_id {
                    self.fetchBookingList(bookTripId: trip_id)
                }
//                DVUtility.createLocalNotification(userInfo)
            case 705:
                self.trip_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "trip_id") as! String
                
                self.d_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "d_user_id") as! String
                self.c_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "c_user_id") as! String
                print("payment successful")
                let alert = UIAlertView(title: "Payment has been done", message: "", delegate: self, cancelButtonTitle:nil, otherButtonTitles: "Ok")
                alert.tag = 103
                alert.show()
            case 800:
                self.trip_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "trip_id") as! String
                
                self.d_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "d_user_id") as! String
                self.c_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "c_user_id") as! String
                print("payment failed")
                let alert = UIAlertView(title: "Payment Failed", message: "Due to some technical reason ", delegate: self, cancelButtonTitle: nil, otherButtonTitles: "See Details")
                alert.tag = 104
                alert.show()
            case 710:
                self.trip_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "trip_id") as! String
                
                self.d_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "d_user_id") as! String
                self.c_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "c_user_id") as! String
                print("trip cancelled")
                var message = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "message") as! String
                let alert = UIAlertView(title: "Trip Cancelled", message: message, delegate: self, cancelButtonTitle: nil, otherButtonTitles: "Ok")
                alert.show()
            case 799:
                self.trip_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "trip_id") as! String
                
                self.d_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "d_user_id") as! String
                self.c_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "c_user_id") as! String
                var message = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "message") as! String
                let alert = UIAlertView(title: "Request Cancelled", message: message , delegate: self, cancelButtonTitle: "Ok")
                alert.tag = 200
                alert.show()
            default:
                print("Default case")
            }
        }
        if application.applicationState == UIApplicationState.background || application.applicationState == UIApplicationState.inactive {
            UIApplication.shared.applicationIconBadgeNumber  = 1+badgeCount
            let shared = SharedPreferences()
            let notification_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "notification_id") as! Double
//            if notification_id != 700 || notification_id != 701{
//                self.trip_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "trip_id") as! String
//                
//                self.d_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "d_user_id") as! String
//                self.c_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "c_user_id") as! String
//            }
            switch notification_id {
            case 700:
                let chat_book_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "booking_id") as! String
                let chat_sender_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "sender_id") as! String
                let chat_receiver_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "receiver_id") as! String
                self.gotoChatScreen(book_id: chat_book_id, sender: chat_sender_id, receiver: chat_receiver_id)
            case 701:
                let request_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "request_id") as! NSNumber
                let req = "\(request_id)"
                self.fetchRequestDetails(requestId: req)
            case 702,703,704,706,707,708:
                self.trip_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "trip_id") as! String
                
                self.d_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "d_user_id") as! String
                self.c_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "c_user_id") as! String
                let book_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "booking_id") as! String
                let trip_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "trip_id") as! String
                let c_user_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "c_user_id") as! String
                let d_user_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "d_user_id") as! String
                if shared.getUserId() == c_user_id {
                    self.fetchBookingList(bookTripId: book_id)
                }else if shared.getUserId() == d_user_id {
                    self.fetchBookingList(bookTripId: trip_id)
                }
            case 705:
                self.trip_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "trip_id") as! String
                
                self.d_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "d_user_id") as! String
                self.c_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "c_user_id") as! String
                print("payment successful")
                let alert = UIAlertView(title: "Payment has been done", message: "", delegate: self, cancelButtonTitle:nil, otherButtonTitles: "Ok")
                alert.tag = 103
                alert.show()
            case 799:
                self.trip_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "trip_id") as! String
                
                self.d_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "d_user_id") as! String
                self.c_id = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "c_user_id") as! String
                var message = ((userInfo["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "message") as! String
                let alert = UIAlertView(title: "Request Cancelled", message: message, delegate: self, cancelButtonTitle: "Ok")
                alert.tag = 200
                alert.show()
                
            
            default:
                print("Default case")
            }

        }
        
    }
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if alertView.tag == 103{
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Rating", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "Rating") as! Rating
            initialViewController.booking_id = self.booking_id
            initialViewController.trip_id = self.trip_id
            initialViewController.d_id = self.d_id
            initialViewController.c_id = self.c_id
            initialViewController.from_page = "Notification"
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
            
            return
        }
        else if alertView.tag == 104{
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "PaymentFailure", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "PaymentFailure") as! PaymentFailure
            initialViewController.booking_id = self.booking_id
            initialViewController.trip_id = self.trip_id
            initialViewController.d_id = self.d_id
            initialViewController.c_id = self.c_id

            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
            
            return
        }
        else if alertView.tag == 105{
            let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
            
            let storyTrip = UIStoryboard(name: "PickDrop", bundle: nil)
            let reqVC:UINavigationController = storyTrip.instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
            
            
            let storyCont = UIStoryboard(name: "PickDrop", bundle: nil)
            let container: MFSideMenuContainerViewController = storyCont.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
            
            container.leftMenuViewController = leftVC
            container.centerViewController = reqVC
            container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
            self.window?.rootViewController = container
            reqVC.pushViewController(reqVC, animated: true)
        }
        else if alertView.tag == 200 {
            print("pending to do")
        }
        else if alertView.tag == 100 {
            if buttonIndex == 1 {
                let chat_book_id = ((chattingInfo!["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "booking_id") as! String
                let chat_sender_id = ((chattingInfo!["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "sender_id") as! String
                let chat_receiver_id = ((chattingInfo!["custom"] as! NSDictionary).object(forKey: "a") as! NSDictionary).object(forKey: "receiver_id") as! String
                self.gotoChatScreen(book_id: chat_book_id, sender: chat_sender_id, receiver: chat_receiver_id)
            }
        }
        else if buttonIndex == 1 {
            if alertView.tag == 101 {
                self.fetchRequestDetails(requestId: req_id!)
            }
            else if alertView.tag == 102{
                let shared = SharedPreferences()
                if shared.getUserId() == notification_c_user_id {
                    self.fetchBookingList(bookTripId: notification_book_id!)
                }else if shared.getUserId() == notification_d_user_id {
                    self.fetchBookingList(bookTripId: notification_trip_id!)
                }
            }
        }
    
        
    }

    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "DV")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    @available(iOS 10.0, *)
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func fetchRequestDetails(requestId: String) {
        
        DVUtility.disaplyWaitMessage()
        
        let jsonDict: NSDictionary = ["dv": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
//        let req = "\(requestId)"
        
        networkCommunication.startNetworkRequest("http://221.121.153.107/user/request/\(id)/\(requestId)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "BookingDetails")
        
    }
    
    func fetchBookingList(bookTripId: String) {
        
        DVUtility.disaplyWaitMessage()
        
        let jsonDict: NSDictionary = ["dv": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        let userType = shared.getUserType()
        if id == self.c_id{
            networkCommunication.startNetworkRequest("http://221.121.153.107/booking/details/\(id)/\(bookTripId)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "BookingDetails")
        }
        else {
            networkCommunication.startNetworkRequest("http://221.121.153.107/trip/details/\(id)/\(bookTripId)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "BookingDetails")
        }
        
    }
    
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
        DVUtility.hideWaitMessage()
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        
        DVUtility.hideWaitMessage()
        
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            print("")
            self.storeTripDetails(details: responseData["data"] as! NSDictionary)
            
        case 600:
            DVUtility.displayAlertBoxTitle("", withMessage: "There are no trips on mentioned time.")
        default:
            print("")
        }
    }
    
    var bookingList = NSMutableArray()
    
    func storeTripDetails(details: NSDictionary) {
        
        let bookingInfo = BookingInfo()
        
        if details.object(forKey: "booking_id") is NSNull {
            bookingInfo.booking_id = ""
        }else {
            bookingInfo.booking_id = details.object(forKey: "booking_id") as! String
        }
        if details.object(forKey: "b_from") is NSNull {
            bookingInfo.b_from = ""
        }else {
            bookingInfo.b_from = details.object(forKey: "b_from") as! String
        }
        if details.object(forKey: "b_to") is NSNull {
            bookingInfo.b_to = ""
        }else {
            bookingInfo.b_to = details.object(forKey: "b_to") as! String
        }
        if details.object(forKey: "b_start") is NSNull {
            bookingInfo.b_start = ""
        }else {
            bookingInfo.b_start = details.object(forKey: "b_start") as! String
        }
        if details.object(forKey: "b_end") is NSNull {
            bookingInfo.b_end = ""
        }else {
            bookingInfo.b_end = details.object(forKey: "b_end") as! String
        }
        if details.object(forKey: "b_from_latlng") is NSNull {
            bookingInfo.b_from_latlng = ""
        }else {
            bookingInfo.b_from_latlng = details.object(forKey: "b_from_latlng") as! String
        }
        if details.object(forKey: "b_to_latlng") is NSNull {
            bookingInfo.b_to_latlng = ""
        }else {
            bookingInfo.b_to_latlng = details.object(forKey: "b_to_latlng") as! String
        }
        
        
        if details.object(forKey: "c_address") is NSNull {
            bookingInfo.c_address = ""
        }else {
            bookingInfo.c_address = details.object(forKey: "c_address") as! String
        }
        if details.object(forKey: "c_avatar") is NSNull {
            bookingInfo.c_avatar = ""
        }else {
            bookingInfo.c_avatar = details.object(forKey: "c_avatar") as! String
        }
        if details.object(forKey: "c_contact") is NSNull {
            bookingInfo.c_contact = ""
        }else {
            bookingInfo.c_contact = details.object(forKey: "c_contact") as! String
        }
        if details.object(forKey: "c_email") is NSNull {
            bookingInfo.c_email = ""
        }else {
            bookingInfo.c_email = details.object(forKey: "c_email") as! String
        }
        if details.object(forKey: "c_firstname") is NSNull {
            bookingInfo.c_firstname = ""
        }else {
            bookingInfo.c_firstname = details.object(forKey: "c_firstname") as! String
        }
        if details.object(forKey: "c_lastname") is NSNull {
            bookingInfo.c_lastname = ""
        }else {
            bookingInfo.c_lastname = details.object(forKey: "c_lastname") as! String
        }
        if details.object(forKey: "c_role") is NSNull {
            bookingInfo.c_role = ""
        }else {
            bookingInfo.c_role = details.object(forKey: "c_role") as! String
        }
        if details.object(forKey: "c_study") is NSNull {
            bookingInfo.c_study = ""
        }else {
            bookingInfo.c_study = details.object(forKey: "c_study") as! String
        }
        if details.object(forKey: "c_user_id") is NSNull {
            bookingInfo.c_user_id = ""
        }else {
            bookingInfo.c_user_id = details.object(forKey: "c_user_id") as! String
        }
        if let interest = details.object(forKey: "interests"){
            if details.object(forKey: "interests") is NSNull{
                bookingInfo.interests = ""
            }
            else{
                bookingInfo.interests = details.object(forKey: "interests") as! String
            }
        }
        if let c_gender = details.object(forKey: "c_gender"){
            if details.object(forKey: "c_gender") is NSNull{
                bookingInfo.c_gender = ""
            }
            else{
                bookingInfo.c_gender = details.object(forKey: "c_gender") as! String
            }
        }
        if let d_gender = details.object(forKey: "d_gender"){
            if details.object(forKey: "d_gender") is NSNull{
                bookingInfo.d_gender = ""
            }
            else{
                bookingInfo.d_gender = details.object(forKey: "d_gender") as! String
            }
        }
        
        if let vehicle = details.object(forKey: "vehicle"){
            if details.object(forKey: "vehicle") is NSNull{
                bookingInfo.vehicle = ""
            }
            else{
                bookingInfo.vehicle = details.object(forKey: "vehicle") as! String
            }
        }
        if let plate = details.object(forKey: "plate"){
            if details.object(forKey: "plate") is NSNull{
                bookingInfo.plate = ""
            }
            else{
                bookingInfo.plate = details.object(forKey: "plate") as! String
            }
        }
        if let colour = details.object(forKey: "colour"){
            if details.object(forKey: "colour") is NSNull{
                bookingInfo.v_color = ""
            }
            else{
                bookingInfo.v_color = details.object(forKey: "colour") as! String
            }
        }
        if let message = details.object(forKey: "message"){
            if details.object(forKey: "message") is NSNull{
                bookingInfo.message = ""
            }
            else{
                bookingInfo.message = details.object(forKey: "message") as! String
            }
        }
        
        if let cal = details.object(forKey: "calculation"){
            if details.object(forKey: "calculation") is NSNull{
              print("calculation not coming")
            }
            else{
                var calculation = details.object(forKey: "calculation") as! NSDictionary
                if let cos = calculation.object(forKey: "cost"){
                    if calculation.object(forKey: "cost") is NSNull{
                        bookingInfo.cost = ""
                    }
                    else{
                        bookingInfo.cost = calculation.object(forKey: "cost") as! String
                    }
                    
                }
                if let d_share = calculation.object(forKey: "driver_share"){
                    if calculation.object(forKey: "driver_share") is NSNull{
                        bookingInfo.driver_share = ""
                    }
                    else{
                        bookingInfo.driver_share = calculation.object(forKey: "driver_share") as! String
                    }
                    
                }
            }
        }
        if details.object(forKey: "created") is NSNull {
            bookingInfo.created = ""
        }else {
            bookingInfo.created = details.object(forKey: "created") as! String
        }
        if details.object(forKey: "updated") is NSNull {
            bookingInfo.updated = ""
        }else {
            bookingInfo.updated = details.object(forKey: "updated") as! String
        }
        
        
        if details.object(forKey: "trip_id") is NSNull {
            bookingInfo.trip_id = ""
        }else {
            bookingInfo.trip_id = details.object(forKey: "trip_id") as! String
        }
        if details.object(forKey: "trip_status_id") is NSNull {
            bookingInfo.trip_status_id = ""
        }else {
            bookingInfo.trip_status_id = details.object(forKey: "trip_status_id") as! String
        }
        if details.object(forKey: "trip_status") is NSNull {
            bookingInfo.trip_status = ""
        }else {
            bookingInfo.trip_status = details.object(forKey: "trip_status") as! String
        }
        if details.object(forKey: "t_from") is NSNull {
            bookingInfo.t_from = ""
        }else {
            bookingInfo.t_from = details.object(forKey: "t_from") as! String
        }
        if details.object(forKey: "t_to") is NSNull {
            bookingInfo.t_to = ""
        }else {
            bookingInfo.t_to = details.object(forKey: "t_to") as! String
        }
        if details.object(forKey: "t_start") is NSNull {
            bookingInfo.t_start = ""
        }else {
            bookingInfo.t_start = details.object(forKey: "t_start") as! String
        }
        if details.object(forKey: "t_end") is NSNull {
            bookingInfo.t_end = ""
        }else {
            bookingInfo.t_end = details.object(forKey: "t_end") as! String
        }
        if details.object(forKey: "t_from_latlng") is NSNull {
            bookingInfo.t_from_latlng = ""
        }else {
            bookingInfo.t_from_latlng = details.object(forKey: "t_from_latlng") as! String
        }
        if details.object(forKey: "t_to_latlng") is NSNull {
            bookingInfo.t_to_latlng = ""
        }else {
            bookingInfo.t_to_latlng = details.object(forKey: "t_to_latlng") as! String
        }
        
        if details.object(forKey: "rating") is NSNull {
            bookingInfo.rating = ""
        }else {
            var x = details.object(forKey: "rating") as! Int
            bookingInfo.rating = String(x)
        }
        
        if details.object(forKey: "d_address") is NSNull {
            bookingInfo.d_address = ""
        }else {
            bookingInfo.d_address = details.object(forKey: "d_address") as! String
        }
        if details.object(forKey: "d_avatar") is NSNull {
            bookingInfo.d_avatar = ""
        }else {
            bookingInfo.d_avatar = details.object(forKey: "d_avatar") as! String
        }
        if details.object(forKey: "d_contact") is NSNull {
            bookingInfo.d_contact = ""
        }else {
            bookingInfo.d_contact = details.object(forKey: "d_contact") as! String
        }
        if details.object(forKey: "d_email") is NSNull {
            bookingInfo.d_email = ""
        }else {
            bookingInfo.d_email = details.object(forKey: "d_email") as! String
        }
        if details.object(forKey: "d_firstname") is NSNull {
            bookingInfo.d_firstname = ""
        }else {
            bookingInfo.d_firstname = details.object(forKey: "d_firstname") as! String
        }
        if details.object(forKey: "d_lastname") is NSNull {
            bookingInfo.d_lastname = ""
        }else {
            bookingInfo.d_lastname = details.object(forKey: "d_lastname") as! String
        }
        if details.object(forKey: "d_role") is NSNull {
            bookingInfo.d_role = ""
        }else {
            bookingInfo.d_role = details.object(forKey: "d_role") as! String
        }
        if details.object(forKey: "d_study") is NSNull {
            bookingInfo.d_study = ""
        }else {
            bookingInfo.d_study = details.object(forKey: "d_study") as! String
        }
        if details.object(forKey: "d_user_id") is NSNull {
            bookingInfo.d_user_id = ""
        }else {
            bookingInfo.d_user_id = details.object(forKey: "d_user_id") as! String
        }
        
        
        
        if bookingInfo.trip_status == "Pending" {
            let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
            
            let story = UIStoryboard(name: "HistoryDetails", bundle: nil)
            let historyDetails = story.instantiateViewController(withIdentifier: "HistoryDetails") as! HistoryDetails
            historyDetails.bookingInfo = bookingInfo
            historyDetails.request = "Incoming"
            historyDetails.pageFrom = "Myrequest"
            historyDetails.currentTitle = "Incoming"

            let storyTrip = UIStoryboard(name: "MyRequests", bundle: nil)
            let reqVC:UINavigationController = storyTrip.instantiateViewController(withIdentifier: "MyRequestsNav") as! UINavigationController
            
            //        navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
            let storyCont = UIStoryboard(name: "PickDrop", bundle: nil)
            let container: MFSideMenuContainerViewController = storyCont.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
            
            container.leftMenuViewController = leftVC
            container.centerViewController = reqVC
            container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
            self.window?.rootViewController = container
            reqVC.pushViewController(historyDetails, animated: true)
        }else if bookingInfo.trip_status == "Alloted"{

            
            
            self.window = UIWindow(frame: UIScreen.main.bounds)
            
            let storyboard = UIStoryboard(name: "Reminder", bundle: nil)
            
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "Reminder") as! Reminder
            initialViewController.role = "from_notification"
            initialViewController.t_start = bookingInfo.t_start
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
            
            return
            
        }
        else{
            let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
            
            let story = UIStoryboard(name: "HistoryDetails", bundle: nil)
            let historyDetails = story.instantiateViewController(withIdentifier: "HistoryDetails") as! HistoryDetails
            historyDetails.bookingInfo = bookingInfo
            historyDetails.pageFrom = "Myrequest"
            historyDetails.request = "Incoming"
            historyDetails.currentTitle = "Incoming"
            let storyTrip = UIStoryboard(name: "MyTrips", bundle: nil)
            let reqVC:UINavigationController = storyTrip.instantiateViewController(withIdentifier: "MyTripsNav") as! UINavigationController
            
            //        navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
            let storyCont = UIStoryboard(name: "PickDrop", bundle: nil)
            let container: MFSideMenuContainerViewController = storyCont.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
            
            container.leftMenuViewController = leftVC
            container.centerViewController = reqVC
            container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
            self.window?.rootViewController = container
            reqVC.pushViewController(historyDetails, animated: true)

        }

        
    }
    
    func gotoChatScreen(book_id: String, sender: String, receiver:String) {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        
        let story = UIStoryboard(name: "ChatScreen", bundle: nil)
        let chatScreen = story.instantiateViewController(withIdentifier: "ChatScreen") as! ChatScreen
        let bookingInfo = BookingInfo()
        bookingInfo.booking_id = book_id
        chatScreen.bookingInfo = bookingInfo
        chatScreen.sender_id = sender
        chatScreen.receiver_id = receiver
        
        let storyTrip = UIStoryboard(name: "MyTrips", bundle: nil)
        let reqVC:UINavigationController = storyTrip.instantiateViewController(withIdentifier: "MyTripsNav") as! UINavigationController
        
        //        navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
        let storyCont = UIStoryboard(name: "PickDrop", bundle: nil)
        let container: MFSideMenuContainerViewController = storyCont.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
        
        container.leftMenuViewController = leftVC
        container.centerViewController = reqVC
        container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
        self.window?.rootViewController = container
        reqVC.pushViewController(chatScreen, animated: true)
    }

}

