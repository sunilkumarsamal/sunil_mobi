//
//  TripDetails.swift
//  DV
//
//  Created by Chinmay on 08/02/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class TripDetails: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, NetworkProtocol {
    
    @IBOutlet weak var blurView: UIView!
    @IBOutlet var view1: UIView!
    @IBOutlet weak var Days: UICollectionView!
    
    @IBOutlet weak var pickuptime: UITextField!
    
    var allDays = ["S","M","T","W","T","F","S"]
    var date_allday = ["S","M","T","W","T","F","S"]
    var selected: [Int] = [0,0,0,0,0,0,0]
    var today = Date()
    let arr = NSMutableArray()
    var from: String?
    var to: String?
    var fromLat: String?
    var toLat: String?
    var i = Int()
    var dates = Array<String>()
    var date: [String: AnyObject]?
    var jsonDateFormat:String?
    var dt: NSDictionary?
    var selected_dates = NSMutableArray()
    var selected_day = NSMutableArray()
    var deselected_day = NSMutableArray()
    var selected_date = String()
    var from_city_name:String?
    var to_city_name:String?
    var jsonDict1: NSDictionary?
    @IBOutlet weak var fromText: UITextField!
    @IBOutlet weak var toText: UITextField!
    @IBOutlet weak var doneButton: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    weak var activeField: UITextField!
    
    @IBOutlet weak var tripDetailsView: UIView!
    
    var pro:UIVisualEffectView?
    
    var acting: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha  = 0.7
        blurEffectView.frame = self.view.frame

        self.view.insertSubview(blurEffectView, at: 0)
        doneButton.backgroundColor = .white
        doneButton.layer.borderWidth = 1.0;
        doneButton.layer.borderColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0).cgColor;

        self.i = 1
        let date = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "EEEE"//"EE" to get short style
        let dayInWeek = dateFormatter.string(from: date as Date)//"Sunday"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let current = formatter.string(from: date as Date)// current date
        let next1 = Calendar.current.date(byAdding: .day, value: 1, to: today)
        let current_1 = formatter.string(from: next1!)
        let next2 = Calendar.current.date(byAdding: .day, value: 2, to: today)
        let current_2 = formatter.string(from: next2!)
        let next3 = Calendar.current.date(byAdding: .day, value: 3, to: today)
        let current_3 = formatter.string(from: next3!)
        let next4 = Calendar.current.date(byAdding: .day, value: 4, to: today)
        let current_4 = formatter.string(from: next4!)
        let next5 = Calendar.current.date(byAdding: .day, value: 5, to: today)
        let current_5 = formatter.string(from: next5!)
        let next6 = Calendar.current.date(byAdding: .day, value: 6, to: today)
        let current_6 = formatter.string(from: next6!)
        
        if dayInWeek == "Sunday"{
            self.allDays = ["S","M","T","W","T","F","S"]
        }
        else if dayInWeek == "Monday"{
            
            self.allDays = ["S","M","T","W","T","F","S"]
            
        }
        else if dayInWeek == "Tuesday"{
            
            self.allDays = ["S","M","T","W","T","F","S"]
            
        }
        else if dayInWeek == "Wednesday"{
            
            self.allDays = ["S","M","T","W","T","F","S"]
            
        }
        else if dayInWeek == "Thusday"{
            
            self.allDays = ["S","M","T","W","T","F","S"]
            
        }
        else if dayInWeek == "Friday"{
            
            self.allDays = ["S","M","T","W","T","F","S"]
            
        }
        else if dayInWeek == "Saturday"{
            
            self.allDays = ["S","M","T","W","T","F","S"]
            
        }
        self.date_allday = [current,current_1,current_2,current_3,current_4,current_5,current_6]
        
       
        Days.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
        fromText.text = from
        toText.text = to
        fromText.delegate = self
        toText.delegate = self
        // Do any additional setup after loading the view.
        let tapOutsideSelectOption = UITapGestureRecognizer(target: self, action: #selector(self.tapOutsideSelectOption))
        self.view.addGestureRecognizer(tapOutsideSelectOption)
        tapOutsideSelectOption.cancelsTouchesInView = false
        
        doneButton.layer.shadowColor = UIColor.init(colorLiteralRed: 68/255, green: 168/255, blue: 157/255, alpha: 1.0).cgColor
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.registerForKeyboardNotifications()
        let date = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "EEEE"//"EE" to get short style
        let dayInWeek = dateFormatter.string(from: date as Date)//"Sunday"
        var indexNum = Int()
        if dayInWeek == "Sunday"{
            indexNum = 0
            let indexPathForFirstRow = IndexPath(row: indexNum, section: 0)
           
            let cell = Days.cellForItem(at: indexPathForFirstRow) as! DayCell
            cell.backgroundColor = UIColor.init(colorLiteralRed: 27/255, green: 168/255, blue: 159/255, alpha: 1.0)
            cell.day.textColor = UIColor.white
            self.selected[indexPathForFirstRow.item] = 1
            
            let date1 = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let current = formatter.string(from: date1)// current date
            self.selected_dates.add(current)
            
            
        }
        else if dayInWeek == "Monday"{
            indexNum = 1
            let indexPathForFirstRow = IndexPath(row: indexNum, section: 0)
           
            let cell = Days.cellForItem(at: indexPathForFirstRow) as! DayCell
            cell.backgroundColor = UIColor.init(colorLiteralRed: 27/255, green: 168/255, blue: 159/255, alpha: 1.0)
            cell.day.textColor = UIColor.white
            self.selected[indexPathForFirstRow.item] = 1
            
            let date1 = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let current = formatter.string(from: date1)// current date
            self.selected_dates.add(current)

            
            
            
        }
        else if dayInWeek == "Tuesday"{
            indexNum = 2
            let indexPathForFirstRow = IndexPath(row: indexNum, section: 0)
           
            let cell = Days.cellForItem(at: indexPathForFirstRow) as! DayCell
            cell.backgroundColor = UIColor.init(colorLiteralRed: 27/255, green: 168/255, blue: 159/255, alpha: 1.0)
            cell.day.textColor = UIColor.white
            self.selected[indexPathForFirstRow.item] = 1
            
            let date1 = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let current = formatter.string(from: date1)// current date
            self.selected_dates.add(current)

        }
        else if dayInWeek == "Wednesday"{
            indexNum = 3
            let indexPathForFirstRow = IndexPath(row: indexNum, section: 0)
           
            let cell = Days.cellForItem(at: indexPathForFirstRow) as! DayCell
            cell.backgroundColor = UIColor.init(colorLiteralRed: 27/255, green: 168/255, blue: 159/255, alpha: 1.0)
            cell.day.textColor = UIColor.white
            self.selected[indexPathForFirstRow.item] = 1
            
            let date1 = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let current = formatter.string(from: date1)// current date
            self.selected_dates.add(current)

        }
        else if dayInWeek == "Thursday"{
            indexNum = 4
            let indexPathForFirstRow = IndexPath(row: indexNum, section: 0)
            let cell = Days.cellForItem(at: indexPathForFirstRow) as! DayCell
            cell.backgroundColor = UIColor.init(colorLiteralRed: 27/255, green: 168/255, blue: 159/255, alpha: 1.0)
            cell.day.textColor = UIColor.white
            self.selected[indexPathForFirstRow.item] = 1
            
            let date1 = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let current = formatter.string(from: date1)// current date
            self.selected_dates.add(current)

            
        }
        else if dayInWeek == "Friday"{
            indexNum = 5
            let indexPathForFirstRow = IndexPath(row: indexNum, section: 0)
            
            let cell = Days.cellForItem(at: indexPathForFirstRow) as! DayCell
            cell.backgroundColor = UIColor.init(colorLiteralRed: 27/255, green: 168/255, blue: 159/255, alpha: 1.0)
            cell.day.textColor = UIColor.white
            self.selected[indexPathForFirstRow.item] = 1
            
            let date1 = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let current = formatter.string(from: date1)// current date
            self.selected_dates.add(current)

            
        }
        else if dayInWeek == "Saturday"{
            indexNum = 6
            let indexPathForFirstRow = IndexPath(row: indexNum, section: 0)
            
            let cell = Days.cellForItem(at: indexPathForFirstRow) as! DayCell
            cell.backgroundColor = UIColor.init(colorLiteralRed: 27/255, green: 168/255, blue: 159/255, alpha: 1.0)
            cell.day.textColor = UIColor.white
            self.selected[indexPathForFirstRow.item] = 1
            
            let date1 = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let current = formatter.string(from: date1)// current date
            self.selected_dates.add(current)

            
        }
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func pickTime(_ sender: UITextField) {
        activeField = sender
//        //Create the view
//        let inputView = UIView(frame: CGRect(x:0, y:0, width:self.view.frame.width, height:240))
//        var datePickerView : UIDatePicker = UIDatePicker(frame: CGRect(x: 0, y: 40, width: 0, height: 0))
//        
//        datePickerView.datePickerMode = UIDatePickerMode.time
//        inputView.addSubview(datePickerView) // add date picker to UIView
//        
//        let doneButton = UIButton(frame: CGRect(x:(self.view.frame.size.width/2) - (100/2), y:0, width:100, height:50))
//        doneButton.setTitle("Done", for: UIControlState.normal)
//        doneButton.setTitle("Done", for: UIControlState.highlighted)
//        doneButton.setTitleColor(UIColor.black, for: UIControlState.normal)
//        doneButton.setTitleColor(UIColor.gray, for: UIControlState.highlighted)
//        
//        inputView.addSubview(doneButton) // add Button to UIView
//        
//        doneButton.addTarget(self, action: "doneButton:", for: UIControlEvents.touchUpInside) // set button click event
//        
//        sender.inputView = inputView
//        datePickerView.addTarget(self, action: Selector("handleDatePicker:"), for: UIControlEvents.valueChanged)
//        
//        handleDatePicker(sender: datePickerView) // Set the date on start.
        let inputView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 240))
        
        let datePickerView: UIDatePicker = UIDatePicker(frame: CGRect(x: 0, y: 40, width: 0, height: 0))
        
        datePickerView.datePickerMode = UIDatePickerMode.time
        inputView.addSubview(datePickerView) // add date picker to UIView
    
        let done = UIButton(frame: CGRect(x:(self.view.frame.size.width/2) - (100/2), y:0, width:100, height:50))
        done.setTitle("Done", for: UIControlState.normal)
        done.setTitle("Done", for: UIControlState.highlighted)
        done.setTitleColor(UIColor.black, for: UIControlState.normal)
        done.setTitleColor(UIColor.gray, for: UIControlState.highlighted)
        
        inputView.addSubview(done) // add Button to UIView
        
        done.addTarget(self, action: #selector(self.doneButton(sender:)),for: UIControlEvents.touchUpInside)// set button click event
        
        sender.inputView = inputView
        datePickerView.addTarget(self, action: #selector(handleDatePicker), for: UIControlEvents.valueChanged)
        
        handleDatePicker(sender: datePickerView)

    }
    
    func handleDatePicker(sender: UIDatePicker) {
        var selectedDate:Date = sender.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        pickuptime.textAlignment = NSTextAlignment.center
        let time = dateFormatter.string(for: sender.date)
        pickuptime.text = time
           }
    
    func doneButton(sender:UIButton?)
    {
        dates.removeAll()
        pickuptime.resignFirstResponder()
      
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let storyboard = UIStoryboard(name: "TripDetails", bundle: nil)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DayCell", for: indexPath) as! DayCell
        
        cell.layer.cornerRadius = 20.0
        cell.clipsToBounds = true
        cell.layer.borderWidth = 1.0
        cell.layer.borderColor = UIColor.init(colorLiteralRed: 27/255, green: 168/255, blue: 159/255, alpha: 1.0).cgColor
        cell.day.text = allDays[indexPath.item]
        cell.day.textColor = UIColor.init(colorLiteralRed: 27/255, green: 168/255, blue: 159/255, alpha: 1.0)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let cellCount: Int = 7
        let totalCellWidth = 40 * cellCount
        let totalSpacingWidth = 2 * (cellCount - 1)
        
        let leftInset = (self.view.bounds.size.width - 48 - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
        let rightInset = leftInset
        
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
    }
    
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if selected[indexPath.item] == 0 {
            let cell = collectionView.cellForItem(at: indexPath) as! DayCell
            cell.backgroundColor = UIColor.init(colorLiteralRed: 27/255, green: 168/255, blue: 159/255, alpha: 1.0)
            cell.day.textColor = UIColor.white
            let date = NSDate()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat  = "EEEE"//"EE" to get short style
            let dayInWeek = dateFormatter.string(from: date as Date)//"Sunday"
            var indexNum = Int()
            if dayInWeek == "Sunday"{
                indexNum = 0
            }
            else if dayInWeek == "Monday"{
                indexNum = 1
            }
            else if dayInWeek == "Tuesday"{
                indexNum = 2
            }
            else if dayInWeek == "Wednesday"{
                indexNum = 3
            }
            else if dayInWeek == "Thursday"{
                indexNum = 4
            }
            else if dayInWeek == "Friday"{
                indexNum = 5
            }
            else if dayInWeek == "Saturday"{
                indexNum = 6
            }
            
            
            
            let date1 = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let current = formatter.string(from: date1)// current date
            let next1 = Calendar.current.date(byAdding: .day, value: 1, to: today)
            let current_1 = formatter.string(from: next1!)
            let next2 = Calendar.current.date(byAdding: .day, value: 2, to: today)
            let current_2 = formatter.string(from: next2!)
            let next3 = Calendar.current.date(byAdding: .day, value: 3, to: today)
            let current_3 = formatter.string(from: next3!)
            let next4 = Calendar.current.date(byAdding: .day, value: 4, to: today)
            let current_4 = formatter.string(from: next4!)
            let next5 = Calendar.current.date(byAdding: .day, value: 5, to: today)
            let current_5 = formatter.string(from: next5!)
            let next6 = Calendar.current.date(byAdding: .day, value: 6, to: today)
            let current_6 = formatter.string(from: next6!)
            var index = indexPath[1]
            var indexy = Int(index) - indexNum
            if indexy == 0{
                print("today is clicked")
                self.selected_day.add("0")
                self.selected_dates.add(current)
            }
            else if indexy == 1{
                print("today+1 clicked")
                self.selected_day.add("1")
                self.selected_dates.add(current_1)
            }
            else if indexy == 2{
                print("today+2 clicked")
                self.selected_day.add("2")
                self.selected_dates.add(current_2)
            }
            else if indexy == 3{
                print("today+3 clicked")
                self.selected_day.add("3")
                self.selected_dates.add(current_3)
            }
            else if indexy == 4{
                print("today+4 clicked")
                self.selected_day.add("4")
                self.selected_dates.add(current_4)
            }
            else if indexy == 5{
                print("today+5 clicked")
                self.selected_day.add("5")
                self.selected_dates.add(current_5)
            }
            else if indexy == 6{
                print("today+6 clicked")
                self.selected_day.add("6")
                self.selected_dates.add(current_6)
            }
            else if indexy == -1{
                print("today+6 clicked")
                self.selected_day.add("6")
                self.selected_dates.add(current_6)
            }
            else if indexy == -2{
                print("today+5 clicked")
                self.selected_day.add("5")
                self.selected_dates.add(current_5)
            }
            else if indexy == -3{
                print("today+4 clicked")
                self.selected_day.add("4")
                self.selected_dates.add(current_4)
            }
            else if indexy == -4{
                print("today+3 clicked")
                self.selected_day.add("3")
                self.selected_dates.add(current_3)
            }
            else if indexy == -5{
                print("today+2 clicked")
                self.selected_day.add("2")
                self.selected_dates.add(current_2)
            }
            else if indexy == -6{
                print("today+1 clicked")
                self.selected_day.add("1")
                self.selected_dates.add(current_1)
            }
            selected[indexPath.item] = 1
        }else {
            let cell = collectionView.cellForItem(at: indexPath) as! DayCell
            cell.backgroundColor = UIColor.white
            cell.day.textColor = UIColor.init(colorLiteralRed: 27/255, green: 168/255, blue: 159/255, alpha: 1.0)
            let date = NSDate()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat  = "EEEE"//"EE" to get short style
            let dayInWeek = dateFormatter.string(from: date as Date)//"Sunday"
            var indexNum = Int()
            if dayInWeek == "Sunday"{
                indexNum = 0
            }
            else if dayInWeek == "Monday"{
                indexNum = 1
            }
            else if dayInWeek == "Tuesday"{
                indexNum = 2
            }
            else if dayInWeek == "Wednesday"{
                indexNum = 3
            }
            else if dayInWeek == "Thursday"{
                indexNum = 4
            }
            else if dayInWeek == "Friday"{
                indexNum = 5
            }
            else if dayInWeek == "Saturday"{
                indexNum = 6
            }
            
            
            
            let date1 = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let current = formatter.string(from: date1)// current date
            let next1 = Calendar.current.date(byAdding: .day, value: 1, to: today)
            let current_1 = formatter.string(from: next1!)
            let next2 = Calendar.current.date(byAdding: .day, value: 2, to: today)
            let current_2 = formatter.string(from: next2!)
            let next3 = Calendar.current.date(byAdding: .day, value: 3, to: today)
            let current_3 = formatter.string(from: next3!)
            let next4 = Calendar.current.date(byAdding: .day, value: 4, to: today)
            let current_4 = formatter.string(from: next4!)
            let next5 = Calendar.current.date(byAdding: .day, value: 5, to: today)
            let current_5 = formatter.string(from: next5!)
            let next6 = Calendar.current.date(byAdding: .day, value: 6, to: today)
            let current_6 = formatter.string(from: next6!)
            var index = indexPath[1]
            var indexy = Int(index) - indexNum
            if indexy == 0{
                print("today is clicked")
                
                self.selected_dates.remove(current)
            }
            else if indexy == 1{
                print("today+1 clicked")
                
                self.selected_dates.remove(current_1)
            }
            else if indexy == 2{
                print("today+2 clicked")
               
                self.selected_dates.remove(current_2)
            }
            else if indexy == 3{
                print("today+3 clicked")
               
                self.selected_dates.remove(current_3)
            }
            else if indexy == 4{
                print("today+4 clicked")
                
                self.selected_dates.remove(current_4)
            }
            else if indexy == 5{
                print("today+5 clicked")
                
                self.selected_dates.remove(current_5)
            }
            else if indexy == 6{
                print("today+6 clicked")
                
                self.selected_dates.remove(current_6)
            }
            else if indexy == -1{
                print("today+6 clicked")
                
                self.selected_dates.remove(current_6)
            }
            else if indexy == -2{
                print("today+5 clicked")
                
                self.selected_dates.remove(current_5)
            }
            else if indexy == -3{
                print("today+4 clicked")
                self.selected_dates.remove(current_4)
            }
            else if indexy == -4{
                print("today+3 clicked")
                
                self.selected_dates.remove(current_3)
            }
            else if indexy == -5{
                print("today+2 clicked")
               
                self.selected_dates.remove(current_2)
            }
            else if indexy == -6{
                print("today+1 clicked")
                
                self.selected_dates.remove(current_1)
            }
            
            
            selected[indexPath.item] = 0
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.activeField = textField
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWasShown(_ aNotification: Notification) {
        let keyboardSize = (aNotification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue
        let contentInsets: UIEdgeInsets? = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize?.height)!, 0.0)
        self.scrollView.contentInset = contentInsets!
        self.scrollView.scrollIndicatorInsets = contentInsets!
        var aRect: CGRect = self.view.frame
        aRect.size.height -= (keyboardSize?.height)!
        if !aRect.contains((self.activeField!.frame.origin)) {
            let scrollPoint = CGPoint(x: CGFloat(0.0), y: CGFloat((self.activeField?.frame.origin.y)! - (keyboardSize?.height)!))
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    func keyboardWillBeHidden(_ aNotification: Notification) {
        let contentInsets: UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func tapOutsideSelectOption(_ tapGestureRecognizer: UITapGestureRecognizer) {
        if tapGestureRecognizer.state == .ended {
            var location: CGPoint = tapGestureRecognizer.location(in: self.tripDetailsView)
            }
    }
    
    @IBAction func onClickClose(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
        
    }
    //city_id, team, dates, from, from_latlng, to, to_latlng
    @IBAction func onClickDone(_ sender: UIButton) {
        let time = self.pickuptime.text
        if self.selected_dates.count == 0{
            let alert = UIAlertView(title: "", message: "Please select atleast one day to continue.", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return
        }
        if (self.pickuptime.text?.characters.count)! > 0 {
            for i in 0..<selected_dates.count{
                
                let dateAsString = time
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "h:mm a"
                
                let date1 = dateFormatter.date(from: dateAsString!)
                dateFormatter.dateFormat = "HH:mm"
                
                
                let Date24 = dateFormatter.string(from: date1!)
                
                let date2 = selected_dates[i] as! String
                
                let date_time = date2 + " " + Date24 + ":00"
                let dateFormatter1 = DateFormatter()
                dateFormatter1.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let date_time_date = dateFormatter1.date(from: date_time)
                print(date_time_date?.description(with: NSLocale.current))
                
                let selectedDate:Date = date_time_date!
                let dateFormatter2 = DateFormatter()
                dateFormatter2.dateFormat = "hh:mm a"

                dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
                date = ["start": dateFormatter2.string(for: date_time_date) as AnyObject,
                        "end": dateFormatter2.string(for: selectedDate.addingTimeInterval(30.0*60.0)) as AnyObject]
                print(date)
                if let jsonDate = try? JSONSerialization.data(withJSONObject: date!, options: []) {
                    if let content = String(data: jsonDate, encoding: String.Encoding.utf8) {
                        
                        print(content)
                        
                        arr.add(content)
                        
                    }
                }
                
                
            }
            DVUtility.disaplyWaitMessage()
            UIApplication.shared.beginIgnoringInteractionEvents()
            
            var str = "["
                for i in 0..<arr.count{
                
                    str += arr[i] as! String
                    str += ","
                    
    
               
            }
            
            
            str = str.substring(to: str.index(before: str.endIndex))
            str += "]"
            print(str)
           
            jsonDict1 = ["from": from,
                        "to": to,
                        "from_latlng": fromLat,
                        "to_latlng": toLat,
                        "city_id": "1",
                        "dates": str,
                        "from_city_name": from_city_name,
                        "to_city_name": to_city_name];
            print(jsonDict1)
            self.bookTripApi()
            }
        else{
            let alert = UIAlertView(title: "", message: "Please enter a trip time.", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
        }
        
    }
    func bookTripApi(){
        let reachability = Reachability1()
        if reachability!.isReachable || reachability!.isReachableViaWiFi || reachability!.isReachableViaWWAN
        {
            let networkCommunication = NetworkCommunication()
            networkCommunication.networkDelegate = self
            let shared = SharedPreferences()
            let id = shared.getUserId()
            
            DVUtility.disaplyWaitMessage()
            if acting! == "Commuter" {
                networkCommunication.startNetworkRequest("http://221.121.153.107/booking/book/\(id)", jsonData: jsonDict1 as! [String : Any], method: "POST", apiType: "Book")
            }else {
                networkCommunication.startNetworkRequest("http://221.121.153.107/trip/save/\(id)", jsonData: jsonDict1 as! [String : Any], method: "POST", apiType: "Trip")
            }
        }
        else{
            let alert = UIAlertView(title: "Unreachable", message: "Please make sure internet is connected and retry", delegate: self, cancelButtonTitle: "retry")
            alert.tag = 100
            alert.show()
        }

    }
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if alertView.tag == 100{
            self.bookTripApi()
            return
        }
    }

    
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
       
        DVUtility.hideWaitMessage()
        UIApplication.shared.endIgnoringInteractionEvents()
        
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        
      
        DVUtility.hideWaitMessage()
        UIApplication.shared.endIgnoringInteractionEvents()
        
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            print("")
            
            self.dismiss(animated: false, completion: nil)
            let storyboard1 = UIStoryboard(name: "SideMenu", bundle: nil)
            let leftVC = storyboard1.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
            let story = UIStoryboard(name: "PickDrop", bundle: nil)
           
            let storyboard = UIStoryboard(name: "Discovery", bundle: nil)
            let discNav = storyboard.instantiateViewController(withIdentifier: "DiscNav") as! UINavigationController
            let dis = discNav.topViewController as! Discovery
            if apiType == "Book" {
                dis.book_id = responseData["booking_id"]?.description
                dis.trip_id = ""
            }else {
                dis.trip_id = responseData["trip_id"]?.description
                dis.book_id = ""
            }
            
            discNav.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
            var next = self.presentingViewController
            next?.menuContainerViewController.centerViewController = discNav
                        
        case 600:
            print("")
            DVUtility.displayAlertBoxTitle("", withMessage: "You already have another booking at this time.")
        case 603:
            DVUtility.displayAlertBoxTitle("", withMessage: "Some error occured. Please try again.")
        case 604:
            DVUtility.displayAlertBoxTitle("", withMessage: "You already have another booking at this time.")
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }
    
}
