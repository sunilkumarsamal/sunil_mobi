//
//  RequestCell.swift
//  DV
//
//  Created by chinmay behera on 20/04/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class RequestCell: UITableViewCell {

    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var to: UILabel!
    @IBOutlet weak var actingDriver: UIImageView!
    @IBOutlet weak var requestId: UILabel!
    @IBOutlet weak var rating_image: UIImageView!
    @IBOutlet weak var status_height: NSLayoutConstraint!
    @IBOutlet weak var name_top: NSLayoutConstraint!
    @IBOutlet weak var status: UILabel!
    var currentTitle: String = "Incoming"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        if self.currentTitle != "Incoming"{
//            self.name_top.constant = 5
//            self.status_height.constant = 15
//        }
//        else{
//            self.name_top.constant = 10
//            self.status_height.constant = 0
//        }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
