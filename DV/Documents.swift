//
//  Documents.swift
//  DV
//
//  Created by Chinmay on 19/02/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class Documents: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate,NetworkProtocol {

    
    @IBOutlet weak var support_mail: UIButton!
    @IBOutlet weak var below_text_label: UILabel!
    @IBOutlet weak var details: UILabel!
    var image1: UIImage?
    var image2: UIImage?
    var image3: UIImage?
    var flag = String()
     var pro:UIVisualEffectView?
    var isCommutor:Bool = false
    @IBOutlet weak var progress: UIProgressView!
    
    @IBOutlet weak var DLTextField: UITextField!
    @IBOutlet weak var regoTextField: UITextField!
    @IBOutlet weak var insuranceTextField: UITextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    weak var activeField: UITextField!
    
    @IBOutlet weak var carName: UITextField!
    @IBOutlet weak var carNumber: UITextField!
    @IBOutlet weak var carColor: UITextField!
    var profileType = "SignUp"
    var imageNumber:Int = 0
    
    @IBOutlet weak var percentLabel: UILabel!
    
    @IBOutlet weak var uploadView: UIView!
    @IBOutlet weak var uploadImage: UIImageView!
    @IBOutlet weak var docName: UILabel!
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var doneBtnBottom: NSLayoutConstraint!
    var DLImage: UIImage?
    var REGOImage: UIImage?
    var INSURANCEImage: UIImage?
    var Model = String()
    var Plate = String()
    var CarColour = String()
    var info = PersonInfo()
    var avatar: UIImage?
    let imagePicker = UIImagePickerController()
    var firstName = String()
    var lastName = String()
    var contact = String()
    var industry = String()
    var gender = String()
    var birthDay = String()
    var role = String()
    var checkreg = String()
    var checkidentity = String()
    var checkinsu = String()

    var id: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if profileType == "SignUp"{
            print("")

        progress.transform = progress.transform.scaledBy(x: 1, y: 15)
        progress.tintColor = UIColor.init(colorLiteralRed: 27/255.0, green: 168/255.0, blue: 159/255.0, alpha: 1.0)
        progress.progress = 0.80
        percentLabel.text = "80%"
        let img:UIImage?
        
        let imgView: UIImageView!
        
        if UserDefaults.standard.object(forKey: "DLImage") == nil {
            let img = UIImage(named: "greater")
            imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imgView.image = img
        }else {
            let img = UIImage(named: "tick")
            imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imgView.image = img

        }

        DLTextField.rightViewMode = UITextFieldViewMode.always
        DLTextField.rightView = imgView
        
        let imgView1: UIImageView!
        
        if UserDefaults.standard.object(forKey: "RegoImage") == nil {
            let img1 = UIImage(named: "greater")
            imgView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imgView1.image = img1
        }else {
            let img1 = UIImage(named: "tick")
            imgView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imgView1.image = img1

        }
        
        regoTextField.rightViewMode = UITextFieldViewMode.always
        regoTextField.rightView = imgView1
        
        let imgView2: UIImageView!
        
        if UserDefaults.standard.object(forKey: "InsuranceImage") == nil {
            let img2 = UIImage(named: "greater")
            imgView2 = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imgView2.image = img2
        }else {
            let img2 = UIImage(named: "tick")
            imgView2 = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imgView2.image = img2

        }
        insuranceTextField.rightViewMode = UITextFieldViewMode.always
        insuranceTextField.rightView = imgView2
        
        uploadView.isHidden = true
        

        DLTextField.setBottomBorder()
        regoTextField.setBottomBorder()
        insuranceTextField.setBottomBorder()
        carName.setBottomBorder()
        carNumber.setBottomBorder()
        carColor.setBottomBorder()
                }
        else {
            self.progress.isHidden = true
            self.percentLabel.isHidden = true

            let img:UIImage?
            
            let imgView: UIImageView!
                if self.DLImage == nil{
                    let img = UIImage(named: "greater")
                    imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
                    imgView.image = img
                    DLTextField.rightViewMode = UITextFieldViewMode.always
                    DLTextField.rightView = imgView


                }
                else{
                    DLTextField.rightView?.isHidden = false
                    let img0 = UIImage(named: "tick")
                    imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
                    imgView.image = img0
                    DLTextField.rightViewMode = UITextFieldViewMode.always
                    DLTextField.rightView = imgView
                }
            let imgView1: UIImageView!
                if self.REGOImage == nil{
                    let img1 = UIImage(named: "greater")
                    imgView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
                    imgView1.image = img1
                    regoTextField.rightViewMode = UITextFieldViewMode.always
                    regoTextField.rightView = imgView1


                }
                else{
                    regoTextField.rightView?.isHidden = false
                    let img1 = UIImage(named: "tick")
                    imgView1 = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
                    imgView1.image = img1
                    regoTextField.rightViewMode = UITextFieldViewMode.always
                    regoTextField.rightView = imgView1
                }
           
            
            let imgView2: UIImageView!
                if self.INSURANCEImage == nil{
                    let img2 = UIImage(named: "greater")
                    imgView2 = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
                    imgView2.image = img2
                    insuranceTextField.rightViewMode = UITextFieldViewMode.always
                    insuranceTextField.rightView = imgView2


                }
                else{
                    insuranceTextField.rightView?.isHidden = false
                    let img2 = UIImage(named: "tick")
                    imgView2 = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
                    imgView2.image = img2
                    insuranceTextField.rightViewMode = UITextFieldViewMode.always
                    insuranceTextField.rightView = imgView2
                }
            
            
            uploadView.isHidden = true
            
           
            DLTextField.setBottomBorder()
            regoTextField.setBottomBorder()
            insuranceTextField.setBottomBorder()
            carName.setBottomBorder()
            carNumber.setBottomBorder()
            carColor.setBottomBorder()
            if self.Model == "Null"{
                self.carName.text?.isEmpty
            }
            else{
                self.carName.text = self.Model
            }
            if self.Plate == "Null"{
                self.carNumber.text?.isEmpty
            }
            else{
                self.carNumber.text = self.Plate
            }
            if self.CarColour == "Null"{
                self.carColor.text?.isEmpty
            }
            else{
                self.carColor.text = self.CarColour
            }
            

            if self.DLImage != nil{
                 self.image1 = self.DLImage
            }
            else{
                self.image1 = UIImage(named: "upload_doc1")
            }
            if self.REGOImage != nil{
               self.image2 = self.REGOImage
            }
            else{
                self.image2 = UIImage(named: "upload_doc1")
            }
            if self.INSURANCEImage != nil {
                self.image3 = self.INSURANCEImage
            }
            else{
                self.image3 = UIImage(named: "upload_doc1")
            }
            
            
        }
            
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if profileType == "SignUp"{
        if textField == DLTextField || textField == regoTextField || textField == insuranceTextField {
            uploadView.isHidden = false
            imageNumber = textField.tag
            switch textField.tag {
            case 0:
                details.font = details.font.withSize(16)
                docName.text = "Driving License"
                details.text = "Please upload a clear photo of your driver's license.Please note that the text in the photo must be readable and not blurry."
                below_text_label.text = "Having trouble? Get in touch with us and we will respond within 24hours."
                self.support_mail.isHidden = false
                if let picture = image1 {
                    uploadImage.image = picture
                }else {
                    uploadImage.image = UIImage(named: "upload_doc1")
                }
            case 1:
                details.font = details.font.withSize(13)
                docName.text = "Registration document"
                details.text = "Please upload a photo of your latest registration document,which will need to include the following.\n \u{2022} Your full name\n \u{2022} Your vehicle's registration number\n \u{2022} Your registration expiry date"
                below_text_label.text = "Please ensure that photos are clear and text is readable.This will be cross-checked with your insurance information to be provided later."
                self.support_mail.isHidden = true
                if let picture = image2 {
                    uploadImage.image = picture
                }else {
                    uploadImage.image = UIImage(named: "upload_doc1")
                }
            case 2:
                details.font = details.font.withSize(15)
                docName.text = "Insurance Copy"
                details.text = "Please ensure that the photo of your policy includes :\n \u{2022} Your full name\n \u{2022} Your vehicle's registration number\n \u{2022} The policy's expiry date."
                below_text_label.text = "Please ensure that photos are clear and the text is readable.This will be cross-checked with your registration details provided."
                self.support_mail.isHidden = true
                if let picture = image3 {
                    uploadImage.image = picture
                }else {
                    uploadImage.image = UIImage(named: "upload_doc1")
                }
            default:
                print("")
            }
            return false
        }
            activeField = textField
            return true
        }
            else{
            if textField == DLTextField || textField == regoTextField || textField == insuranceTextField {
                uploadView.isHidden = false
                imageNumber = textField.tag
                
                switch textField.tag {
                case 0:
                    details.font = details.font.withSize(16)
                    docName.text = "Driving License"
                    details.text = "Please upload a clear photo of your driver's license. Please note that the text in the photo must be readable and not blurry."
                    below_text_label.text = "Having trouble? Get in touch with us and we will respond within 24hours."
                    self.support_mail.isHidden = false
                    uploadImage.image = self.image1
                    
                case 1:
                    details.font = details.font.withSize(13)
                    docName.text = "Registration document"
                    details.text = "Please upload a photo of your latest registration document,which will need to include the following.\n \u{2022} Your full name\n \u{2022} Your vehicle's registration number\n \u{2022} Your registration expiry date"
                    below_text_label.text = "Please ensure that photos are clear and text is readable.This will be cross-checked with your insurance information to be provided later."
                    self.support_mail.isHidden = true
                    uploadImage.image = self.image2
                    
                case 2:
                    details.font = details.font.withSize(15)
                    docName.text = "Insurance Copy"
                    details.text = "Please ensure that the photo of your policy includes :\n \u{2022} Your full name\n \u{2022} Your vehicle's registration number\n \u{2022} The policy's expiry date."
                    below_text_label.text = "Please ensure that photos are clear and the text is readable.This will be cross-checked with your registration details provided."
                    self.support_mail.isHidden = true
                    uploadImage.image = self.image3
                   
                default:
                    print("")
                }
            return false
        }
        activeField = textField
        return true
    }
    }
    
    
    @IBAction func onClick_mail(_ sender: UIButton) {
        let email = "support@mobishare.com.au"
        if let url = URL(string: "mailto:\(email)") {
            UIApplication.shared.openURL(url)

           
        }
    }
    

    @IBAction func onClickCamera(_ sender: UIButton) {
        self.openOptionsForUpload()
    }
    
    @IBAction func onClickText(_ sender: UITextField) {
    }
    
    @IBAction func onClickBackOnUpload(_ sender: UIButton) {
        uploadView.isHidden = true
    }
    
    func openOptionsForUpload (){
        let actionSheet = UIAlertController(title: "Choose Option", message: "", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default) { action -> Void in
            print("open camera")
            self.openCamera()
        })
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default) { action -> Void in
            print("Cancel")
            self.openGallery()
        })
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel")
        })
        self.present(actionSheet, animated: true, completion: nil)
    }
    func openCamera() {

        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
            print("camera not available")
        }
    }
    func openGallery() {
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.sourceType = .photoLibrary
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
            print("Library not available")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        

        print("captured")

        
        if let chosenImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            uploadImage.image = chosenImage
        } else if let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            uploadImage.image = chosenImage
        } else {
            uploadImage.image = nil
        }
        let chosenImage = uploadImage.image!
        
        if self.imageNumber == 0 {
            dismiss(animated:true, completion: nil)
            self.image1 = chosenImage
            self.DLImage = self.image1

            if DLTextField.rightView?.isHidden == true{
                DLTextField.rightView?.isHidden = false
            }
            let img = UIImage(named: "tick")
            let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imgView.image = img
            self.DLTextField.rightView = imgView
            
        }else if self.imageNumber == 1 {
            dismiss(animated:true, completion: nil)
            self.image2 = chosenImage
            self.REGOImage = self.image2
            if regoTextField.rightView?.isHidden == true{
                regoTextField.rightView?.isHidden = false
            }
            let img = UIImage(named: "tick")
            let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imgView.image = img
            self.regoTextField.rightView = imgView
            
        }else if self.imageNumber == 2 {
            dismiss(animated:true, completion: nil)
            self.image3 = chosenImage
            self.INSURANCEImage = self.image3

            if insuranceTextField.rightView?.isHidden == true{
                insuranceTextField.rightView?.isHidden = false
            }
            let img = UIImage(named: "tick")
            let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imgView.image = img
            self.insuranceTextField.rightView = imgView
            
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:true, completion: nil)
    }
    
    @IBAction func onClickDone(_ sender: UIButton) {
        if self.carName.text?.characters.count == 0{
            let alert = UIAlertView(title: "", message: "Please provide the car model.", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return
        }
        if self.carColor.text?.characters.count == 0{
            let alert = UIAlertView(title: "", message: "Please provide the car colour.", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return
        }
        if self.carNumber.text?.characters.count == 0{
            let alert = UIAlertView(title: "", message: "Please provide the car number.", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
            return
        }
        if profileType == "SignUp"{
        if image1 != nil && image2 != nil && image3 != nil{
            
            info.identity = image1!
            info.rego = image2!
            info.insurance = image3!
            info.carName = carName.text!
            info.carNumber = carNumber.text!
            info.carColor = carColor.text!
            
            let storyboard = UIStoryboard(name: "Payment", bundle: nil)
            let pay = storyboard.instantiateViewController(withIdentifier: "AddCard") as! AddCard
            pay.info = info
            pay.id = id!
            self.present(pay, animated: true, completion: nil)
        }else {
            let alert = UIAlertView(title: "", message: "Please upload all documents.", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
        }
        }
        else{
            let role:String
            let shared = SharedPreferences()
            if isCommutor {
                
                role = "commutor"
                shared.saveUserType("Commutor")
            }else {
                
                role = "driver"
                shared.saveUserType("Driver")
            }

            if image1 != nil && image2 != nil && image3 != nil{
                
                info.role = role
                info.identity = self.image1!
                info.rego = self.image2!
                info.insurance = self.image3!
                info.carName = self.carName.text!
                info.carNumber = self.carNumber.text!
                info.carColor = self.carColor.text!
                

                DVUtility.disaplyWaitMessage()
                self.startNetworkRequest(urlString: "http://221.121.153.107/user/profile/\(id!)")
            }
            else {
                let alert = UIAlertView(title: "", message: "Please upload all documents.", delegate: self, cancelButtonTitle: "Ok")
                alert.show()
            }
        }
    }
    @IBAction func onClickBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWasShown(_ aNotification: Notification) {
        let keyboardSize = (aNotification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue

        let contentInsets: UIEdgeInsets? = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize?.height)!, 0.0)
        self.scrollView.contentInset = contentInsets!
        self.scrollView.scrollIndicatorInsets = contentInsets!
        var aRect: CGRect = self.view.frame
        aRect.size.height -= (keyboardSize?.height)!
        if !aRect.contains((self.activeField!.frame.origin)) {
            let scrollPoint = CGPoint(x: CGFloat(0.0), y: CGFloat((self.activeField?.frame.origin.y)! - (keyboardSize?.height)!))
            self.scrollView.setContentOffset(scrollPoint, animated: true)
        }
        
    }
    
    func keyboardWillBeHidden(_ aNotification: Notification) {
        let contentInsets: UIEdgeInsets = UIEdgeInsets.zero
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets

    }
    func startNetworkRequest(urlString: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let request = NSMutableURLRequest()
        //Set Params
        request.httpShouldHandleCookies = false
        request.timeoutInterval = 60
        request.httpMethod = "POST"
        //Create boundary, it can be anything
        let boundary = "DVRidesBoundary"
        // set Content-Type in HTTP header
        let contentType = "multipart/form-data; boundary=\(boundary)"
        request.setValue(contentType, forHTTPHeaderField: "Content-Type")
        // post body
        let body = NSMutableData()
        //Populate a dictionary with all the regular values you would like to send.
        
        //        firstname, lastname, contact, address, study, avatar, interests, (role, identity, registration, insurance)
        
        let param = ["vehicle": (info.carName),
                     "plate": (info.carNumber),
                     "colour": (info.carColor),
                     "role": (info.role),
                     "role_flag":(info.flag)] as [String : Any]
        
        print(param)
        
        for (key, value) in param {
            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
            body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
            body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
        }

        
        if info.role == "driver" {
            let FileParamConstant1 = "identity"
            let imageData1 = UIImageJPEGRepresentation((info.identity)!, 0.5)
            if (imageData1 != nil) {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(FileParamConstant1)\"; filename=\"image.png\"\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Type:image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append(imageData1!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            let FileParamConstant2 = "registration"
            let imageData2 = UIImageJPEGRepresentation((info.rego)!, 0.5)
            if (imageData2 != nil) {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(FileParamConstant2)\"; filename=\"image.png\"\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Type:image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append(imageData2!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            let FileParamConstant3 = "insurance"
            let imageData3 = UIImageJPEGRepresentation((info.insurance)!, 0.5)
            if (imageData3 != nil) {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(FileParamConstant3)\"; filename=\"image.png\"\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Type:image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append(imageData3!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
        }
        
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
        request.httpBody = body as Data
        let postLength = "\(UInt(body.length))"
        request.setValue(postLength, forHTTPHeaderField: "Content-Length")
        // set URL
        request.url = NSURL(string: urlString)! as URL
        //        GuideZeeUtallity.HideWaitMessage()
        let task = session.dataTask(with: request as URLRequest) {
            ( data, response, error) in
            
            guard let _:NSData = data as NSData?, let _:URLResponse = response, error == nil else {
                DispatchQueue.main.async { [unowned self] in
                    self.view.alpha = 0.7
                    self.pro = ProgressHud(text: "Please Wait...")
                    self.view.addSubview(self.pro!)

                    let alert = UIAlertView(title: "Error!", message: "Something went wrong. Please try later.", delegate: self, cancelButtonTitle: "Ok")
                    alert.show()
                }
                return
            }
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            let dict = self.convertStringToDictionary(text: dataString as! String)
            print(dataString ?? NSDictionary())
            print(dict?["status_code"] ?? String())
            DispatchQueue.main.async { [unowned self] in
                self.view.alpha = 1

                UIApplication.shared.endIgnoringInteractionEvents()
                let shared = SharedPreferences()
                if self.info.role == "driver" {
                    
                    shared.saveUserType("Driver")
                }else {
                    
                    shared.saveUserType("Commutor")
                }
                //                let shared = UserDefaults.standard
                shared.saveFirstName((self.info.firstName))
                shared.saveLastName((self.info.lastName))
                shared.saveUserId(self.id!)
                
                self.registerDeviceToken()
                
            }
            
        }
        
        task.resume()
    }
    func registerDeviceToken() {
       
        let shared = SharedPreferences()
        
        let token = shared.getToken()
        let jsonDict: NSDictionary
        jsonDict = ["token": token,
                    "device_type": "ios"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self as NetworkProtocol
        networkCommunication.startNetworkRequest("http://221.121.153.107/user/subscribe/\(id!)", jsonData: jsonDict as! [String : Any], method: "POST", apiType: "RegisterGCM")
        
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
        
    }
    
    func failure() {
        self.view.alpha = 1
        pro!.removeFromSuperview()
        UIApplication.shared.endIgnoringInteractionEvents()
        let alert = UIAlertView(title: "Oops!!!", message: "There was a technical difficulty while fulfilling your request. Please try again.", delegate: self, cancelButtonTitle: "Ok")
        alert.show()
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        DVUtility.hideWaitMessage()
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            if apiType == "RegisterGCM" {
                DVUtility.hideWaitMessage()
                self.dismiss(animated: true, completion: nil)
//                
//                self.presentingViewController?.dismiss(animated: true, completion: nil)

            }
        case 600:
            DVUtility.displayAlertBoxTitle("", withMessage: "Some error occured. You might not get Notifications of your trips. Please Login again.")
            gotoHome()
        case 600:
            DVUtility.displayAlertBoxTitle("", withMessage: "User not found")
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }
    func gotoHome() {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        let story = UIStoryboard(name: "PickDrop", bundle: nil)
       
        let navVC:UINavigationController = story.instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
        navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
        let container: MFSideMenuContainerViewController = story.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
        container.leftMenuViewController = leftVC
        container.centerViewController = navVC
        container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
        self.present(container, animated: true, completion: nil)
    }
    @IBAction func onClickOfMenu(_ sender: UIBarButtonItem) {
        let next = storyboard?.instantiateViewController(withIdentifier: "Documents") as! Documents
        self.present(next, animated: true, completion: nil)
    }

   }
