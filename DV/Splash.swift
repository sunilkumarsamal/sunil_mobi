//
//  Splash.swift
//  DV
//
//  Created by Chinmay on 23/02/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class Splash: UIViewController,NetworkProtocol {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Thread.sleep(forTimeInterval: 1)
        self.nextScreen()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func nextScreen() {
        if UserDefaults.standard.string(forKey: "userId") == nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let home = storyboard.instantiateViewController(withIdentifier: "Walkthrough") as! Walkthrough
            self.present(home, animated: false, completion: nil)
        }else {
//            http://45.79.189.135/divi/general/status/{id}
//            DVUtility.disaplyWaitMessage()
            self.statusApi()
            
            
        }
        
    }
    func statusApi(){
        let reachability = Reachability1()
        if reachability!.isReachable || reachability!.isReachableViaWiFi || reachability!.isReachableViaWWAN
        {
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        let jsonDict: NSDictionary = ["DV": "mobi"]
        networkCommunication.startNetworkRequest("http://221.121.153.107/general/status/\(id)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "checkStatus")
        }
        else{
            let alert = UIAlertView(title: "Unreachable", message: "Please make sure internet is connected and retry", delegate: self, cancelButtonTitle: "retry")
            alert.tag = 100
            alert.show()
        }
    }
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if alertView.tag == 100{
            self.statusApi()
            return
        }
    }
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
        UIApplication.shared.endIgnoringInteractionEvents()
        
        print("There was a technical difficulty. please try again")
        let alert = UIAlertView(title: "", message: "There was a technical difficulty. Please try again by going back.", delegate: self, cancelButtonTitle: "Ok")
        alert.show()
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
//        DVUtility.hideWaitMessage()
        UIApplication.shared.endIgnoringInteractionEvents()
        
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
           if  apiType == "checkStatus"{
            var profile = responseData["profile"]
            if  profile?["firstname"] is NSNull{
//                if  profile?["firstname"] as! NSNull != nil
                var shared = SharedPreferences()
                let storyboard = UIStoryboard(name: "Details", bundle: nil)
                let details = storyboard.instantiateViewController(withIdentifier: "Details") as! Details
                details.id = profile?["user_id"] as! String
                shared.saveUserId(profile?["user_id"] as! String)
                details.profileType = "SignUp"
                self.present(details, animated: true, completion: nil)
            }
            if let val = responseData["pending_payment"]{
                let storyboard = UIStoryboard(name: "PaymentFailure", bundle: nil)
                let next = storyboard.instantiateViewController(withIdentifier: "PaymentFailure") as! PaymentFailure
                next.booking_id = val["booking_id"] as! String
                var cal = val["calculation"] as! [String:AnyObject]
                var cost = "\(cal["cost"]!)"
                var from = val["from"] as! String
                var to = val["to"] as! String
                var date_show = val["start"] as! String
                var profile = responseData["profile"]
                var f_name = profile?["firstname"] as! String
                var l_name = profile?["lastname"] as! String
                var commuter_name = f_name + " " + l_name
                var card = responseData["card"] as! [String:AnyObject]
                var id = card["user_id"] as! String
                next.c_id = id
                next.cost = cost
                next.from = from
                next.to = to
                next.date_show = date_show
                next.commuter_Name = commuter_name
                self.present(next, animated: true, completion: nil)

            }
            
            else{
            let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
            let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
            let story = UIStoryboard(name: "PickDrop", bundle: nil)
            let PickDrop = story.instantiateViewController(withIdentifier: "PickUpDropOff") as! PickUpDropOff
            let navVC:UINavigationController = story.instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
            navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
            
            let container: MFSideMenuContainerViewController = story.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
            container.leftMenuViewController = leftVC
            container.centerViewController = navVC
            container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
            self.present(container, animated: true, completion: nil)
            }
           }
        case 603:
            var message = responseData["message"] as! String
            DVUtility.displayAlertBoxTitle("", withMessage: "\(message)")
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }


}
