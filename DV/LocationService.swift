//
//  LocationService.swift
//  DV
//
//  Created by chinmay behera on 08/05/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

protocol LocationServiceDelegate {
    func fetchingLocation(currentLocation: CLLocation)
    func fetchingLocationDidFailWithError(error: NSError)
}

class LocationService: NSObject, CLLocationManagerDelegate, NetworkProtocol{
    
    static let sharedInstance: LocationService = { LocationService() }()
    
    var trip_id: String? = ""
    var timer = Timer()
//    class var sharedInstance: LocationService {
//        struct Static {
//            static var onceToken: dispatch_once_t = 0
//            
//            static var instance: LocationService? = nil
//        }
//        dispatch_once(&Static.onceToken) {
//            Static.instance = LocationService()
//        }
//        return Static.instance!
//    }
    
    var locationManager: CLLocationManager?
    var lastLocation: CLLocation?
    var delegate: LocationServiceDelegate?
    
    override init() {
        super.init()
        
        
        
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        guard let locationManager = self.locationManager else {
            return
        }
        
        if CLLocationManager.authorizationStatus() == .notDetermined {
            // you have 2 choice
            // 1. requestAlwaysAuthorization
            // 2. requestWhenInUseAuthorization
            locationManager.requestAlwaysAuthorization()
        }
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest // The accuracy of the location data
        locationManager.distanceFilter = kCLDistanceFilterNone // The minimum distance (measured in meters) a device must move horizontally before an update event is generated.
        
    }
    
    func startUpdatingLocation() {
//        print("Starting Location Updates")
//        self.locationManager?.startUpdatingLocation()
//        self.locationManager?.delegate = self
        self.locationManager?.requestAlwaysAuthorization() 
        self.locationManager?.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            self.locationManager?.delegate = self
            self.locationManager?.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.locationManager?.startUpdatingLocation()
            self.locationManager?.startMonitoringSignificantLocationChanges()
            self.locationManager?.startUpdatingHeading()
            print("start update lcoation")
//            updateLocation(currentLocation: lastLocation!)
        }
        locationManager?.startUpdatingLocation()
        
    }
    
    func stopUpdatingLocation() {
        print("Stop Location Updates")
        self.stopTimer()
//        LocationService.sharedInstance.stopUpdatingLocation()
        self.locationManager?.stopMonitoringSignificantLocationChanges()
        self.locationManager?.stopUpdatingLocation()
        self.locationManager?.stopUpdatingHeading()
        self.locationManager = nil
        
    }
    
    // CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("didUpdateLocation")
        
        guard let location = locations.last else {
            return
        }
        
        // singleton for get last location
        self.lastLocation = location
        print("stop")
        if self.trip_id != ""{
//            timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.save), userInfo: nil, repeats: true)
            
            let shared = UserDefaults.standard
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            shared.set(latitude, forKey: "latitude")
            shared.set(longitude, forKey: "longitude")
            shared.synchronize()
            
            updateLocation(currentLocation: location)
//            self.stopUpdatingLocation()
        }
        else{
            print("trip id is not there")
            self.stopUpdatingLocation()
        }
        
        // use for real time update location
        
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("update failure.")
        // do on error
//        updateLocationDidFailWithError(error: error as NSError)
    }
    
    // Private function
    private func updateLocation(currentLocation: CLLocation){
        
        guard let delegate = self.delegate else {
            return
        }
//        timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.save), userInfo: nil, repeats: true)
        let shared = UserDefaults.standard
        let latitude = currentLocation.coordinate.latitude
        let longitude = currentLocation.coordinate.longitude
        shared.set(latitude, forKey: "latitude")
        shared.set(longitude, forKey: "longitude")
        shared.synchronize()
        
        if self.trip_id != ""{
            self.save()
//            self.saveLocation(String(currentLocation.coordinate.latitude), String(currentLocation.coordinate.longitude))
        }
        delegate.fetchingLocation(currentLocation: currentLocation)
    }
    
    private func updateLocationDidFailWithError(error: NSError) {
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.fetchingLocationDidFailWithError(error: error)
    }
    func save(){
        print("save func called")
        let shared = UserDefaults.standard
        let latitude = shared.string(forKey: "latitude") as! String
        let longitude = shared.string(forKey: "longitude") as! String
        if self.trip_id != ""{

            self.saveLocation(latitude , longitude)
        }
    }
    func stopTimer() {
        timer.invalidate()
    }
//************************************* Save Location API *****************************************
    
    func saveLocation(_ latitude: String, _ longitude: String) {
        let jsonDict: NSDictionary = ["trip_id": trip_id!,
                                      "latitude": latitude,
                                      "longitude": longitude]
        let shared = SharedPreferences()
        let id = shared.getUserId()
        print("hitting savelocation api")
        let url = "http://221.121.153.107/trip/savelocation/\(id)"
        self.sendToServer(url: url, apiType: "SaveLocation", jsonDict: jsonDict as! [String : Any], method: "POST")
    }
    
    func sendToServer(url: String, apiType: String, jsonDict: [String : Any], method: String) {
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        networkCommunication.startNetworkRequest(url, jsonData: jsonDict , method: method, apiType: apiType)
    }
    
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response , apiType: apiType)
    }
    
    func failure() {
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            print("")
            self.startUpdatingLocation()
        case 600:
            self.stopUpdatingLocation()
        case 101:
            self.stopUpdatingLocation()
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }
}
