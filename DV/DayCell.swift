//
//  DayCell.swift
//  DV
//
//  Created by Chinmay on 09/02/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class DayCell: UICollectionViewCell {
    
    @IBOutlet weak var day: UILabel!
    
}
