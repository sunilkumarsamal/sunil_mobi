//
//  PersonInfo.swift
//  DV
//
//  Created by chinmay behera on 21/03/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class PersonInfo{
    
    var firstName = ""
    var lastName = ""
    var contact = ""
    var course = ""
    var role = ""
    var flag = ""
    var avatar:UIImage?
    var interest = [Int]()
    var identity:UIImage?
    var rego:UIImage?
    var insurance:UIImage?
    
    var gender = ""
    var dob = ""
    var carName = ""
    var carNumber = ""
    var carColor = ""

}
