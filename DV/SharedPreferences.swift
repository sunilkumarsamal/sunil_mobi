//
//  SharedPreferences.swift
//  DV
//
//  Created by Chinmay on 22/02/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class SharedPreferences: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func saveUserStatus(_ status: Bool) {
        let preferences = UserDefaults.standard
        let statusKey: String = "isLogin"
        let statusValue: Bool = status
        preferences.set(statusValue, forKey: statusKey)
        preferences.synchronize()
    }
    
    
    public func getUserStatus() -> Bool {
        let preferences = UserDefaults.standard
        let statusKey: String = "isLogin"
        return preferences.object(forKey: statusKey)! as! Bool
    }
    public func saveProfileType(_ type: String){
        let preferences = UserDefaults.standard
        let statusKey: String = "profileType"
        let statusValue: String = type
        preferences.set(statusValue, forKey: statusKey)
        preferences.synchronize()
    }
    public func getProfileType() -> String{
        let preferences = UserDefaults.standard
        let statusKey: String = "profileType"
        return preferences.object(forKey: statusKey)! as! String
    }
    public func saveFirstName(_ status: String) {
        let preferences = UserDefaults.standard
        let statusKey: String = "firstName"
        let statusValue: String = status
        preferences.set(statusValue, forKey: statusKey)
        preferences.synchronize()
    }
    
    public func getFirstName() -> String {
        let preferences = UserDefaults.standard
        let statusKey: String = "firstName"
        return preferences.object(forKey: statusKey)! as! String
    }
    
    public func saveLastName(_ status: String) {
        let preferences = UserDefaults.standard
        let statusKey: String = "LastName"
        let statusValue: String = status
        preferences.set(statusValue, forKey: statusKey)
        preferences.synchronize()
    }
    
    public func getLastName() -> String {
        let preferences = UserDefaults.standard
        let statusKey: String = "LastName"
        return preferences.object(forKey: statusKey)! as! String
    }
    
    public func saveUserImage(_ image: String) {
        let preferences = UserDefaults.standard
        let statusKey: String = "named"
        let statusValue: String = image
        preferences.set(statusValue, forKey: statusKey)
        preferences.synchronize()
    }
    
    public func getUserImage() -> String {
        let preferences = UserDefaults.standard
        let statusKey: String = "named"
        return preferences.object(forKey: statusKey)! as! String
    }
    
    public func saveUserId(_ userId: String) {
        let preferences = UserDefaults.standard
        let statusKey: String = "userId"
        let statusValue: String = userId
        preferences.set(statusValue, forKey: statusKey)
        preferences.synchronize()
    }
    
    public func getUserId() -> String {
        let preferences = UserDefaults.standard
        let statusKey: String = "userId"
        return preferences.object(forKey: statusKey)! as! String
    }
    
    public func saveUserType(_ userType: String) {
        let preferences = UserDefaults.standard
        let statusKey: String = "userType"
        let statusValue: String = userType
        preferences.set(statusValue, forKey: statusKey)
        preferences.synchronize()
    }
    
    public func getUserType() -> String {
        let preferences = UserDefaults.standard
        let statusKey: String = "userType"
        return preferences.object(forKey: statusKey)! as! String
    }
    public func saveUserFlag(_ userFlag: String){
        let preferences = UserDefaults.standard
        let statusKey: String = "userFlag"
        let statusValue: String = userFlag
        preferences.set(statusValue, forKey: statusKey)
        preferences.synchronize()
    }
    public func getUserFlag() -> String {
        let preferences = UserDefaults.standard
        let statusKey: String = "userFlag"
        return preferences.object(forKey: statusKey)! as! String
    }
    public func clearPreferences() {
        if let bundle = Bundle.main.bundleIdentifier {
            UserDefaults.standard.removePersistentDomain(forName: bundle)
        }
    }
    
    public func saveToken(_ userType: String) {
        let preferences = UserDefaults.standard
        let statusKey: String = "token"
        let statusValue: String = userType
        preferences.set(statusValue, forKey: statusKey)
        preferences.synchronize()
    }
    
    public func getToken() -> String {
        let preferences = UserDefaults.standard
        let statusKey: String = "token"
        return preferences.object(forKey: statusKey)! as! String
    }
    public func saveGender(_ Gen: String){
        let preferences = UserDefaults.standard
        let statusKey: String = "Gender"
        let statusValue: String = Gen
        preferences.set(statusValue, forKey: statusKey)
        preferences.synchronize()
    }
    public func getGender() -> String{
        let preferences = UserDefaults.standard
        let statusKey: String = "Gender"
        return preferences.object(forKey: statusKey)! as! String
    }
    public func saveDOB(_ DOB: String){
        let preferences = UserDefaults.standard
        let statusKey: String = "DOB"
        let statusValue: String = DOB
        preferences.set(statusValue, forKey: statusKey)
        preferences.synchronize()
    }
    public func getDOB() -> String{
        let preferences = UserDefaults.standard
        let statusKey: String = "DOB"
        return preferences.object(forKey: statusKey)! as! String
    }
    
    public func saveDetails(contact: String, industry: String, avatar: String, role: String) {
        let preferences = UserDefaults.standard
        preferences.set(contact, forKey: "Contact")
        preferences.set(industry, forKey: "Industry")
        preferences.set(avatar, forKey: "Avatar")
        preferences.set(role, forKey: "Role")
        preferences.synchronize()
    }

    
    public func getContact() -> String {
        let preferences = UserDefaults.standard
        return preferences.object(forKey: "Contact")! as! String
    }
    
    public func getAvatar() -> String {
        let preferences = UserDefaults.standard
        return preferences.object(forKey: "Avatar")! as! String
    }
    
    public func getRole() -> String {
        let preferences = UserDefaults.standard
        return preferences.object(forKey: "Role")! as! String
    }
    public func getIndustry() -> String{
        let preferences = UserDefaults.standard
        return preferences.object(forKey: "Industry") as! String
    }
    
}
