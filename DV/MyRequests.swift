//
//  MyRequests.swift
//  DV
//
//  Created by chinmay behera on 20/04/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class MyRequests: UIViewController, UITableViewDelegate, UITableViewDataSource, NetworkProtocol {

    @IBOutlet weak var requestTable: UITableView!
    
    var pro: UIVisualEffectView?
    var deletePlanetIndexPath: NSIndexPath? = nil
    var requestList = NSMutableArray()
    var requestId:String?
    var status: String?
    var indexx:Int?
    var req: String?
    var currentTitle = String()
    var noReqMsg = ""
    
    @IBOutlet weak var btnLabel: UILabel!
    @IBOutlet weak var btnLabelX: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestTable.register(UINib(nibName: "RequestCell", bundle: nil), forCellReuseIdentifier: "RequestCell")
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationItem.title = "Request"
        btnLabelX.constant = 0
        let shared = SharedPreferences()
        let id = shared.getUserId()
        self.currentTitle = "Incoming"
        let url = "http://221.121.153.107/booking/request/\(id)"
        self.fetchRequestList(url: url, apiType: "Incoming")
        self.requestTable.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        btnLabelX.constant = 0
        let shared = SharedPreferences()
        let id = shared.getUserId()
        self.currentTitle = "Incoming"
        let url = "http://221.121.153.107/booking/request/\(id)"
        self.fetchRequestList(url: url, apiType: "Incoming")
    }
    
    @IBAction func onClickDiviMenu(_ sender: UIBarButtonItem) {
        self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    @IBAction func fetchIncomingRequest(_ sender: UIButton) {
        btnLabelX.constant = 0
        let shared = SharedPreferences()
        let id = shared.getUserId()
        DVUtility.disaplyWaitMessage()
        self.currentTitle = "Incoming"
        let url = "http://221.121.153.107/booking/request/\(id)"
        self.currentTitle = sender.currentTitle!
        self.fetchRequestList(url: url, apiType: sender.currentTitle!)
    }
    
    @IBAction func fetchOutgoingRequest(_ sender: UIButton) {
        btnLabelX.constant = btnLabel.frame.size.width
        let shared = SharedPreferences()
        let id = shared.getUserId()
        DVUtility.disaplyWaitMessage()
        let url = "http://221.121.153.107/booking/sent/\(id)"
        self.currentTitle = sender.currentTitle!
        self.fetchRequestList(url: url, apiType: sender.currentTitle!)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if requestList.count == 0 {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
            messageLabel.text = noReqMsg
            messageLabel.textColor = UIColor.init(colorLiteralRed: 65/255, green: 98/255, blue: 151/255, alpha: 1.0)
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center

            messageLabel.sizeToFit()
            requestTable.backgroundView = messageLabel
            requestTable.separatorStyle = .none
            return 0
        }else {
            requestTable.backgroundView = nil
            requestTable.separatorStyle = .none
            return requestList.count
        }
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteButton = UITableViewRowAction(style: .default, title: "Cancel", handler: { (action, indexPath) in
            self.requestTable.dataSource?.tableView!(self.requestTable, commit: .delete, forRowAt: indexPath)
            
            return
        })
        
        deleteButton.backgroundColor = UIColor.red
        
        return [deleteButton]
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        let data = requestList.object(at: indexPath.section) as! BookingInfo
        self.requestId = data.request_id
        self.indexx = indexPath.section
        if editingStyle == .delete {
            deletePlanetIndexPath = indexPath as NSIndexPath
            let planetToDelete = requestList.object(at: indexPath.section) as! BookingInfo
            confirmDelete(planet: String(describing: planetToDelete))
        }
    }
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if self.currentTitle == "Incoming"{
            return UITableViewCellEditingStyle.delete
        }
        return UITableViewCellEditingStyle.none
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }
    
   
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView()
        v.backgroundColor = UIColor.clear
        return v
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell") as! RequestCell
        cell.currentTitle = self.currentTitle
        
        
        let data = requestList.object(at: indexPath.section) as! BookingInfo
        let shared = SharedPreferences()
        let id = shared.getUserId()
        if data.status == "1"{
            self.status = "Posted"
        }
        else if data.status == "2"{
            self.status = "Booked"
        }
        else if data.status == "3"
        {
            self.status = "Cancelled"
        }
        else if data.status == "4"{
            self.status = "Completed"
        }
        if self.currentTitle != "Incoming"{
            cell.name_top.constant = 5
            cell.status.text = self.status
            cell.status_height.constant = 15
        }
        else{
            cell.name_top.constant = 10
            cell.status_height.constant = 0
        }

        
        if id == data.c_user_id {
            
            cell.actingDriver.image = UIImage(named: "taxi")
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dt = format.date(from: data.t_start)
            format.dateFormat = "dd MMM yyyy hh:mm a"
            let dateString = format.string(from: dt!)
            cell.time.text = dateString
            
            cell.requestId.text = "$"+data.cost
            cell.name.text = data.d_firstname + " " + data.d_lastname

            cell.from.text = data.t_from
            cell.to.text = data.t_to
            var x = Double(data.rating)!
            if x == 0.0 {
                cell.rating_image.image = UIImage(named: "5star")!
            }
            else if x > 0.0 && x <= 0.5 {
                cell.rating_image.image = UIImage(named: "0.5stars")
            }
            else if x > 0.5 && x <= 1.0 {
                cell.rating_image.image = UIImage(named: "1starcolor")
            }
            else if x > 1.0 && x <= 1.5 {
                cell.rating_image.image = UIImage(named: "1.5star")
            }
            else if x > 1.5 && x <= 2.0 {
                cell.rating_image.image = UIImage(named: "2starcolor")
            }
            else if x > 2.0 && x <= 2.5{
                cell.rating_image.image = UIImage(named: "2.5star")
            }
            else if x > 2.5 && x <= 3.0 {
                cell.rating_image.image = UIImage(named: "3starcolor")
            }
            else if x > 3.0 && x <= 3.5 {
                cell.rating_image.image = UIImage(named: "3.5star")
            }
            else if x > 3.5 && x <= 4.0 {
                cell.rating_image.image = UIImage(named: "4starcolor")
            }
            else if x > 4.0 && x <= 4.5 {
                cell.rating_image.image = UIImage(named: "4.5star")
            }
            else if x > 4.5 {
                cell.rating_image.image = UIImage(named: "5starcolor")
            }
            var profileUrl = data.d_avatar
            if profileUrl.contains("avatar"){
                let ImagePath = "http://221.121.153.107/public/" + data.d_avatar
                let url = URL(string: ImagePath)
                DispatchQueue.global(qos: .background).async {
                    let data = try? Data(contentsOf: url!)
                    DispatchQueue.main.async {
                        if data != nil {
                            cell.profileImage.image = UIImage(data: data!)
                        }else {
                            cell.profileImage.image = UIImage(named: "avatar")
                        }
                    }
                }
            }
            else{
            let ImagePath = data.d_avatar
            let url = URL(string: ImagePath)
            DispatchQueue.global(qos: .background).async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                    if data != nil {
                        cell.profileImage.image = UIImage(data: data!)
                    }else {
                        cell.profileImage.image = UIImage(named: "avatar")
                    }
                }
            }
            }
        }else if id == data.d_user_id {

            cell.actingDriver.image = UIImage(named: "Car icon")
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dt = format.date(from: data.b_start)
            format.dateFormat = "dd MMM yyyy hh:mm a"
            let dateString = format.string(from: dt!)
            cell.time.text = dateString
            
            cell.requestId.text = "$"+data.driver_share
            cell.name.text = data.c_firstname + " " + data.c_lastname

            cell.from.text = data.b_from
            cell.to.text = data.b_to
            var x = Double(data.rating)!
            if x == 0.0 {
                cell.rating_image.image = UIImage(named: "5star")!
            }
            else if x > 0.0 && x <= 0.5 {
                cell.rating_image.image = UIImage(named: "0.5stars")
            }
            else if x > 0.5 && x <= 1.0 {
                cell.rating_image.image = UIImage(named: "1starcolor")
            }
            else if x > 1.0 && x <= 1.5 {
                cell.rating_image.image = UIImage(named: "1.5star")
            }
            else if x > 1.5 && x <= 2.0 {
                cell.rating_image.image = UIImage(named: "2starcolor")
            }
            else if x > 2.0 && x <= 2.5{
                cell.rating_image.image = UIImage(named: "2.5star")
            }
            else if x > 2.5 && x <= 3.0 {
                cell.rating_image.image = UIImage(named: "3starcolor")
            }
            else if x > 3.0 && x <= 3.5 {
                cell.rating_image.image = UIImage(named: "3.5star")
            }
            else if x > 3.5 && x <= 4.0 {
                cell.rating_image.image = UIImage(named: "4starcolor")
            }
            else if x > 4.0 && x <= 4.5 {
                cell.rating_image.image = UIImage(named: "4.5star")
            }
            else if x > 4.5 {
                cell.rating_image.image = UIImage(named: "5starcolor")
            }
//            var profiletype = shared.getProfileType()
//            if profiletype == "FBSignUp"{
//                let ImagePath = data.c_avatar
//                let url = URL(string: ImagePath)
//                DispatchQueue.global(qos: .background).async {
//                    let data = try? Data(contentsOf: url!)
//                    DispatchQueue.main.async {
//                        if data != nil {
//                            cell.profileImage.image = UIImage(data: data!)
//                        }else {
//                            cell.profileImage.image = UIImage(named: "avatar")
//                        }
//                    }
//                }
//            }
//            else{
            let profileUrl  = data.c_avatar
            if profileUrl.contains("avatar"){
                let ImagePath = "http://221.121.153.107/public/" + data.c_avatar
                let url = URL(string: ImagePath)
                DispatchQueue.global(qos: .background).async {
                    let data = try? Data(contentsOf: url!)
                    DispatchQueue.main.async {
                        if data != nil {
                            cell.profileImage.image = UIImage(data: data!)
                        }else {
                            cell.profileImage.image = UIImage(named: "avatar")
                        }
                    }
                }
            }
            else{
            let ImagePath = data.c_avatar
            let url = URL(string: ImagePath)
            DispatchQueue.global(qos: .background).async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                    if data != nil {
                        cell.profileImage.image = UIImage(data: data!)
                    }else {
                        cell.profileImage.image = UIImage(named: "avatar")
                    }
                }
            }
            }
//            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let requestInfo = requestList.object(at: indexPath.section) as! BookingInfo
        if requestInfo.trip_status == "Pending" {
            let storyboard = UIStoryboard.init(name: "HistoryDetails", bundle: nil)
            let details = storyboard.instantiateViewController(withIdentifier: "HistoryDetails") as! HistoryDetails
            details.bookingInfo = requestInfo
            details.pageFrom = "Myrequest"
            details.currentTitle = self.currentTitle
            details.request = req!
            self.navigationController?.pushViewController(details, animated: true)
        }
        else{
            let storyboard = UIStoryboard.init(name: "HistoryDetails", bundle: nil)
            let details = storyboard.instantiateViewController(withIdentifier: "HistoryDetails") as! HistoryDetails
            details.bookingInfo = requestInfo
            details.pageFrom = "Myrequest"
            details.currentTitle = self.currentTitle
            details.request = req!
            self.navigationController?.pushViewController(details, animated: true)
        }
    }
    func confirmDelete(planet: String) {
        let alert = UIAlertController(title: "Cancel Request", message: "Are you sure you want to cancel the request?", preferredStyle: .actionSheet)
        
        let DeleteAction = UIAlertAction(title: "Cancel Request", style: .destructive, handler: handleDeletePlanet)
        let CancelAction = UIAlertAction(title: "Done", style: .cancel, handler: cancelDeletePlanet)
        
        alert.addAction(DeleteAction)
        alert.addAction(CancelAction)
        
        // Support display in iPad
        alert.popoverPresentationController?.sourceView = self.view
        
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
        
        self.present(alert, animated: true, completion: nil)
    }
    func handleDeletePlanet(alertAction: UIAlertAction!) -> Void {
        self.cancel()
    }
    func cancel() {
        
        DVUtility.disaplyWaitMessage()
        let jsonDict: NSDictionary = ["DV": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        var requestId = self.requestId
        networkCommunication.startNetworkRequest("http://221.121.153.107/request/deny/\(requestId!)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "cancelRequest")
    }
    func cancelDeletePlanet(alertAction: UIAlertAction!) {
        deletePlanetIndexPath = nil
    }

    func fetchRequestList(url: String, apiType: String) {
        

        DVUtility.disaplyWaitMessage()
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let jsonDict: NSDictionary = ["dv": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        
        networkCommunication.startNetworkRequest(url, jsonData: jsonDict as! [String : Any], method: "GET", apiType: apiType)
        
       
    }
    
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
        DVUtility.hideWaitMessage()
        UIApplication.shared.endIgnoringInteractionEvents()
        let alert = UIAlertView(title: "", message: "There was a trchnical difficulty. Please try again", delegate: self, cancelButtonTitle: "Ok")
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        

        DVUtility.hideWaitMessage()
        UIApplication.shared.endIgnoringInteractionEvents()
        
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            if apiType == "cancelRequest"{
                self.requestList.removeObject(at: self.indexx!)
                let indexSet = NSMutableIndexSet()
                indexSet.add(self.indexx!)
                var c = IndexPath(row: 0, section: self.indexx!)
                self.requestTable.beginUpdates()
                self.requestTable.deleteSections(indexSet as IndexSet, with: .left)
                self.requestTable.endUpdates()
                if self.requestList.count == 0{
                    noReqMsg = "You have no requests at this time."
                    self.requestTable.reloadData()
                }
            }
            else{
            print(responseData["data"])
            self.storeTripDetails(details: responseData["data"] as! Array<Any>)
            if apiType == "Incoming" {
                req = "Incoming"
            }else if apiType == "Outgoing" {
                req = "Outgoing"
            }
            }
        case 600:
            if apiType == "cancelRequest"{
                DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
            }
            else{
                requestList.removeAllObjects()
                noReqMsg = "You have no requests at this time."
                requestTable.reloadData()
            }

        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }
    
    func storeTripDetails(details: Array<Any>) {
        requestList.removeAllObjects()
        for d in details {
            let bookingInfo = BookingInfo()
            let detail = d as! NSDictionary
            
            if detail.object(forKey: "request_id") is NSNull {
                bookingInfo.request_id = ""
            }else {
                bookingInfo.request_id = detail.object(forKey: "request_id") as! String
            }
            if let status = detail.object(forKey: "status"){
                if detail.object(forKey: "status") is NSNull{
                    bookingInfo.status = ""
                }else{
                    bookingInfo.status = detail.object(forKey: "status") as! String
                }
            }
            
            if detail.object(forKey: "booking_id") is NSNull {
                bookingInfo.booking_id = ""
            }else {
                bookingInfo.booking_id = detail.object(forKey: "booking_id") as! String
            }
            if detail.object(forKey: "b_start") is NSNull {
                bookingInfo.b_start = ""
            }else {
                bookingInfo.b_start = detail.object(forKey: "b_start") as! String
            }
            if detail.object(forKey: "b_end") is NSNull {
                bookingInfo.b_end = ""
            }else {
                bookingInfo.b_end = detail.object(forKey: "b_end") as! String
            }
            if detail.object(forKey: "b_to") is NSNull {
                bookingInfo.b_to = ""
            }else {
                bookingInfo.b_to = detail.object(forKey: "b_to") as! String
            }
            if detail.object(forKey: "b_from") is NSNull {
                bookingInfo.b_from = ""
            }else {
                bookingInfo.b_from = detail.object(forKey: "b_from") as! String
            }
            if detail.object(forKey: "b_from_latlng") is NSNull {
                bookingInfo.b_from_latlng = ""
            }else {
                bookingInfo.b_from_latlng = detail.object(forKey: "b_from_latlng") as! String
            }
            if detail.object(forKey: "b_to_latlng") is NSNull {
                bookingInfo.b_to_latlng = ""
            }else {
                bookingInfo.b_to_latlng = detail.object(forKey: "b_to_latlng") as! String
            }
            
            
            
            if detail.object(forKey: "c_address") is NSNull {
                bookingInfo.c_address = ""
            }else {
                bookingInfo.c_address = detail.object(forKey: "c_address") as! String
            }
            if detail.object(forKey: "c_avatar") is NSNull {
                bookingInfo.c_avatar = ""
            }else {
                bookingInfo.c_avatar = detail.object(forKey: "c_avatar") as! String
            }
            if detail.object(forKey: "c_contact") is NSNull {
                bookingInfo.c_contact = ""
            }else {
                bookingInfo.c_contact = detail.object(forKey: "c_contact") as! String
            }
            if detail.object(forKey: "c_email") is NSNull {
                bookingInfo.c_email = ""
            }else {
                bookingInfo.c_email = detail.object(forKey: "c_email") as! String
            }
            if detail.object(forKey: "c_firstname") is NSNull {
                bookingInfo.c_firstname = ""
            }else {
                bookingInfo.c_firstname = detail.object(forKey: "c_firstname") as! String
            }
            if detail.object(forKey: "c_lastname") is NSNull {
                bookingInfo.c_lastname = ""
            }else {
                bookingInfo.c_lastname = detail.object(forKey: "c_lastname") as! String
            }
            if detail.object(forKey: "c_role") is NSNull {
                bookingInfo.c_role = ""
            }else {
                bookingInfo.c_role = detail.object(forKey: "c_role") as! String
            }
            if detail.object(forKey: "c_gender") is NSNull {
                bookingInfo.c_gender = ""
            }
            else{
                bookingInfo.c_gender = detail.object(forKey: "c_gender") as! String
            }
            if detail.object(forKey: "c_study") is NSNull {
                bookingInfo.c_study = ""
            }else {
                bookingInfo.c_study = detail.object(forKey: "c_study") as! String
            }
            if detail.object(forKey: "c_user_id") is NSNull {
                bookingInfo.c_user_id = ""
            }else {
                bookingInfo.c_user_id = detail.object(forKey: "c_user_id") as! String
            }
            
            
            if detail.object(forKey: "trip_id") is NSNull {
                bookingInfo.trip_id = ""
            }else {
                bookingInfo.trip_id = detail.object(forKey: "trip_id") as! String
            }
            if detail.object(forKey: "trip_status") is NSNull {
                bookingInfo.trip_status = ""
            }else {
                bookingInfo.trip_status = detail.object(forKey: "trip_status") as! String
            }
            if detail.object(forKey: "t_start") is NSNull {
                bookingInfo.t_start = ""
            }else {
                bookingInfo.t_start = detail.object(forKey: "t_start") as! String
            }
            if detail.object(forKey: "t_end") is NSNull {
                bookingInfo.t_end = ""
            }else {
                bookingInfo.t_end = detail.object(forKey: "t_end") as! String
            }
            if detail.object(forKey: "t_to") is NSNull {
                bookingInfo.t_to = ""
            }else {
                bookingInfo.t_to = detail.object(forKey: "t_to") as! String
            }
            if detail.object(forKey: "t_from") is NSNull {
                bookingInfo.t_from = ""
            }else {
                bookingInfo.t_from = detail.object(forKey: "t_from") as! String
            }
            if detail.object(forKey: "t_to_latlng") is NSNull {
                bookingInfo.t_to_latlng = ""
            }else {
                bookingInfo.t_to_latlng = detail.object(forKey: "t_to_latlng") as! String
            }
            if detail.object(forKey: "t_from_latlng") is NSNull {
                bookingInfo.t_from_latlng = ""
            }else {
                bookingInfo.t_from_latlng = detail.object(forKey: "t_from_latlng") as! String
            }
            
            
            if detail.object(forKey: "interests") is NSNull{
                bookingInfo.interests = ""
            }
            else{
                bookingInfo.interests = detail.object(forKey: "interests") as! String
            }
            if detail.object(forKey: "rating") is NSNull {
                bookingInfo.rating = ""
            }else {
                var rate = Double()
                var rate1 = detail.object(forKey: "rating") as! Double
                print(String(rate1))
                bookingInfo.rating = String(rate1)
            }
            if let message = detail.object(forKey: "message"){
                if detail.object(forKey: "message") is NSNull{
                    bookingInfo.message = ""
                }
                else{
                    bookingInfo.message = detail.object(forKey: "message") as! String
                }
            }
            
            if let vehicle = detail.object(forKey: "vehicle"){
                if detail.object(forKey: "vehicle") is NSNull{
                    bookingInfo.vehicle = ""
                }
                else{
                    bookingInfo.vehicle = detail.object(forKey: "vehicle") as! String
                }
            }
            if let plate = detail.object(forKey: "plate"){
                if detail.object(forKey: "plate") is NSNull{
                    bookingInfo.plate = ""
                }
                else{
                    bookingInfo.plate = detail.object(forKey: "plate") as! String
                }
            }
            if let color = detail.object(forKey: "colour"){
                if detail.object(forKey: "colour") is NSNull{
                    bookingInfo.v_color = ""
                }
                else{
                    bookingInfo.v_color = detail.object(forKey: "colour") as! String
                }
            }
            if let calculation = detail.object(forKey: "calculation"){
                var calculation = detail.object(forKey: "calculation") as! NSDictionary
                if let cost = calculation.object(forKey: "cost") {
                    if calculation.object(forKey: "cost") is NSNull{
                        bookingInfo.cost = ""
                    }
                    else{
                        bookingInfo.cost = (calculation.object(forKey: "cost") as? String)!
                    }
                }
                if let drivershare = calculation.object(forKey: "driver_share"){
                    if calculation.object(forKey: "driver_share") is NSNull{
                        bookingInfo.driver_share = ""
                    }
                    else{
                        bookingInfo.driver_share = (calculation.object(forKey: "driver_share") as? String)!
                    }
                }
                if let distance = calculation.object(forKey: "dist_kms"){
                    if calculation.object(forKey: "dist_kms") is NSNull{
                        bookingInfo.distance = 0.0
                    }
                    else{
                        bookingInfo.distance = calculation.object(forKey: "dist_kms") as! Double
                    }
                }
                
            }
            
            
            if detail.object(forKey: "d_address") is NSNull {
                bookingInfo.d_address = ""
            }else {
                bookingInfo.d_address = detail.object(forKey: "d_address") as! String
            }
            if detail.object(forKey: "d_avatar") is NSNull {
                bookingInfo.d_avatar = ""
            }else {
                bookingInfo.d_avatar = detail.object(forKey: "d_avatar") as! String
            }
            if detail.object(forKey: "d_contact") is NSNull {
                bookingInfo.d_contact = ""
            }else {
                bookingInfo.d_contact = detail.object(forKey: "d_contact") as! String
            }
            if detail.object(forKey: "d_email") is NSNull {
                bookingInfo.d_email = ""
            }else {
                bookingInfo.d_email = detail.object(forKey: "d_email") as! String
            }
            if detail.object(forKey: "d_firstname") is NSNull {
                bookingInfo.d_firstname = ""
            }else {
                bookingInfo.d_firstname = detail.object(forKey: "d_firstname") as! String
            }
            if detail.object(forKey: "d_lastname") is NSNull {
                bookingInfo.d_lastname = ""
            }else {
                bookingInfo.d_lastname = detail.object(forKey: "d_lastname") as! String
            }
            if detail.object(forKey: "d_role") is NSNull {
                bookingInfo.d_role = ""
            }else {
                bookingInfo.d_role = detail.object(forKey: "d_role") as! String
            }
            if detail.object(forKey: "d_gender") is NSNull {
                bookingInfo.d_gender = ""
            }
            else{
                bookingInfo.d_gender = detail.object(forKey: "d_gender") as! String
            }
            if detail.object(forKey: "d_study") is NSNull {
                bookingInfo.d_study = ""
            }else {
                bookingInfo.d_study = detail.object(forKey: "d_study") as! String
            }
            if detail.object(forKey: "d_user_id") is NSNull {
                bookingInfo.d_user_id = ""
            }else {
                bookingInfo.d_user_id = detail.object(forKey: "d_user_id") as! String
            }
            
            requestList.add(bookingInfo)
            
        }
        
        requestTable.reloadData()
        
    }
    
}
