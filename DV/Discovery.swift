   //
//  Discovery.swift
//  DV
//
//  Created by Chinmay on 10/03/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class Discovery: UIViewController, UITableViewDelegate, UITableViewDataSource, NetworkProtocol {

    @IBOutlet weak var discoveryTable: UITableView!
    
    var book_id:String?
    var trip_id:String?
    var list = NSArray()
    var interests_array = NSArray()
    var noMatchesMsg = ""
    var interest_string = String()
    var selectedInterests: NSMutableArray = NSMutableArray()
    var interests1: NSMutableArray = NSMutableArray()
    var details = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Discovery"
        discoveryTable.register(UINib(nibName: "DiscoveryCell", bundle: nil), forCellReuseIdentifier: "DiscoveryCell")
        // Do any additional setup after loading the view.

        
        let gradientLayer = CAGradientLayer()
        var updatedFrame = self.navigationController!.navigationBar.bounds
        updatedFrame.size.height += 20
        gradientLayer.frame = updatedFrame
        gradientLayer.colors = [UIColor.init(colorLiteralRed: 09/255, green: 148/255, blue: 145/255, alpha: 1.0).cgColor, UIColor.init(colorLiteralRed: 65/255, green: 98/255, blue: 151/255, alpha: 1.0).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        print("ok")
        UIGraphicsBeginImageContext(gradientLayer.bounds.size)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.navigationController!.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print("hahaghjgjhg")
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("scsduic")
        super.viewDidAppear(true)
        print("haha")
        self.fetchDriverOrComuutorList()
    }
    
    @IBAction func onClickDiviMenu(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        let sideMenu = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if list.count == 0 {
            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
            messageLabel.text = noMatchesMsg
            messageLabel.textColor = UIColor.init(colorLiteralRed: 65/255, green: 98/255, blue: 151/255, alpha: 1.0)
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            
            messageLabel.sizeToFit()
            discoveryTable.backgroundView = messageLabel
            discoveryTable.separatorStyle = .none
            return 0
        }else {
            discoveryTable.backgroundView = nil
            discoveryTable.separatorStyle = .none
            return list.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 8
//    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView()
        v.backgroundColor = UIColor.clear
        return v
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiscoveryCell") as! DiscoveryCell
        let shared = SharedPreferences()
        let userType = shared.getUserType()
        
        if book_id == ""{
            details = (list.object(at: indexPath.section) as! NSDictionary).object(forKey: "booking_details") as! NSDictionary
        }
        else if trip_id == ""{
            details = (list.object(at: indexPath.section) as! NSDictionary).object(forKey: "trip_details") as! NSDictionary
        }
        
        

        if details.object(forKey: "request_id") is NSNull{
            print("request ID is not there till now.")
        }
        else{
            cell.contents_view.alpha = 0.5
        }
        if details.object(forKey: "firstname") is NSNull {
            cell.name.text = "Vin Diesel"
        }else {
            cell.name.text = details.object(forKey: "firstname") as! String?
        }
        if details.object(forKey: "study") is NSNull {
            cell.fieldOfWork.text = "Hollywood"
        }else {
            let x = details.object(forKey: "study") as! String?
            cell.fieldOfWork.text = "(" + x! + ")"
        }
        let rating = details.object(forKey: "rating") as! Double
        var x = rating
        
        if x == 0.0 {
             cell.rating_image.image = UIImage(named: "5star")!
        }
        else if x > 0.0 && x <= 0.5 {
             cell.rating_image.image = UIImage(named: "0.5stars")
        }
        else if x > 0.5 && x <= 1.0 {
             cell.rating_image.image = UIImage(named: "1starcolor")
        }
        else if x > 1.0 && x <= 1.5 {
             cell.rating_image.image = UIImage(named: "1.5star")
        }
        else if x > 1.5 && x <= 2.0 {
             cell.rating_image.image = UIImage(named: "2starcolor")
        }
        else if x > 2.0 && x <= 2.5{
            cell.rating_image.image = UIImage(named: "2.5star")
        }
        else if x > 2.5 && x <= 3.0 {
             cell.rating_image.image = UIImage(named: "3starcolor")
        }
        else if x > 3.0 && x <= 3.5 {
             cell.rating_image.image = UIImage(named: "3.5star")
        }
        else if x > 3.5 && x <= 4.0 {
             cell.rating_image.image = UIImage(named: "4starcolor")
        }
        else if x > 4.0 && x <= 4.5 {
             cell.rating_image.image = UIImage(named: "4.5star")
        }
        else if x > 4.5 {
             cell.rating_image.image = UIImage(named: "5starcolor")
        }
        var calculation = details.object(forKey: "calculation") as! NSDictionary
        var distance = String(calculation.object(forKey: "dist_kms") as! Double)
        var first4 = String(distance.characters.prefix(4))
        cell.distance.text = first4 + "kms"
        self.interest_string = (details.object(forKey: "interests") as! String?)!
        cell.interest_string = self.interest_string

        var profileUrl = (details.object(forKey: "avatar") as! String?)!
        if profileUrl.contains("avatar"){
            let ImagePath = "http://221.121.153.107/public/" + (details.object(forKey: "avatar") as! String?)!
            let url = URL(string: ImagePath)
            DispatchQueue.global(qos: .background).async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                    if data != nil {
                        cell.profileImage.image = UIImage(data: data!)
                    }else {
                        cell.profileImage.image = UIImage(named: "avatar")
                    }
                }
            }
        }
        else{
            let ImagePath = (details.object(forKey: "avatar") as! String?)!
            let url = URL(string: ImagePath)
            DispatchQueue.global(qos: .background).async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async {
                    if data != nil {
                        cell.profileImage.image = UIImage(data: data!)
                    }else {
                        cell.profileImage.image = UIImage(named: "avatar")
                    }
                }
            }
        }
        

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var x:NSDictionary?
        var y:NSDictionary?
        var z:NSDictionary?
        x = list.object(at: indexPath.section) as! NSDictionary
        print(list)
        if let u = x?.object(forKey: "booking_details") {
            y = x?.object(forKey: "booking_details") as? NSDictionary
        }
        else{
            y = x?.object(forKey: "trip_details") as? NSDictionary
        }
        


        if y?.object(forKey: "request_id") is NSNull{
            let storyboard = UIStoryboard(name: "PersonDetails", bundle: nil)
            let disc = storyboard.instantiateViewController(withIdentifier: "PersonDetails") as! PersonDetails
            disc.page = "Ongoing"
            disc.info = list.object(at: indexPath.section) as? NSDictionary
            if y?.object(forKey: "social_id") is NSNull{
                disc.social_id = "nil"
            }
            else{
                disc.social_id = y?.object(forKey: "social_id") as! String
            }
            disc.departure = y?.object(forKey: "start") as! String
            if let booking_details = y?.object(forKey: "booking_details"){
                disc.role = "Driver"
            }
            if let trip_details = y?.object(forKey: "trip_details"){
                disc.role = "Commuter"
            }
            if let vehicle = y?.object(forKey: "vehicle"){
                if y?.object(forKey: "vehicle") is NSNull{
                    disc.v_model = ""
                }
                else{
                    disc.v_model = y?.object(forKey: "vehicle") as! String
                }
            }
            if let plate = y?.object(forKey: "plate"){
                if y?.object(forKey: "plate") is NSNull{
                    disc.v_number = ""
                }
                else{
                    disc.v_number = y?.object(forKey: "plate") as! String
                }
            }
            if let color = y?.object(forKey: "colour"){
                if y?.object(forKey: "colour") is NSNull{
                    disc.v_color = ""
                }
                else{
                    disc.v_color = y?.object(forKey: "colour") as! String
                }
            }

            disc.viewingType = "from discovery"
            disc.book_id = self.book_id!
            disc.trip_id = self.trip_id!
            disc.rating = y?.object(forKey: "rating") as! Double
            disc.interest_string = self.interest_string
            self.navigationController?.pushViewController(disc, animated: true)
        }
        else{
            let alert = UIAlertView(title: "", message: "Your request has been sent! You can find this request in your MY REQUESTS TAB", delegate: self, cancelButtonTitle: "Ok")
            alert.show()
        }
    }
    
   
    func fetchDriverOrComuutorList() {
        let reachability = Reachability1()
        if reachability!.isReachable || reachability!.isReachableViaWiFi || reachability!.isReachableViaWWAN
        {
        
        DVUtility.disaplyWaitMessage()
        
        let jsonDict: NSDictionary = ["dv": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        print(book_id!)
        print(trip_id!)

        
        if book_id == ""{
            networkCommunication.startNetworkRequest("http://221.121.153.107/booking/search/\(id)/\(trip_id!)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "CommutorList")
        }
        else if trip_id == ""{
            networkCommunication.startNetworkRequest("http://221.121.153.107/trip/search/\(id)/\(book_id!)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "DriverList")
        }
        }
        else{
            let alert = UIAlertView(title: "Unreachable", message: "Please make sure internet is connected and retry", delegate: self, cancelButtonTitle: "retry")
            alert.tag = 100
            alert.show()
        }
        

    }
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if alertView.tag == 100{
            self.fetchDriverOrComuutorList()
            return
        }
    }


    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
        DVUtility.hideWaitMessage()
        
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        
        DVUtility.hideWaitMessage()
        
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            print("")
            list = responseData["data"] as! NSArray
            
            discoveryTable.reloadData()
        case 600:
            
            noMatchesMsg = "We cannot find any match as per your request. Please wait for some time."
            discoveryTable.reloadData()
            let message = responseData["message"] as! String
            let alert = UIAlertController(title: "", message:"\(message)", preferredStyle: UIAlertControllerStyle.alert)
            
            let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel,handler: { (action) -> Void in
                let storyboard = UIStoryboard(name: "MyTrips", bundle: nil)
                let navVC:UINavigationController = storyboard.instantiateViewController(withIdentifier: "MyTripsNav") as! UINavigationController
                self.menuContainerViewController.centerViewController = navVC
            })
            alert.addAction(okButton)
            self.present(alert, animated: true, completion: nil)
        

            
        default:
            DVUtility.displayAlertBoxTitle("", withMessage: "There was a technical difficulty while fulfilling your request. Please try again.")
        }
    }

}
