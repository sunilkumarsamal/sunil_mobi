//
//  Home.swift
//  DV
//
//  Created by Chinmay on 16/01/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class Home: UIViewController {

    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var terms_conditions: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        signupButton.layer.borderWidth = 1.0;
        signupButton.layer.borderColor = UIColor.white.cgColor;
        signupButton.clipsToBounds = true;
        signupButton.layer.cornerRadius = 22.0;
        
        loginButton.layer.borderWidth = 1.0;
        loginButton.layer.borderColor = UIColor.white.cgColor;
        loginButton.clipsToBounds = true;
        loginButton.layer.cornerRadius = 22.0;
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickSignUp(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LoginSignUp", bundle: nil)
        let loginSignUp = storyboard.instantiateViewController(withIdentifier: "LoginSignUp") as! LoginSignUp
        loginSignUp.loginType = "Sign up"
        self.present(loginSignUp, animated: true, completion: nil)
    }

    @IBAction func onClickLogin(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "LoginSignUp", bundle: nil)
        let loginSignUp = storyboard.instantiateViewController(withIdentifier: "LoginSignUp") as! LoginSignUp
        loginSignUp.loginType = "Log in"
        self.present(loginSignUp, animated: true, completion: nil)
    }
    
    @IBAction func terms_conditions_clicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Termsandconditions", bundle: nil)
        let next = storyboard.instantiateViewController(withIdentifier: "Termsandconditions") as! Termsandconditions
        self.present(next, animated: true, completion: nil)
    }

}
