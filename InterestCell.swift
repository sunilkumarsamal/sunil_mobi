//
//  InterestCell.swift
//  DV
//
//  Created by Chinmay on 19/02/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class InterestCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var interestName: UILabel!
    
}
