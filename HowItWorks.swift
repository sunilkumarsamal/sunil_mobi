//
//  HowItWorks.swift
//  DV
//
//  Created by Cherian Sankey on 26/06/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class HowItWorks: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
//         UIApplication.shared.openURL(NSURL(string: "http://45.79.140.96/MobiShare/howitworks.php")! as URL)
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationItem.title = "How it Works"

        self.webView.scrollView.bounces = false
        let url = NSURL (string: "http://45.79.140.96/MobiShare/howitworks.php")
        let requestObj = URLRequest(url: url! as URL)
        webView.loadRequest(requestObj)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickDiviMenu(_ sender: UIBarButtonItem) {
        self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
