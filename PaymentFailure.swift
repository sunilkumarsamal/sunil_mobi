//
//  PaymentFailure.swift
//  DV
//
//  Created by Cherian Sankey on 24/06/17.
//  Copyright © 2017 Six30. All rights reserved.
//

import UIKit

class PaymentFailure: UIViewController,NetworkProtocol {

    @IBOutlet weak var driver_name: UILabel!
    @IBOutlet weak var where_from: UILabel!
    @IBOutlet weak var where_to: UILabel!
    @IBOutlet weak var commuter_name: UILabel!
    @IBOutlet weak var estimated_price: UILabel!
    @IBOutlet weak var travel_date_month: UILabel!
    @IBOutlet weak var travel_year: UILabel!
    @IBOutlet weak var driver_name_top: NSLayoutConstraint!
    @IBOutlet weak var where_to_top: NSLayoutConstraint!
    @IBOutlet weak var cost_bottom: NSLayoutConstraint!
    @IBOutlet weak var receipt_view: UIView!
    @IBOutlet weak var driver_name_bottom: NSLayoutConstraint!
    
    @IBOutlet weak var where_to_bottom: NSLayoutConstraint!
    @IBOutlet weak var passenger_top: NSLayoutConstraint!
    @IBOutlet weak var where_from_bottom: NSLayoutConstraint!
    
    
    var cost = String()
    var from = String()
    var to = String()
    var date_show = String()
    var commuter_Name = String()
    var phoneNum = String()
    var booking_id = String()
    var trip_id = String()
    var c_id = String()
    var d_id = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.estimated_price.text = "$"+cost
        self.where_from.text = from
        self.where_to.text = to
        self.commuter_name.text = commuter_Name
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let startdate = dateFormatter.date(from: date_show)
        let sdate = String(describing: startdate)
        var without_time = String(sdate.characters.prefix(10))
        var y = String(without_time.characters.suffix(5))
        self.travel_date_month.text = y
        var x = String(sdate.characters.prefix(4))
        self.travel_year.text = x

        self.fetchBookingList(bookTripId: "\(booking_id)")
     }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        print(self.receipt_view.frame.size)
        if self.receipt_view.frame.height >= 400{
            self.driver_name_top.constant = 50
            self.driver_name_bottom.constant = 74
            self.where_to_bottom.constant = 30
            self.passenger_top.constant = 30
            self.cost_bottom.constant = 30
//            self.passengername_bottom.constant = 25
        }
        else if self.receipt_view.frame.height >= 390 && self.receipt_view.frame.height < 400{
            self.driver_name_top.constant = 47
            self.driver_name_bottom.constant = 60
            self.where_to_bottom.constant = 15
            self.passenger_top.constant = 15
            self.cost_bottom.constant = 20
        }
        else if self.receipt_view.frame.height >= 290 && self.receipt_view.frame.height < 390{
            self.driver_name_top.constant = 35
            self.driver_name_bottom.constant = 30
            self.where_from_bottom.constant = 30
            self.where_to_top.constant = 30
            self.where_to_bottom.constant = 8
            self.passenger_top.constant = 8
            self.cost_bottom.constant = 15
        }
        
    }
    
    @IBAction func pay(_ sender: UIButton) {

        DVUtility.disaplyWaitMessage()
        let id = self.c_id
        self.startNetworkRequest(urlString: "http://221.121.153.107/payment/pay/\(id)")
    }
    @IBAction func change_card(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Payment", bundle: nil)
        let next = storyboard.instantiateViewController(withIdentifier: "AddCard") as! AddCard
        next.entryType = "changeCardDuringPayment"
        self.present(next, animated: true, completion: nil)
    }

    @IBAction func contact_support(_ sender: UIButton) {
        self.phone(phoneNum: "0474767526")
    }
    
    func phone(phoneNum: String) {
        if let url = URL(string: "tel://\(phoneNum)") {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    func startNetworkRequest(urlString: String) {
        let jsonDict: NSDictionary = ["booking_id": "\(booking_id)"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        networkCommunication.startNetworkRequest(urlString, jsonData: jsonDict as! [String : Any], method: "POST", apiType: "Pay")
    }

    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    
    func fetchBookingList(bookTripId: String) {
        
        DVUtility.disaplyWaitMessage()
        
        let jsonDict: NSDictionary = ["dv": "mobi"]
        let networkCommunication = NetworkCommunication()
        networkCommunication.networkDelegate = self
        let shared = SharedPreferences()
        let id = shared.getUserId()
        let userType = shared.getUserType()
        if userType == "Driver" {
            networkCommunication.startNetworkRequest("http://221.121.153.107/trip/details/\(id)/\(bookTripId)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "BookingDetails")
        }
        else {
            networkCommunication.startNetworkRequest("http://221.121.153.107/booking/details/\(id)/\(bookTripId)", jsonData: jsonDict as! [String : Any], method: "GET", apiType: "BookingDetails")
        }
        
    }
    func success(response: [String: AnyObject], apiType: String) {
        print(response)
        self.responseParse(responseData: response as! [String : AnyObject], apiType: apiType)
    }
    
    func failure() {
        DVUtility.hideWaitMessage()
        print("There was a technical difficulty. please try again")
    }
    
    func responseParse(responseData: [String:AnyObject], apiType: String) {
        
        DVUtility.hideWaitMessage()
        
        let code = responseData["status_code"] as! Int
        switch code {
        case 100:
            print("")
            if apiType == "BookingDetails"{
                var data = responseData["data"] as! [String:AnyObject]
                var d_firstname = data["d_firstname"] as! String
                var d_lastname = data["d_lastname"] as! String
                self.driver_name.text = d_firstname + " " + d_lastname
            }
            else if apiType == "Pay"{
                let alert = UIAlertController(title: " ", message:"Payment done successfully.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {(action : UIAlertAction) in
                        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
                        let leftVC = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
                        let story = UIStoryboard(name: "PickDrop", bundle: nil)
                        let PickDrop = story.instantiateViewController(withIdentifier: "PickUpDropOff") as! PickUpDropOff
                        let navVC:UINavigationController = story.instantiateViewController(withIdentifier: "Navigation") as! UINavigationController
                        navVC.navigationBar.setBackgroundImage(UIImage(named: "Rectangle"), for: .default)
                        
                        let container: MFSideMenuContainerViewController = story.instantiateViewController(withIdentifier: "ContainerVC") as! MFSideMenuContainerViewController
                        container.leftMenuViewController = leftVC
                        container.centerViewController = navVC
                        container.leftMenuWidth = UIScreen.main.bounds.size.width - 55
                        self.present(container, animated: true, completion: nil)
        
                    
                })
                )
            }
            
            
        case 600:
            if apiType == "Pay"{
                DVUtility.displayAlertBoxTitle("", withMessage: "Can't fulfill your request now.please try again.")
            }
            else{
                DVUtility.displayAlertBoxTitle("", withMessage: "Problem occured during fetching the data.")
            }
        default:
            print("")
        }
    }
    func convertDateFormate(date : Date) -> String{
        // Day
        let calendar = Calendar.current
        let anchorComponents = calendar.dateComponents([.day, .month, .year], from: date)
        
        // Formate
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "MMM, yyyy"
        let newDate = dateFormate.string(from: date)
        
        var day  = "\(anchorComponents.day!)"
        switch (day) {
        case "1" , "21" , "31":
            day.append("st")
        case "2" , "22":
            day.append("nd")
        case "3" ,"23":
            day.append("rd")
        default:
            day.append("th")
        }
        return day + " " + newDate
    }
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
